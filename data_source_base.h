#ifndef DATA_SOURCE_BASE_H
#define DATA_SOURCE_BASE_H

#include "midas.h"
#include <data_structures.h>

/** \file
 * Base class that all data sources inherit from.
 */

/** Base class that means the FEP frontend doesn't have to
 * care about whether data is being read from a real VX2740
 * or a software simulation of one.
 * */
class DataSourceBase {
public:
   virtual ~DataSourceBase(){};

   /** This will be called when a new midas run starts.
    *
    * \param[in] _slice_width_ms The width of each slice, useful if the data source is
    *    simulating data and needs to inject fake "end-of-slice" events.
    * \return Midas status code.
    */
   virtual INT begin_of_run(double _slice_width_ms) { return SUCCESS; }

   /** This will be called when a midas run ends.
    *
    * \return Midas status code.
    */
   virtual INT end_of_run() { return SUCCESS; }

   /** This will be called when the FEP starts. You should create/connect to any ODB
    * keys your data source needs access to.
    *
    * \param[in] base_odb_path ODB location where FEP settings are stored, usually of the
    *    form /Equipment/FEP_001/Settings.
    * \return Midas status code.
    */
   virtual INT open_records(char *base_odb_path) { return SUCCESS; }

   /** Whether this board is enabled or not (so the FEP knows whether
    * to expect data or not).
    */
   virtual bool is_enabled() { return true; }

   /** This is the main function that must be implemented in each
    * derived class. It should write `VXData` events to the provided
    * vector if any events are available. Otherwise return quickly.
    *
    * \param[out] data Events read.
    * \param[in] debug Whether to write debug statements to stdout.
    *
    * \return
    * - SUCCESS if no problems encountered (even if no event available)
    * - Something else if there was a problem
    */
   virtual INT populate_next_event(std::vector<std::shared_ptr<VXData> >& data, BOOL debug) = 0;

   /** Read the board's acquisition status.
    *
    * \param[out] status The status of the VX2740 board.
    */
   virtual void get_acq_status(uint32_t& status) {
      status = 0;
   }

   /** Read temperature metadata from a board.
    *
    * \param[out] in Input temperature.
    * \param[out] out Ouput temperature.
    * \param[out] hottest_adc Hottest ADC temperature.
    */
   virtual void get_temperatures(float& in, float& out, float& hottest_adc) {
      in = 0;
      out = 0;
      hottest_adc = 0;
   }

};

#ifdef HAVE_VX
#include "vx2740_wrapper.h"

/** Base class for reading data from VX2740s, whether they're running
 * Scope mode or User mode firmware.
 *
 * The actual `populate_next_event()` function implementations are in the
 * derived classes `DataSourceScopeMode` and `DataSourceUserMode`.
 */
class DataSourceRealBase : public DataSourceBase {
public:
   DataSourceRealBase() {
      vx = NULL;
      enable = false;
      name = "";
   }
   virtual ~DataSourceRealBase(){};

   /** Set the digitizer parameters for this data source.
    *
    * \param[in] _board Connection to digitizer.
    * \param[in] _enable Whether we should read data from this board.
    * \param[in] _name The hostname of this board.
    * \return Midas status code.
    */
   virtual INT set_board_info(VX2740* _board, bool _enable, std::string _name) {
      vx = _board;
      enable = _enable;
      name = _name;
      return SUCCESS;
   }

   /** Whether we should read data from this board. */
   virtual bool is_enabled() override {
      return enable;
   }

   virtual void get_acq_status(uint32_t& status) override {
      if (is_enabled() && vx){
         vx->params().get_acquisition_status(status);
      } else {
         DataSourceBase::get_acq_status(status);
      }
   }

   virtual void get_temperatures(float& in, float& out, float& hottest_adc) override {
      if (is_enabled() && vx) {
         vx->params().get_temperatures(in, out, hottest_adc);
      } else {
         DataSourceBase::get_temperatures(in, out, hottest_adc);
      }
   }

protected:
   VX2740* vx; //!< Connection to digitizer.
   bool enable; //!< Whether this digitizer is enabled.
   std::string name; //!< Digitizer hostname
};
#endif

#endif

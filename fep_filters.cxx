#include "fep_filters.h"
#include "data_structures.h"
#include "midas.h"
#include <math.h>
#include <numeric>

/** \file
 * Filters used in the FEP to smooth waveforms or summarize into QT objects.
 */

std::string FEPFilterBase::get_error_message() {
   return error_message;
}

INT FEPFilterNone::apply(const ChannelId& chan, std::shared_ptr<Waveform> in, std::vector<std::shared_ptr<HitBase> >& out, bool debug) {
   error_message = "";

   out.push_back(in);

   return SUCCESS;
}

FEPFilterDownsample::FEPFilterDownsample(midas::odb settings) : FEPFilterBase(settings) {
   factor = settings["Downsample filter"]["Factor"];
 }

bool FEPFilterDownsample::is_enabled(midas::odb& settings) {
   return settings["Downsample filter"]["Enable"];
}

void FEPFilterDownsample::create_settings(midas::odb& settings) {
   if (settings["Global"].is_subkey("Enable downsample filter")) {
      // Handle migration between old and new ODB structures
      // Can't just do settings["Enable downsample filter"].delete_key()
      // as odbxx throws an error when we do settings.read() later...
      midas::odb(settings.get_full_path() + "/Global/Enable downsample filter").delete_key();
   }

   if (!settings.is_subkey("Downsample filter")) {
      // Create settings for this filter in a subdirectory of the overall
      // FEP ODB settings.
      midas::odb these_settings({{"Enable", true}, 
                                 {"Factor", 2U}});

      these_settings.connect(settings.get_full_path() + "/Downsample filter");
   }
}

INT FEPFilterDownsample::apply(const ChannelId& chan, std::shared_ptr<Waveform> in, std::vector<std::shared_ptr<HitBase> >& out, bool debug) {
   error_message = "";

   std::shared_ptr<Waveform> wf = std::make_shared<Waveform>();
   out.push_back(wf);

   wf->time_since_run_start_secs = in->time_since_run_start_secs;
   wf->algorithms_applied |= Slice::FilterAlgo::Downsample;

   for (unsigned int i = 0; i < in->samples.size(); i += factor) {
      wf->samples.push_back(in->samples[i]);
   }

   return SUCCESS;
}

FEPFilterExp2GausMatched::FEPFilterExp2GausMatched(midas::odb settings) : FEPFilterBase(settings) {
   kernelSize = settings["Matched filter"]["Kernel Size"];
   riseTimeSigma = settings["Matched filter"]["Rise Time"];
   fallTimeTau = settings["Matched filter"]["Fall Time"];
   time2Frac = settings["Matched filter"]["Fall Time 2 Fraction"];
   fallTime2Tau = settings["Matched filter"]["Fall Time 2"];
   binSize = settings["Matched filter"]["Bin Size"];
   createPulseShapeKernel();
}

bool FEPFilterExp2GausMatched::is_enabled(midas::odb& settings) {
   return settings["Matched filter"]["Enable"];
}

void FEPFilterExp2GausMatched::create_settings(midas::odb& settings) {
   if (!settings.is_subkey("Matched filter")) {
      // Create settings for this filter in a subdirectory of the overall
      // FEP ODB settings.
      midas::odb these_settings({{"Enable", true},
                                 {"Kernel Size", 501},
                                 {"Rise Time", 3.2},
                                 {"Fall Time", 853},
                                 {"Fall Time 2 Fraction", 0.55},
                                 {"Fall Time 2", 28},
                                 {"Bin Size", 2.0}});

      these_settings.connect(settings.get_full_path() + "/Matched filter");
   }
}

INT FEPFilterExp2GausMatched::apply(const ChannelId& chan, std::shared_ptr<Waveform> in, std::vector<std::shared_ptr<HitBase>> &out, bool debug) {
   error_message = "";

   std::shared_ptr<Waveform> wf = std::make_shared<Waveform>();
   out.push_back(wf);

   wf->time_since_run_start_secs = in->time_since_run_start_secs;
   wf->algorithms_applied |= Slice::FilterAlgo::Exp2GausMatched;

   int nBins = in->samples.size();
   int kernel_offset = (kernelSize - 1) / 2;

   for (int i = 0; i < nBins; i++) {
      if (i >= kernel_offset && i < nBins - kernel_offset) {
         double val = 0;
         double c = 0.0;
         for (int k = 0; k < kernelSize; k++) {
            val += kernel[k] * (in->samples[(i + k - kernel_offset)]);
         }
         wf->samples.push_back(val);
      } else {
         wf->samples.push_back(in->samples[i]);
      }
   }

   return SUCCESS;
}

/**
 * Creates the kernel that will be run across the signal by sampling the signal
 * shape. The size of the filter is defined by the `kernelSize` parameter.
 */
void FEPFilterExp2GausMatched::createPulseShapeKernel() {
   kernelSize = round(kernelSize);
   kernel = new double[kernelSize];
   double mag = 0.0;
   for (int i = 0; i < kernelSize; i++) {
      int index = i;
      kernel[index] = calcPulseShapeValue((i)*binSize);
      mag += kernel[index];
   }

   for (int i = 0; i < kernelSize; i++) {
      kernel[i] /= mag;
   }
}

/**
 * Calculates and returns the value for the pulse shape using an exp.
 */
double FEPFilterExp2GausMatched::calcPulseShapeValue(double x) {
   /* p[0]: baseline
      p[1]: gaussian sig
      p[2]: first  exponential decay constant
      p[3]: second exponential fraction
      p[4]: second exponential decay constant
      p[5]: number of pulses
      p[6+2*i]: pulse[i] amplitude
      p[7+2*i]: pulse[i] time */
   double p[8] = {0, riseTimeSigma, fallTimeTau, time2Frac, fallTime2Tau, 1, 1.0, 1.0};

   double val = 0.;
   for (int iPulse = 0; iPulse < p[5]; iPulse++) {
      double time = x - p[7 + 2 * iPulse];
      if (((p[1] * p[1] / p[2] / 2. - time) / p[2]) < 700) {// otherwise exponential explodes
         val += (p[6 + 2 * iPulse] / 2. / p[2] * exp((p[1] * p[1] / p[2] / 2. - time) / p[2]) * erfc((p[1] * p[1] / p[2] - time) / sqrt(2) / p[1])) * (1 - fabs(p[3]));
         if (p[3] > 0.) {
            val += (p[3] * p[6 + 2 * iPulse] / 2. / p[2] * exp((p[1] * p[1] / p[4] / 2. - time) / p[4]) * erfc((p[1] * p[1] / p[4] - time) / sqrt(2) / p[1]));
         }
         if (p[3] < 0.) {
            val += (fabs(p[3]) * p[6 + 2 * iPulse] / 2. / p[2] * exp((p[4] * p[4] / p[2] / 2. - time) / p[2]) * erfc((p[4] * p[4] / p[2] - time) / sqrt(2) / p[4]));
         }
      }
   }
   val += p[0];

   return val;
}

FEPFilterExp2GausMatched::~FEPFilterExp2GausMatched() {
   delete kernel;
}


/**
 * Find Hits after baseline subtraction and filtering
 */
FEPDS20kProcessing::FEPDS20kProcessing(midas::odb settings):FEPFilterBase(settings)
{
  // baseline value read from ODB "/VX2740 defaults/DC offset (pct)" x 2^16-1
  fBaseline      = 65440.;

  // filter parameters
  // exponential time constant
  ftau                 = settings["Processing"]["AR tau"];
  // hit finder moving average window
  fWindow              = settings["Processing"]["MA Window"];

  // peak finder parameters
  // select fixed or dynamic threshold for hit finding
  fFixed = settings["Processing"]["Fixed threshold"];
  // peak finder threshold, if fFixed is TRUE
  fThreshold           = settings["Processing"]["Hit threshold"];
  // pedestal length to calculate baseline RMS, if fFixed is FALSE
  fPedestalLength      = settings["Processing"]["Pedestal length"];
  // sigma above baseline RMS for peak detection, if fFixed is FALSE
  fNsigma              = settings["Processing"]["N Sigmas"];
  // peak finder charge/prominence cut
  fChargePromCut       = settings["Processing"]["Hit cut"];
  
  // downsampling parameters
  ffactor              = settings["Processing"]["Downsampling factor"];
   
  // filter convenience variables
  _tau =  1./static_cast<double>(ftau);
  _a = 1. - _tau;
  _dwin = 1./static_cast<double>(fWindow);
  fHalfWindow = fWindow/2;
  
  // filtered waveform 
  ma_sub = new std::vector<double>;
}

FEPDS20kProcessing::~FEPDS20kProcessing()
{
  // clean-up after the fact
  ma_sub->clear();
  delete ma_sub;
}

bool FEPDS20kProcessing::is_enabled(midas::odb& settings)
{
  return settings["Processing"]["Enable"];
}

void FEPDS20kProcessing::create_settings(midas::odb& settings)
{
  if( !settings.is_subkey("Processing") )
    {
      // Create settings for this filter in a subdirectory of the overall
      // FEP ODB settings.
      midas::odb these_settings({ {"Enable",              true}, 
	                          {"AR tau",              static_cast<uint16_t>(30)},
				  {"MA Window",           static_cast<uint16_t>(50)},
				  {"Fixed threshold",     true},
				  {"Pedestal length",     static_cast<uint16_t>(100)},
				  {"N Sigmas",            static_cast<double>(3)},
                                  {"Hit threshold",       static_cast<double>(4)},
				  {"Hit cut",             static_cast<double>(4)},
				  {"Downsampling factor", static_cast<uint16_t>(2)} });
      these_settings.connect(settings.get_full_path() + "/Processing");
    }
}


INT FEPDS20kProcessing::apply(const ChannelId& chan, std::shared_ptr<Waveform> in, std::vector<std::shared_ptr<HitBase>> &out, bool debug)
{
   error_message = "";
   if( debug ) std::cout<<"FEPDS20kProcessing::apply"<<std::endl;

   // create output: MultiHit
   std::shared_ptr<MultiHit> hit = std::make_shared<MultiHit>();
   hit->time_since_run_start_secs = in->time_since_run_start_secs;
   if( debug ) std::cout<<"FEPDS20kProcessing::apply TS: "<<hit->time_since_run_start_secs<<std::endl;
   
   // baseline subtraction + AR filter + MA subtraction
   filter( in, ma_sub );

   if( !fFixed ) ComputeThreshold( ma_sub, debug );

   // find peaks above threshold
   // remove fluctuations by cutting on charge/amplitude
   // fill MultiHit with baseline subtracted amplitude
   peak_finder(ma_sub, hit, debug);

   // put MultiHit in the output vector
   out.push_back(hit);

   // clean-up after the fact
   ma_sub->clear();

   return SUCCESS;
}

void FEPDS20kProcessing::downsampler(std::shared_ptr<Waveform> in, std::shared_ptr<Waveform> out)
{
  for (unsigned int i = 0; i < in->samples.size(); i += ffactor)
    {
      out->samples.push_back(in->samples[i]);
    }
}

void FEPDS20kProcessing::filter(std::shared_ptr<Waveform> in,
				std::vector<double>* out)
{
  // initialize
  const size_t nsamples = in->samples.size();
  const uint16_t* adc_samples = in->samples.data();
      
  // prepare output
  // baseline subtracted + AR filtered and MA subtracted
  out->resize(nsamples);
  double* hit = out->data(); // AR-MA
      
  // prepare AR kernel
  double AR[nsamples];
  //  if( fInvert ) AR[0] = static_cast<double>(fBaseline - adc_samples[nsamples-1]);
  //  else
  AR[0] = static_cast<double>(adc_samples[nsamples-1]-fBaseline);

  // helper variables
  size_t i, j=1, n; // running index
  double y; // baseline subtracted waveform
  double flt[nsamples]; // AR filtered
  double asum = 0.; // partial sum
  size_t center_window = nsamples - fHalfWindow;
      
  // Baseline Subtraction and Auto-Recursive kernel generation
  // the raw waveform is readout backwards to fill the AR kernel
  for( i=1; i<nsamples; ++i)
    {
      //      if( fInvert ) y = static_cast<double>(fBaseline-adc_samples[nsamples-1-i]);
      //      else
      y = static_cast<double>(adc_samples[nsamples-1-i]-fBaseline);
      AR[i] = y + _a * AR[i - 1];
    }
      
  // -------------------------------------------------------
  // create output:
  // Auto Recursive filtering and Moving Average Subtraction
  // -------------------------------------------------------
  // MA and subtraction first point and
  // filter [0,0.5*MA window) in half window steps
  for( i=0; i<fHalfWindow; ++i )
    {
      flt[i] = AR[ nsamples - 1 - i ] * _tau; // AR filter
      asum += 2*flt[i]; 
    }
  hit[0] = flt[0] - asum * _dwin;
  // -------------------------------------------------------
  // MA and subtraction in first half window and
  // filter [0.5*MA window, MA window) in half window steps
  for( i=1; i<=fHalfWindow; ++i )
    {
      n = center_window - i;  // index in AR vector
      flt[ i + fHalfWindow - 1 ] = AR[ n ] * _tau; // AR filter
      asum += flt[ i + fHalfWindow - 1 ] - flt[ fHalfWindow - i ];
      hit[i] = flt[i] - asum/fWindow; // MA subtraction
    }
  // -------------------------------------------------------
  // MA and subtraction from half window to number of samples minus half window
  // and filter [MA window, N samples) in N samples - 2*MA windows steps
  for( i=fHalfWindow+1; i<=center_window; ++i )
    {
      n = center_window - i; // index in AR vector
      flt[ i + fHalfWindow - 1 ] = AR[ n ] * _tau; // AR filter
      asum += flt[ i + fHalfWindow - 1 ] - flt[ i - fHalfWindow - 1 ];
      hit[i] = flt[i] - asum * _dwin;  // MA subtraction
    }
  // -------------------------------------------------------
  // MA and subtraction from number of samples minus half window to number of samples
  // and filter is already done
  for( i=center_window+1; i<nsamples; ++i )
    {
      asum += flt[ nsamples - j++ ] - flt[ i - fHalfWindow - 1 ];
      hit[i] = flt[i] - asum * _dwin;  // MA subtraction
    }
  // -------------------------------------------------------
}


void FEPDS20kProcessing::peak_finder(const std::vector<double>* in, std::shared_ptr<MultiHit> out, bool debug)
{
  auto it = in->begin();
  double thr = fThreshold;
  bool first = true;
  uint16_t s0 = 0;
  
  while(it!=in->end())
    {

      // find where waveform is above thr
      it = std::find_if( it, in->end(),
			 [thr](double const& r)
			 { return (r>thr); } );
      // if it's never above thr, quit
      if( it==in->end() ) break;
      
      uint32_t iStart = std::distance(in->begin(),it);
      if(debug) std::cout<<"\t["<<iStart;
      
      // find where waveform goes back below thr
      it = std::find_if( it, in->end(),
			 [thr](double const& r)
			 { return (r<thr); } );
      uint32_t iEnd = std::distance(in->begin(),it);
      if(debug) std::cout<<" "<<iEnd<<"] ";
      
      // find peak pulse
      std::vector<double>::const_iterator max_pos = std::max_element(in->begin()+iStart,
								     in->begin()+iEnd);
      // amplitude
      double A = *max_pos;
      // charge
      double Q = std::accumulate(in->begin()+iStart,in->begin()+iEnd,double(0));
      if(debug) std::cout<<"  A: "<<A<<"   Q: "<<Q<<"\tQ/A: "<<Q/A;
      // if the pulse is too 'small'
      if( Q/A < fChargePromCut )
	{
      if( it==in->end() ) break;
	  ++it;
	  if(debug) std::cout<<" too short"<<std::endl;
	  continue;
	}

      // charge
      out->charge += static_cast<uint32_t>(Q);

      // peak-width
      uint32_t duration = iEnd-iStart;
      out->length += static_cast<uint16_t>(duration);

      // peak time (sample)
      uint16_t sample = std::distance(in->begin(),max_pos);
      if( first ) // store time of first hit
	{
	  double t0 = static_cast<double>(sample/VX2740_CLOCK_FREQ);
	  out->time_since_run_start_secs += t0;
	  s0 = sample;
	  first = false;
	}
      
      out->prominances[ sample-s0 ] = A; // delta-t (in samples) to prominance, first entry delta-t=0
      if(debug) std::cout<<"  sample: "<<sample<<"( "<<sample-s0<<" ) --> "<<A<<std::endl;
      
      if( it==in->end() ) break;
      ++it;
    }
}

void FEPDS20kProcessing::ComputeThreshold(const std::vector<double>* in, bool debug)
{
  uint16_t len = fPedestalLength;
  if( fPedestalLength >= in->size() ) len = in->size()-1;
  double den = static_cast<double>(len);
  // calculate the baseline and its rms of the filtered waveform
  double bline = std::accumulate(in->begin(),
				 in->begin()+len,
				 double(0))/den;
  double rms = std::inner_product(in->begin(),
				  in->begin()+len,
				  in->begin(),double(0));
  // rms = sqrt( (rms / den) - (bline * bline) );
  double m2 = bline * bline / den;
  rms = sqrt( (rms - m2) / den );
  
  fThreshold = bline + fNsigma * rms;
  if( debug ) std::cout<<"Baseline: "<<bline<<"  RMS:"<<rms<<"  Threshold: "<<fThreshold<<std::endl;
}

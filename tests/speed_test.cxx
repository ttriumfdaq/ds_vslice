#include "midas.h"
#include "socket_utils.h"
#include "vslice_utils.h"
#include "zmqpp/zmqpp.hpp"
#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sstream>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

// Tool for comparing speed of sending data via ZMQ or raw linux socket.
// We measure how long it takes to send a large payload over the network
// and to receive a small reply.
//
// We must measure this "round-trip" time rather than just the time taken
// to call send_raw()/receive_raw()/read()/write() so that we are actually
// measuring the transmission time, not just how long it takes to pump data
// into a buffer.
//
// In particular, one cannot just use the time it takes to call ZMQ's receive_raw()
// function to time how long it takes to read data, as ZMQ's background threads are
// already reading data from the network before you call receive_raw()!
//
// To run this test, you should have two instances running:
// - one in receive mode (-r)
// - one on another machine in transmit mode (-t).
//
// There is little error-checking in this quick test code. Make sure to start the
// receiver before the transmitter.
// Data transmission times will be reported on the transmit client.

void usage() {
   printf("speed_test.exe - Test sending 100MB of data via zmq or raw socket.\n");
   printf("  -z/-s       run in zmq or socket mode\n");
   printf("  -t/-r       transmit or receive data\n");
   printf("  -p <port>   integer port to bind/connect to\n");
   printf("  -h <dest>   data destination host (string) if transmitting data\n");
   printf(" [-m <MiB>]   data size in MiB (integer, defaults to 100)\n\n");
   printf("Run in -r mode on one node first, then start -t mode on other node.\n");
   printf("E.g. `./speed_test.exe -z -r -p 7000 -m 500`\n");
   printf("     `./speed_test.exe -z -t -p 7000 -m 500 -h dsvslice`\n");
}

#define NUM_PAYLOADS 10

void send_data(bool is_zmq, std::string dest_host, int dest_port, uint64_t size_bytes) {
   int theory_ms = (double) size_bytes / 10e9 * 1000. * 8.;// 10Gb speed, 1000 ms per s, 8 bytes per bit
   vslice::ts_printf("Theoretical min time to send %s over a 10Gb link is %dms\n", vslice::format_bytes(size_bytes).c_str(), theory_ms);

   char *payload = (char *) malloc(size_bytes);

   std::vector<double> durations_us;

   if (is_zmq) {
      zmqpp::context zmq_context;
      zmqpp::socket *zmq_socket = new zmqpp::socket(zmq_context, zmqpp::socket_type::request);

      char dest[128];
      snprintf(dest, 127, "tcp://%s:%d", dest_host.c_str(), dest_port);

      vslice::ts_printf("Connecting to %s to send data\n", dest);
      zmq_socket->connect(dest);
      vslice::ts_printf("Sending data...\n");

      while (durations_us.size() < NUM_PAYLOADS) {
         int num_waits = 0;
         timeval start;
         gettimeofday(&start, NULL);

         bool sent = zmq_socket->send_raw((char *) payload, size_bytes, zmqpp::socket_t::dont_wait);

         if (sent) {
            zmqpp::message reply;
            zmq_socket->receive(reply);

            timeval end;
            gettimeofday(&end, NULL);
            int delta_us = (end.tv_sec - start.tv_sec) * 1e6 + (end.tv_usec - start.tv_usec);

            if (num_waits > 0) {
               vslice::ts_printf("Took %d attempts to actually send the data!\n", num_waits);
            }

            vslice::ts_printf("Took %fms to send %s of data and get ack\n", delta_us / 1000., vslice::format_bytes(size_bytes).c_str());
            durations_us.push_back(delta_us);
         } else {
            num_waits++;
            ss_sleep(1);
         }

         if (num_waits > 1000) {
            vslice::ts_printf("Giving up after %d attempts to send the data!\n", num_waits);
            break;
         }
      }

      zmq_socket->close();
   } else {
      // Raw socket
      int sockfd = socket_utils::connect(dest_host.c_str(), dest_port);

      if (sockfd < 0) {
         vslice::ts_printf("Connect failed\n");
         return;
      }

      while (durations_us.size() < NUM_PAYLOADS) {
         timeval start;
         gettimeofday(&start, NULL);

         // Send the data
         if (!socket_utils::write_fully_with_size_protocol(sockfd, payload, size_bytes)) {
            vslice::ts_printf("Sending failed\n");
            break;
         }

         // Wait for reply
         while (!socket_utils::wait_for_data(sockfd, 1000)) {
            vslice::ts_printf("Waiting for response...\n");
         }

         // Read reply
         uint64_t reply_len;
         if (!socket_utils::read_fully_with_size_protocol(sockfd, payload, 10, reply_len)) {
            vslice::ts_printf("Reading response failed\n");
            break;
         }

         timeval end;
         gettimeofday(&end, NULL);
         int delta_us = (end.tv_sec - start.tv_sec) * 1e6 + (end.tv_usec - start.tv_usec);
         durations_us.push_back(delta_us);

         vslice::ts_printf("Took %fms to send %s of data and get ack\n", delta_us / 1000., vslice::format_bytes(size_bytes).c_str());
      }

      close(sockfd);
   }

   double average_us = 0;
   for (auto dur : durations_us) {
      average_us += dur / durations_us.size();
   }

   vslice::ts_printf("Average time to send %s and get an ack: %fms\n", vslice::format_bytes(size_bytes).c_str(), average_us / 1000);
}

void recv_data(bool is_zmq, int port, uint64_t size_bytes) {
   size_t len_bytes = size_bytes + 10000;
   char *recv_buffer = (char *) malloc(len_bytes);

   if (is_zmq) {
      zmqpp::context zmq_context;
      zmqpp::socket *zmq_socket = new zmqpp::socket(zmq_context, zmqpp::socket_type::reply);

      char bind_addr[256];
      snprintf(bind_addr, 255, "tcp://*:%i", port);

      vslice::ts_printf("Binding to %s for receiving data\n", bind_addr);
      zmq_socket->bind(bind_addr);

      int num_recv = 0;

      while (num_recv < NUM_PAYLOADS) {
         if (zmq_socket->receive_raw(recv_buffer, len_bytes, zmqpp::socket::dont_wait)) {
            zmqpp::message reply;
            reply << 1;
            zmq_socket->send(reply);

            vslice::ts_printf("Received %s of data\n", vslice::format_bytes(len_bytes).c_str());

            num_recv++;
         } else {
            ss_sleep(1);
         }
      }

      zmq_socket->close();
   } else {
      // Raw socket
      int listenfd = socket_utils::bind(port);

      if (listenfd < 0) {
         vslice::ts_printf("Failed to bind to port %d\n", port);
         return;
      }

      int connfd = socket_utils::accept(listenfd);

      if (connfd < 0) {
         vslice::ts_printf("Failed to accept on port %d\n", port);
         return;
      } else {
         vslice::ts_printf("Accepted connection on port %d\n", port);
      }

      int num_recv = 0;

      while (num_recv < NUM_PAYLOADS) {
         // Wait for data
         while (!socket_utils::wait_for_data(connfd, 5000)) {
            vslice::ts_printf("Waiting for data...\n");
         }

         // Read data
         uint64_t read_len;
         if (!socket_utils::read_fully_with_size_protocol(connfd, recv_buffer, size_bytes, read_len)) {
            vslice::ts_printf("Reading data failed\n");
            break;
         }

         // Send reply
         char payload[10];

         if (!socket_utils::write_fully_with_size_protocol(connfd, payload, 10)) {
            vslice::ts_printf("Sending reply failed\n");
            break;
         }

         vslice::ts_printf("Received %s of data\n", vslice::format_bytes(size_bytes).c_str());

         num_recv++;
         ss_sleep(1);
      }

      close(connfd);
      close(listenfd);
   }
}

int main(int argc, char **argv) {
   // Parse options
   bool is_zmq = false;
   bool is_socket = false;
   bool is_transmit = false;
   bool is_recieve = false;

   int bind_port = -1;
   std::string destination;
   int size_MB = 100;

   for (int i = 1; i < argc; i++) {
      if (strlen(argv[i]) != 2 || argv[i][0] != '-') {
         printf("Invalid argument %s\n", argv[i]);
         usage();
         return FE_ERR_DRIVER;
      }

      if (argv[i][1] == 'z') {
         is_zmq = true;
      } else if (argv[i][1] == 's') {
         is_socket = true;
      } else if (argv[i][1] == 't') {
         is_transmit = true;
      } else if (argv[i][1] == 'r') {
         is_recieve = true;
      } else if (argv[i][1] == 'p') {
         bind_port = atoi(argv[++i]);
      } else if (argv[i][1] == 'h') {
         destination = argv[++i];
      } else if (argv[i][1] == 'm') {
         size_MB = atoi(argv[++i]);
      } else {
         printf("Unknown argument '%s'\n", argv[i]);
         usage();
         return FE_ERR_DRIVER;
      }
   }

   if ((!is_zmq && !is_socket) || (is_zmq && is_socket)) {
      printf("Specify -z or -s\n");
      usage();
      return FE_ERR_DRIVER;
   }

   if ((!is_transmit && !is_recieve) || (is_transmit && is_recieve)) {
      printf("Specify -t or -r\n");
      usage();
      return FE_ERR_DRIVER;
   }

   if (is_transmit && destination == "") {
      printf("Specify data destination host\n");
      usage();
      return FE_ERR_DRIVER;
   }

   if (bind_port < 0) {
      printf("Specify port to bind/connect to\n");
      usage();
      return FE_ERR_DRIVER;
   }

   unsigned int size_bytes = size_MB * 1024 * 1024;

   if (is_transmit) {
      send_data(is_zmq, destination, bind_port, size_bytes);
   } else {
      recv_data(is_zmq, bind_port, size_bytes);
   }

   return 0;
}

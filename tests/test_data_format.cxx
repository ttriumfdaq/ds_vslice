#include "catch.hpp"
#include "data_structures.h"

SCENARIO("Basic tests of slice data encoding/decoding", "[data_format]") {

   GIVEN("A slice") {
      Slice in;
      in.slice_idx = 1;
      in.slice_start_time_secs = 1;

      Slice out;
      uint64_t *buffer = (uint64_t *) calloc(1e6, sizeof(uint64_t));

      ChannelId chan_a;
      chan_a.frontend_id = 3;
      chan_a.board_id = 1;
      chan_a.channel_id = 63;

      ChannelId chan_b;
      chan_b.frontend_id = 9;
      chan_b.board_id = 3;
      chan_b.channel_id = 15;

      std::shared_ptr<QT> qt_a = std::make_shared<QT>();
      qt_a->charge = 1e5;
      qt_a->time_since_run_start_secs = 1.23;

      std::shared_ptr<QT> qt_b = std::make_shared<QT>();
      qt_b->charge = 5000;
      qt_b->time_since_run_start_secs = 1.36;

      std::shared_ptr<MultiHit> mh_a = std::make_shared<MultiHit>();
      mh_a->charge = 10;
      mh_a->time_since_run_start_secs = 1.44;
      mh_a->prominances[0] = 100;
      mh_a->prominances[100] = 1300;

      std::shared_ptr<MultiHit> mh_b = std::make_shared<MultiHit>();
      mh_b->charge = 3300;
      mh_b->time_since_run_start_secs = 1.49;
      mh_b->prominances[0] = 45;

      std::shared_ptr<MultiHit> mh_b2 = std::make_shared<MultiHit>();
      mh_b2->charge = 300;
      mh_b2->time_since_run_start_secs = 1.51;
      mh_b2->prominances[0] = 2;
      mh_b2->prominances[100] = 1600;
      mh_b2->prominances[300] = 8004;

      std::shared_ptr<Waveform> wf_a = std::make_shared<Waveform>();
      wf_a->algorithms_applied = 0x0;
      wf_a->time_since_run_start_secs = 1.56;

      for (int i = 0; i < 16; i++) {
         wf_a->samples.push_back(i);
      }

      std::shared_ptr<Waveform> wf_b = std::make_shared<Waveform>();
      wf_b->algorithms_applied = 0x0;
      wf_b->time_since_run_start_secs = 1.88;

      for (int i = 0; i < 32; i++) {
         wf_b->samples.push_back(i);
      }

      WHEN("A single QT") {
         in.channel_qts[chan_a].push_back(qt_a);

         THEN("QT decoded") {
            uint32_t num_bytes_written;
            uint32_t num_bytes_read;
            bool ok_write = in.encode(buffer, num_bytes_written);

            if (!ok_write) { FAIL(in.error_message); }

            bool ok_read = out.decode(buffer, num_bytes_read);

            if (!ok_read) { FAIL(out.error_message); }

            REQUIRE(num_bytes_written == num_bytes_read);
            REQUIRE(out.slice_idx == in.slice_idx);
            REQUIRE(out.slice_start_time_secs == Approx(in.slice_start_time_secs));
            REQUIRE(out.channel_qts.find(chan_a) != out.channel_qts.end());
            REQUIRE(out.channel_qts[chan_a].size() == 1);
            REQUIRE(out.channel_qts[chan_a][0]->charge == qt_a->charge);
            REQUIRE(out.channel_qts[chan_a][0]->time_since_run_start_secs == Approx(qt_a->time_since_run_start_secs));
         }
      }

      WHEN("Two QTs for one channel") {
         in.channel_qts[chan_a].push_back(qt_a);
         in.channel_qts[chan_a].push_back(qt_b);

         THEN("QTs decoded") {
            uint32_t num_bytes_written;
            uint32_t num_bytes_read;
            bool ok_write = in.encode(buffer, num_bytes_written);

            if (!ok_write) { FAIL(in.error_message); }

            bool ok_read = out.decode(buffer, num_bytes_read);

            if (!ok_read) { FAIL(out.error_message); }

            REQUIRE(num_bytes_written == num_bytes_read);
            REQUIRE(out.channel_qts.find(chan_a) != out.channel_qts.end());
            REQUIRE(out.channel_qts[chan_a].size() == 2);
            REQUIRE(out.channel_qts[chan_a][0]->charge == qt_a->charge);
            REQUIRE(out.channel_qts[chan_a][0]->time_since_run_start_secs == Approx(qt_a->time_since_run_start_secs));
            REQUIRE(out.channel_qts[chan_a][1]->charge == qt_b->charge);
            REQUIRE(out.channel_qts[chan_a][1]->time_since_run_start_secs == Approx(qt_b->time_since_run_start_secs));
         }
      }

      WHEN("A single MultiHit") {
         in.channel_multihits[chan_a].push_back(mh_a);

         THEN("MultiHit decoded") {
            uint32_t num_bytes_written;
            uint32_t num_bytes_read;
            bool ok_write = in.encode(buffer, num_bytes_written);

            if (!ok_write) { FAIL(in.error_message); }

            bool ok_read = out.decode(buffer, num_bytes_read);

            if (!ok_read) { FAIL(out.error_message); }

            REQUIRE(num_bytes_written == num_bytes_read);
            REQUIRE(out.slice_idx == in.slice_idx);
            REQUIRE(out.slice_start_time_secs == Approx(in.slice_start_time_secs));
            REQUIRE(out.channel_multihits.find(chan_a) != out.channel_multihits.end());
            REQUIRE(out.channel_multihits[chan_a].size() == 1);
            REQUIRE(out.channel_multihits[chan_a][0]->charge == mh_a->charge);
            REQUIRE(out.channel_multihits[chan_a][0]->time_since_run_start_secs == Approx(mh_a->time_since_run_start_secs));
            REQUIRE(out.channel_multihits[chan_a][0]->prominances.size() == mh_a->prominances.size());
            REQUIRE(out.channel_multihits[chan_a][0]->prominances[0] == mh_a->prominances[0]);
            REQUIRE(out.channel_multihits[chan_a][0]->prominances[100] == mh_a->prominances[100]);
         }
      }

      WHEN("A single waveform") {
         in.channel_waveforms[chan_a].push_back(wf_a);

         THEN("Waveform decoded") {
            uint32_t num_bytes_written;
            uint32_t num_bytes_read;
            bool ok_write = in.encode(buffer, num_bytes_written);

            if (!ok_write) { FAIL(in.error_message); }

            bool ok_read = out.decode(buffer, num_bytes_read);

            if (!ok_read) { FAIL(out.error_message); }

            REQUIRE(num_bytes_written == num_bytes_read);
            REQUIRE(out.channel_waveforms.find(chan_a) != out.channel_waveforms.end());
            REQUIRE(out.channel_waveforms[chan_a].size() == 1);
            REQUIRE(out.channel_waveforms[chan_a][0]->time_since_run_start_secs == Approx(wf_a->time_since_run_start_secs));
            REQUIRE(out.channel_waveforms[chan_a][0]->samples == wf_a->samples);

         }
      }

      WHEN("Two waveforms for one channel") {
         in.channel_waveforms[chan_a].push_back(wf_a);
         in.channel_waveforms[chan_a].push_back(wf_b);

         THEN("Waveforms decoded") {
            uint32_t num_bytes_written;
            uint32_t num_bytes_read;
            bool ok_write = in.encode(buffer, num_bytes_written);

            if (!ok_write) { FAIL(in.error_message); }

            bool ok_read = out.decode(buffer, num_bytes_read);

            if (!ok_read) { FAIL(out.error_message); }

            REQUIRE(num_bytes_written == num_bytes_read);
            REQUIRE(out.channel_waveforms.find(chan_a) != out.channel_waveforms.end());
            REQUIRE(out.channel_waveforms[chan_a].size() == 2);
            REQUIRE(out.channel_waveforms[chan_a][0]->time_since_run_start_secs == Approx(wf_a->time_since_run_start_secs));
            REQUIRE(out.channel_waveforms[chan_a][0]->samples == wf_a->samples);
            REQUIRE(out.channel_waveforms[chan_a][1]->time_since_run_start_secs == Approx(wf_b->time_since_run_start_secs));
            REQUIRE(out.channel_waveforms[chan_a][1]->samples == wf_b->samples);

         }
      }

      WHEN("Multiple channels") {
         in.channel_qts[chan_a].push_back(qt_a);
         in.channel_multihits[chan_a].push_back(mh_a);
         in.channel_waveforms[chan_a].push_back(wf_a);
         in.channel_qts[chan_b].push_back(qt_b);
         in.channel_multihits[chan_b].push_back(mh_b);
         in.channel_waveforms[chan_b].push_back(wf_b);
         in.channel_multihits[chan_b].push_back(mh_b2);

         THEN("Channels decoded") {
            uint32_t num_bytes_written;
            uint32_t num_bytes_read;
            bool ok_write = in.encode(buffer, num_bytes_written);

            if (!ok_write) { FAIL(in.error_message); }

            bool ok_read = out.decode(buffer, num_bytes_read);

            if (!ok_read) { FAIL(out.error_message); }

            REQUIRE(num_bytes_written == num_bytes_read);

            REQUIRE(out.channel_qts.find(chan_a) != out.channel_qts.end());
            REQUIRE(out.channel_qts.find(chan_b) != out.channel_qts.end());
            REQUIRE(out.channel_multihits.find(chan_a) != out.channel_multihits.end());
            REQUIRE(out.channel_multihits.find(chan_b) != out.channel_multihits.end());
            REQUIRE(out.channel_waveforms.find(chan_a) != out.channel_waveforms.end());
            REQUIRE(out.channel_waveforms.find(chan_b) != out.channel_waveforms.end());

            REQUIRE(out.channel_qts[chan_a].size() == 1);
            REQUIRE(out.channel_qts[chan_a].size() == 1);
            REQUIRE(out.channel_multihits[chan_a].size() == 1);
            REQUIRE(out.channel_multihits[chan_b].size() == 2);
            REQUIRE(out.channel_waveforms[chan_a].size() == 1);
            REQUIRE(out.channel_waveforms[chan_b].size() == 1);

            REQUIRE(out.channel_qts[chan_a][0]->time_since_run_start_secs == Approx(qt_a->time_since_run_start_secs));
            REQUIRE(out.channel_qts[chan_b][0]->time_since_run_start_secs == Approx(qt_b->time_since_run_start_secs));
            REQUIRE(out.channel_multihits[chan_a][0]->time_since_run_start_secs == Approx(mh_a->time_since_run_start_secs));
            REQUIRE(out.channel_multihits[chan_b][0]->time_since_run_start_secs == Approx(mh_b->time_since_run_start_secs));
            REQUIRE(out.channel_multihits[chan_b][1]->time_since_run_start_secs == Approx(mh_b2->time_since_run_start_secs));
            REQUIRE(out.channel_waveforms[chan_a][0]->time_since_run_start_secs == Approx(wf_a->time_since_run_start_secs));
            REQUIRE(out.channel_waveforms[chan_b][0]->time_since_run_start_secs == Approx(wf_b->time_since_run_start_secs));

            REQUIRE(out.channel_multihits[chan_a][0]->prominances.size() == 2);
            REQUIRE(out.channel_multihits[chan_b][0]->prominances.size() == 1);
            REQUIRE(out.channel_multihits[chan_b][1]->prominances.size() == 3);

            REQUIRE(out.channel_multihits[chan_a][0]->prominances.find(0) != out.channel_multihits[chan_a][0]->prominances.end());
            REQUIRE(out.channel_multihits[chan_b][0]->prominances.find(0) != out.channel_multihits[chan_b][0]->prominances.end());
            REQUIRE(out.channel_multihits[chan_b][1]->prominances.find(0) != out.channel_multihits[chan_b][1]->prominances.end());

            REQUIRE(out.channel_multihits[chan_a][0]->prominances[0] == 100);
            REQUIRE(out.channel_multihits[chan_a][0]->prominances[100] == 1300);
            REQUIRE(out.channel_multihits[chan_b][0]->prominances[0] == 45);
            REQUIRE(out.channel_multihits[chan_b][1]->prominances[0] == 2);
            REQUIRE(out.channel_multihits[chan_b][1]->prominances[100] == 1600);
            REQUIRE(out.channel_multihits[chan_b][1]->prominances[300] == 8004);

         }
      }

      free(buffer);
   }
}

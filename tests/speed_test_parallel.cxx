#include "midas.h"
#include "socket_utils.h"
#include "vslice_utils.h"
#include <errno.h>
#include <sstream>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <map>
#include <mutex>

// Tool for testing data transmission from multiple nodes to a single socket.
//
// You should start one "receive" instance, and tell it how many "transmit" instances
// you plan to start. Then start all the "transmit" instances (probably all on different
// nodes); these will send a handshake to the "receive" instance. Once the "receive" 
// instance has seen handshakes from all the "transmit" instances it expects, it will tell
// them to start sending data.
//
// After all the data has been received, timing information will be printed to screen.

void usage() {
   printf("speed_test_parallel.exe - Test sending large data packets from multiple nodes to a single recipient.\n");
   printf("  -t/-r        transmit or receive data\n");
   printf("  -p <port>    integer port to bind/connect to\n");
   printf("  -h <dest>    data destination host (string) if transmitting data\n");
   printf(" [-m <MiB>]    data size in MiB (integer, defaults to 100)\n");
   printf(" [-n <num_tx>] if running with -r, the number of -t instances you will start\n\n");
   printf("Run in -r mode on one node first, then start in -t mode on other nodes.\n");
   printf("E.g. `./speed_test_parallel.exe -r -p 7000 -m 500 -n 2`\n");
   printf("     `./speed_test_parallel.exe -t -p 7000 -m 500 -h dsvslice`\n");
   printf("     `./speed_test_parallel.exe -t -p 7000 -m 500 -h dsvslice`\n");
}

#define HANDHSAKE_SIZE 3
#define HANDSHAKE_0 54
#define HANDSHAKE_1 70
#define HANDSHAKE_2 32

#define START_SIGNAL_SIZE 2
#define START_SIGNAL_0 10
#define START_SIGNAL_1 99

// We need an object that will live along time for passing args to 
// recv_data_thread().
std::map<int, int> conn_fds;
std::map<int, pthread_t*> recv_threads;

std::mutex conn_fds_mutex;
std::vector<int> conn_fds_handshake;
std::vector<int> conn_fds_finished;

bool time_to_send_data = false;

uint64_t payload_size_bytes = 0;

void send_data(std::string dest_host, int dest_port) {
   char *handshake = (char *) malloc(HANDHSAKE_SIZE);
   handshake[0] = HANDSHAKE_0;
   handshake[1] = HANDSHAKE_1;
   handshake[2] = HANDSHAKE_2;

   char *start_signal = (char *) malloc(START_SIGNAL_SIZE);
   char *payload = (char *) malloc(payload_size_bytes);

   // Raw socket
   int sockfd = socket_utils::connect(dest_host.c_str(), dest_port);

   if (sockfd < 0) {
      vslice::ts_printf("Connect failed\n");
      return;
   }

   // Send a handshake
   if (!socket_utils::write_fully_with_size_protocol(sockfd, handshake, HANDHSAKE_SIZE)) {
      vslice::ts_printf("Sending handshake failed\n");
      return;
   }

   // Wait for "receive" instance to tell us to start.
   while (!socket_utils::wait_for_data(sockfd, 2500)) {
      vslice::ts_printf("Waiting for signal to start...\n");
   }

   // Read signal to start
   uint64_t reply_len;
   if (!socket_utils::read_fully_with_size_protocol(sockfd, start_signal, START_SIGNAL_SIZE, reply_len)) {
      vslice::ts_printf("Reading start signal failed\n");
      return;
   }

   if (start_signal[0] != START_SIGNAL_0 || start_signal[1] != START_SIGNAL_1) {
      vslice::ts_printf("Invalid start signal received\n");
      return;
   }

   // Send the data
   if (!socket_utils::write_fully_with_size_protocol(sockfd, payload, payload_size_bytes)) {
      vslice::ts_printf("Sending failed\n");
      return;
   }

   close(sockfd);

   vslice::ts_printf("Data sent\n");
}

void *recv_data_thread(void *arg) {
   // One thread spawned per "transmit" instance that connects to us.
   int connfd = *(int*)arg;
   std::string failure_msg;

   char *payload = (char*) malloc(payload_size_bytes);
   char *handshake = (char *) malloc(HANDHSAKE_SIZE);
   char *start_signal = (char *) malloc(START_SIGNAL_SIZE);
   start_signal[0] = START_SIGNAL_0;
   start_signal[1] = START_SIGNAL_1;

   // Wait for handshake
   while (!socket_utils::wait_for_data(connfd, 5000)) {
   }

   // Read handshake
   uint64_t handshake_read_len;
   if (!socket_utils::read_fully_with_size_protocol(connfd, handshake, HANDHSAKE_SIZE, handshake_read_len)) {
      vslice::ts_printf("Reading data failed\n");
      return NULL;
   }

   if (handshake_read_len != HANDHSAKE_SIZE || handshake[0] != HANDSHAKE_0 || handshake[1] != HANDSHAKE_1 || handshake[2] != HANDSHAKE_2) {
      vslice::ts_printf("Invalid handshake received\n");
      return NULL;
   }

   // Tell main thread that we've received a valid handshake
   conn_fds_mutex.lock();
   conn_fds_handshake.push_back(connfd);
   conn_fds_mutex.unlock();

   // Wait for signal to start
   while (!time_to_send_data) {
      usleep(1);
   }

   if (!socket_utils::write_fully_with_size_protocol(connfd, start_signal, START_SIGNAL_SIZE)) {
      vslice::ts_printf("Sending start signal failed\n");
      return NULL;
   }

   // Read data
   uint64_t read_len;
   if (!socket_utils::read_fully_with_size_protocol(connfd, payload, payload_size_bytes, read_len)) {
      vslice::ts_printf("Reading data failed\n");
      return NULL;
   }

   // Tell main thread that we've received all the data
   conn_fds_mutex.lock();
   conn_fds_finished.push_back(connfd);
   conn_fds_mutex.unlock();

   close(connfd);
   return NULL;
}

void recv_data_main(int port, int num_tx_expected) {
   int theory_ms = (double) payload_size_bytes * num_tx_expected / 10e9 * 1000. * 8.;// 10Gb speed, 1000 ms per s, 8 bytes per bit
   vslice::ts_printf("Theoretical min time to send %d * %s over a 10Gb link is %dms\n", num_tx_expected, vslice::format_bytes(payload_size_bytes).c_str(), theory_ms);

   int listenfd = socket_utils::bind(port);

   if (listenfd < 0) {
      vslice::ts_printf("Failed to bind to port %d\n", port);
      return;
   }

   while (true) {
      int connfd = socket_utils::accept(listenfd);

      if (connfd < 0) {
         vslice::ts_printf("Failed to accept on port %d\n", port);
         return;
      } else {
         vslice::ts_printf("Accepted connection on port %d\n", port);
      }

      conn_fds[connfd] = connfd;
      recv_threads[connfd] = new pthread_t;

      INT status = pthread_create(recv_threads[connfd], NULL, recv_data_thread, &conn_fds[connfd]);

      if (status != 0) {
         cm_msg(MERROR, __FUNCTION__, "Failed to create thread for handling new connection");
         return;
      }

      if (conn_fds.size() == num_tx_expected) {
         // Received connections from all the transmit instances we expect.
         break;
      }
   }

   vslice::ts_printf("Waiting for handshakes to complete...\n");

   while (conn_fds_handshake.size() < num_tx_expected) {
      ss_sleep(1);
   }

   timeval start;
   gettimeofday(&start, NULL);

   timeval last_print = start;

   vslice::ts_printf("Telling transmit instances to send data...\n");
   time_to_send_data = true;

   while (conn_fds_finished.size() < num_tx_expected) {
      ss_sleep(1);

      timeval now;
      gettimeofday(&now, NULL);

      if (now.tv_sec != last_print.tv_sec) {
         vslice::ts_printf("%u transmit instances have finished so far...\n", conn_fds_finished.size());
         last_print = now;
      }
   }

   vslice::ts_printf("All transmit instances have finished!\n", conn_fds_finished.size());

   timeval end;
   gettimeofday(&end, NULL);
   int delta_us = (end.tv_sec - start.tv_sec) * 1e6 + (end.tv_usec - start.tv_usec);
   int delta_ms = delta_us / 1000.;

   vslice::ts_printf("Took %dms to receive %d * %s of data, compared to theoretical prediction of %dms\n", delta_ms, num_tx_expected, vslice::format_bytes(payload_size_bytes).c_str(), theory_ms);

   double payload_MiB = payload_size_bytes / 1024. / 1024.;
   printf("\nnum_tx,data_per_tx_MiB,total_data_MiB,theory_ms,actual_ms\n");
   printf("%u,%f,%f,%d,%d\n", num_tx_expected, payload_MiB, payload_MiB * num_tx_expected, theory_ms, delta_ms);

   close(listenfd);

   for (auto connfd : conn_fds) {
      pthread_join(*(recv_threads[connfd.first]), NULL);
   }

}

int main(int argc, char **argv) {
   // Parse options
   bool is_transmit = false;
   bool is_recieve = false;

   int num_tx_expected = 0;
   int bind_port = -1;
   std::string destination;
   uint64_t size_MB = 100;

   for (int i = 1; i < argc; i++) {
      if (strlen(argv[i]) != 2 || argv[i][0] != '-') {
         printf("Invalid argument %s\n", argv[i]);
         usage();
         return FE_ERR_DRIVER;
      }

      if (argv[i][1] == 't') {
         is_transmit = true;
      } else if (argv[i][1] == 'r') {
         is_recieve = true;
      } else if (argv[i][1] == 'n') {
         num_tx_expected = atoi(argv[++i]);
      } else if (argv[i][1] == 'p') {
         bind_port = atoi(argv[++i]);
      } else if (argv[i][1] == 'h') {
         destination = argv[++i];
      } else if (argv[i][1] == 'm') {
         size_MB = atoi(argv[++i]);
      } else {
         printf("Unknown argument '%s'\n", argv[i]);
         usage();
         return FE_ERR_DRIVER;
      }
   }

   if ((!is_transmit && !is_recieve) || (is_transmit && is_recieve)) {
      printf("Specify -t or -r\n");
      usage();
      return FE_ERR_DRIVER;
   }

   if (is_transmit && num_tx_expected > 0) {
      printf("-n is only valid for the receiving end\n");
      usage();
      return FE_ERR_DRIVER;
   }

   if (is_recieve && num_tx_expected <= 0) {
      printf("Specify number of connections expected\n");
      usage();
      return FE_ERR_DRIVER;
   }

   if (is_transmit && destination == "") {
      printf("Specify data destination host\n");
      usage();
      return FE_ERR_DRIVER;
   }

   if (bind_port < 0) {
      printf("Specify port to bind/connect to\n");
      usage();
      return FE_ERR_DRIVER;
   }

   payload_size_bytes = size_MB * 1024 * 1024;

   if (is_transmit) {
      send_data(destination, bind_port);
   } else {
      recv_data_main(bind_port, num_tx_expected);
   }

   return 0;
}

#ifndef DATA_SOURCE_REAL_H
#define DATA_SOURCE_REAL_H

#include "data_source_base.h"
#include "vx2740_wrapper.h"

/** \file
 * Data source that reads from real VX2740s that are running Scope firmware.
 */

/** Data source that reads from real VX2740s that are running Scope firmware.
 *
 * This firmware uses a common trigger for all channels, so each read from
 * the board gives us data for 64 channels. The rest of the Vertical Slice
 * system expects one channel per "event", so we must convert the data
 * accordingly (effectively creating 64 1-channel events from 1 64-channel event).
 */
class DataSourceScopeMode : public DataSourceRealBase {
public:
   DataSourceScopeMode(HNDLE _hDB, INT _frontend_index, INT _board_id, bool _sw_eos);
   virtual ~DataSourceScopeMode() {}
   virtual INT begin_of_run(double _slice_width_ms) override;
   virtual INT populate_next_event(std::vector<std::shared_ptr<VXData> >& data, BOOL debug) override;
   virtual INT set_board_info(VX2740* _board, bool _enable, std::string _name) override;

private:
   HNDLE hDB; //!< ODB handle
   INT frontend_index; //!< FEP frontend index, so we can write more user-friendly error messages.
   INT board_id; //!< Board index within this FEP.
   DWORD max_raw_bytes; //!< Max number of bytes we can read from digitizer in a single event.
   bool software_eos; //!< Whether we should inject artificial end-of-slice events.

   unsigned char* raw_scope_event; //!< Buffer for reading raw data from the VX2740.

   double slice_width_secs; //!< Width of each slice, for if `software_eos` is true.
   double last_eos_secs; //!< End time of most recent slice, for if `software_eos` is true.
};

#endif

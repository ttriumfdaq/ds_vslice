#include "data_source_toy_mc.h"
#include "data_structures.h"
#include "socket_utils.h"
#include "data_structures.h"

#include <sstream>
#include<chrono>
#include<thread>
#include<iomanip>
#include<cstdlib>
#include<experimental/filesystem>

namespace fs = std::experimental::filesystem;

DataSourceToyMC::DataSourceToyMC(int portNumber, int frontEndIndex, int boardID, int statusPort){
	//First bind to the port
	int socket =  socket_utils::bind(portNumber);
	//Now accept the connection (Set to insocket)
	// This shouldn't really be in the constructor...
	insocket = socket_utils::accept(socket);
	if (insocket == -1){
		cm_msg(MERROR, __FUNCTION__, "Unable to accept connection. Was socket shutdown?");
		throw std::runtime_error("Unable to accept connection. Was socket shutdown?");
	}
	cm_msg(MINFO, __FUNCTION__, "Connected to ToyMC server for data transfer on port %d",portNumber);
	//Also initialize the input buffer
	inBuff = new uint64_t[0x7FFF+3];
	max_packet_bytes = (0x7FFF+3)*sizeof(uint64_t);

	cm_msg(MINFO, __FUNCTION__, "Maximum number of bytes: %d",max_packet_bytes);
	recv_bytes = 0;

	//If neccessary create the status port
	statsocket = -1;
	if(statusPort > -1){
		int statusconn = socket_utils::bind(statusPort);
		while(statsocket == -1){
	    		cm_msg(MINFO, __FUNCTION__, "Waiting for status connection from ToyMC server on port %d",statusPort);
			statsocket = socket_utils::accept(statusconn);
			//sleep(1000);
		}
		cm_msg(MINFO, __FUNCTION__, "Connected to ToyMC server for status transfer on port %d",statusPort);

	}
	//Initialize the log file
	std::stringstream nameStream;
	nameStream << std::getenv("TOYMC_LOGS_DIR");
	nameStream <<"/FEPLogs/";
	//Now check if the directory exists	
	bool isDir = fs::is_directory(fs::path(nameStream.str()));
	if(! isDir){
		std::cout << "DataSourceToyMC::Unable to find directory: "<<nameStream.str()<<" to create log file in "<<std::endl;
		exit(1);
	}
		
	nameStream << "Frontend_" << frontEndIndex << "_Board_"<<boardID<<"_";
	time_t t = std::time(nullptr);
	tm tm;
	localtime_r(&t, &tm);
	nameStream << std::put_time( &tm, "%Y-%m-%d_%H:%M" ) << ".log";
	cm_msg(MINFO, __FUNCTION__, "DataSourceToyMC::Outputting logging information to: %s",nameStream.str().c_str());
	logStream = std::ofstream(nameStream.str(), std::ios::out);
}

//Send ready signal to ToyMC
INT DataSourceToyMC::begin_of_run(double _slice_width_ms) {
	//Check for start of run signal 
	if(statsocket > -1){
		char ready[1] = {1}; 
		uint64_t readySize = 1;
		bool write_success = socket_utils::write_fully_with_size_protocol(statsocket, ready, readySize);
		
		//Now wait for response from ToyMC acknowledging it has recieved the confirmation
		while (!socket_utils::wait_for_data(statsocket, 100)) {
			cm_msg(MINFO, __FUNCTION__, "Waiting for response from ToyMC server");
		}
		char toyMCResponse[10];
		uint64_t respBufSize = 10;
		uint64_t recv_bytes;
		bool read_success = socket_utils::read_fully_with_size_protocol(statsocket, toyMCResponse, respBufSize, recv_bytes);
		if(!read_success){
			cm_msg(MERROR, __FUNCTION__, "Could not successfully get response from ToyMC server");
		}
		cm_msg(MINFO, __FUNCTION__, "Successfully got response from ToyMC server");
	}
	
	return SUCCESS;
}

DataSourceToyMC::~DataSourceToyMC(){
	delete inBuff;
	logStream.close();
}

//Send 
INT DataSourceToyMC::end_of_run(){
	if (statsocket > -1) {
		char signal[4] = "EOR";
		uint64_t sendSize = 3;
		bool write_success = false;
		int fail_counter = 0;

		while(!write_success && fail_counter < 10) {
			write_success = socket_utils::write_fully_with_size_protocol(statsocket, signal, sendSize);
			//Sleep for 0.1s then try again 
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
			fail_counter++;
		}

		if (write_success) {
			cm_msg(MINFO, __FUNCTION__, "Successfully sent EOR to ToyMC");
		} else {
			cm_msg(MERROR, __FUNCTION__, "Failed to send EOR to ToyMC");
		}
	}

	return SUCCESS;
}

INT DataSourceToyMC::populate_next_event(std::vector<std::shared_ptr<VXData> >& data, BOOL debug) {
	if (!socket_utils::wait_for_data(insocket, 10)) {
		// No data to read from ToyMC server.
		return SUCCESS;
	}

	bool read_success = socket_utils::read_fully_with_size_protocol(insocket, (char*) inBuff, max_packet_bytes, recv_bytes);
	
	if (!read_success){
		// If we fail to read the event for some reason print some error messages
		vslice::ts_printf("(data) Could not successfully read in data over ethernet\n");
		return FE_ERR_DRIVER;
	} 

	// Otherwise read in the data and process.
	// Just need to copy raw bytes into ring buffer.
	std::shared_ptr<VXData> event = std::make_shared<VXData>();
	event->decode(inBuff);
	data.push_back(event);

	if( debug ) {
	  time_t t = std::time(nullptr);
	  tm tm;
	  localtime_r(&t, &tm);
	  logStream << "["<<std::put_time( &tm, "%H:%M:%S" ) << "] ";
	  
	  if (event->is_end_of_slice) {
	    logStream << "End-of-slice received: Board: "<<event->board_id<<" Time: "<< event->timestamp_secs << std::endl;
	  } else {
	    logStream << "Event recieved: Board: "<<event->board_id<<" Channel: "<<event->channel_id<<" Time: "<< std::setprecision(6)<<event->timestamp_secs << std::endl;
	  }
	}

	return SUCCESS;
}

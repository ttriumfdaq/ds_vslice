#ifndef DEMO_DATA_STRUCTURE_H
#define DEMO_DATA_STRUCTURE_H

#include <cstdint>
#include <vector>
#include <string>
#include <memory>
#include <map>
#include <set>

/** \file
 * Structures for VX2740/Slice data, and their conversion to/from binary buffers.
 */

#define VX2740_CLOCK_FREQ 125000000

/** Data structure that mimics the key points of the raw VX2740 data structure.
 *
 * In particular:
 * - Encodes/decodes using 64 bit words
 * - First word encodes 48-bit timestamp (@125MHz), channel ID, and some user flags
 * - Second word encode energy (which we don't use in this dummy data yet)
 * - Third word encodes number of 64-bit waveform data words
 * - Then the waveform data follows
 *
 * Helper functions are provided for encoding/decoding from a buffer of uint64_t.
 */
class VXData {
public:
   double timestamp_secs; //!< Timestamp relative to start of run. Max timestamp is ~26 days!
   uint32_t board_id; //!< Board ID within this frontend.
   uint16_t channel_id; //!< Channel ID within a board (0 to 63)
   bool is_end_of_slice; //!< Whether this is a special end-of-slice event rather than a real data event.
   std::vector<uint16_t> wf_samples; //!< The actual waveform data.

   VXData();

   /** How many bytes it will take to encode this object. */
   uint32_t get_encoded_size_bytes();

   /** Encode to a buffer of uint64_t. */
   void encode(uint64_t *buf);

   /** Decode from a buffer of uint64_t. */
   void decode(uint64_t *buf);

   /** Decode only header info, not the waveform.
    *
    * \param[in] buf Buffer of uint64_t to decode from.
    * \param[out] wf_words Number of 64-bit waveform words this event contains.
    * \param[out] size_bytes Size of this event (including header words).
    */
   void decode_header(uint64_t* buf, uint16_t& wf_words, uint32_t& size_bytes);

   /** Number of 64-bit header words used when encoding this event. */
   uint16_t num_header_words();

   /** Number of 64-bit waveform words used when encoding this event (not the number of 16-bit waveform samples!). */
   uint16_t num_waveform_words();
};


/** Create a bitmask of length `mask_bits`, shifted by `shift_bits`.
 * E.g. `mask(8, 4) == 0xF00`
 */
inline uint64_t mask(unsigned int shift_bits, unsigned int mask_bits) {
   if (mask_bits + shift_bits > 64) {
      throw std::runtime_error("Too many bits to fit into a 64-bit word");
   }

   if (mask_bits == 64) {
      return 0xFFFFFFFFFFFFFFFF;
   }

   return (((uint64_t)1 << mask_bits) - 1) << shift_bits;
}

/** Get the bits of `word` that are in positions `shift_bits` to `shift_bits + mask_bits`.
 * E.g. `extract_bits(0xFC3, 4, 8) == 0xFC`.
 */
inline uint64_t extract_bits(uint64_t word, unsigned int shift_bits, unsigned int mask_bits) {
   return (word & mask(shift_bits, mask_bits)) >> shift_bits;
}

/** Get the lower `mask_bits` of `word`, then shift them by `shift_bits`.
 * E.g. `encode_bits(0xFA, 8, 4) == 0xA00`.
 */
inline uint64_t encode_bits(uint64_t word, unsigned int shift_bits, unsigned int mask_bits) {
   return (word & mask(0, mask_bits)) << shift_bits;
}

/** Encode the lower 4 bits of `flag` into the upper 4 bits of a 64-bit word. */
inline uint64_t encode_4bit_flag(uint8_t flag) {
   return encode_bits(flag, 60, 4);
}

/** Encode the lower 3 bits of `flag` into the upper 3 bits of a 64-bit word. */
inline uint64_t encode_3bit_flag(uint8_t flag) {
   return encode_bits(flag, 61, 3);
}

/** Extract the upper 4 bits of `word`. */
inline uint8_t extract_4bit_flag(uint64_t word) {
   return extract_bits(word, 60, 4);
}

/** Extract the upper 3 bits of `word`. */
inline uint8_t extract_3bit_flag(uint64_t word) {
   return extract_bits(word, 61, 3);
}

/** Check if the upper 4 bits of `word` match the `expected` value. */
inline bool check_4bit_flag(uint64_t word, uint8_t expected) {
   return extract_4bit_flag(word) == expected;
}

/** Check if the upper 3 bits of `word` match the `expected` value. */
inline bool check_3bit_flag(uint64_t word, uint8_t expected) {
   return extract_3bit_flag(word) == expected;
}

/** Convert a time difference in seconds to a number of VX2740 clock ticks. */
inline uint32_t time_diff_to_ticks(double start, double end) {
   return (end-start) * VX2740_CLOCK_FREQ;
}

/** Base class for QTs, Waveforms etc (anything that has a timestamp
 * from the digitizer).
 *
 * All derived classes must implement `get_num_bytes_needed_to_encode()`.
 */
struct HitBase {
   virtual ~HitBase() {};
   double time_since_run_start_secs; //<! Time since start of run, in seconds.

   /** The number of bytes needed to encode this object. Should always be a multiple
    * of 8 bytes (as we use 64-bit words).
    */
   virtual uint32_t get_num_bytes_needed_to_encode() = 0;
};

/** Represents an end-of-slice marker from the digitizer.
 *
 * We never actually write these to disk/network, so don't
 * need to encode. But we do still want to add to queues of
 * HitBase objects.
 */
struct EndOfSlice : public HitBase {
   virtual ~EndOfSlice() {};
   virtual uint32_t get_num_bytes_needed_to_encode() override {return 0;}
};

/** Represents a QT (charge + time) summary of a hit.
 *
 * Note that time is stored in `time_since_run_start_secs` from `HitBase` base class.
 */
struct QT : public HitBase {
   virtual ~QT() {};
   uint32_t charge; //<! Charge of this hit

   /** Encode this object as binary data.
    *
    * \param[in] buf Buffer to write data to.
    * \param[out] num_bytes_written Number of bytes written to `buf`.
    * \param[in] slice_start_time_secs Start time of the slice. We save
    *    bits in the binary representation by storing times relative to the
    *    start of the slice, rather than the start of the run.
    * \return Whether encoding was successful.
    */
   bool encode(uint64_t* buf, uint32_t& num_bytes_written, double slice_start_time_secs);

   /** Populate this object by decoding binary data.
    *
    * \param[in] buf Buffer to read data from.
    * \param[out] num_bytes_read Number of bytes read from `buf`.
    * \param[in] slice_start_time_secs Start time of the slice. We save
    *    bits in the binary representation by storing times relative to the
    *    start of the slice, rather than the start of the run.
    * \return Whether decoding was successful.
    */
   bool decode(uint64_t* buf, uint32_t& num_bytes_read, double slice_start_time_secs);

   virtual uint32_t get_num_bytes_needed_to_encode() override;
};

/** Represents a QT (charge + time) summary of a hit.
 *
 * Note that time is stored in `time_since_run_start_secs` from `HitBase` base class.
 */
struct MultiHit : public HitBase {
   virtual ~MultiHit() {};
   uint32_t charge; //<! Charge of this multi-hit
   uint16_t rise_time; //<! Rise time of this multi-hit
   uint16_t length; //<! Length of this multi-hit

   /// Map from delta-t (in samples) to prominance. There should always be an entry
   /// with a delta-t of 0, i.e. the prominance that matches the main 
   /// "time" of this MultiHit.
   std::map<uint16_t, uint16_t> prominances;

   /** Encode this object as binary data.
    *
    * \param[in] buf Buffer to write data to.
    * \param[out] num_bytes_written Number of bytes written to `buf`.
    * \param[in] slice_start_time_secs Start time of the slice. We save
    *    bits in the binary representation by storing times relative to the
    *    start of the slice, rather than the start of the run.
    * \return Whether encoding was successful.
    */
   bool encode(uint64_t* buf, uint32_t& num_bytes_written, double slice_start_time_secs);

   /** Populate this object by decoding binary data.
    *
    * \param[in] buf Buffer to read data from.
    * \param[out] num_bytes_read Number of bytes read from `buf`.
    * \param[in] slice_start_time_secs Start time of the slice. We save
    *    bits in the binary representation by storing times relative to the
    *    start of the slice, rather than the start of the run.
    * \return Whether decoding was successful.
    */
   bool decode(uint64_t* buf, uint32_t& num_bytes_read, double slice_start_time_secs);

   virtual uint32_t get_num_bytes_needed_to_encode() override;
};

/** Represents a waveform from the digitizer of a hit.
 * Note that time is stored in `time_since_run_start_secs` from `HitBase` base class.
 *
 * Algorithms applied may relate to smoothing or a special way of encoding/compressing
 * the samples when writing to network/disk. Specify bit meanings in `Slice::FilterAlgo` enum.
 */
struct Waveform : public HitBase {
   virtual ~Waveform() {};

   /** Bitmask of which filtering algothims have been applied.
    *
    * Bit meanings are stored in the `Slice::FilterAlgo` enum.
    */
   uint16_t algorithms_applied;

   /** Waveform data. */
   std::vector<uint16_t> samples;

   /** Encode this object as binary data.
    *
    * \param[in] buf Buffer to write data to.
    * \param[out] num_bytes_written Number of bytes written to `buf`.
    * \param[in] slice_start_time_secs Start time of the slice. We save
    *    bits in the binary representation by storing times relative to the
    *    start of the slice, rather than the start of the run.
    * \return Whether encoding was successful.
    */
   bool encode(uint64_t* buf, uint32_t& num_bytes_written, double slice_start_time_secs);

   /** Populate this object by decoding binary data.
    *
    * \param[in] buf Buffer to read data from.
    * \param[out] num_bytes_read Number of bytes read from `buf`.
    * \param[in] slice_start_time_secs Start time of the slice. We save
    *    bits in the binary representation by storing times relative to the
    *    start of the slice, rather than the start of the run.
    * \return Whether decoding was successful.
    */
   bool decode(uint64_t* buf, uint32_t& num_bytes_read, double slice_start_time_secs);

   virtual uint32_t get_num_bytes_needed_to_encode() override;
};

/** Whether a `HitBase` is an `EndOfSlice`. */
inline bool is_eos(std::shared_ptr<HitBase> hit) {
   return std::dynamic_pointer_cast<EndOfSlice>(hit).get() != 0;
}

/** Whether a `HitBase` is a `QT`. */
inline bool is_qt(std::shared_ptr<HitBase> hit) {
   return std::dynamic_pointer_cast<QT>(hit).get() != 0;
}

/** Whether a `HitBase` is a `MultiHit`. */
inline bool is_multihit(std::shared_ptr<HitBase> hit) {
   return std::dynamic_pointer_cast<MultiHit>(hit).get() != 0;
}

/** Whether a `HitBase` is a `Waveform`. */
inline bool is_waveform(std::shared_ptr<HitBase> hit) {
   return std::dynamic_pointer_cast<Waveform>(hit).get() != 0;
}

/** Simple struct for storing a channel/board ID.
 *
 * Includes a `<` operator with strict weak ordering, so `ChannelId`
 * can be used as a key in maps etc.
 */
struct ChannelId {
   uint16_t frontend_id;
   uint8_t board_id;
   uint8_t channel_id;

   bool operator < (const ChannelId &other) const {
      // Be careful if you change this logic - it must still implement
      // strict weak ordering, or you will break lots of code that
      // uses a ChannelId as a key in a map!
      if (this->frontend_id < other.frontend_id) {
         return true;
      } else if (this->frontend_id == other.frontend_id) {
         if (this->board_id < other.board_id) {
            return true;
         } else if (this->board_id == other.board_id) {
            if (this->channel_id < other.channel_id) {
               return true;
            }
         }
      }

      return false;
   }
};

/** Trivial wrapper for associating a `HitBase` and channel ID info. */
struct HitWithChannelId {
   ChannelId chan;
   std::shared_ptr<HitBase> hit;
};

/** Trivial wrapper for associating a filtered `HitBase` with the unfiltered
 * `Waveform` it came from, plus channel ID info. */
struct FilteredAndUnfiltered {
   ChannelId chan;
   std::shared_ptr<HitBase> filtered;
   std::shared_ptr<HitBase> unfiltered;

   uint32_t get_total_size_bytes() {
      return filtered->get_num_bytes_needed_to_encode() + unfiltered->get_num_bytes_needed_to_encode();
   }
};

/** Toy example of how we might store event summary info in the
 * slice data structure. Encoding/decoding not implemented yet, as
 * use case not well-defined.
 */
struct EventSummary {
   double time_since_run_start_secs;
   double energy_keV;
   int16_t pos_x_mm;
   int16_t pos_y_mm;
   int16_t pos_z_mm;
};

/** Main object for storing all data from a time slice.
 *
 * Each FEP generates a `Slice` for the data from its boards.
 * The TSP then receives a `Slice` from all FEPs, and builds a final
 * `Slice` combining data from all of them.
 *
 * Methods for encoding/decoding to/from binary representation are included.
 */
class Slice {
public:
   Slice();

   /** Maximum data format version we know how to encode/decode.
    * Update this whenever new features are added that affect how data
    * is encoded / decoded.
    */
   static const uint16_t kMaxDataFormatVersion = 0x0;

   // Global flags can go from 0x0 to 0xF
   static const uint8_t kGlobalFlagLastWord = 0xF;
   static const uint8_t kSectionFlagChannelData = 0xB;
   static const uint8_t kSectionFlagEventSummaries = 0xE;

   // Sub-section flags within the data section can go from 0x00 to 0xF
   static const uint8_t kBoardDataFlagNextChannel = 0xC;

   // Hit flags within a channel can go from 0x0 to 0x7
   static const uint8_t kHitFlagWaveform = 0x0;
   static const uint8_t kHitFlagQT = 0x1;
   static const uint8_t kHitFlagMultiHit = 0x2;

   // These should be bits, 0x1, 0x2, 0x4, 0x8, 0x10 etc...
   typedef enum {
     Downsample = 0x1,
     Exp2GausMatched = 0x2,
     HitFinder = 0x3
   } FilterAlgo;

   /** Restore this Slice to an empty state. */
   void reset();

   /** Encode this object as binary data.
    *
    * \param[in] buf Buffer to write data to.
    * \param[out] num_bytes_written Number of bytes written to `buf`
    * \return Whether encoding was successful. If encoding failed, the
    *    reason will be contained in the `error_message` member.
    */
   bool encode(uint64_t* buf, uint32_t& num_bytes_written);

   /** Decode binary data into this Slice object.
    *
    * \param[in] buf Buffer to read data from.
    * \param[out] num_bytes_read Number of bytes read from `buf`
    * \return Whether decoding was successful. If decoding failed, the
    *    reason will be contained in the `error_message` member.
    */
   bool decode(uint64_t* buf, uint32_t& num_bytes_read);

   /** Get the number of bytes needed to encode this object. */
   int get_num_bytes_needed_to_encode();

   /** Print content to screen.
    * \param[in] with_hits If true, print the times of each hit; if false just
    *    print the number of hits per channel.
    */
   void dump(bool with_hits=false);

   /** Get all the channels that have QT and/or Waveform data. */
   std::set<ChannelId> get_channels_with_hit_data();

   /** Get all the hits in this slice, sorted by time. */
   std::vector<std::shared_ptr<HitWithChannelId>> get_hits_sorted_by_time();

   /** Add a hit to this slice. */
   void add_hit(std::shared_ptr<HitWithChannelId> hit);

   /** Merge all the data from `other` with the data in this Slice.
    * Requires that the `slice_idx` of the two objects are the same!
    *
    * * All hits (QTs and Waveforms) from the other object get added to
    *    this one.
    * * Event summaries from the other object get added to this one.
    * * The start time of the slice is chosen to be the earlier of the
    *    two options.
    * * The data format version is chosen to be the higher of the two
    *    options.
    *
    * \return Whether merging was successful. Will be false if the two
    *    slices have different slice indexes.
    */
   bool merge(Slice& other);

   /** Whether this slice already contains this hit.
    * We check the address of the shared pointer of the hit.
    */
   bool contains(ChannelId& chan, std::shared_ptr<HitBase> hit);

   /** Reason why encoding/decoding failed. */
   char error_message[1024];

   /** Data format used to encode this data. */
   uint16_t data_format_version;

   /** Slice number (starts at 0 at start of run). */
   uint32_t slice_idx;

   /** Start time of this slice (since start of run). */
   double slice_start_time_secs;

   /** QT objects in this slice. */
   std::map<ChannelId, std::vector<std::shared_ptr<QT> > > channel_qts;

   /** MultiHit objects in this slice. */
   std::map<ChannelId, std::vector<std::shared_ptr<MultiHit> > > channel_multihits;

   /** Waveform objects in this slice. */
   std::map<ChannelId, std::vector<std::shared_ptr<Waveform> > > channel_waveforms;

   /** Summary info for this slice (encoding/decoding not implemented yet). */
   std::vector<EventSummary> event_summaries;
};

/** Tiny header used when sending data from FEP to TSP.
 *
 * Tells the TSP which FEP is sending the data, and which slice it is
 * sending. Particularly helpful as it allows the TSP to print more useful error
 * info if decoding the `Slice` object fails.
 */
class FEPSliceHeader {
public:
   uint32_t slice_idx;
   uint32_t fe_idx;

   void encode(uint64_t *buf);
   void decode(uint64_t *buf);
   int get_num_bytes_needed_to_encode();
};

#endif

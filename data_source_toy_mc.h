#ifndef DATA_SOURCE_TOYMC_H
#define DATA_SOURCE_TOYMC_H

#include "data_source_base.h"
#include "sys/time.h"
#include "msystem.h"
#include "vslice_utils.h"

#include <fstream>

/** \file
 * Data source for the fake data from the Toy MC code.
 */

/** Data source recieves data from the ToyMC code over ethernet.
 *
 * The properties of the incomming waveforms are set using the ToyMC code 
 * they are determined using the properties set using the 
 *
 */
class DataSourceToyMC : public DataSourceBase {
public:
   DataSourceToyMC(int portNumber, int frondEndIndex, int boardID, int statusPort=-1);
   ~DataSourceToyMC();
   INT populate_next_event(std::vector<std::shared_ptr<VXData> >& data, BOOL debug) override;
   INT begin_of_run(double _slice_width_ms) override;
   INT end_of_run() override;

private:

   HNDLE hDB; //!< ODB access
   HNDLE settings_key; //!< ODB settings handle
   //Input socket into the FEP
   int insocket;
   int statsocket;
   uint64_t * inBuff;
   int max_packet_bytes;
   uint64_t recv_bytes;

   //Output filestream for logging
   std::ofstream logStream;


};

#endif

#include "vslice_utils.h"
#include "midas.h"
#include "msystem.h"
#include <algorithm>
#include <chrono>
#include <ctime>
#include <sstream>
#include <stdarg.h>
#include <stdio.h>
#include <sys/time.h>
#include <thread>

using namespace nlohmann;// nlohmann::json -> json

/** \file
 * Helper functions used throughout the codebase.
 */

// Helper function to do a printf, but automatically prepend a timestamp.
void vslice::ts_printf(const char *format, ...) {
   // Handle va args for message
   va_list argptr;
   char message[10000];
   va_start(argptr, format);
   vsnprintf(message, 10000, (char *) format, argptr);
   va_end(argptr);

   // Handle timestamp
   std::chrono::system_clock::time_point now = std::chrono::system_clock::now();

   // Get number of milliseconds for the current second
   int milli = (std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) % 1000).count();

   // Convert main timestamp to broken time for text formatting
   time_t timer = std::chrono::system_clock::to_time_t(now);
   tm buf;
   char formatted_time[80];
   strftime(formatted_time, 80, "%Y-%m-%d %H:%M:%S", localtime_r(&timer, &buf));

   // Do the actual print
   printf("%s.%03d %s", formatted_time, milli, message);
}

INT vslice::empty_ring_buffer(int rb_handle, int max_event_size_bytes) {
   int prev_buf_level = -1;

   // Implementation of rb_increment_wp only allows incrementing by max_event_size_bytes
   // at a time. Iterate as needed.
   while (true) {
      int buf_level;
      INT status = rb_get_buffer_level(rb_handle, &buf_level);

      if (status != SUCCESS) {
         cm_msg(MERROR, __FUNCTION__, "Failed to read buffer level for rb %d: %d", rb_handle, status);
         return status;
      }

      if (buf_level == 0) {
         break;
      }

      if (buf_level == prev_buf_level) {
         cm_msg(MERROR, __FUNCTION__, "Failed to free up space in rb %d: level is stuck at %d bytes", rb_handle, buf_level);
         return FE_ERR_DRIVER;
      }

      int this_inc = std::min(buf_level, max_event_size_bytes);
      rb_increment_rp(rb_handle, this_inc);
      prev_buf_level = buf_level;
   }

   return SUCCESS;
}

std::vector<int> vslice::get_running_frontend_indexes(HNDLE hDB, std::string prefix) {
   INT size;
   KEY key;
   HNDLE hClientsKey, hSubkey;
   std::vector<int> retval;

   // Get Eq key
   if (db_find_key(hDB, 0, "/System/Clients", &hClientsKey) != DB_SUCCESS) {
      ts_printf("Failed to find /System/Clients!");
      return retval;
   }

   for (int i = 0;; i++) {
      const char fename_char[NAME_LENGTH] = {""};
      db_enum_key(hDB, hClientsKey, i, &hSubkey);

      if (!hSubkey) {
         break;
      }

      db_get_key(hDB, hSubkey, &key);

      // Go in to client directory
      if (key.type == TID_KEY) {
         std::string name;
         INT status = db_get_value_string(hDB, hSubkey, "Name", 0, &name);

         if (status != SUCCESS) {
            continue;
         }

         // Check if the client name matches one we're looking for
         if (name.find(prefix) == 0) {
            int index = -1;
            std::string fmt = prefix + "_%d";
            sscanf(name.c_str(), fmt.c_str(), &index);

            if (index >= 0) {
               retval.push_back(index);
            }
         }
      }
   }

   std::sort(retval.begin(), retval.end());
   return retval;
}

std::string vslice::format_list_of_ints(std::vector<int> vec) {
   std::stringstream retval;

   for (auto it = vec.begin(); it != vec.end(); it++) {
      if (it != vec.begin()) {
         retval << ", ";
      }

      retval << *it;
   }

   return retval.str();
}

std::string vslice::format_bytes(uint64_t num_bytes) {
   double size = num_bytes;

   int idx = 0;
   std::vector<std::string> prefixes = {"", "ki", "Mi", "Gi", "Ti", "Pi"};

   while (size >= 1024) {
      idx++;
      size /= 1024;
   }

   char res[100];

   if (num_bytes < 1024) {
      // Integer exact bytes
      snprintf(res, 100, "%uB", (uint32_t)num_bytes);
   } else {
      // 2 d.p. with prefix
      snprintf(res, 100, "%.2f%sB", size, prefixes[idx].c_str());
   }
   return res;
}

json odbxx_strip_keys(json j_in) {
   json j_out;

   for (auto &el : j_in.items()) {
      if (el.key().find("/key") == std::string::npos) {
         if (el.value().is_object()) {
            // Recurse down a level
            j_out[el.key()] = odbxx_strip_keys(el.value());
         } else {
            j_out[el.key()] = el.value();
         }
      }
   }

   return j_out;
}

json vslice::odbxx_to_json(midas::odb odbxx, bool keep_key_metadata) {
   json j = json::parse(odbxx.dump());

   if (keep_key_metadata) {
      // Trivial. Just return everything we got when dumping the ODB object.
      return j;
   } else {
      // More complex. Strip all the entries that include "/key" in their name.
      return odbxx_strip_keys(j);
   }
}

vslice::cpu_info vslice::init_cpu_info() {
   cpu_info info = {};
   struct tms timeSample;

   info.cpu_tot = times(&timeSample);
   info.cpu_sys = timeSample.tms_stime;
   info.cpu_user = timeSample.tms_utime;
   info.num_processors = std::thread::hardware_concurrency();
   info.this_proc_pct = 0;

   if (info.num_processors <= 0) {
      info.num_processors = 1;
   }

   return info;
}

void vslice::update_cpu_info(vslice::cpu_info &prev_info) {
   cpu_info new_info = {};
   struct tms timeSample;

   new_info.cpu_tot = times(&timeSample);
   new_info.cpu_sys = timeSample.tms_stime;
   new_info.cpu_user = timeSample.tms_utime;
   new_info.num_processors = prev_info.num_processors;

   if (new_info.cpu_tot == prev_info.cpu_tot) {
      // No update. Keep the old values so we get a real update next time.
      new_info = prev_info;
   } else if (new_info.cpu_tot < prev_info.cpu_tot || new_info.cpu_sys < prev_info.cpu_sys || new_info.cpu_user < prev_info.cpu_user) {
      // Overflow. Just use the previous percentage this time.
      new_info.this_proc_pct = prev_info.this_proc_pct;
   } else {
      // Normal case. Calculate the new usage.
      new_info.this_proc_pct = (new_info.cpu_sys - prev_info.cpu_sys) + (new_info.cpu_user - prev_info.cpu_user);
      new_info.this_proc_pct /= (new_info.cpu_tot - prev_info.cpu_tot);
      new_info.this_proc_pct /= new_info.num_processors;
      new_info.this_proc_pct *= 100;
   }

   prev_info = new_info;
}


//From stack overflow - run a command and get its output (Used for MD5Sum)
std::string vslice::exec(const char* cmd) {
	std::array<char, 128> buffer;
	std::string result;
	std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
	if (!pipe) {
		throw std::runtime_error("popen() failed!");
	}
	while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
		result += buffer.data();
	}
	return result;
}

uint64_t vslice::current_time_ms() {
   struct timeval tp;
   gettimeofday(&tp, NULL);
   return ((uint64_t)tp.tv_sec * 1000) + ((uint64_t)tp.tv_usec / 1000);
}
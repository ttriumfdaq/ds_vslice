# ODB settings

## PM ODB settings

In `/Equipment/PoolManager/Settings`:

* `TSP_IN_port` - port to bind to with ZMQ, for TSPs to send us their status
* `TSP_OUT_port` - port to bind to with ZMQ, for us to send info to TSPs (e.g. run transitions)
* `FEP_port` - port to bind to with ZMQ, for us to send info to FEPs (e.g. which TSP to send data to for each slice)
* `TSP_tapN` - hotlink that tells TSPs to send the next N slices to the TSP Tap, so it can forward them to midas buffers.
* `TSP` - settings that are forwarded to the TSPs (as they may be running in standalone mode with no access to midas ODB). See TSP usage section for more.


## FEP ODB settings

In `/FEP shared` (common settings for all FEP instances):

* `Slice width (ms)` - width of each time slice
* `Slice overlap (ms)` - how much of each slice to send to 2 TSPs instead of 1
* `PoolManager endpoint` - how to talk to the Pool Manager (e.g. `tcp://localhost:4713`)
* `Max TSP data send retries` - how many times we should try to send data to a TSP before we give up and accept some data loss. In each attempt we wait 1s for the TSP to be ready for our data, so this shouldn't be set too high (as we're still accumulating data in other threads while trying to send data to a TSP).

In `/Equipment/FEP_001/Settings/Global` (for frontend index 1) are settings that are always relevant:

* `Use demo data (checked at init)` - whether to actually read data or generate simulated data
* `Write data to midas banks` - whether to write filtered data to midas banks (named like `F001` etc) or just send to TSPs over ZMQ
* `Write unfiltered to midas banks` - whether to write unfiltered data to midas banks (named like `U001` etc)
* `Enable downsample filter` - whether to enable a simple downsampling filter
* `Debug` - whether to print debug information to screen

In `/Equipment/FEP_001/Settings/DemoData` (for frontend index 1) are settings for when we're simulating data rather than reading out real VX2740s:
* `Num boards (checked at init)` - number of VX2740 boards to simulate reading out

In `/Equipment/FEP_001/Settings/DemoData/Board 0` (for board 0 of frontend index 1) are setting for when we're simulating data rather than reading out real VX2740s:

* `Event rate (Hz)` - event rate to simulate
* `Data slice width (s)` - how often to inject special "end-of-slice" events
* `WF size (64bit words)` - how many 64-bit words to generate for each channel. Max 2048.
* `Channel mask (31-0)` - which channels to enable
* `Channel mask (63-32)` - which channels to enable

In `/Equipment/FEP_001/Settings/RealData` (for frontend index 1) are settings for when we're reading out real VX2740s:
* `Is scope mode (read at init)` - whether the boards are running Scope or User firmware
* `SW end-of-slices (read at init)` - whether to inject end-of-slice events in the FEP (if they're not being created by the VX2740 itself)


## TSP ODB settings

In `/Equipment/PoolManager/Settings/TSP` (as the PM forwards the settings to TSPs at begin-of-run):

* `Debug` - whether to print debug info to screen
* `Write data` - whether to write data to disk. Files are currently named like `run012345_tsp001_subrun0001.mid`
* `Max file size (MiB)` - if writing data to disk, max size of each subrun
* `Heartbeat (s)` - how often to ping the PM to tell it we're still alive
* `Basic time filter/Enable` - whether to enable a simple filter that just keeps the first fraction of each slice
* `Basic time filter/Keep first ms of slice` - how many ms of each slice to keep

#include <cstring>
#include <unistd.h>

#include "zmqpp/zmqpp.hpp"
#include "midas.h"
#include "tmfe_rev0.h"
#include "odbxx.h"

#include "tsp_tap_class.h"

#ifndef HOST_NAME_MAX
#define HOST_NAME_MAX 64
#endif

void vslice::tapcallback(INT hDB, INT hkey, INT index, void *feptr) {
   TspTap *fe = (TspTap *) feptr;
   fe->fecallback(hDB, hkey, index);
}

TspTap::TspTap(TMFE* mfe, TMFeEquipment* eq): socket(zmq_context, zmqpp::socket_type::pull) // ctor
{
   fMfe = mfe;
   fEq  = eq;
   fEventSize = 0;
   fEventBuf  = NULL;
}

TspTap::~TspTap() // dtor
{
   if (fEventBuf) {
      free(fEventBuf);
      fEventBuf = NULL;
   }
}

void TspTap::Init()
{
   fEventSize = 500*1024*1024;
   fEq->fOdbEqSettings->RI("max_event_size", &fEventSize, true);
   if (fEventBuf) {
      free(fEventBuf);
   }
   fEventBuf = (char*)malloc(fEventSize);

   fEq->fOdbEqSettings->RS("hostname", &hostname, true);
   if(!hostname.size()){
      char hn[HOST_NAME_MAX];
      gethostname(hn, HOST_NAME_MAX);

      hostname = hn;
      size_t pos = hostname.find(".localdomain");
      if(pos != std::string::npos) hostname.erase(pos);
   }

   fEq->fOdbEqSettings->RI("port", &port, true);
   std::ostringstream oss;
   oss << "tcp://*:" << port;
   endpoint = oss.str();
   socket.bind(endpoint);
   vslice::ts_printf("Bound to %s for TSP tapping\n", endpoint.c_str());


   std::string data_endpoint;
   std::ostringstream oss2;
   oss2 << "tcp://" << hostname << ':' << port;
   data_endpoint = oss2.str();

   char tmpbuf[256];
   sprintf(tmpbuf, "/Equipment/%s/Readback", fMfe->fFrontendName.c_str());
   readback.connect(tmpbuf);
   readback["endpoint"] = data_endpoint;

   sprintf(tmpbuf, "/Equipment/PoolManager/Settings/TSP_tapN");
   HNDLE hkey;
   db_find_key(fMfe->fDB, 0, tmpbuf, &hkey);
   if(hkey){
      db_watch(fMfe->fDB, hkey, vslice::tapcallback, (void *) this);
   }
}

void TspTap::SendEvent(const char *bank, const char *buf, int eventsize)
{
   if(eventsize > fEventSize){
      fMfe->Msg(MERROR, "SendEvent", "Event size too large: %d > %d", eventsize, fEventSize);
      return;
   }
   fEq->ComposeEvent(fEventBuf, fEventSize);
   fEq->BkInit(fEventBuf, fEventSize);

   char* ptr = (char*)fEq->BkOpen(fEventBuf, bank, TID_QWORD);
   memcpy(ptr, buf, eventsize);
   fEq->BkClose(fEventBuf, ptr+eventsize);

   fEq->SendEvent(fEventBuf);
}

std::string TspTap::HandleRpc(const char* cmd, const char* args)
{
   fMfe->Msg(MINFO, "HandleRpc", "RPC cmd [%s], args [%s]", cmd, args);
   return "OK";
}

void TspTap::HandleBeginRun()
{
   fMfe->Msg(MINFO, "HandleBeginRun", "Begin run!");
   fEq->SetStatus("Running", "var(--mgreen)");
}

void TspTap::HandleEndRun()
{
   fMfe->Msg(MINFO, "HandleEndRun", "End run!");
   fEq->SetStatus("Stopped", "var(--mgreen)");
}

//void HandleStartAbortRun()
//{
//   fMfe->Msg(MINFO, "HandleStartAbortRun", "Begin run aborted!");
//   fEq->SetStatus("Stopped", "#00FF00");
//}

void TspTap::HandlePeriodic()
{
   // printf("periodic!\n");
   //char buf[256];
   //sprintf(buf, "buffered %d (max %d), dropped %d, unknown %d, max flushed %d", gUdpPacketBufSize, fMaxBuffered, fCountDroppedPackets, fCountUnknownPackets, fMaxFlushed);
   if(ntap){
      char buf[256];
      if(ntapped < ntap)
         sprintf(buf, "Tapping TSP, received %d/%d", ntapped, ntap);
      else
         sprintf(buf, "Finished tapping TSP, received %d/%d", ntapped, ntap);
      if(ntapped > ntap)
         fMfe->Msg(MERROR, "HandlePeriodic", "Received more slices than expected: %d > %d", ntapped, ntap);
      fEq->SetStatus(buf, "var(--mgreen)");
   } else {
      fEq->SetStatus("Idle", "var(--mgreen)");
   }
   fEq->WriteStatistics();
}

void TspTap::PollTSP() {
   zmqpp::message message;
   if (socket.receive(message, true)){
      if(message.parts() != 2){
         fMfe->Msg(MERROR, "PollTSP", "Unexpected message format, got %d instead of 2 parts", (int)message.parts());
         return;
      }
      std::string bankname;
      message >> bankname;
      if(bankname.size() != 4){
         fMfe->Msg(MERROR, "PollTSP", "Bankname %s is not 4 characters", bankname.c_str());
         return;
      }
      SendEvent(bankname.c_str(), (const char*)message.raw_data(1), message.size(1));
      printf("Wrote slice %d of ntap\n", ntapped);
      ntapped++;
      fEq->WriteStatistics();
   }
}

void TspTap::fecallback(HNDLE hDB, HNDLE hkey, INT index) {
   KEY key;
   int status = db_get_key(fMfe->fDB, hkey, &key);
   if (status == DB_SUCCESS) {
      INT nt;
      INT size = sizeof(nt);
      db_get_data(hDB, hkey, &nt, &size, key.type);
      if(nt > ntap || (ntapped >= ntap && nt > 0)){
         ntap = nt;
         ntapped = 0;
         printf("tapN changed to %d\n", nt);
      }
   }
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

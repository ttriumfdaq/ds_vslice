var ds = ds || {};

/**
* Functions for the vslice_status.html page.
*/ 

ds.vslice_status = (function() {
  var fep_idx_strs = [];
  var tsp_idx_strs = [];
  var fep_idx_ints = [];
  var tsp_idx_ints = [];
  var plot_update_rate_ms;
  var plot_update_timeout;
  var built_fep_summary_table = false;
  var built_tsp_summary_table = false;
  var default_plot_bg_color = "rgba(0,0,0,0)";
  var default_plot_width = 550;
  var default_plot_height = 300;
  
  var TSP_OFFLINE = 0;
  var TSP_INIT = 10;
  var TSP_IDLE = 11;
  var TSP_TXSTARTED = 20;
  var TSP_TXCOMPLETE = 21;
  var TSP_RECVTIMEOUT = 22;
  var TSP_ANASTARTED = 30;
  var TSP_ANAFINISHED = 31;
  var TSP_ANAFAILED = 32;
  
  // Colors, light to dark.
  var blues = ["#f7fbff","#deebf7","#c6dbef","#9ecae1","#6baed6","#4292c6","#2171b5","#084594"];
  var greens = ["#f7fcf5","#e5f5e0","#c7e9c0","#a1d99b","#74c476","#41ab5d","#238b45","#005a32"];
  var greys = ["#ffffff","#f0f0f0","#d9d9d9","#bdbdbd","#969696","#737373","#525252","#252525"];
  var oranges = ["#fff5eb","#fee6ce","#fdd0a2","#fdae6b","#fd8d3c","#f16913","#d94801","#8c2d04"];
  var purples = ["#fcfbfd","#efedf5","#dadaeb","#bcbddc","#9e9ac8","#807dba","#6a51a3","#4a1486"];
  var reds = ["#fff5f0","#fee0d2","#fcbba1","#fc9272","#fb6a4a","#ef3b2c","#cb181d","#99000d"];
  var teals = ["#9fd3d4", "#85c6c6","#6eb8b7","#58abaa","#419e9d","#2f9090","#218282","#137374"];

  /**
  * Entry function that builds dynamic bits of webpage, then registers
  * with midas.
  */
  var init = function() {
    mjsonrpc_db_get_values(["/Equipment"]).then(function(rpc_result) {
      let equip = rpc_result.result.data[0];
      
      // Find all the FEP equipments
      for (let k in equip) {
        if (k.indexOf("fep_") == 0 && k.indexOf("/") == -1) {
          fep_idx_strs.push(k.replace("fep_", ""));
        }
      }
      
      fep_idx_strs = fep_idx_strs.sort();
      
      for (let i in fep_idx_strs) {
        fep_idx_ints.push(parseInt(fep_idx_strs[i]));
      }
      
      // Find how many TSPs PM is giving data for
      let num_tsps = equip["tsp_pool"]["variables"]["slice size kb"].length;
      
      for (let i = 0; i < num_tsps; i++) {
        let tsp_id = pad_zeros(i, 3);
        tsp_idx_strs.push(tsp_id);
        tsp_idx_ints.push(i);
      }

      // Read current refresh period. Plots will automatically be updated.
      refresh_period_changed();
      
      mhttpd_init('Vertical slice status');
    });
  };
  
  var update_plots = function() {
    let path_list = ["/System/Clients", "/Equipment/TSP_Pool/Variables"];
    
    for (let i in fep_idx_strs) {
      path_list.push("/Equipment/FEP_" + fep_idx_strs[i] + "/Variables");
      path_list.push("/Equipment/FEP_" + fep_idx_strs[i] + "/Common");
    }
    
    mjsonrpc_db_get_values(path_list).then(function(rpc_result) {
      // FEP ID -> Variables dict
      let fep_var_map = {};
      
      // FEP ID -> Common dict
      let fep_com_map = {};
      
      // TSP ID -> Variable key -> Variable value
      let tsp_var_map = {};
      
      let clients = rpc_result.result.data[0];
      let tsp_data = rpc_result.result.data[1];
      
      for (let i in tsp_idx_strs) {
        tsp_var_map[tsp_idx_strs[i]] = {};
      }
      
      for (let k in tsp_data) {
        if (k.indexOf("/") != -1) {
          continue;
        }
      
        for (let i in tsp_idx_strs) {
          tsp_var_map[tsp_idx_strs[i]][k] = tsp_data[k][i];
        }
      }
      
      let fep_offset = 2;
      
      for (let i in fep_idx_strs) {
        fep_var_map[fep_idx_strs[i]] = rpc_result.result.data[i * 2 + fep_offset];
        fep_com_map[fep_idx_strs[i]] = rpc_result.result.data[i * 2 + fep_offset + 1];
      }
      
      let plotly_config = {
        "displaylogo": false,
        "modeBarButtonsToRemove": ["select2d", "lasso2d", "zoomIn2d", "zoomOut2d", "sendDataToCloud", "hoverClosestCartesian", "hoverCompareCartesian", "toggleHover", "toggleSpikelines"]
      }

      build_fep_summary_table(fep_var_map, fep_com_map, clients);  
      build_tsp_summary_table(tsp_var_map);  
      Plotly.react("fep_backlog", get_data_fep_backlog(fep_var_map), get_layout_fep_backlog(fep_var_map), plotly_config);
      Plotly.react("fep_rates",   get_data_fep_rates(fep_var_map),   get_layout_fep_rates(fep_var_map),   plotly_config);
      Plotly.react("fep_buffers", get_data_fep_buffers(fep_var_map), get_layout_fep_buffers(fep_var_map), plotly_config);
      Plotly.react("fep_cpu",     get_data_fep_cpu(fep_var_map),     get_layout_fep_cpu(fep_var_map),     plotly_config);
      Plotly.react("tsp_rates",   get_data_tsp_rates(tsp_var_map),   get_layout_tsp_rates(tsp_var_map),   plotly_config);
      Plotly.react("tsp_ana_time",get_data_tsp_ana_time(tsp_var_map),get_layout_tsp_ana_time(tsp_var_map),plotly_config);
      Plotly.react("tsp_slices",  get_data_tsp_slices(tsp_var_map),  get_layout_tsp_slices(tsp_var_map),  plotly_config);
      Plotly.react("tsp_cpu",     get_data_tsp_cpu(tsp_var_map),     get_layout_tsp_cpu(tsp_var_map),     plotly_config);
  
      plot_update_timeout = setTimeout(update_plots, plot_update_rate_ms);
    });
  };
  
  var pad_zeros = function(number, str_len) {
    let fmt = "";
    for (let i = 0; i < str_len; i++) {
      fmt += "0";
    }
    return (fmt + number).slice(-1 * str_len);
  }
  
  var build_fep_summary_table = function(fep_var_map, fep_com_map, clients) {
    if (!built_fep_summary_table) {
      // Build table for the first time.
      built_fep_summary_table = true;
      let html = '';

      // Figure out how big the table needs to be (rows x columns).
      // We show up to 10 FEPs per row.
      let min_fep_id = 1000;
      let max_fep_id = -1;
      
      for (let i in fep_var_map) {
        min_fep_id = Math.min(min_fep_id, i);
        max_fep_id = Math.max(max_fep_id, i);
      }
      
      let row_range = [0, Math.ceil(max_fep_id/10)];
      let col_range = [0, 10];
      
      if (max_fep_id < 10) {
        // Fewer than 10 FEPs ever run. Just show the range that matters,
        // so we don't have a lot of empty cells.
        col_range[0] = min_fep_id;
        col_range[1] = max_fep_id + 1;
      }
      
      // Build the key row
      html += '<tr><td colspan="' + col_range[1] + '">';
      html += '<span style="background-color: var(--mgreen); padding: 0px 10px 0px 10px;">OK</span>';
      html += '<span style="background-color: var(--myellow); padding: 0px 10px 0px 10px;">Error</span>';
      html += '<span style="background-color: var(--mred); padding: 0px 10px 0px 10px;">Offline</span>';
      html += '</div>';
      html += '</td></tr>';
      
      // Now build the table.
      for (let r = row_range[0]; r < row_range[1]; r++) {
        html += '<tr>';
    
        for (let c = col_range[0]; c < col_range[1]; c++) {
          let fep_id = r * 10 + c;
          
          if (fep_id > max_fep_id) {
            break;
          }
          
          let fep_str = pad_zeros(fep_id, 3);
          html += '<td id="fep_summ_' + fep_str + '" style="text-align:center" title="FEP ' + fep_str + ': Never started">' + fep_str + '</td>';
        }
      
        html += "</tr>";
      }
    
      $("#fep_summary_title").attr("colspan", col_range[1]);
      $("#fep_summary").html(html);
    }

    // Every time we get called we update the color / title for each FEP
    let client_names = [];
    
    for (let k in clients) {
      client_names.push(clients[k]["name"]);
    }

    for (let i in fep_idx_strs) {
      let fep_str = fep_idx_strs[i];
      let fe_name = fep_com_map[fep_str]["frontend name"];
      
      if (client_names.indexOf(fe_name) == -1) {
        // Not running
        color = "redLight";
        title = "Stopped";
      } else {
        // Running
        color = fep_com_map[fep_str]["status color"];
        title = fep_com_map[fep_str]["status"];
      }
      
      // Convert greenLight to var(--mgreen) etc
      if (color.indexOf("Light") != -1) {
        color = "var(--m" + color.replace("Light", "") + ")";
      }
      
      $("#fep_summ_" + fep_str).css("background-color", color);
      $("#fep_summ_" + fep_str).attr("title", "FEP " + fep_str + ": " + title);
    }
  }

  var build_tsp_summary_table = function(tsp_var_map) {
    if (!built_tsp_summary_table) {
      // Build table for the first time.
      built_tsp_summary_table = true;
      let html = '';

      // Figure out how big the table needs to be (rows x columns).
      // We show up to 10 TSPs per row.
      let min_tsp_id = 0;
      let max_tsp_id = tsp_idx_ints.length - 1;
      
      let row_range = [0, Math.ceil(max_tsp_id/10)];
      let col_range = [0, 10];
      
      if (max_tsp_id < 10) {
        // Fewer than 10 TSPs ever run. Just show the range that matters,
        // so we don't have a lot of empty cells.
        col_range[0] = min_tsp_id;
        col_range[1] = max_tsp_id + 1;
      }
      
      // Build the key row
      html += '<tr><td colspan="' + col_range[1] + '">';
      html += '<span style="background-color: var(--mgreen); padding: 0px 10px 0px 10px;">Idle</span>';
      html += '<span style="background-color: var(--morange); padding: 0px 10px 0px 10px;">Receiving data</span>';
      html += '<span style="background-color: var(--mblue); padding: 0px 10px 0px 10px;">Processing data</span>';
      html += '<span style="background-color: var(--myellow); padding: 0px 10px 0px 10px;">Error</span>';
      html += '<span style="background-color: var(--mred); padding: 0px 10px 0px 10px;">Offline</span>';
      html += '</td></tr>';
      
      // Now build the table.
      for (let r = row_range[0]; r < row_range[1]; r++) {
        html += '<tr>';
    
        for (let c = col_range[0]; c < col_range[1]; c++) {
          let tsp_id = r * 10 + c;
          
          if (tsp_id > max_tsp_id) {
            break;
          }
          
          let tsp_str = pad_zeros(tsp_id, 3);
          html += '<td id="tsp_summ_' + tsp_str + '" style="text-align:center" title="TSP ' + tsp_str + ': Never started">' + tsp_str + '</td>';
        }
      
        html += "</tr>";
      }
    
      $("#tsp_summary_title").attr("colspan", col_range[1]);
      $("#tsp_summary").html(html);
    }

    for (let i in tsp_idx_strs) {
      let tsp_str = tsp_idx_strs[i];
      let tsp_int = tsp_idx_ints[i];
      let state = tsp_var_map[tsp_str]["state"];
      let recv = tsp_var_map[tsp_str]["receiving"];
      let ana = tsp_var_map[tsp_str]["analyzing"];
      let gradient = false;
      
      if (state == TSP_OFFLINE) {
        color = "var(--mred)";
        title = "Stopped";
      } else if (state == TSP_INIT || state == TSP_IDLE) {
        color = "var(--mgreen)";
        title = "Ready";
      } else if (state == TSP_RECVTIMEOUT) {
        color = "var(--myellow)";
        title = "Timed-out waiting for data";
      } else if (state == TSP_ANAFAILED) {
        color = "var(--myellow)";
        title = "Processing data failed";
      } else if (recv && ana) { 
        color = "var(--mblue)";
        gradient = ["var(--morange)", "var(--mblue)"]
        title = "Receiving and processing data";
      } else if (recv) {
        color = "var(--morange)";
        title = "Receiving data";
      } else if (ana) {
        color = "var(--mblue)";
        title = "Processing data";
      } else if (state == TSP_ANAFINISHED) {
        color = "var(--mgreen)";
        title = "Ready";
      } else {
        color = "var(--myellow)";
        title = "Unknown state " + state;
      }
      
      if (gradient) {
        $("#tsp_summ_" + tsp_str).css("background-image", "linear-gradient(to bottom right, " + gradient[0] + " 50%," + gradient[1] + " 50%)");
      } else {
        $("#tsp_summ_" + tsp_str).css("background-image", "");
      }

      if (color == "var(--myellow)") {
        console.log("TSP " + tsp_str + ": " + title);
      }

      $("#tsp_summ_" + tsp_str).css("background-color", color);
      $("#tsp_summ_" + tsp_str).attr("title", "TSP " + tsp_str + ": " + title);
    }
  }
  
  var get_data_fep_backlog = function(fep_var_map) {
    let data_list = [];
    let x = [];
    let backlog = [];
    let lost = [];
    let slice_per_sec = [];
    let text = [];
        
    for (let i in fep_idx_strs) {
      let fep = fep_idx_strs[i];
      x.push(fep);
      
      if (fep_var_map[fep] === null) {
        backlog.push(0);
        lost.push(0);
        slice_per_sec.push(0);
        text.push("FEP " + fep + ": Error reading FEP variables");
      } else {
        let b = fep_var_map[fep]["slice backlog"][0];
        let l = fep_var_map[fep]["num slices lost"][0];
        let r = fep_var_map[fep]["dispatch rate (slices per sec)"][0];
        backlog.push(b);
        lost.push(l);
        slice_per_sec.push(r);
        text.push("FEP " + fep + ":<br>Backlog: " + b + " slices<br>Lost: " + l + " slices<br>Sent: " + format_to_max_n_decimal_places(r, 3) + " slices/sec");
      }
    }
    
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Backlog",
      "line": {"color": purples[5]},
      "x": x,
      "y": backlog,
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Lost",
      "line": {"color": reds[5]},      
      "x": x,
      "y": lost,
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Sent (per sec)",
      "line": {"color": teals[5]},      
      "x": x,
      "y": slice_per_sec,
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    
    return data_list;
  };
  
  var get_layout_fep_backlog = function(fep_var_map) {
    return {"showlegend": true,
            "hovermode": "closest",
            "height": default_plot_height,
            "width": default_plot_width,
            "margin": {
              l: 50,
              r: 10,
              b: 100,
              t: 10,
              pad: 4
            },
            "plot_bgcolor": default_plot_bg_color,
            "paper_bgcolor": default_plot_bg_color,
            "yaxis": {
              "title": "Number of slices", 
              "rangemode": "tozero", 
              "tickformat": ",d"
            },
            "xaxis": {
              "title": "FEP", 
              "tickformat": ",d"
            },
          };
  };

  var get_data_fep_rates = function(fep_var_map) {
    let data_list = [];
    let x = [];
    let raw_latest = [];
    let raw_100 = [];
    let raw_run = [];
    let filt_latest = [];
    let filt_100 = [];
    let filt_run = [];
    
    let text = [];
    
    for (let i in fep_idx_strs) {
      let fep = fep_idx_strs[i];
      
      if (fep_var_map[fep] !== null) {
        for (let b in fep_var_map[fep]["last slice raw bytes"]) {
          x.push(x.length + 1);
          raw_latest.push(fep_var_map[fep]["last slice raw bytes"][b] / (1024 * 1024));
          filt_latest.push(fep_var_map[fep]["last slice filtered bytes"][b] / (1024 * 1024));
          raw_100.push(fep_var_map[fep]["100 slice avg raw bytes"][b] / (1024 * 1024));
          filt_100.push(fep_var_map[fep]["100 slice avg filtered bytes"][b] / (1024 * 1024));
          raw_run.push(fep_var_map[fep]["whole run avg raw bytes"][b] / (1024 * 1024));
          filt_run.push(fep_var_map[fep]["whole run avg filtered bytes"][b] / (1024 * 1024));
          
          let this_text = "FEP " + fep + ", board " + b + ":<br>";
          let idx = raw_latest.length - 1;
          this_text += "Latest slice (raw) = " + raw_latest[idx].toFixed(2) + "MiB<br>";
          this_text += "Latest slice (filt) = " + filt_latest[idx].toFixed(2) + "MiB<br>";
          this_text += "100 slice avg (raw) = " + raw_100[idx].toFixed(2) + "MiB<br>";
          this_text += "100 slice avg (filt) = " + filt_100[idx].toFixed(2) + "MiB<br>";
          this_text += "Whole run avg (raw) = " + raw_run[idx].toFixed(2) + "MiB<br>";
          this_text += "Whole run avg (filt) = " + filt_run[idx].toFixed(2) + "MiB";
          text.push(this_text);
        }
      }
    }
    
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Latest slice (raw)",
      "x": x,
      "y": raw_latest,
      "line": {"color": blues[6]},
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "100 slice avg (raw)",
      "x": x,
      "y": raw_100,
      "line": {"color": blues[4]},
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Whole run avg (raw)",
      "x": x,
      "y": raw_run,
      "line": {"color": blues[3]},
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Latest slice (filt)",
      "x": x,
      "y": filt_latest,
      "line": {"color": oranges[6]},
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "100 slice avg (filt)",
      "x": x,
      "y": filt_100,
      "line": {"color": oranges[4]},
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Whole run avg (filt)",
      "x": x,
      "y": filt_run,
      "line": {"color": oranges[3]},
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    
    return data_list;
  };
  
  var get_layout_fep_rates = function(fep_var_map) {
    let ticklabels = [];
    let tickvals = [];
    
    for (let i in fep_idx_strs) {
      let fep = fep_idx_strs[i];
      
      if (fep_var_map[fep] !== null) {
        for (let b in fep_var_map[fep]["last slice raw bytes"]) {
          tickvals.push(tickvals.length + 1);
          ticklabels.push(fep + "/" + b);
        }
      }
    }


    return {"showlegend": true,
            "hovermode": "closest",
            "height": default_plot_height,
            "width": default_plot_width,
            "margin": {
              l: 50,
              r: 10,
              b: 100,
              t: 10,
              pad: 4
            },
            "plot_bgcolor": default_plot_bg_color,
            "paper_bgcolor": default_plot_bg_color,
            "yaxis": {
              "title": "Data size per slice (MiB)"
            },
            "xaxis": {
              "title": "FEP/Board", 
              "tickmode": "array",
              "ticktext": ticklabels,
              "tickvals": tickvals,
            },
          };
  };
  
  var get_data_fep_buffers = function(fep_var_map) {
    let data_list = [];
    let x = [];
    let raw = [];
    let filt = [];
    let text = [];
    
    for (let i in fep_idx_strs) {
      let fep = fep_idx_strs[i];
      
      if (fep_var_map[fep] !== null) {
        for (let b in fep_var_map[fep]["raw buffer fill pct"]) {
          x.push(x.length + 1);
          raw.push(fep_var_map[fep]["raw buffer fill pct"][b]);
          filt.push(fep_var_map[fep]["filtered buffer fill pct"][b]);
          
          let this_text = "FEP " + fep + ", board " + b + ":<br>";
          let idx = raw.length - 1;
          this_text += "Raw buffer = " + (raw[idx] === undefined ? "?" : raw[idx].toFixed(2)) + "%<br>";
          this_text += "Filtered buffer = " + (filt[idx] === undefined ? "?" : filt[idx].toFixed(2)) + "%";
          text.push(this_text);
        }
      }
    }
    
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Raw buffer",
      "x": x,
      "y": raw,
      "line": {"color": blues[4]},
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Filtered buffer",
      "x": x,
      "y": filt,
      "line": {"color": oranges[4]},
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    
    return data_list;
  };
  
  var get_layout_fep_buffers = function(fep_var_map) {
    let ticklabels = [];
    let tickvals = [];
    
    for (let i in fep_idx_strs) {
      let fep = fep_idx_strs[i];
      
      if (fep_var_map[fep] !== null) {
        for (let b in fep_var_map[fep]["raw buffer fill pct"]) {
          tickvals.push(tickvals.length + 1);
          ticklabels.push(fep + "/" + b);
        }
      }
    }

    return {"showlegend": true,
            "hovermode": "closest",
            "height": default_plot_height,
            "width": default_plot_width,
            "margin": {
              l: 50,
              r: 10,
              b: 100,
              t: 10,
              pad: 4
            },
            "plot_bgcolor": default_plot_bg_color,
            "paper_bgcolor": default_plot_bg_color,
            "yaxis": {
              "title": "Buffer fullness (%)",
              "range": [0, 100]
            },
            "xaxis": {
              "title": "FEP/Board", 
              "tickmode": "array",
              "ticktext": ticklabels,
              "tickvals": tickvals,
            },
          };
  };

  var get_data_fep_cpu = function(fep_var_map) {
    let data_list = [];
    let x = [];
    let cpu = [];
    let text = [];
        
    for (let i in fep_idx_strs) {
      let fep = fep_idx_strs[i];
      x.push(fep);
      
      if (fep_var_map[fep] === null) {
        cpu.push(0);
        text.push("FEP " + fep + ": Error reading FEP variables");
      } else {
        let pct = fep_var_map[fep]["fe cpu usage pct"][0];
        cpu.push(pct);
        text.push("FEP " + fep + ": " + pct.toFixed(2) + "%");
      }
    }
    
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Last 1s",
      "line": {"color": reds[7]},
      "x": x,
      "y": cpu,
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    
    return data_list;
  };
  
  var get_layout_fep_cpu = function(fep_var_map) {
    return {"showlegend": true,
            "hovermode": "closest",
            "height": default_plot_height,
            "width": default_plot_width,
            "margin": {
              l: 50,
              r: 10,
              b: 100,
              t: 10,
              pad: 4
            },
            "plot_bgcolor": default_plot_bg_color,
            "paper_bgcolor": default_plot_bg_color,
            "yaxis": {
              "title": "CPU usage (%)", 
              "rangemode": "tozero", 
              "tickformat": ",d"
            },
            "xaxis": {
              "title": "FEP", 
              "tickformat": ",d"
            },
          };
  };
  
  var get_data_tsp_rates = function(tsp_var_map) {
    let data_list = [];
    let x = [];
    let input = [];
    let output = [];
    
    let text = [];
    
    for (let i in tsp_idx_strs) {
      let tsp = tsp_idx_strs[i];
      
      if (tsp_var_map[tsp] !== null) {
        x.push(i);
        input.push(tsp_var_map[tsp]["slice size kb"] / (1024));
        output.push(tsp_var_map[tsp]["processed bytes"] / (1024 * 1024));
        
        let this_text = "TSP " + tsp + ":<br>";
        let idx = input.length - 1;
        this_text += "Latest slice (input) = " + input[idx].toFixed(2) + "MiB<br>";
        this_text += "Latest slice (output) = " + output[idx].toFixed(2) + "MiB";
        text.push(this_text);
      }
    }
    
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Input size",
      "x": x,
      "y": input,
      "line": {"color": oranges[4]},
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Output size",
      "x": x,
      "y": output,
      "line": {"color": greens[4]},
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    
    return data_list;
  };
  
  var get_layout_tsp_rates = function(tsp_var_map) {
    return {"showlegend": true,
            "hovermode": "closest",
            "height": default_plot_height,
            "width": default_plot_width,
            "margin": {
              l: 50,
              r: 10,
              b: 100,
              t: 10,
              pad: 4
            },
            "plot_bgcolor": default_plot_bg_color,
            "paper_bgcolor": default_plot_bg_color,
            "yaxis": {
              "title": "Data size per slice (MiB)"
            },
            "xaxis": {
              "title": "TSP",
              "tickformat": ",d"
            },
          };
  };
  
  var get_data_tsp_ana_time = function(tsp_var_map) {
    let data_list = [];
    let x = [];
    let recv = [];
    let ana = [];
    
    let text = [];
    
    for (let i in tsp_idx_strs) {
      let tsp = tsp_idx_strs[i];
      
      if (tsp_var_map[tsp] !== null) {
        x.push(i);
        recv.push(tsp_var_map[tsp]["transfer time"]);
        ana.push(tsp_var_map[tsp]["analysis time"]);
        
        let idx = recv.length - 1;
        let this_text = "TSP " + tsp + ":<br>";
        this_text += "Transfer time = " + recv[idx].toFixed(3) + "s<br>";
        this_text += "Analysis time = " + ana[idx].toFixed(3) + "s";
        text.push(this_text);
      }
    }
    
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Data transfer",
      "x": x,
      "y": recv,
      "line": {"color": teals[1]},
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Analysis",
      "x": x,
      "y": ana,
      "line": {"color": teals[5]},
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    
    return data_list;
  };
  
  var get_layout_tsp_ana_time = function(tsp_var_map) {
    return {"showlegend": true,
            "hovermode": "closest",
            "height": default_plot_height,
            "width": default_plot_width,
            "margin": {
              l: 50,
              r: 10,
              b: 100,
              t: 10,
              pad: 4
            },
            "plot_bgcolor": default_plot_bg_color,
            "paper_bgcolor": default_plot_bg_color,
            "yaxis": {
              "title": "Time for latest slice (s)"
            },
            "xaxis": {
              "title": "TSP",
              "tickformat": ",d"
            },
          };
  };
    
  var get_data_tsp_slices = function(tsp_var_map) {
    let data_list = [];
    let x = [];
    let proc = [];
    let lost = [];
    
    let text = [];
    
    for (let i in tsp_idx_strs) {
      let tsp = tsp_idx_strs[i];
      
      if (tsp_var_map[tsp] !== null) {
        x.push(i);
        proc.push(tsp_var_map[tsp]["slices processed"]);
        lost.push(tsp_var_map[tsp]["slices lost"]);
        
        let idx = proc.length - 1;
        let this_text = "TSP " + tsp + ":<br>";
        this_text += "Processed " + proc[idx] + " slices<br>";
        this_text += "Lost " + lost[idx] + " slices";
        text.push(this_text);
      }
    }
    
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Processed",
      "x": x,
      "y": proc,
      "line": {"color": greens[5]},
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Lost",
      "x": x,
      "y": lost,
      "line": {"color": reds[5]},
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    
    return data_list;
  };
  
  var get_layout_tsp_slices = function(tsp_var_map) {
    return {"showlegend": true,
            "hovermode": "closest",
            "height": default_plot_height,
            "width": default_plot_width,
            "margin": {
              l: 50,
              r: 10,
              b: 100,
              t: 10,
              pad: 4
            },
            "plot_bgcolor": default_plot_bg_color,
            "paper_bgcolor": default_plot_bg_color,
            "yaxis": {
              "title": "Number of slices",
              "rangemode": "tozero", 
              "tickformat": ",d"
            },
            "xaxis": {
              "title": "TSP",
              "tickformat": ",d"
            },
          };
  };
  
  var get_data_tsp_cpu = function(tsp_var_map) {
    let data_list = [];
    let x = [];
    let cpu = [];
    
    let text = [];
    
    for (let i in tsp_idx_strs) {
      let tsp = tsp_idx_strs[i];
      
      if (tsp_var_map[tsp] !== null) {
        x.push(i);
        cpu.push(tsp_var_map[tsp]["analysis cpu usage pct"]);
        
        let idx = cpu.length - 1;
        text.push("TSP " + tsp + ": " + cpu[idx].toFixed(2) + "%");
      }
    }
    
    data_list.push({
      "type": "scattergl",
      "mode": "lines+markers",
      "name": "Latest slice",
      "x": x,
      "y": cpu,
      "line": {"color": reds[7]},
      "text": text,
      "hovertemplate": "%{text}<extra></extra>"
    });
    
    return data_list;
  };
  
  var get_layout_tsp_cpu = function(tsp_var_map) {
    return {"showlegend": true,
            "hovermode": "closest",
            "height": default_plot_height,
            "width": default_plot_width,
            "margin": {
              l: 50,
              r: 10,
              b: 100,
              t: 10,
              pad: 4
            },
            "plot_bgcolor": default_plot_bg_color,
            "paper_bgcolor": default_plot_bg_color,
            "yaxis": {
              "title": "CPU usage (%)", 
              "rangemode": "tozero", 
              "tickformat": ",d"
            },
            "xaxis": {
              "title": "TSP", 
              "tickformat": ",d"
            },
          };
  };
  
  var refresh_period_changed = function() {
    // Cancel any pending update
    clearTimeout(plot_update_timeout);
    
    // Update period for next time
    plot_update_rate_ms = parseInt($("#refresh_secs").val()) * 1000;
    
    // Immediately update the plots
    update_plots();
  };
  
  var pause_updates = function() {
    $("#pause").hide();
    $("#resume").show();
    clearTimeout(plot_update_timeout);
  };
  
  var resume_updates = function() {
    $("#resume").hide();
    $("#pause").show();
    refresh_period_changed();
  };
  
  /**
   * Format a number to have at most N decimal places, but with
   * trailing zeros removed.
   *
   * E.g.
   *   (1.2000, 3) => 1.2
   *   (1.2345, 3) => 1.235
   *
   * @param {number} value
   * @param {int} max_n_dp
   * @return {string}
   */
  var format_to_max_n_decimal_places = function(value, max_n_dp=6) {
    let retval = "";

    if (typeof(value) == "number" && !isNaN(value)) {
      // Silly JS hack to round to N dp, then drop any trailing zeros.
      // Makes it nicer for users so they don't have to see floating point
      // issues, or a bunch of trailing 0 normally shown with toFixed().
      retval = +value.toFixed(max_n_dp);
    }

    return retval;
  }
  
  return {
    init: init,
    refresh_period_changed: refresh_period_changed,
    pause_updates: pause_updates,
    resume_updates: resume_updates
  };
})();
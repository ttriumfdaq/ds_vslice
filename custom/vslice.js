var ds = ds || {};

/**
* Functions for the vslice.html page.
*/ 

ds.vslice = (function() {
  /**
  * Entry function that builds dynamic bits of webpage, then registers
  * with midas.
  */
  var init = function() {
    mjsonrpc_db_get_values(["/Equipment", "/FEP shared"]).then(function(rpc_result) {
      let equip = rpc_result.result.data[0];
      let fep_shared = rpc_result.result.data[1];
      
      build_pm_table(equip);
      build_tsp_table(equip);
      build_fep_shared_table(fep_shared);
      build_fep_boards_table(equip);
      
      mhttpd_init('Vertical slice settings');
    });
  };
  
  /**
  * Build table showing PoolManager parameters. Recurses down /Equipment/PoolManager/Settings,
  * except for the TSP directory.
  * Populates HTML table with ID "tsp".
  * 
  * @protected
  * @param {Object} equip - Current state of /Equipment in ODB.
  */
  var build_pm_table = function(equip) {
    let settings;
    
    try {
      settings = equip["poolmanager"]["settings"];
    } catch(err) {}

    let html = '<thead><tr><th class="mtableheader" colspan="2">PM settings</th></tr></thead>';
    html += '<tbody>';
    
    if (settings == undefined) {
      html += '<tr><td colspan="2">No PM settings found</td></tr>';
    } else {
      html += recurse_settings(settings, "/Equipment/PoolManager/Settings", "", ["tsp"]);
    }
    
    html += '</tbody>';
    $("#pm").html(html);
  };
   
  /**
  * Build table showing TSP parameters. Recurses down /Equipment/PoolManager/Settings/TSP.
  * Populates HTML table with ID "tsp".
  * 
  * @protected
  * @param {Object} equip - Current state of /Equipment in ODB.
  */
  var build_tsp_table = function(equip) {
    let settings;
    
    try {
      settings = equip["poolmanager"]["settings"]["tsp"];
    } catch(err) {}

    let html = '<thead><tr><th class="mtableheader" colspan="2">TSP settings</th></tr></thead>';
    html += '<tbody>';
    
    if (settings == undefined) {
      html += '<tr><td colspan="2">No TSP settings found</td></tr>';
    } else {
      html += recurse_settings(settings, "/Equipment/PoolManager/Settings/TSP", "");
    }
    
    html += '</tbody>';
    $("#tsp").html(html);
  };
  
  /**
  * Build table showing shared FEP parameters. Recurses down /FEP shared.
  * Populates HTML table with ID "fep_shared".
  * 
  * @protected
  * @param {Object} fep_shared - Current state of "/FEP shared" in ODB.
  */
  var build_fep_shared_table = function(fep_shared) {
    let html = '<thead><tr><th class="mtableheader" colspan="2">Shared FEP settings</th></tr></thead>';
    html += '<tbody>';
    
    if (fep_shared == undefined) {
      html += '<tr><td colspan="2">No shared FEP settings found</td></tr>';
    } else {
      html += recurse_settings(fep_shared, "/FEP shared", "");
    }
    
    html += '</tbody>';
    $("#fep_shared").html(html);
  };
  
  /**
  * Build table showing per-baord FEP parameters. Recurses down /Equipment/FEP_xxx/Settings, showing each
  * FEP in a separate column of a single table. Assumes that the first FEP has all the keys we
  * want to show (doesn't merge key list from all FEPs).
  *
  * Populates HTML table with ID "fep_boards".
  * 
  * @protected
  * @param {Object} equip - Current state of /Equipment in ODB.
  */
  var build_fep_boards_table = function(equip) {
    let fep_settings = {};
    
    for (let k in equip) {
      if (k.indexOf("fep_") == 0 && k.indexOf("/") == -1) {
        let proper_case = equip[k + "/name"];
        fep_settings[proper_case] = equip[k]["settings"];
      }
    }

    let num_feps = Object.keys(fep_settings).length;
    let tot_cols = num_feps + 1;
    
    let html = '<thead><tr><th class="mtableheader" colspan="' + tot_cols + '">Per-frontend FEP settings</th></tr></thead>';
    html += '<tbody>';
    
    if (num_feps == 0) {
      html += '<tr><td>No FEP settings found</td></tr>';
    } else {
      // Header row
      html += '<tr><th>Parameter</th>';
      
      for (let k in fep_settings) {
        html += '<th>' + k + '</th>';
      }
      
      // Data rows
      let fep_formats = { "Channel mask (31-0)": "x",
                          "Channel mask (63-32)": "x"};
      let fep_ignore = ["Names", "Comment on names", "VX2740"];
      html += recurse_related_settings(fep_settings, "/Equipment", "Settings", "", fep_formats, fep_ignore);
    }
    
    html += '</tbody>';
    $("#fep_boards").html(html);
  }
  
  /**
  * Recurse down through an ODB section, building rows of an HTML table for each
  * setting found. First column contains param name, second the value.
  *
  * @protected
  * @param {Object} settings - ODB settings for current recursion position
  * @param {string} odb_path - Current ODB location
  * @param {string} desc_prefix - Prefix to write before key name in first column
  * @param {string[]} ignore - Any key names to ignore
  * @param {Object=} formats - Map from key name to format to use for that key. See modb_html() for more.
  * @return {string} - HTML
  */
  var recurse_settings = function(settings, odb_path, desc_prefix, ignore=[], formats={}) {
    let html = '';
    
    let subdirs = [];
    
    // All the current-level settings
    for (let k in settings) {
      if (k.indexOf("/") != -1) {
        continue;
      }
      
      if (ignore.indexOf(k) != -1) {
        continue;
      }
      
      if (typeof(settings[k]) === 'object' && !Array.isArray(settings[k])) {
        subdirs.push(k);
        continue;
      }
      
      let proper_case = settings[k + "/name"];
      html += '<tr>';
      html += '<td style="vertical-align:top">' + desc_prefix + proper_case + '</td>';
      html += '<td>' + modb_html(odb_path + '/' + proper_case, settings[k], formats[proper_case]) + '</td>';
      html += '</tr>';
    }
    
    // All the sub-directories
    for (let i in subdirs) {
      let k = subdirs[i];
      let proper_case = settings[k + "/name"];
      html += recurse_settings(settings[k], odb_path + "/" + proper_case, proper_case + " &gt; ");
    }
    
    return html;
  }

  /**
  * Recurse down through similar ODB sections, building rows of an HTML table for each
  * setting found. First column contains param name; other columns contain values for each
  * 'related' ODB directory.
  *
  * E.g. For "/example/pos_1/settings" and "/example/pos_2/settings", we would recurse down
  * through all the settings beneath "/example/pos_1/settings", and create columns for the
  * values of both "pos_1" and "pos_2".
  *
  * @protected
  * @param {Object} settings - Map from section name to ODB settings for that section at
  *   current recursion position (e.g. {"pos_1": data_1, "pos_2": data_2}).
  * @param {string} odb_prefix - ODB path before the section name (e.g. "/example").
  * @param {string} odb_suffix - ODB path after the section name for current position (e.g. "settings").
  * @param {string} desc_prefix - Prefix to write before key name in first column
  * @param {Object=} formats - Map from key name to format to use for that key. See modb_html() for more.
  * @param {string[]} ignore - Any ODB keys to ignore.
  * @return {string} - HTML
  */
  var recurse_related_settings = function(settings_dict, odb_prefix, odb_suffix, desc_prefix, formats={}, ignore=[]) {
    let html = '';
    
    // Assume first FEP has entries for all the settings we care about
    let first_fep = Object.keys(settings_dict)[0];
    let settings = settings_dict[first_fep];
    let subdirs = [];
    
    // All the current-level settings
    for (let k in settings) {
      if (k.indexOf("/") != -1) {
        continue;
      }
      
      let proper_case = settings[k + "/name"];
      
      if (ignore.indexOf(proper_case) != -1) {
        continue;
      }
      
      if (typeof(settings[k]) === 'object' && !Array.isArray(settings[k])) {
        subdirs.push(k);
        continue;
      } 
      
      html += '<tr>';
      html += '<td style="vertical-align:top">' + desc_prefix + proper_case + '</td>';
      
      for (let f in settings_dict) {
        if (settings_dict[f] && settings_dict[f].hasOwnProperty(k) && settings_dict[f][k] !== undefined) {
          html += '<td>' + modb_html(odb_prefix + '/' + f + '/' + odb_suffix + '/' + proper_case, settings[k], formats[proper_case]) + '</td>';
        } else {
          html += '<td>N/A</td>';
        }
      }
      
      html += '</tr>';
    }
    
    // All the sub-directories to recurse down in to
    for (let i in subdirs) {
      let k = subdirs[i];
      let proper_case = settings[k + "/name"];
      let related = {};
       
       for (let f in settings_dict) {
         try {
           related[f] = settings_dict[f][k];
         } catch(err) {
           related[f] = undefined;
         }
       }
      
       html += recurse_related_settings(related, odb_prefix, odb_suffix + "/" + proper_case, desc_prefix + proper_case + " &gt; ", formats, ignore);
    }
    
    return html;
  };
  
  /**
  * Create HTML for displaying a checkbox or field for editing an ODB value.
  *
  * @protected
  * @param {string} odb_path - ODB location
  * @param {anything} curr_val - Current ODB value
  * @param {string=} format - Format to use. See https://midas.triumf.ca/MidasWiki/index.php/Custom_Page#Formatting
  * @return {string} - HTML
  */
  var modb_html = function(odb_path, curr_val, format) {
    let html = '';
  
    if (Array.isArray(curr_val)) {
      for (let i = 0; i < curr_val.length; i++) {
        let idx = '[' + i + ']';
        html += idx + " ";
        html += modb_html(odb_path + idx, curr_val[i], format);
        if (i < curr_val.length - 1) {
          html += '<br>';
        }
      }
    } else if (typeof(curr_val) === 'boolean') {
      html += '<input type="checkbox" class="modbcheckbox" data-odb-path="' + odb_path + '" data-odb-editable="1" />';
    } else {
      let fmt_str = "";
      if (format !== undefined) {
        fmt_str = 'data-format="' + format + '"';
      }
      html += '<span class="modbvalue" data-odb-path="' + odb_path + '" data-odb-editable="1" ' + fmt_str + '></span>';
    }

    return html;    
  }

  return {
    init: init
  };
})();
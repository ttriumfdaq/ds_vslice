#include "midas.h"
#include "data_structures.h"
#include "odbxx.h"

/** \file
 * Filters used in the FEP to smooth waveforms or summarize into QT objects.
 */

/** Base class that all filters at the FEP level should inherit from.
 *
 * After adding a new filter class, you must add support to the `FrontEndProcessor` class:
 * - In `FrontEndProcessor::init()`, create any ODB keys needed by this filter.
 * - In `FrontEndProcessor::thread_data_filter()`, check whether the filter should be 
 *    enabled, and create an instance if so.
 * 
 * Ideally we want the actual code that creates the ODB keys to be close to the code that
 * reads from them (i.e. both are in `fep_filters.cxx`, rather than creating the keys
 * in `fep_class.cxx` and reading them in `fep_filters.cxx`). However, there is a bit
 * of awkwardness here, as `init()` is in the main thread of the `FrontEndProcessor`
 * (before any readout threads have been spawned) while `thread_data_filter()` is part 
 * of each readout thread. The cleanest implementation seems to be to use static functions 
 * when creating the ODB keys and checking whether the filter should be enabled.
 * 
 * So the implementation of each new filter would look like:
 * 
 *     class FEPFilterExample : public FEPFilterBase {
 *        public:
 *           FEPFilterExample(midas::odb settings) {};
 *           virtual ~FEPFilterExample() {}
 *           static void create_settings(midas::odb& settings) {};
 *           static bool is_enabled(midas::odb& settings) {};
 *           virtual INT apply(const ChannelId& chan, std::shared_ptr<Waveform> in, std::vector<std::shared_ptr<HitBase> >& out, bool debug) override {};
 *     }
 * 
 * `FrontEndProcessor::init()` would contain:
 * 
 *     FEPFilterExample::create_settings(fep_settings);
 * 
 * `FrontEndProcessor::thread_data_filter()` would contain:
 * 
 *     if (FEPFilterExample::is_enabled(fep_settings)) {
 *        filters.push_back(new FEPFilterExample(fep_settings));
 *     }
 */
class FEPFilterBase {
public:
   /** Constructor.
    *
    * \param[in] settings The full FEP settings directory from the ODB.
    */
   FEPFilterBase(midas::odb settings) {}
   virtual ~FEPFilterBase() {};

   /** The main function to implement in a derived class.
    *
    * FEP filters work on a single waveform from a single channel.
    *
    * When creating a new QT/Waveform, always use shared pointers so we
    * can manage memory correctly:
    * - `std::shared_ptr<Waveform> my_hit = std::make_shared<Waveform>();`
    * - `std::shared_ptr<QT>       my_hit = std::make_shared<QT>();`
    *
    * Then you can do `out.push_back(my_hit)` etc.
    *
    * \param[in] in The unfiltered waveform (or output from a previous filter).
    * \param[out] out The list of QTs/filtered waveforms found by this filter.
    * \param[in] debug Whether to print debug statements to screen or not.
    * \return Midas status code. If returning anything other than `SUCCESS`, set the
    *    reason why in the `error_message` member.
    */
   virtual INT apply(const ChannelId& chan, std::shared_ptr<Waveform> in, std::vector<std::shared_ptr<HitBase> >& out, bool debug) = 0;

   /** Return any error message that was set if `apply()` failed. */
   std::string get_error_message();

protected:
   std::string error_message; //!< Error message that can be set by derived classes to explain problems.
};

/** No-op FEP filter that doesn't do anything (just copies info from the raw waveform). */
class FEPFilterNone : public FEPFilterBase {
public:
   FEPFilterNone(midas::odb settings) : FEPFilterBase(settings) {}
   virtual ~FEPFilterNone() {}
   virtual INT apply(const ChannelId& chan, std::shared_ptr<Waveform> in, std::vector<std::shared_ptr<HitBase> >& out, bool debug) override;
};

/** Trivial FEP filter that just down-samples waveforms by a factor of 2. */
class FEPFilterDownsample : public FEPFilterBase {
public:
   FEPFilterDownsample(midas::odb settings);
   virtual ~FEPFilterDownsample() {}
   static void create_settings(midas::odb& settings);
   static bool is_enabled(midas::odb& settings);
   virtual INT apply(const ChannelId& chan, std::shared_ptr<Waveform> in, std::vector<std::shared_ptr<HitBase> >& out, bool debug) override;

private:
   DWORD factor;
};

/**
 * FEP filter implementation of a matched filter.
 * 
 * calcPulseShapeValue calculates the pulse shape value that is time reversed and
 * discretized in createPulseShapeKernel. This kernel is then run across the
 * waveform using convolution.
 */
class FEPFilterExp2GausMatched : public FEPFilterBase {
public:
   FEPFilterExp2GausMatched(midas::odb settings);
   virtual ~FEPFilterExp2GausMatched();
   static void create_settings(midas::odb& settings);
   static bool is_enabled(midas::odb& settings);
   virtual INT apply(const ChannelId& chan, std::shared_ptr<Waveform> in, std::vector<std::shared_ptr<HitBase> >& out, bool debug) override;
   void createPulseShapeKernel();
   double calcPulseShapeValue(double x);

private:
   INT kernelSize;
   double riseTimeSigma;
   double fallTimeTau;
   double time2Frac;
   double fallTime2Tau;
   double binSize;
   double *kernel;
};

/**
 * FEP Processing of DarkSide-20k data
 *  - baseline subtraction
 *  - implementation of an AR filter
 *  - peak finding with MA subtration 
 *  - large pulses are only downsampled
 */

class FEPDS20kProcessing : public FEPFilterBase {
 public:
  FEPDS20kProcessing(midas::odb settings);
  virtual ~FEPDS20kProcessing();
  static void create_settings(midas::odb& settings);
  static bool is_enabled(midas::odb& settings);
  virtual INT apply(const ChannelId& chan, std::shared_ptr<Waveform> in, std::vector<std::shared_ptr<HitBase> >& out, bool debug) override;

 private:
  // baseline
  double fBaseline;
    
  // filter parameters:
  // exponential time constant
  uint16_t ftau;
  // filter convenience variables
  double _tau;
  double _a;
  
  // hit finder parameters:
  // moving average window
  uint16_t fWindow;
  // filter convenience variables
  double _dwin;
  uint16_t fHalfWindow;

  // peak detection threshold type: TRUE->fixed, FALSE->dyanmic
  bool fFixed;
  // pedestal length to calculate baseline RMS
  uint16_t fPedestalLength;
  // sigma above baseline RMS for peak detection
  double fNsigma;
  // peak finder threshold
  double fThreshold;
  // peak finder charge/prominence cut
  double fChargePromCut;

  // filter method output
  std::vector<double>* ma_sub;

  // downsampling factor
  uint16_t ffactor;
 
 private:

  // this method is used to 'compress' large waveforms where peak finder is not useful
  void downsampler(std::shared_ptr<Waveform> in, std::shared_ptr<Waveform> out);

  // perform baseline subtraction and AR filtering and subtraction of its MA in two loops
  void filter(std::shared_ptr<Waveform> in, std::vector<double>* out);

  // find peaks above a given threshold and with charge/prominence ratio greater than cut value
  void peak_finder(const std::vector<double>* in, std::shared_ptr<MultiHit> out, bool debug=false);

  // compute threshold from baseline of filtered waveform
  void ComputeThreshold(const std::vector<double>* in, bool debug=false);
};

\mainpage

# Vertical slice overview

This repository contains tools for online processing of data for Darkside-20k.

Online analysis requires seeing all data for the detector. However, the reconstruction algorithms are too slow to handle the full data rate on a single machine. Therefore the data is split into time slices, with waveform data sent to different machines based on their timestamp.

The components are:

* A Front End Processor (FEP) that reads data from (up to) 8 VX2740 boards and applies a first level of filtering/compression (that only requires seeing data from 1 board).
* A Time Slice Processor (TSP) that reads data from all the FEPs for a given time slice, applies the higher level filtering/compression, and writes the data to disk.
* A Pool Manager (PM) that tells the FEPs where to send each data slice.

For Darkside-20k we anticipate 20+ FEPs and 100+ TSPs, with a time slice of 1 second. 

To reduce edge cases in reconstruction, an overlap period at the start/end of each slice is sent to both TSP N and TSP N+1. [ZMQ](https://zeromq.org/) is used for exchanging messages and data between the different programs.

Currently only very simplistic filtering algorithms are implemented:
* a downsampling filter at the FEP level (only keep 1 in N samples of each waveform)
* a timestamp filter at the the TSP level (only keep waveforms in the first N ms of each slice)

## Compiling

### Dependencies

zmq and zmqpp are required. On Ubuntu, you can use `apt install libzmqpp-dev` to install these system-wide.

Support for real VX2740 boards is optional (if not enabled, we will just create fake "demo data" events). VX2740 support comes from the [dsproto_vx2740](https://bitbucket.org/ttriumfdaq/dsproto_vx2740/) repository. If this package is found, we will automatically enable support for real VX2740 boards. You must set the `$CAEN_INSTALL` environment variable so we know where to find the CAEN FE libraries. See the `dsproto_vx2740` repository for more.

A [midas](https://bitbucket.org/tmidas/midas) installation is required. Using the `feature/midas-2020-12` branch is currently recommended (newer versions will not allow the ds_vslice code to compile).

### This code

The first time you compile this code, do:

    mkdir build
    cd build
    cmake ..
    make install
    
There are several options that can be passed to cmake:

* `-DZMQ_INCLUDE=...` if libzmq headers aren't in a standard location
* `-DZMQPP_INCLUDE=...` if zmqpp headers aren't in a standard location
* `-DZMQ_LIB=...` if "-lzmq" isn't enough to find the zmq library
* `-DZMQPP_INCLUDE=...` if "-lzmqpp" isn't enough to find the zmqpp library
* `-DVX2740_DIR=...` if you want real VX2740 support, and dsproto_vx2740 code isn't in `..`, `~/packages`, or `~/online`

A possible full cmake invocation may be:

    cmake .. -DZMQ_INCLUDE=/usr/local/include -DZMQ_LIB=/usr/local/lib/libzmq.a -DZMQPP_LIB=/usr/local/lib/libzmqpp.a

After the first compilation, re-compilation is as simple as:

    cd build
    make install
    
### Executables

The following executables are built:

* `bin/fep.exe` - Front End Processor, built as a midas frontend
* `bin/tsp_standalone.exe` - Time Slice Processor, built as a standalone program
* `bin/tsp_midasfe.exe` - Time Slice Processor, built as a midas frontend
* `bin/pm.exe` - Pool Manager, built as a midas frontend
* `bin/tsp_tap.exe` - TSP Tap, built as a midas frontend, which allows TSPs to route data to midas buffers
* `bin/dummy_vx2740_fe.exe` - Dummy VX2740 for use with the FEP if wanting to simulate VX2740 data across a network

We anticipate running the TSP in standalone mode most of the time. However the frontend-based version allows easier program control during development.

## Quick-start usage

For running with 2 FEPs and 2 TSPs on the same host:

    ./bin/pm.exe &
    
    ./bin/fep.exe -i 1 &
    ./bin/fep.exe -i 2 &
    
    ./bin/tsp_standalone.exe -s tcp://localhost:4712 -p tcp://localhost:4711 -i 1 &
    ./bin/tsp_standalone.exe -s tcp://localhost:4712 -p tcp://localhost:4711 -i 2 &

## Program details

### PM usage

`bin/pm.exe`

No command-line arguments need to be provided.

### FEP usage

`bin/fep.exe`

Arguments:

* `-i <frontend_index>` - the frontend index. Each instance that is running should be given a different index. 

### TSP usage

Two programs available:

* `bin/tsp_standalone.exe` - standalone program
* `bin/tsp_midasfe.exe` - midas frontend

Both programs accept the same arguments:

* `-i <frontend_index>` - the frontend index. Each instance that is running should be given a different index. 
* `-s <pool_manager_sub_endpoint>` - how to connect for PM->TSP communication (e.g. `tcp://localhost:4712`)
* `-p <pool_manager_push_endpoint>` - how to connect for TSP->PM communicaation (e.g. `tcp://localhost:4711`)
* `-o <our_hostname>` - (optional) what hostname to use when telling FEPs how to send data to us (e.g. `localhost`, `dsfe00`). If not specified, the result of `gethostname()` will be used.

The TSP will bind to port `7770 + frontend_index` for receiving data from FEPs.

### TSP Tap usage

`bin/tsp_tap.exe`

No command-line arguments need to be provided.

### Dummy VX2740 usage

`bin/dummy_vx2740_fe.exe`

Arguments:

* `-i <frontend_index>` - the frontend index. This should match one of the FEP indices, and will send data to that FEP.
* `-b <board_index>` - OPTIONAL the board index to simulate. If not specified, we simulate all the boards for that FEP.
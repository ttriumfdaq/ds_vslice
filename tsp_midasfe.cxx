#include "midas.h"
#include "msystem.h"
#include "tmfe_rev0.h"
#include "tsp_class.h"
#include "vslice_utils.h"
#include <iostream>
#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

/** \file
 *
 * Version of the TSP that runs as a midas frontend.
 * Like `tsp_standalone.cxx`, it receives ODB settings etc from the Pool Manager.
 * However, registering as a midas client simplifies some testing, as it's
 * possible to start/stop the program from the midas Programs webpage.
 */

void usage() {
   printf("usage: tsp_midasfe.exe \n");
   printf("          -i <frontend_index>\n");
   printf("          -s <pool_manager_sub_endpoint>\n");
   printf("          -p <pool_manager_push_endpoint>\n");
   printf("         [-o <our_hostname>]\n");
   printf("\n");
   printf("If -o not specified, gethostname() will be used to look up the hostname that FEPs should use\n");
   printf("to send data to us.\n");
}

class TSPRpcHandler : public TMFeRpcHandlerInterface {
public:
   TSPRpcHandler(HNDLE _hDB, TimeSliceProcessor *_tsp_class, TMFeEquipment *_eq) {
      hDB = _hDB;
      tsp_class = _tsp_class;
      eq = _eq;
   }

   virtual ~TSPRpcHandler(){};

   virtual void HandleBeginRun() override {
   }

   virtual void HandleEndRun() override {
   }

   virtual void HandleStartAbortRun() override {
      HandleEndRun();
   }

private:
   HNDLE hDB;
   TimeSliceProcessor *tsp_class;
   TMFeEquipment *eq;
};

int main(int argc, char *argv[]) {
   signal(SIGPIPE, SIG_IGN);

   // Parse options
   std::string pool_manager_sub_endpoint;
   std::string pool_manager_push_endpoint;
   std::string our_hostname;
   int frontend_index = -1;

   for (int i = 1; i < argc; i++) {
      if (strlen(argv[i]) != 2 || argv[i][0] != '-' || i + 1 >= argc || argv[i + 1][0] == '-') {
         printf("Invalid argument %s\n", argv[i]);
         usage();
         return FE_ERR_DRIVER;
      }

      if (argv[i][1] == 'p') {
         pool_manager_push_endpoint = argv[++i];
      } else if (argv[i][1] == 's') {
         pool_manager_sub_endpoint = argv[++i];
      } else if (argv[i][1] == 'o') {
         our_hostname = argv[++i];
      } else if (argv[i][1] == 'i') {
         frontend_index = atoi(argv[++i]);
      } else if (std::string("dDOv").find(argv[i][1]) == std::string::npos) {
         printf("Unknown option %s\n", argv[i]);
         usage();
         return FE_ERR_DRIVER;
      }
   }

   if (frontend_index < 0) {
      printf("Specify -i\n");
      usage();
      return FE_ERR_DRIVER;
   }

   if (pool_manager_push_endpoint == "" || pool_manager_sub_endpoint == "") {
      printf("Specify -s and -p options.\n");
      usage();
      return FE_ERR_DRIVER;
   }

   char name[255];
   snprintf(name, 255, "TSP_%03d", frontend_index);

   TMFE *mfe = TMFE::Instance();

   TMFeError err = mfe->Connect(name, __FILE__);

   if (err.error) {
      printf("Cannot connect: %s\n", err.error_string.c_str());
      return 1;
   }

   TMFeCommon *common = new TMFeCommon();
   common->EventID = 102;
   common->LogHistory = 1;
   common->Buffer = "SYSTEM";

   TMFeEquipment *eq = new TMFeEquipment(mfe, name, common);
   eq->Init();
   eq->SetStatus("Starting...", "white");
   eq->ZeroStatistics();
   eq->WriteStatistics();

   INT max_event_size = 500 * 1024 * 1024;

   // Configure main TSP class based on command-line options.
   TimeSliceProcessor tsp_class;
   tsp_class.set_frontend_index(frontend_index);
   tsp_class.set_pool_manager_endpoints(pool_manager_sub_endpoint, pool_manager_push_endpoint);

   if (our_hostname != "") {
      tsp_class.set_hostname_override(our_hostname);
   }

   INT status = tsp_class.init();

   if (status != SUCCESS) {
      mfe->Disconnect();
      return status;
   }

   TSPRpcHandler rpc_handler(mfe->fDB, &tsp_class, eq);

   mfe->RegisterEquipment(eq);
   mfe->RegisterRpcHandler(&rpc_handler);

   eq->SetStatus("Running...", "greenLight");

   char *buffer = (char *) malloc(max_event_size + 100);

   timeval last_stats_update;
   gettimeofday(&last_stats_update, NULL);

   try{
       while (!mfe->fShutdownRequested) {
           tsp_class.do_periodic_work();

           if (tsp_class.is_event_ready_to_send()) {
               eq->ComposeEvent(buffer, max_event_size);
               char *pevent = buffer + sizeof(EVENT_HEADER);
               int midas_size_bytes = tsp_class.send_data(pevent, false, max_event_size);

               if (midas_size_bytes > 0) {
                  eq->SendEvent(buffer);
               }
           }

           mfe->PollMidas(1);

           timeval now;
           gettimeofday(&now, NULL);

           if (now.tv_sec > last_stats_update.tv_sec) {
               eq->WriteStatistics();

               std::string status, color;
               tsp_class.get_status_strings(status, color);
               eq->SetStatus(status.c_str(), color.c_str());

               last_stats_update = now;
           }
       }
   }
   catch(int e){
       if(e == TSP_KILL_PM){
           mfe->fShutdownRequested = TRUE;
       }
   }

   eq->SetStatus("Frontend stopped", "redLight");

   mfe->Disconnect();

   free(buffer);

   return 0;
}

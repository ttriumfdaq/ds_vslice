#include "midas.h"
#include "data_structures.h"
#include "json.hpp"

/** \file
 * Filters used in the TSP to reduce the amount of data saved per slice.
 */

/** Base class that all filters at the TSP level should inherit from.
 *
 * After adding a new filter class, you must add support to the `TimeSliceProcessor`
 * and `PoolManager` classes:
 * - Create any ODB keys you need in the `PoolManager::PoolManager()` constructor or
 *    `PoolManager::Init()` function (as the TSP does not have direct access to the ODB itself).
 * - Create the correct object in `TimeSliceProcessor::handle_delayed_begin_of_run()`
 */
class TSPFilterBase {
public:
   /** Constructor.
    *
    * \param[in] settings The full set of TSP settings.
    */
   TSPFilterBase(nlohmann::json settings);
   virtual ~TSPFilterBase();

   /** Main function to implement in all derived classes.
    *
    * \param[in] in The data from the FEPs.
    * \param[out] out The place to write your filtered data. You only need to
    *    worry about the HitBase members, the other metadata (slice index, time
    *    of start of slice etc) will be set by the calling code.
    * \return Midas status code. If returning anything other than `SUCCESS`, set the
    *    reason why in the `error_message` member.
    */
   virtual INT apply(Slice& in, Slice& out) = 0;

   /** Return any error message that was set if `apply()` failed. */
   std::string get_error_message();

   /** Perform any tidying-up needed after a slice is finished. */
   virtual void reset();

protected:
   std::string error_message; //!< Error message that can be set by derived classes to explain problems.
};

/** No-op TSP filter that doesn't do anything (just copies over all the hits). */
class TSPFilterNone : public TSPFilterBase {
public:
   TSPFilterNone(nlohmann::json settings);
   virtual ~TSPFilterNone();
   virtual INT apply(Slice& in, Slice& out) override;
};

/** Basic TSP filter that keeps hits that were within the first X ms of a slice.
 *
 * Reads two parameters from the ODB:
 * - `Basic time filter/Keep first ms of slice`
 * - `Basic time filter/Fake processing delay (ms)`
 */
class TSPBasicTimeFilter : public TSPFilterBase {
public:
   TSPBasicTimeFilter(nlohmann::json settings);
   virtual ~TSPBasicTimeFilter();
   virtual INT apply(Slice& in, Slice& out) override;

protected:
   double keep_ms; //!< First X ms of slice to keep hits from.
   double fake_delay_ms; //!< Artifical delay to add to pretend that processing a slice took longer.
};

#ifndef VSLICE_UTILS_H
#define VSLICE_UTILS_H

#include "json.hpp"
#include "midas.h"
#include "odbxx.h"
#include "sys/times.h"

/** \file
 * Helper functions used throughout the codebase.
 */

/** \namespace vslice
 *
 * Helper functions used throughout the codebase.
 */
namespace vslice {
/** Helper function to do a `printf()`, but automatically prepend the current timestamp.
 *
 * This is very useful for debugging issues related to communication between multiple processes,
 * between threads in the same process, or simply for identifying slow operations.
 *
 * E.g.
 * \code{.cxx}
 * vslice::ts_printf("Hello %s\n", "World");
 * \endcode
 *
 * could print `2021-05-12 13:27:04.532 Hello World` to stdout.
 */
void ts_printf(const char *format, ...);

/** Empty a midas ring buffer, so the write pointer and read pointer
 * are in the same place.
 *
 * \param[in] rb_handle Midas ring buffer handle.
 * \param[in] max_event_size_bytes The max event size that was specified when creating
 *    this ring buffer.
 *
 * \return Midas status code.
 */
INT empty_ring_buffer(int rb_handle, int max_event_size_bytes);

/** Find clients connected to midas that are of the form
 * `<prefix>_<index>`.
 *
 * \param[in] hDB ODB handle.
 * \param[in] prefix Client name prefix to search for.
 * \return The list of indexes found.
 */
std::vector<int> get_running_frontend_indexes(HNDLE hDB, std::string prefix);

/** Return a string that contains a comma-separated list of ints. */
std::string format_list_of_ints(std::vector<int> vec);

/** Return a string like "1.23GB" for human-readable data sizes. */
std::string format_bytes(uint64_t num_bytes);

/** Convert a midas odb object to a json object (from json.hpp).
 *
 * \param[in] odbxx `midas::odb` object
 * \param[in] keep_key_metadata If true, the returned JSON object will include
 *    extra keys of the form `<odb_name>/key`, with sub-keys like `type` and
 *    `last_written`.
 * \return JSON object
 */
nlohmann::json odbxx_to_json(midas::odb odbxx, bool keep_key_metadata = false);

/** \struct cpu_info
 *
 * Structure for usage with `update_cpu_info()`. Create with `init_cpu_info()`.
 *
 * The user probably only cares about the `this_proc_pct` member.
 *
 * See `update_cpu_info()` for more details.
 */
struct cpu_info {
   clock_t cpu_tot; //!< Total CPU time when `update_cpu_info()` was last called.
   clock_t cpu_sys; //!< System CPU time when `update_cpu_info()` was last called.
   clock_t cpu_user; //!< User CPU time when `update_cpu_info()` was last called.
   int num_processors; //!< Number of CPUs on this machine.

   /// The %age of CPU usage that was taken by this process between when
   /// `update_cpu_info()` was most recently called, and the time before that.
   float this_proc_pct;
};

/** Initialize an object that can be passed to `update_cpu_info()`.
 *
 * Do not just create a `cpu_info` struct manually, as we need to
 * grab the number of processors on a machine etc.
 */
cpu_info init_cpu_info();

/** Calculate the % of CPU usage by this process since you last called
 * `update_cpu_info()`. The result is stored in `info.this_proc_pct`.
 *
 * Example usage:
 *
 * \code{.cxx}
 * vslice::cpu_info info = vslice::init_cpu_info();
 * vslice::update_cpu_info(info);
 * # Do some work
 * vslice::update_cpu_info(info);
 * # `info.this_proc_pct` contains % CPU usage of "Do some work".
 * \endcode
 */
void update_cpu_info(cpu_info& info);


std::string exec(const char* cmd);

/** Get the current UNIX time in ms. */ 
uint64_t current_time_ms();

}// namespace vslice

#endif

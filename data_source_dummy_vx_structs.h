#ifndef DATA_SOURCE_DUMMY_VX_STRUCTS
#define DATA_SOURCE_DUMMY_VX_STRUCTS

#include "midas.h"

#define DUMMY_VX_DATA_ODB_SETTINGS_STR "\
Event rate (Hz) = FLOAT : 3\n\
WF size (64bit words) = INT : 100\n\
Channel mask (31-0) = DWORD : 0xFFFFFFFF\n\
Channel mask (63-32) = DWORD : 0xFFFFFFFF\n\
Port = WORD : 0\n\
"

/**
* \struct DummyVxDataSettings
*
* User settings from the ODB.
*/
typedef struct {
  float demo_data_rate_Hz; //!< Average event rate.
  INT demo_data_wf_64bit_word_size; //!< Number of 64-bit words per channel per event.
  DWORD demo_data_ch_mask_31_0; //!< Bitmask of which channels to generate waveforms for.
  DWORD demo_data_ch_mask_63_32; //!< Bitmask of which channels to generate waveforms for.
  WORD port;
} DummyVxDataSettings;

#endif
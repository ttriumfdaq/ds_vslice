#include <sstream>
#include <iomanip>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>

#include "pm_class.h"
#include "json.hpp"

using namespace nlohmann;// nlohmann::json -> json

void vslice::pmcallback(INT hDB, INT hkey, INT index, void *feptr) {
   PoolManager *fe = (PoolManager *) feptr;
   fe->fecallback(hDB, hkey, index);
}

PoolManager::PoolManager(TMFE *mfe, TMFeEquipment *eq, TMFeEquipment *tspeq) : fep_socket(zmq_context, zmqpp::socket_type::publish),
                                                                      tsp_in_socket(zmq_context, zmqpp::socket_type::pull),
                                                                      tsp_out_socket(zmq_context, zmqpp::socket_type::publish),
                                                                      tsp_settings({{"Debug", false},
                                                                                    {"Write data", false},
                                                                                    {"Max file size (MiB)", 500},
                                                                                    {"Heartbeat (s)", 1.0},
                                                                                    {"Basic time filter", {
                                                                                                           {"Enable", false},
                                                                                                           {"Keep first ms of slice", 500},
                                                                                                           {"Fake processing delay (ms)", 500}
                                                                                        }
                                                                                    }})
{
  fMfe = mfe;
  fEq = eq;
  fTspEq = tspeq;
  tsp_in_port = 4711;
  tsp_out_port = 4712;
  fep_port = 4713;
  slice_idx_for_next_tsp = 0;
  in_run = false;
  curr_run_number = 0;
  tsp_heartbeat_timeout_secs = 5;
}

void PoolManager::Init() {
   fEq->fOdbEqSettings->RI("TSP_IN_port", &tsp_in_port, true);
   fEq->fOdbEqSettings->RI("TSP_OUT_port", &tsp_out_port, true);
   fEq->fOdbEqSettings->RI("FEP_port", &fep_port, true);
   fEq->fOdbEqSettings->RI("TSP_tapN", &tsp_tap, true);
   fEq->fOdbEqSettings->WI("TSP_tapN", 0); // TSP tap should never be on by default
   fEq->fOdbEqSettings->RI("TSP heartbeat timeout (s)", &tsp_heartbeat_timeout_secs, true);
   fEq->fOdbEqSettings->RB("Debug", &debug, true);
   fEq->fOdbEqSettings->RB("Debug heartbeats", &debug_heartbeats, true);

   OpenSockets();
   char tmpbuf[256];
   HNDLE hkey;
   sprintf(tmpbuf, "/Equipment/%s/Settings/TSP_IN_port", fMfe->fFrontendName.c_str());
   db_find_key(fMfe->fDB, 0, tmpbuf, &hkey);
   db_watch(fMfe->fDB, hkey, vslice::pmcallback, (void *) this);
   sprintf(tmpbuf, "/Equipment/%s/Settings/TSP_OUT_port", fMfe->fFrontendName.c_str());
   db_find_key(fMfe->fDB, 0, tmpbuf, &hkey);
   db_watch(fMfe->fDB, hkey, vslice::pmcallback, (void *) this);
   sprintf(tmpbuf, "/Equipment/%s/Settings/FEP_port", fMfe->fFrontendName.c_str());
   db_find_key(fMfe->fDB, 0, tmpbuf, &hkey);
   db_watch(fMfe->fDB, hkey, vslice::pmcallback, (void *) this);
   sprintf(tmpbuf, "/Equipment/%s/Settings/TSP_tapN", fMfe->fFrontendName.c_str());
   db_find_key(fMfe->fDB, 0, tmpbuf, &hkey);
   db_watch(fMfe->fDB, hkey, vslice::pmcallback, (void *) this);
   sprintf(tmpbuf, "/Equipment/%s/Settings/Debug", fMfe->fFrontendName.c_str());
   db_find_key(fMfe->fDB, 0, tmpbuf, &hkey);
   db_watch(fMfe->fDB, hkey, vslice::pmcallback, (void *) this);

   sprintf(tmpbuf, "/Equipment/%s/Settings/TSP", fMfe->fFrontendName.c_str());
   tsp_settings.connect(tmpbuf);

   // odbxx doesn't automatically add variables that are missing.
   // "Fake processing delay (ms)" was added after development started, so need to manually ensure it
   // gets created.
   if (!tsp_settings["Basic time filter"].is_subkey("Fake processing delay (ms)")) {
      tsp_settings["Basic time filter"]["Fake processing delay (ms)"] = 500;
   }

   tsp_tap_readback.connect("/Equipment/TSP_Tap/Readback");
   if (!tsp_tap_readback.is_subkey("endpoint")) {
      tsp_tap_readback["endpoint"] = std::string();
   }

   // If TSP tap hasn't been run, but PM has, we must ensure Variables dir exists, or else
   // logger will complain.
   midas::odb tap_var;
   tap_var.connect("/Equipment/TSP_Tap/Variables");

   std::vector<double> dummy(1);
   std::vector<int> dummy_int(1);
   std::vector<bool> dummy_bool(1);
   fTspEq->fOdbEqVariables->RDA("Slice size kB", &dummy, true);
   fTspEq->fOdbEqVariables->RDA("Transfer time", &dummy, true);
   fTspEq->fOdbEqVariables->RDA("Data rate", &dummy, true);
   fTspEq->fOdbEqVariables->RDA("Analysis time", &dummy, true);
   fTspEq->fOdbEqVariables->RDA("Analysis speed", &dummy, true);
   fTspEq->fOdbEqVariables->RDA("Analysis CPU usage pct", &dummy, true);
   fTspEq->fOdbEqVariables->RDA("Processed bytes", &dummy, true);
   fTspEq->fOdbEqVariables->RIA("Slices processed", &dummy_int, true);
   fTspEq->fOdbEqVariables->RIA("Slices lost", &dummy_int, true);

   std::vector<int> tsp_states_odb;
   fTspEq->fOdbEqVariables->RIA("State", &tsp_states_odb, true);
   fTspEq->fOdbEqVariables->RBA("Receiving", &dummy_bool, true);
   fTspEq->fOdbEqVariables->RBA("Analyzing", &dummy_bool, true);
   // fTspEq->fOdbEqVariables->RSA("Endpoint", &tsp_endpoints, true);

   fOdbEqStatus = fEq->fOdbEq->Chdir("Status", true);
   
   std::vector<std::string> tsp_endpoints(1);
   fOdbEqStatus->RSA("TSP Endpoint", &tsp_endpoints, true, 64);
   tsp_endpoints.assign(1, std::string());
   fOdbEqStatus->WSA("TSP Endpoint", tsp_endpoints, 0);

   ResetODBStatus();
   last_finished_slice = -1;

   // Sinkhole any messages that ZMQ has pending in its queue.
   // These are from TSPs we don't know about yet.
   // We need to sleep a bit, to give ZMQ enough time to spin up.
   ss_sleep(500);

   vslice::ts_printf("Clearing out ZMQ message queue.\n");
   zmqpp::message in_message;

   while (RecvTSPMessage(in_message)) {
      usleep(100);
   }

   // If we started after TSPs were started, ask the TSPs to tell us
   // about themselves again.
   zmqpp::message out_message;
   out_message << "reregister";
   vslice::ts_printf("Asking any running TSPs to re-register with us.\n");
   PubTSPMessage(out_message);
}

void PoolManager::ResetODBStatus() {
   std::vector<int> tsp_states_odb;
   fTspEq->fOdbEqVariables->RIA("State", &tsp_states_odb, true);

   for (int i = 0; i < tsp_states_odb.size(); i++) {
      fTspEq->fOdbEqVariables->WIAI("State", i, TSP_OFFLINE);
      fTspEq->fOdbEqVariables->WBAI("Receiving", i, false);
      fTspEq->fOdbEqVariables->WBAI("Analyzing", i, false);
      fTspEq->fOdbEqVariables->WDAI("Slice size kB", i, 0.);
      fTspEq->fOdbEqVariables->WDAI("Transfer time", i, 0.);
      fTspEq->fOdbEqVariables->WDAI("Data rate", i, 0.);
      fTspEq->fOdbEqVariables->WDAI("Analysis time", i, 0.);
      fTspEq->fOdbEqVariables->WDAI("Analysis speed", i, 0.);
      fTspEq->fOdbEqVariables->WDAI("Analysis CPU usage pct", i, 0.);
      fTspEq->fOdbEqVariables->WDAI("Processed bytes", i, 0.);
      fTspEq->fOdbEqVariables->WIAI("Slices processed", i, 0);
      fTspEq->fOdbEqVariables->WIAI("Slices lost", i, 0);
   }
}

int PoolManager::GetRunNumber() {
   int run_num;
   fMfe->fOdbRoot->RI("Runinfo/Run number", &run_num, false);
   return run_num;
}

void PoolManager::HandleBeginRun() {
   fMfe->Msg(MINFO, "HandleBeginRun", "Begin run! Informing TSPs.");

   ResetODBStatus();

   for (auto& tsp : tsp_states) {
      tsp.second.reset_bor();
   }

   while (!tsp_queue.empty()) {
      pop_from_queue();
   }

   slice_idx_for_next_tsp = 0;
   last_finished_slice = -1;

   curr_run_number = GetRunNumber();

   // Use JSON to encode the vector of integers of the FEPs the TSP should expect data from.
   tsp_bor_info.clear();
   tsp_bor_info["run_number"] = curr_run_number;
   tsp_bor_info["fep_list"] = vslice::get_running_frontend_indexes(fMfe->fDB, "FEP");

   std::string data_dir;
   db_get_value_string(fMfe->fDB, 0, "/Logger/Data dir", 0, &data_dir);
   tsp_bor_info["data_dir"] = data_dir;

   // Convert TSP settings from ODBXX object to part of the JSON object.
   tsp_settings.read();
   tsp_bor_info["tsp_settings"] = vslice::odbxx_to_json(tsp_settings);

   if(tsp_settings["Write data"]){
      std::string newMap = WriteSliceMap(curr_run_number);

      std::string messages_dir;
      db_get_value_string(fMfe->fDB, 0, "/Logger/Message dir", 0, &messages_dir);

      if (messages_dir == "") {
         messages_dir = data_dir;
      }

      // Create symlink so current file will appear on midas messages page.
      // mhttpd only shows files named like <singleword>.log; the filename cannot
      // contain underscores or numbers.
      std::string link_name = messages_dir + "/slicemap.log";

      unlink(link_name.c_str());

      if(symlink(newMap.c_str(), link_name.c_str())){
         fMfe->Msg(MINFO, "HandleBeginRun", "Couldn't generate symlink %s", link_name.c_str());
      }
   }

   // Send BeginOfRun info to the TSPs
   PubTSPBeginOfRun();

   in_run = true;
}

std::string PoolManager::WriteSliceMap(int run, int slice, std::string filename){
   std::string data_dir;
   db_get_value_string(fMfe->fDB, 0, "/Logger/Data dir", 0, &data_dir);
   std::ostringstream oss;
   oss << data_dir << "/slice_maps";
   if(mkdir(oss.str().c_str(), 0755)){
      if(errno != EEXIST){
         fMfe->Msg(MERROR, "WriteSliceMap", "Couldn't create slice_maps directory %s", oss.str().c_str());
         abort();
      }
   }
   oss << "/slice_map_run" << std::setw(6) << std::setfill('0') << run << ".txt";
   std::ofstream f(oss.str().c_str(), std::ofstream::app);
   if(!f){
      fMfe->Msg(MERROR, "WriteSliceMap", "Couldn't open slice_map file %s", oss.str().c_str());
      return std::string();
   }
   if(filename.size()){
      f << int(fMfe->GetTime()) << '\t' << slice << '\t' << filename << std::endl;
   }
   return oss.str();
}

void PoolManager::PubTSPBeginOfRun() {
   zmqpp::message message;
   message << "begin_of_run" << tsp_bor_info.dump();
   PubTSPMessage(message);
}

void PoolManager::HandleEndRun() {
   in_run = false;

   fMfe->Msg(MINFO, "HandleEndRun", "End run! Informing TSPs");

   zmqpp::message message;
   message << "end_of_run" << GetRunNumber();
   PubTSPMessage(message);
}

void PoolManager::TerminateTSP(uint32_t pid, std::string reason){
   zmqpp::message message;
   message << "terminate" << pid << reason;
   PubTSPMessage(message);
}

void PoolManager::SwitchTSPTap(std::string endpoint){
   zmqpp::message message;
   message << "tsp_tap" << endpoint;
   PubTSPMessage(message);
}

void PoolManager::HandlePeriodic() {
   double now = fMfe->GetTime();
   std::vector<int> tsps_to_delete;

   for (auto& tsp : tsp_states) {
      int tsp_idx = tsp.first;
      double hb_time = tsp.second.heartbeat_time;

      if (hb_time > 0 && now - hb_time > tsp_heartbeat_timeout_secs) {
         fMfe->Msg(MERROR, "HandlePeriodic", "TSP %d hasn't reported back in %.2fs, removed from queues", tsp_idx, now - hb_time);
         tsps_to_delete.push_back(tsp.first);
      }
   }

   for (auto& tsp_id : tsps_to_delete) {
      RemoveTSP(tsp_id);
      UpdateODBState(tsp_id, TSP_OFFLINE);
   }

   int num_tsps_idle = 0;
   int num_tsps_recv = 0;
   int num_tsps_ana = 0;
   double tx_min = 0.;
   double tx_max = 0.;
   double ana_min = 0.;
   double ana_max = 0.;

   for (auto& tsp : tsp_states) {
      TSPState& state = tsp.second;

      if (state.analyzing) {
         num_tsps_ana++;
      }
      if (state.active) {
         num_tsps_recv++;
      }
      if (!state.analyzing && !state.active) {
         num_tsps_idle++;
      }

      if (tx_min == 0 || state.tsp_data_rate < tx_min) {
         tx_min = state.tsp_data_rate;
      }

      if (tx_max == 0 || state.tsp_data_rate > tx_max) {
         tx_max = state.tsp_data_rate;
      }

      if (ana_min == 0 || state.tsp_ana_time < ana_min) {
         ana_min = state.tsp_ana_time;
      }

      if (ana_max == 0 || state.tsp_ana_time > ana_max) {
         ana_max = state.tsp_ana_time;
      }
   }

   int num_tsps_not_analyzing = tsp_states.size() - num_tsps_ana;

   std::ostringstream oss;
   oss << "idle: " << num_tsps_idle << ", receiving: " << num_tsps_recv << ", analyzing: " << num_tsps_ana;
   std::string colour;
   if (num_tsps_not_analyzing < 2)
      colour = "var(--morange)";
   else if (num_tsps_not_analyzing < 3)
      colour = "var(--myellow)";
   else
      colour = "var(--mgreen)";
   fEq->SetStatus(oss.str().c_str(), colour.c_str());

   std::ostringstream oss2;
   oss2 << "last slice: " << last_finished_slice << ", tx: " << std::setprecision(4) << tx_min << " - " << tx_max <<
      " MB/s, ana: " << ana_min << " - " << ana_max << " s";
   fTspEq->SetStatus(oss2.str().c_str(), "var(--mgreen)");
}

void PoolManager::PollTSP() {
   zmqpp::message message;

   while (RecvTSPMessage(message)) {
      std::string error;
      bool success = ProcessTSPMessage(message, error);

      if (!success) {
         fMfe->Msg(MERROR, "PollTSP", "Couldn't decode message from TSP: %s", error.c_str());
      }
   }

   while (tsp_queue.size()) {
      PubNextTSP();
   }
}

void PoolManager::fecallback(HNDLE hDB, HNDLE hkey, INT index) {
   KEY key;
   int status = db_get_key(fMfe->fDB, hkey, &key);
   if (status == DB_SUCCESS) {// Use template functions here
      if (std::string(key.name) == std::string("TSP_IN_port")) {
         fEq->fOdbEqSettings->RI("TSP_IN_port", &tsp_in_port, true);
         OpenSockets();
      } else if (std::string(key.name) == std::string("TSP_OUT_port")) {
         fEq->fOdbEqSettings->RI("TSP_OUT_port", &tsp_out_port, true);
         OpenSockets();
      } else if (std::string(key.name) == std::string("FEP_port")) {
         fEq->fOdbEqSettings->RI("FEP_port", &fep_port, true);
         OpenSockets();
      } else if (std::string(key.name) == std::string("TSP_tapN")) {
         fEq->fOdbEqSettings->RI("TSP_tapN", &tsp_tap, true);
         std::string endpoint;
         if(tsp_tap){
            endpoint = tsp_tap_readback["endpoint"];
         }
         SwitchTSPTap(endpoint);
      } else if (std::string(key.name) == std::string("Debug")) {
         fEq->fOdbEqSettings->RB("Debug", &debug, true);
      } else if (std::string(key.name) == std::string("Debug heartbeats")) {
         fEq->fOdbEqSettings->RB("Debug heartbeats", &debug_heartbeats, true);
      } else {
         fMfe->Msg(MERROR, "fecallback", "Unexpected ODB key %s!", key.name);
      }
   } else {
      fMfe->Msg(MERROR, "fecallback", "Coudn't retrieve ODB key!");
   }
}

int PoolManager::get_tsp_idx_from_endpoint(std::string endpoint) {
   for (auto& tsp : tsp_states) {
      if (tsp.second.endpoint == endpoint) {
         return tsp.first;
      }
   }

   return -1;
}

bool PoolManager::PubNextTSP() {
   if (!tsp_queue.size()) {
      fMfe->Msg(MERROR, "PubNextTSP", "TSP queue empty!");
      return false;
   }
   
   zmqpp::message message;
   int next_tsp_idx = pop_from_queue();
   std::string next_tsp_endpoint = tsp_states[next_tsp_idx].endpoint;
   message << next_tsp_endpoint << slice_idx_for_next_tsp;
   
   vslice::ts_printf("TSP %03d: Publishing %s as TSP for slice %d.\n", next_tsp_idx, next_tsp_endpoint.c_str(), slice_idx_for_next_tsp);

   slice_idx_for_next_tsp++;

   tsp_states[next_tsp_idx].assigned = true;

   fep_socket.send(message);
   return true;
}

void PoolManager::PubTSPMessage(zmqpp::message &message) {
   if (!tsp_out_socket.send(message)) {
      std::string cmd;
      message.get(cmd, 0);
      vslice::ts_printf("Failed to send %s message\n", cmd.c_str());
   }
}

bool PoolManager::ProcessTSPMessage(zmqpp::message &message, std::string &err_msg) {
   uint8_t tsp_idx;
   std::string endpoint;
   uint8_t state;
   int slice_idx;
   double duration;
   uint32_t info;

   int min_parts = 6;

   if (message.parts() < min_parts) {
      std::stringstream s;
      s << "Message contained " << message.parts() << " parts. Expected >= " << min_parts << " parts.";
      err_msg = s.str();
      return false;
   }

   message >> tsp_idx >> endpoint >> state >> slice_idx >> duration >> info;

   if (!endpoint.size()) {
      err_msg = "Endpoint string was empty.";
      return false;
   }

   if ((state == TSP_HEARTBEAT && debug_heartbeats) || (state != TSP_HEARTBEAT && debug)) {
      vslice::ts_printf("TSP %03d: Received %s from TSP idx %d. Message contains %d parts.\n", tsp_idx, tsp_state_to_text(state).c_str(), tsp_idx, message.parts());
   }

   bool update_odb_state = true;

   switch (state) {
   case TSP_OFFLINE: {
      if (duration == 1) {        // Termination report from unregistered TSP, should not change state of registered one
         fMfe->Msg(MINFO, "ProcessTSPMessage", "Requested TSP termination %d successful.", tsp_idx);
      } else if(info == tsp_states[tsp_idx].pid){
         fMfe->Msg(MERROR, "ProcessTSPMessage", "TSP %d died, error code %d", tsp_idx, int(duration));
         RemoveTSP(tsp_idx);
         UpdateODBState(tsp_idx, TSP_OFFLINE);
      } else {
         fMfe->Msg(MERROR, "ProcessTSPMessage", "Unknown TSP %d died, unknown pid %d", tsp_idx, info);
      }
      
      update_odb_state = false;
      break;
   }
   case TSP_RECVTIMEOUT: {
      fMfe->Msg(MERROR, "ProcessTSPMessage", "TSP %03d at %s reported a timeout waiting for data", tsp_idx, endpoint.c_str());
      tsp_states[tsp_idx].active = false;
      tsp_states[tsp_idx].active_from_previous_run = false;
      tsp_states[tsp_idx].tsp_num_lost++;
      enqueue(tsp_idx);
      break;
   }
   case TSP_ANAFAILED: {
      if (message.parts() != min_parts + 1) {
         std::stringstream s;
         s << "Message contained " << message.parts() << " parts. Expected " << min_parts + 1 << " parts for TSP_ANAFAILED.";
         err_msg = s.str();
         return false;
      }

      std::string tsp_fail_msg;
      message >> tsp_fail_msg;
      fMfe->Msg(MERROR, "ProcessTSPMessage", "TSP %03d at %s reports processing failure for slice %d: %s", tsp_idx, endpoint.c_str(), slice_idx, tsp_fail_msg.c_str());
      vslice::ts_printf("TSP %03d: Ana FAILURE report for slice %d: %s\n", tsp_idx, slice_idx, tsp_fail_msg.c_str());

      if (!tsp_states[tsp_idx].analyzing) {
         fMfe->Msg(MERROR, "ProcessTSPMessage", "Analysis FAILURE report was from unexpected TSP %s", endpoint.c_str());
         update_odb_state = false;
      } else {
         tsp_states[tsp_idx].analyzing = false;
         tsp_states[tsp_idx].active_from_previous_run = false;
         tsp_states[tsp_idx].tsp_num_lost++;
      }

      break;
   }
   case TSP_ANAFINISHED: {
      last_finished_slice = slice_idx;

      if (message.parts() != min_parts + 3) {
         std::stringstream s;
         s << "Message contained " << message.parts() << " parts. Expected " << min_parts + 3 << " parts for TSP_ANAFINISHED.";
         err_msg = s.str();
         return false;
      }

      std::string slice_file;
      int runNo;
      float cpu_pct;
      message >> runNo >> slice_file >> cpu_pct;

      if (!tsp_states[tsp_idx].analyzing) {
         fMfe->Msg(MERROR, "ProcessTSPMessage", "Analysis finished report from unexpected TSP %s", endpoint.c_str());
         update_odb_state = false;
         break;
      }

      vslice::ts_printf("TSP %03d: Ana finished report for slice %d, duration %lf s. Processed size %d bytes\n", tsp_idx, slice_idx, duration, info);

      if(slice_file.size()){
         WriteSliceMap(runNo, slice_idx, slice_file);
      }

      // Remove tsp from "analyzing" list
      tsp_states[tsp_idx].analyzing = false;
      tsp_states[tsp_idx].active_from_previous_run = false;

      if (runNo == curr_run_number) {
         tsp_states[tsp_idx].tsp_ana_time = duration;
         tsp_states[tsp_idx].tsp_ana_speed = double(info)/double(1 << 20)/duration; // MB/s
         tsp_states[tsp_idx].tsp_ana_size = info;
         tsp_states[tsp_idx].tsp_ana_cpu_pct = cpu_pct;
         tsp_states[tsp_idx].tsp_num_proc++;
      }

      if (tsp_tap > 0){
         fEq->fOdbEqSettings->WI("TSP_tapN", --tsp_tap);
      }

      break;
   }
   case TSP_INIT: {
      if (tsp_states.find(tsp_idx) != tsp_states.end() && tsp_states[tsp_idx].pid != info) {
         fMfe->Msg(MERROR, "ProcessTSPMessage", "TSP %d (%s) connected twice, second connection refused.", tsp_idx, endpoint.c_str());
         TerminateTSP(info, "Index in use");
         update_odb_state = false;
      } else {
         tsp_states[tsp_idx].endpoint = endpoint;
         tsp_states[tsp_idx].pid = info;
         fOdbEqStatus->WSAI("TSP Endpoint", tsp_idx, endpoint.c_str(), 0);

         if (in_run && !tsp_bor_info.empty()) {
            // Tell it about the current run.
            vslice::ts_printf("TSP %03d: Registered from %s with PID %d. Broadcast the current run information.\n", tsp_idx, endpoint.c_str(), info);
            PubTSPBeginOfRun();
         } else {
            vslice::ts_printf("TSP %03d: Registered from %s with PID %d, but we're not in a run. Nothing to do.\n", tsp_idx, endpoint.c_str(), info);
         }
      }
      break;
   }
   case TSP_IDLE: {
      vslice::ts_printf("TSP %03d: Reporting for duty.\n", tsp_idx);

      // Add new tsp to queue (only at start of tsp)
      assert(!tsp_states[tsp_idx].active);
      assert(!tsp_states[tsp_idx].analyzing);

      enqueue(tsp_idx);
      break;
   }
   case TSP_TXSTARTED:
      vslice::ts_printf("TSP %03d: Tx started for next slice\n", tsp_idx);
      tsp_states[tsp_idx].active = true;
      tsp_states[tsp_idx].assigned = false;
      break;
   case TSP_TXCOMPLETE: {
      vslice::ts_printf("TSP %03d: Tx finished report for slice %d, duration %lf. Size %d kB -> %f MB/s\n", tsp_idx, slice_idx, duration, info >> 10, double(info) / duration / double(1 << 20));

      if (!tsp_states[tsp_idx].active) {
         fMfe->Msg(MERROR, "ProcessTSPMessage", "Transfer finished report from unexpected TSP %03d", tsp_idx);
         update_odb_state = false;
      } else {
         tsp_states[tsp_idx].active = false;
         tsp_states[tsp_idx].tsp_rx_time = duration;
         tsp_states[tsp_idx].tsp_slice_kb = info >> 10;
         tsp_states[tsp_idx].tsp_data_rate = double(info) / duration / double(1 << 20);
      }
      break;
   }
   case TSP_ANASTARTED: {
      // Move tsp to "analyzing" list, and allow TSP to receive next slice.
      tsp_states[tsp_idx].analyzing = true;
      enqueue(tsp_idx);
      vslice::ts_printf("TSP %03d: Ana started for slice %d\n", tsp_idx, slice_idx);
      break;
   }
   case TSP_HEARTBEAT: {
      if (tsp_states[tsp_idx].pid != info || tsp_states[tsp_idx].endpoint != endpoint) {
         if (in_run) {
            // Only terminate program if we're in a run. Otherwise PM may kill
            // TSPs before they have a chance to re-register when PM starts.
            if (tsp_states[tsp_idx].endpoint != endpoint) {
               fMfe->Msg(MERROR, "ProcessTSPMessage", "Received heartbeat from unknown TSP endpoint %s", endpoint.c_str());
            } else {
               fMfe->Msg(MERROR, "ProcessTSPMessage", "Received heartbeat from TSP %s at unknown PID %u (expected %u)", endpoint.c_str(), info, tsp_states[tsp_idx].pid);               
            }
            TerminateTSP(info, "Unknown heartbeats");
         }
      } else {
         tsp_states[tsp_idx].heartbeat_time = fMfe->GetTime();
      }
      
      update_odb_state = false;
      break;
   }
   default:
      std::stringstream s;
      s << "Unknown state " << state;
      err_msg = s.str();
      return false;
   }

   if(update_odb_state) {
      // TSP_OFFLINE may be sent back by duplicate TSP, so gets set only when this is checked further up
      UpdateODBState(tsp_idx, state);
   }

   return true;
}

void PoolManager::OpenSockets() {
   std::ostringstream fep_oss, tsp_in_oss, tsp_out_oss;
   fep_oss << "tcp://*:" << fep_port;
   tsp_in_oss << "tcp://*:" << tsp_in_port;
   tsp_out_oss << "tcp://*:" << tsp_out_port;

   if (fep_oss.str() != fep_endpoint) {
      if (fep_endpoint.size())
         fep_socket.unbind(fep_endpoint);
      fep_endpoint = fep_oss.str();
      fep_socket.bind(fep_endpoint);
      vslice::ts_printf("Bound to %s for FEP comms\n", fep_endpoint.c_str());
   }
   if (tsp_in_oss.str() != tsp_in_endpoint) {
      if (tsp_in_endpoint.size())
         tsp_in_socket.unbind(tsp_in_endpoint);
      tsp_in_endpoint = tsp_in_oss.str();
      tsp_in_socket.bind(tsp_in_endpoint);
      vslice::ts_printf("Bound to %s for incoming TSP comms\n", tsp_in_endpoint.c_str());
   }
   if (tsp_out_oss.str() != tsp_out_endpoint) {
      if (tsp_out_endpoint.size())
         tsp_out_socket.unbind(tsp_out_endpoint);
      tsp_out_endpoint = tsp_out_oss.str();
      tsp_out_socket.bind(tsp_out_endpoint);
      vslice::ts_printf("Bound to %s for outgoing TSP comms\n", tsp_out_endpoint.c_str());
   }
}

bool PoolManager::enqueue(int tsp_idx) {
   bool already_queued = false;

   for (auto queued_tsp_idx : tsp_queue) {
      if (queued_tsp_idx == tsp_idx) {
         already_queued = true;
         break;
      }
   }

   if (!already_queued) {
      vslice::ts_printf("TSP %03d: Adding to queue of TSPs ready to receive data.\n", tsp_idx);
      tsp_queue.push_back(tsp_idx);
      return true;
   } else {
      vslice::ts_printf("TSP %03d: Endpoint already in a queue, skipped.\n", tsp_idx);
      return false;
   }
}

int PoolManager::pop_from_queue() {
   int tsp = tsp_queue.front();
   tsp_queue.pop_front();
   return tsp;
}

void PoolManager::RemoveTSP(int tsp_idx) {
   bool already_queued = false;

   for (auto queued_tsp_idx : tsp_queue) {
      if (queued_tsp_idx == tsp_idx) {
         already_queued = true;
         break;
      }
   }

   if(already_queued){
      std::deque<int> new_tsp_queue;

      while(tsp_queue.size()){
         if (tsp_queue.front() != tsp_idx){
            new_tsp_queue.push_back(tsp_queue.front());
         } else {
            vslice::ts_printf("TSP %03d: REMOVED FROM QUEUE\n", tsp_idx);
         }
         tsp_queue.pop_front();
      }

      tsp_queue.swap(new_tsp_queue);
   }

   vslice::ts_printf("TSP %03d: REMOVED FROM POOL\n", tsp_idx);
   tsp_states.erase(tsp_idx);
}

void PoolManager::UpdateODBState(int tsp_idx, int latest_state) {
   if (tsp_states.find(tsp_idx) == tsp_states.end() || latest_state == TSP_OFFLINE) {
      fTspEq->fOdbEqVariables->WIAI("State", tsp_idx, TSP_OFFLINE);
      fTspEq->fOdbEqVariables->WBAI("Receiving", tsp_idx, false);
      fTspEq->fOdbEqVariables->WBAI("Analyzing", tsp_idx, false);
   } else {
      fTspEq->fOdbEqVariables->WIAI("State", tsp_idx, latest_state);
      fTspEq->fOdbEqVariables->WBAI("Receiving", tsp_idx, tsp_states[tsp_idx].active);
      fTspEq->fOdbEqVariables->WBAI("Analyzing", tsp_idx, tsp_states[tsp_idx].analyzing);
      fTspEq->fOdbEqVariables->WIAI("Slices lost", tsp_idx, tsp_states[tsp_idx].tsp_num_lost);
      fTspEq->fOdbEqVariables->WIAI("Slices processed", tsp_idx, tsp_states[tsp_idx].tsp_num_proc);

      if (latest_state == TSP_ANAFINISHED) {
         fTspEq->fOdbEqVariables->WDAI("Analysis time", tsp_idx, tsp_states[tsp_idx].tsp_ana_time);
         fTspEq->fOdbEqVariables->WDAI("Analysis speed", tsp_idx, tsp_states[tsp_idx].tsp_ana_speed);
         fTspEq->fOdbEqVariables->WDAI("Processed bytes", tsp_idx, tsp_states[tsp_idx].tsp_ana_size);
         fTspEq->fOdbEqVariables->WDAI("Analysis CPU usage pct", tsp_idx, tsp_states[tsp_idx].tsp_ana_cpu_pct);
      } else if (latest_state == TSP_TXCOMPLETE) {
         fTspEq->fOdbEqVariables->WDAI("Transfer time", tsp_idx, tsp_states[tsp_idx].tsp_rx_time);
         fTspEq->fOdbEqVariables->WDAI("Slice size kB", tsp_idx, tsp_states[tsp_idx].tsp_slice_kb);
         fTspEq->fOdbEqVariables->WDAI("Data rate", tsp_idx, tsp_states[tsp_idx].tsp_data_rate);
      }
   }
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

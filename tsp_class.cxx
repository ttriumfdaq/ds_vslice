#include "tsp_class.h"
#include "data_structures.h"
#include "json.hpp"
#include "midas.h"
#include "msystem.h"
#include "socket_utils.h"
#include "vslice_utils.h"
#include "zmqpp/zmqpp.hpp"
#include <algorithm>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <pthread.h>
#include <sstream>
#include <stdarg.h>
#include <stdexcept>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>

#include "tsp_message.h"// constants

#define ZMQ_ERROR 100001

using namespace nlohmann;// nlohmann::json -> json

#ifndef HOST_NAME_MAX
// Sometimes defined by system, sometimes not.
#define HOST_NAME_MAX 64
#endif

typedef struct {
   TimeSliceProcessor *obj;
   int connfd;
} ConnectionThreadArgs;

// We need an object that lives a long time to back the arguments passed to
// thread_connection_helper spawn functions.
std::map<int, ConnectionThreadArgs> thread_args;

void *thread_listener_helper(void *arg) {
   TimeSliceProcessor *obj = (TimeSliceProcessor *) arg;
   return obj->thread_listen_for_connections();
}

void *thread_connection_helper(void *arg) {
   ConnectionThreadArgs *arg_cast = (ConnectionThreadArgs *) arg;
   TimeSliceProcessor *obj = arg_cast->obj;
   return obj->thread_handle_connection(arg_cast->connfd);
}

void *thread_processing_helper(void *arg) {
   TimeSliceProcessor *obj = (TimeSliceProcessor *) arg;
   return obj->thread_process_data();
}

TimeSliceProcessor::TimeSliceProcessor() {
   this_frontend_index = -1;
   in_run = false;
   max_event_size_bytes = 0;
   data_listenfd = -1;
   zmq_pm_sub_socket = NULL;
   zmq_pm_push_socket = NULL;
   next_slice_idx = -1;
   curr_slice_idx = -1;
   prev_slice_idx = -1;
   output_file = NULL;
   curr_file_size_bytes = 0;
   next_data_in_size_bytes = 0;
   data_in_size_bytes = 0;
   data_out_size_bytes = 0;
   subrun = 0;
   event_serial = 0;
   sent_txstarted = false;
   sent_txcomplete = false;

   slice_recv_timeout_secs = 20;
   slice_first_fep_recv_time = {0, 0};

   filter = NULL;

   processing_thread = NULL;
   processing_complete = false;

   listener_thread = NULL;

   curr_run_number = 0;
   next_run_start_pending = false;
   next_run_number = 0;
   end_run_pending = false;
   end_run_number = 0;

   cpu_info = vslice::init_cpu_info();
}

TimeSliceProcessor::~TimeSliceProcessor() {
   vslice::ts_printf("(main) ~TimeSliceProcessor destructor called\n");
   // Force closure of all threads that are reading data
   tidy_up_conn_handler_threads(true);

   if (data_listenfd != -1) {
      close(data_listenfd);
   }
   if (listener_thread) {
      pthread_join(*listener_thread, NULL);
      delete listener_thread;
   }

   send_pm_message(TSP_OFFLINE, double(error_state), getpid(), false);

   if (zmq_pm_sub_socket) {
      zmq_pm_sub_socket->close();
   }
   if (zmq_pm_push_socket) {
      zmq_pm_push_socket->close();
   }
   if (zmq_tap_push_socket) {
      zmq_tap_push_socket->close();
   }

   delete zmq_pm_sub_socket;
   delete zmq_pm_push_socket;
   delete filter;
   delete processing_thread;
}

bool TimeSliceProcessor::is_event_ready_to_send() {
   // Data is ready to be sent out after we've finished processing it
   // and have closed the thread that was doing the processing.
   return processing_complete && !processing_thread;
}

INT TimeSliceProcessor::send_data(char *pevent, bool write_to_banks, int max_bank_event_size_bytes) {
   if (!is_event_ready_to_send()) {
      return 0;
   }

   INT midas_bank_size = 0;
   data_out_size_bytes = data_out.get_num_bytes_needed_to_encode();
   std::string human_size_in = vslice::format_bytes(data_in_size_bytes);
   std::string human_size_out = vslice::format_bytes(data_out_size_bytes);

   uint32_t data_out_written;
   uint64_t* payload = (uint64_t*)malloc(data_out_size_bytes);
   data_out.encode(payload, data_out_written);

   if (data_out_size_bytes != data_out_written) {
      cm_msg(MERROR, __FUNCTION__, "Error encoding data - expected to write %u bytes but wrote %u bytes", data_out_size_bytes, data_out_written);
      abort();
   }

   if (pevent && write_to_banks) {
      if (data_out_size_bytes >= max_bank_event_size_bytes) {
         vslice::ts_printf("(main) Event size %d bytes (%s) is too large to be written to midas bank (max size %d bytes)\n", data_out_size_bytes, human_size_out.c_str(), max_event_size_bytes);
      } else {
         bk_init32(pevent);

         uint64_t *pdata;
         char bank_name[5];
         sprintf(bank_name, "T%03d", this_frontend_index);
         bk_create(pevent, bank_name, TID_QWORD, (void **) &pdata);
         memcpy(pdata, payload, data_out_size_bytes);
         pdata += data_out_size_bytes / sizeof(uint64_t);
         bk_close(pevent, pdata);

         vslice::ts_printf("(main) Wrote %d bytes to bank %s\n", data_out_size_bytes, bank_name);

         midas_bank_size = bk_size(pevent);
      }
   }

   if(zmq_tap_push_socket){
      zmqpp::message m;
      char bank_name[5];
      sprintf(bank_name, "T%03d", this_frontend_index);
      m << std::string(bank_name);
      m.add_raw(payload, data_out_size_bytes);
      bool sent = zmq_tap_push_socket->send(m, true);
      if(!sent){
         vslice::ts_printf("(main) Failed to send data to TSP tap\n");
      }
   }

   if (should_write_data() && output_file) {
      // Build midas header
      const size_t header_size = sizeof(EVENT_HEADER) + sizeof(BANK_HEADER) + sizeof(BANK32);
      char event_header[header_size];
      populate_midas_headers(event_header);

      // Actually write the data
      uint64_t max_file_size_bytes = (uint64_t)settings["Max file size (MiB)"] * 1024 * 1024;

      if (data_out_size_bytes + header_size + curr_file_size_bytes > max_file_size_bytes && curr_file_size_bytes > 0 && max_file_size_bytes > 0) {
         // Need to open a new file to keep max file size below user-set limit.
         // If opening the new file fails, output_file will be reset to NULL, and
         // a message will already have been printed.
         open_next_subrun_file();
      }

      if (output_file) {
         // Write midas header
         fwrite((void *) event_header, sizeof(uint32_t), header_size / sizeof(uint32_t), output_file);

         // Write actual payload
         fwrite(payload, sizeof(char), data_out_size_bytes / sizeof(char), output_file);
         vslice::ts_printf("(main) \e[1;31mWrote %d bytes (%s) to subrun %d for slice %d. Pre-filtered size was %d bytes (%s).\e[0;39m\n", data_out_size_bytes, human_size_out.c_str(), subrun, curr_slice_idx, data_in_size_bytes, human_size_in.c_str());
         curr_file_size_bytes += data_out_size_bytes + header_size;
      }
   } else {
      vslice::ts_printf("(main) \e[1;31mSlice %d has reduced from %d bytes (%s) to %d bytes (%s), but not writing to disk for this run\e[0;39m\n", curr_slice_idx, data_in_size_bytes, human_size_in.c_str(), data_out_size_bytes, human_size_out.c_str());
      curr_file_name = "";
   }

   free(payload);
   tidy_up_slice_and_tell_pm();

   return midas_bank_size;
}

INT TimeSliceProcessor::open_next_subrun_file() {
   subrun++;

   if (output_file) {
      close_current_file();
   }

   char filename[1024];
   snprintf(filename, 1024, "%s/run%06d_tsp%03d_subrun%04d.mid", this_run_data_dir.c_str(), curr_run_number, this_frontend_index, subrun);
   vslice::ts_printf("(main) \e[1;31mOpening output file %s\e[0;39m\n", filename);

   output_file = fopen(filename, "w");

   if (!output_file) {
      vslice::ts_printf("(main) Failed to open %s\n", filename);
      return FE_ERR_DRIVER;
   }

   curr_file_name = filename;
   curr_file_size_bytes = 0;

   return SUCCESS;
}

INT TimeSliceProcessor::close_current_file() {
   if (output_file) {
      vslice::ts_printf("(main) \e[1;31mClosing output file %s\e[0;39m\n", curr_file_name.c_str());
      fclose(output_file);
   }

   output_file = NULL;
   return SUCCESS;
}

void TimeSliceProcessor::tidy_up_slice_and_tell_pm(bool was_recv_timeout, bool was_ana_failure) {
   if (debug()) {
      vslice::ts_printf("(main) Tidying up slice %d\n", curr_slice_idx);
   }

   std::chrono::time_point<std::chrono::steady_clock> now = std::chrono::steady_clock::now();
   std::chrono::duration<double> duration = now - timepoint_anastart;

   data_in.reset();
   data_in_size_bytes = 0;
   processing_complete = false;
   processing_failure_message = "";
   slice_first_fep_recv_time = {0, 0};

   filter->reset();

   if (was_ana_failure) {
      send_pm_message(TSP_ANAFAILED, duration.count(), 0);
   } else if (was_recv_timeout) {
      send_pm_message(TSP_RECVTIMEOUT, duration.count(), 0);
   } else {
      send_pm_message(TSP_ANAFINISHED, duration.count(), data_out_size_bytes);
   }

   // send_pm_message() accesses curr_slice_idx, so reset it after
   // telling the PM what work we've done.
   prev_slice_idx = curr_slice_idx;
   curr_slice_idx = -1;

   data_out.reset();
   data_out_size_bytes = 0;
}

void *TimeSliceProcessor::thread_listen_for_connections() {
   while (true) {
      // This call will block until someone connects, or the
      // socket we're listening on is closed.
      int connfd = socket_utils::accept(data_listenfd);

      if (connfd == -1) {
         // Socket we're listening on has closed. Finish this thread.
         break;
      }

      thread_args[connfd].obj = this;
      thread_args[connfd].connfd = connfd;

      connection_threads_mutex.lock();
      connection_threads[connfd] = new pthread_t;
      connection_finished[connfd] = false;
      connection_threads_mutex.unlock();

      if (debug()) {
         vslice::ts_printf("(main) Spawning thread to receive data on accepted connfd %d\n", connfd);
      }

      INT status = pthread_create(connection_threads[connfd], NULL, thread_connection_helper, &thread_args[connfd]);

      if (status != 0) {
         cm_msg(MERROR, __FUNCTION__, "Failed to create thread for handling new connection");
         return NULL;
      }
   }

   return NULL;
}

void *TimeSliceProcessor::thread_handle_connection(int connfd) {
   // This thread is spawned each time an FEP connects to send us data.
   std::string failure_msg;
   uint64_t *payload = NULL;
   char ack = 1;
   uint64_t payload_size = 0;

   if (!socket_utils::wait_for_data(connfd, 1000)) {
      failure_msg = "Timeout waiting for data";
   } else if (!socket_utils::read_payload_size(connfd, payload_size)) {
      failure_msg = "Failed to read payload size";
   }

   if (failure_msg == "") {
      // We've successfully read the payload size from FEP.
      // Now read the data itself, then send an acknowledgment
      // message back to the FEP.

      // Avoid race condition where multiple threads see `sent_txstarted`
      // as false.
      parsed_data_mutex.lock();

      if (!sent_txstarted) {
         // Tell PM that we've started to receive data
         timepoint_txstart = std::chrono::steady_clock::now();
         send_pm_message(TSP_TXSTARTED, 0, 0);
         sent_txstarted = true;
         gettimeofday(&slice_first_fep_recv_time, NULL);
      }

      parsed_data_mutex.unlock();

      payload = (uint64_t*) malloc(payload_size);

      if (payload == NULL) {
         failure_msg = "Failed to alloc enough bytes for receiving data";
      } else if (!socket_utils::read_fully(connfd, (char*)payload, payload_size)) {
         failure_msg = "Failed to read payload content";
      } else if (!socket_utils::write_fully_with_size_protocol(connfd, &ack, 1)) {
         failure_msg = "Failed to send ack";
      }
   }

   if (failure_msg == "") {
      // We've successfully read the data from FEP.
      // Parse the data and add hits to overall `next_data_in` slice.
      Slice this_board;
      uint32_t this_board_size = 0;

      FEPSliceHeader this_header;
      this_header.decode(payload);

      int fe_idx = this_header.fe_idx;
      int slice_idx = this_header.slice_idx;

      uint32_t header_size = this_header.get_num_bytes_needed_to_encode();

      bool decode_okay = this_board.decode(payload + (header_size/sizeof(uint64_t)), this_board_size);

      if (!decode_okay) {
         cm_msg(MERROR, __FUNCTION__, "Failed to decode data for slice %d from FE %d: %s\n", slice_idx, fe_idx, this_board.error_message);
         abort();
      }
      if (header_size + this_board_size != payload_size) {
         cm_msg(MERROR, __FUNCTION__, "Unexpected amount of data parsed for slice %d from FE %d. Payload was %" PRIu64 " bytes but we only parsed %u+%u bytes!\n", slice_idx, fe_idx, payload_size, header_size, this_board_size);
         abort();
      }
      if (slice_idx != this_board.slice_idx) {
         cm_msg(MERROR, __FUNCTION__, "Failed to decode data from FE %d. Header claimed slice %d, but payload claimed slice %d.\n", fe_idx, slice_idx, this_board.slice_idx);
         abort();
      }

      if (debug()) {
         vslice::ts_printf("(main) --> Frontend %u, slice %u, with %u channels with QTs and %u channels with waveforms\n", fe_idx, slice_idx, this_board.channel_qts.size(), this_board.channel_waveforms.size());
      }

      parsed_data_mutex.lock();

      if (fe_idxs_seen.size() == 0) {
         // First FEP for this slice
         next_slice_idx = this_board.slice_idx;
         next_data_in.slice_idx = next_slice_idx;
         next_data_in.data_format_version = this_board.data_format_version;
         next_data_in.slice_start_time_secs = this_board.slice_start_time_secs;
      } else if (this_board.slice_idx == prev_slice_idx) {
         // FE may have thought it failed to send us data and sent it again,
         // but was actually successsful the first time. Ignore the duplicated data.
         vslice::ts_printf("(conn) Ignoring repeated slice %u from frontend %u!\n", this_board.slice_idx, fe_idx);
      } else if (this_board.slice_idx != next_slice_idx) {
         cm_msg(MERROR, __FUNCTION__, "Unexpected slice %u from frontend %u (expected to see %d)!\n", this_board.slice_idx, fe_idx, next_slice_idx);
         abort();
      }

      // Merge hits from this board with overall one
      if (!next_data_in.merge(this_board)) {
         cm_msg(MERROR, __FUNCTION__, "Failed to merge data from FE %d for slice %d.\n", fe_idx, slice_idx);
         abort();
      }

      fe_idxs_seen.push_back(fe_idx);
      next_data_in_size_bytes += payload_size;

      parsed_data_mutex.unlock();
   }

   if (payload) {
      free(payload);
   }

   // We don't close() the connection here, as then linux could
   // reuse the same handle for another connection, before we've
   // tidied up our threads/maps that use the connfd. We'll close
   // the connection when we join this thread.
   connection_finished[connfd] = true;

   return NULL;
}

void *TimeSliceProcessor::thread_process_data() {
   if (debug()) {
      vslice::ts_printf("(proc) Spawned thread for processing data\n");
   }

   // By the time this thread is called, data has been
   // moved from `next_data_in` to `data_in`.

   // Tell PM we're starting to process data
   std::chrono::time_point<std::chrono::steady_clock> now = std::chrono::steady_clock::now();
   timepoint_anastart = now;
   send_pm_message(TSP_ANASTARTED, 0, data_in_size_bytes);

   // Start CPU usage tracking
   vslice::update_cpu_info(cpu_info);

   // Print the data summary to screen for debugging
   if (debug()) {
      vslice::ts_printf("(proc) Sorted data list:\n");

      std::vector<std::shared_ptr<HitWithChannelId>> sorted_hits = data_in.get_hits_sorted_by_time();

      for (auto it : sorted_hits) {
         if (is_eos(it->hit)) {
            vslice::ts_printf("(proc) --> Board %u, end-of-slice, timestamp %lf\n", it->chan.board_id, it->hit->time_since_run_start_secs);
         } else {
            vslice::ts_printf("(proc) --> Board %u, channel %u, timestamp %lf\n", it->chan.board_id, it->chan.channel_id, it->hit->time_since_run_start_secs);
         }
      }
   }

   data_out.slice_idx = data_in.slice_idx;
   data_out.slice_start_time_secs = data_in.slice_start_time_secs;

   // Do the actual filtering
   if (filter->apply(data_in, data_out) != SUCCESS) {
      processing_failure_message = "Failed to process data: " + filter->get_error_message();
      return NULL;
   }

   // Finish CPU usage tracking
   vslice::update_cpu_info(cpu_info);

   processing_complete = true;

   if (debug()) {
      vslice::ts_printf("(proc) Processing of data complete\n");
   }

   return NULL;
}

void TimeSliceProcessor::populate_midas_headers(char* header_buffer) {
   // Populate midas headers for putting all data into a single bank.
   EVENT_HEADER eh = {};
   eh.serial_number = ++event_serial;
   eh.time_stamp = ss_time();

   BANK_HEADER bh = {};
   bh.data_size = 0;
   bh.flags = BANK_FORMAT_32BIT;

   BANK32 bk = {};
   bk.type = TID_QWORD;
   bk.data_size = 0;

   // Need space for trailing \0 when using snprintf.
   // So write bank name to temporary variable before populating
   // the real char[4] of b.name.
   char bank_name[5];
   snprintf(bank_name, 5, "T%03d", this_frontend_index);
   memcpy(bk.name, bank_name, 4);

   eh.data_size = data_out_size_bytes + sizeof(BANK_HEADER) + sizeof(BANK32);
   bh.data_size = data_out_size_bytes + sizeof(BANK32);
   bk.data_size = data_out_size_bytes;

   // Copy the event header
   memcpy(header_buffer, &eh, sizeof(eh));

   // Copy the overall bank header
   memcpy(header_buffer + sizeof(EVENT_HEADER), &bh, sizeof(bh));

   // Copy the bank header
   memcpy(header_buffer + sizeof(EVENT_HEADER) + sizeof(BANK_HEADER), &bk, sizeof(bk));
}

INT TimeSliceProcessor::do_periodic_work() {
   if (next_run_start_pending && !in_run) {
      // We've been told that a new run has started, and we're not doing
      // any work related to the old run, so act on the begin-of-run message.
      handle_delayed_begin_of_run();
   }

   if (end_run_pending && fe_idxs_seen.size() == 0) {
      // We've been told that the run has ended, and we don't have any pending data,
      // so act on the end-of-run message.
      handle_delayed_end_of_run();
   }

   tidy_up_conn_handler_threads();
   handle_pm_sub_payload();

   bool seen_all_fe = seen_all_frontends_for_this_slice();

   if (seen_all_fe && !sent_txcomplete) {
      // Tell PM we've got all data
      std::chrono::time_point<std::chrono::steady_clock> now = std::chrono::steady_clock::now();
      std::chrono::duration<double> duration = now - timepoint_txstart;
      send_pm_message(TSP_TXCOMPLETE, duration.count(), next_data_in_size_bytes);
      sent_txcomplete = true;
   }

   if (seen_all_fe && !processing_thread) {
      // Set up data for processing
      fe_idxs_seen.clear();
      data_in = next_data_in;
      next_data_in.reset();
      data_in_size_bytes = next_data_in_size_bytes;
      next_data_in_size_bytes = 0;
      curr_slice_idx = next_slice_idx;
      next_slice_idx = -1;

      slice_first_fep_recv_time = {0, 0};
      sent_txstarted = false;
      sent_txcomplete = false;

      // Spawn thread for processing data
      processing_thread = new pthread_t;
      INT status = pthread_create(processing_thread, NULL, thread_processing_helper, this);

      if (status != 0) {
         vslice::ts_printf("(main) Data loss - failed to create processing thread. pthread_create status was %d\n", status);
         processing_failure_message = "Failed to create processing thread";
         tidy_up_slice_and_tell_pm(false, true);
      }
   } else if (processing_complete && processing_thread) {
      // Close the thread that was processing data
      // We'll tell the PM after we've written data to disk.
      pthread_join(*(processing_thread), NULL);
      delete processing_thread;
      processing_thread = NULL;
   } else if (processing_failure_message != "" && processing_thread) {
      // Close the thread that was processing data
      pthread_join(*(processing_thread), NULL);
      delete processing_thread;
      processing_thread = NULL;

      // Tell PM that we failed.
      tidy_up_slice_and_tell_pm(false, true);
   } else if (slice_first_fep_recv_time.tv_sec > 0 && !processing_complete && !processing_thread && !seen_all_frontends_for_this_slice()) {
      // See if we've timed-out waiting for all data to arrive
      timeval now;
      gettimeofday(&now, NULL);
      int slice_recv_time = now.tv_sec - slice_first_fep_recv_time.tv_sec;

      if (slice_recv_time > slice_recv_timeout_secs) {
         vslice::ts_printf("(main) Data loss - waited %ds to receive all data for slice %d, but it didn't arrive.\n", slice_recv_time, next_slice_idx);
         tidy_up_slice_and_tell_pm(true);
      }
   }

   heartbeat();
   return SUCCESS;
}

void TimeSliceProcessor::tidy_up_conn_handler_threads(bool force) {
   connection_threads_mutex.lock();
   std::vector<int> connfds;

   if (force) {
      // Close all threads, even if thery're still doing work.
      for (auto it : connection_threads) {
         connfds.push_back(it.first);
      }
   } else {
      // Only close threads that have finished their work.
      for (auto it : connection_finished) {
         if (it.second) {
            connfds.push_back(it.first);
         }
      }
   }

   for (auto connfd : connfds) {
      if (connection_threads.find(connfd) != connection_threads.end()) {
         if (debug()) {
            vslice::ts_printf("(main) Closing thread for connfd %d that has finished\n", connfd);
         }

         close(connfd);

         pthread_join(*(connection_threads[connfd]), NULL);
         delete connection_threads[connfd];
         connection_threads.erase(connfd);
         connection_finished.erase(connfd);

         if (debug()) {
            vslice::ts_printf("(main) Closed thread for connfd %d that has finished\n", connfd);
         }
      }
   }

   connection_threads_mutex.unlock();
}

void TimeSliceProcessor::handle_pm_sub_payload() {
   zmqpp::message msg;

   if (zmq_pm_sub_socket && zmq_pm_sub_socket->receive(msg, true)) {
      std::string cmd;
      msg >> cmd;

      vslice::ts_printf("(main) Received command %s from Pool Manager\n", cmd.c_str());

      if (cmd == "begin_of_run") {
         std::string json_str;
         msg >> json_str;
         json json_data = json::parse(json_str);

         int run_number = json_data["run_number"].get<int>();

         if (run_number == next_run_number) {
            // PM re-broadcasts the BOR info if a TSP joins while a run
            // is in progress. We've already handled the message that was
            // sent at the start of the run, so nothing more for us to do.
            vslice::ts_printf("(main) Already handled BOR for run %d. Ignore duplicated message.\n", run_number);
         } else {
            // We need to actually handle the BOR message. We don't run the
            // "begin_of_run" function immediately though, as we may still
            // be processing data in another thread. We cache the settings,
            // and will handle it once processing is finished.
            next_run_start_pending = true;
            next_run_settings = json_data;
            next_run_number = run_number;

            vslice::ts_printf("(main) Pool Manager has told us that run %d is starting\n", next_run_number);
         }
      } else if (cmd == "end_of_run") {
         msg >> end_run_number;
         end_run_pending = true;
      } else if (cmd == "terminate") {
         uint32_t pid;
         std::string reason;
         msg >> pid >> reason;
         if(pid == getpid()){
             vslice::ts_printf("(main) Pool Manager ordered termination, given reason: '%s'\n", reason.c_str());
             error_state = 1;
             throw(TSP_KILL_PM);
         }
      } else if (cmd == "tsp_tap") {
         msg >> tsp_tap_endpoint;
         vslice::ts_printf("(main) TSP tap %s\n", (tsp_tap_endpoint.size()?tsp_tap_endpoint.c_str():"OFF"));
         if(tsp_tap_endpoint.size() && !zmq_tap_push_socket){
            zmq_tap_push_socket = new zmqpp::socket(zmq_context, zmqpp::socket_type::push);
            zmq_tap_push_socket->connect(tsp_tap_endpoint);
         } else if(!tsp_tap_endpoint.size() && zmq_tap_push_socket){
            zmq_tap_push_socket->close();
            delete zmq_tap_push_socket;
            zmq_tap_push_socket = NULL;
         }
      } else if (cmd == "reregister") {
         send_pm_message(TSP_INIT, 0, getpid());
      } else {
         vslice::ts_printf("(main) Unhandled PM sub command '%s'\n", cmd.c_str());
      }
   }
}

INT TimeSliceProcessor::send_pm_message(uint8_t state, double duration, uint32_t info, bool block) {
   if (debug()) {
      vslice::ts_printf("(main) Sending PM message %d\n", state);
   }

   INT retval;
   switch (state) {
      case TSP_INIT:
      case TSP_HEARTBEAT:
      case TSP_OFFLINE:
      case TSP_IDLE:
      case TSP_TXSTARTED:
      case TSP_TXCOMPLETE:
      case TSP_RECVTIMEOUT:
      case TSP_ANASTARTED:
      case TSP_ANAFAILED:
      case TSP_ANAFINISHED: {
         pm_push_mutex.lock();
         zmqpp::message msg;
         int s_idx = (state == TSP_ANASTARTED || state == TSP_ANAFAILED || state == TSP_ANAFINISHED) ? curr_slice_idx : next_slice_idx;
         msg << (uint8_t) this_frontend_index << data_endpoint << state << s_idx << duration << info;

         if (state == TSP_ANAFINISHED) {
            msg << curr_run_number << curr_file_name.substr(curr_file_name.find_last_of('/')+1) << cpu_info.this_proc_pct;
         } else if (state == TSP_ANAFAILED) {
            msg << processing_failure_message;
         }

         if (zmq_pm_push_socket->send(msg, !block)) {
            retval = SUCCESS;
         } else {
            vslice::ts_printf("(main) ZMQ send error\n");
            retval = ZMQ_ERROR;
         }

         pm_push_mutex.unlock();
         break;
      }
      default:
         vslice::ts_printf("(main) Unknown TSP state '%d'\n", state);
         retval = ZMQ_ERROR;
   }
   return retval;
}

bool TimeSliceProcessor::seen_all_frontends_for_this_slice() {
   if (next_slice_idx < 0) {
      return false;
   }

   parsed_data_mutex.lock();

   std::sort(fe_idxs_expected.begin(), fe_idxs_expected.end());
   std::sort(fe_idxs_seen.begin(), fe_idxs_seen.end());
   bool retval = (fe_idxs_seen == fe_idxs_expected && fe_idxs_expected.size() > 0);

   parsed_data_mutex.unlock();

   return retval;
}

bool TimeSliceProcessor::debug() {
   if (settings.contains("Debug")) {
      return settings["Debug"];
   }

   return true;
}

bool TimeSliceProcessor::should_write_data() {
   if (settings.contains("Write data")) {
      return settings["Write data"];
   }

   return false;
}

double TimeSliceProcessor::get_heartbeat_period() {
   if (settings.contains("Heartbeat (s)")) {
      return settings["Heartbeat (s)"];
   }

   return 1.0;
}

void TimeSliceProcessor::heartbeat(){
    static std::chrono::time_point<std::chrono::steady_clock> tlast = std::chrono::steady_clock::now();
    std::chrono::time_point<std::chrono::steady_clock> t = std::chrono::steady_clock::now();
    std::chrono::duration<double> duration = t -tlast;
    if(duration.count() >= get_heartbeat_period()){
        send_pm_message(TSP_HEARTBEAT, duration.count(), getpid());
        tlast = t;
    }
}

INT TimeSliceProcessor::init() {
   int data_port = 7770 + this_frontend_index;
   char hostname[HOST_NAME_MAX];

   if (hostname_override == "") {
      gethostname(hostname, HOST_NAME_MAX);

      // Replace my_host.localdomain with my_host so others can find us.
      char *ld = strstr(hostname, ".localdomain");
      if (ld != NULL) {
         *ld = '\0';
      }
   } else {
      snprintf(hostname, HOST_NAME_MAX, "%s", hostname_override.c_str());
   }

   std::ostringstream oss;
   oss << hostname << ':' << data_port;
   data_endpoint = oss.str();

   vslice::ts_printf("(main) Binding to port %d for receiving data\n", data_port);

   data_listenfd = socket_utils::bind(data_port);

   if (data_listenfd == -1) {
      // Failed to bind. Port probably already in use. Abort.
      vslice::ts_printf("(main) Failed to bind to port %d\n", data_port);
      error_state = FE_ERR_DRIVER;;
      return FE_ERR_DRIVER;
   }

   listener_thread = new pthread_t;
   INT status = pthread_create(listener_thread, NULL, thread_listener_helper, this);

   if (status != 0) {
      cm_msg(MERROR, __FUNCTION__, "Failed to create listener thread");
      error_state = FE_ERR_DRIVER;;
      return FE_ERR_DRIVER;
   }

   send_pm_message(TSP_INIT, 0, getpid());

   return SUCCESS;
}

void TimeSliceProcessor::set_pool_manager_endpoints(std::string sub_endpoint, std::string push_endpoint) {
   // Socket for receiving info from pool manager
   if (sub_endpoint != "") {
      vslice::ts_printf("(main) Connecting to %s for incoming PM comms\n", sub_endpoint.c_str());
      delete zmq_pm_sub_socket;
      zmq_pm_sub_socket = new zmqpp::socket(zmq_context, zmqpp::socket_type::subscribe);
      zmq_pm_sub_socket->connect(sub_endpoint);

      // Subscriber types MUST choose a topic to subscribe to, even if it's just the default one.
      zmq_pm_sub_socket->subscribe("");
   }

   // Socket for sending info to pool manager
   if (push_endpoint != "") {
      vslice::ts_printf("(main) Connecting to %s for outgoing PM comms\n", push_endpoint.c_str());
      delete zmq_pm_push_socket;
      zmq_pm_push_socket = new zmqpp::socket(zmq_context, zmqpp::socket_type::push);
      zmq_pm_push_socket->connect(push_endpoint);
   }
}

void TimeSliceProcessor::set_expected_frontend_indexes(std::vector<int> expected) {
   fe_idxs_expected = expected;

   vslice::ts_printf("(main) Expect to see data from FEPs: %s\n", vslice::format_list_of_ints(fe_idxs_expected).c_str());
}

INT TimeSliceProcessor::handle_delayed_begin_of_run() {
   INT status = SUCCESS;

   vslice::ts_printf("(main) \e[1;36mHandling begin-of-run for run %d\e[0;39m\n", next_run_number);

   curr_run_number = next_run_number;
   next_run_start_pending = false;
   settings = next_run_settings["tsp_settings"]["TSP"];

   set_expected_frontend_indexes(next_run_settings["fep_list"].get<std::vector<int>>());

   vslice::ts_printf("(main) TSP settings for this run are:\n%s\n", settings.dump(2).c_str());

   curr_file_size_bytes = 0;
   subrun = -1;
   event_serial = 0;

   next_data_in.reset();
   next_data_in_size_bytes = 0;
   data_in.reset();
   data_in_size_bytes = 0;

   next_slice_idx = -1;
   curr_slice_idx = -1;
   prev_slice_idx = -1;
   fe_idxs_seen.clear();

   slice_first_fep_recv_time = {0, 0};
   processing_complete = false;
   processing_failure_message = "";

   if (should_write_data()) {
      this_run_data_dir = next_run_settings["data_dir"].get<std::string>();

      status = open_next_subrun_file();

      if (status != SUCCESS) {
         return status;
      }
   } else {
      vslice::ts_printf("(main) \e[1;31mNot writing data to disk for this run\e[0;39m\n");
   }

   if (settings.contains("Basic time filter") && settings["Basic time filter"]["Enable"]) {
      delete filter;
      filter = new TSPBasicTimeFilter(settings);
   } else {
      delete filter;
      filter = new TSPFilterNone(settings);
   }

   filter->reset();

   in_run = true;
   send_pm_message(TSP_IDLE);

   return SUCCESS;
}

INT TimeSliceProcessor::handle_delayed_end_of_run() {
   end_run_pending = false;

   vslice::ts_printf("(main) \e[1;36mHandling end-of-run for run %d\e[0;39m\n", end_run_number);

   if (should_write_data() && output_file) {
      close_current_file();
   }

   if (filter) {
      filter->reset();
   }

   in_run = false;

   return SUCCESS;
}

void TimeSliceProcessor::get_status_strings(std::string& msg, std::string& color) {
   if (!in_run) {
      msg = "Run finished";
      color = "greenLight";
   } else if (next_slice_idx == -1 && curr_slice_idx == -1 && prev_slice_idx == -1) {
      msg = "Waiting for first slice";
      color = "greenLight";
   } else if (next_slice_idx == -1 && curr_slice_idx == -1) {
      char c_msg[255];
      snprintf(c_msg, 255, "Processed slice %d", prev_slice_idx);
      msg = c_msg;
      color = "greenLight";
   } else if (curr_slice_idx == -1) {
      char c_msg[255];
      snprintf(c_msg, 255, "Receiving slice %d", next_slice_idx);
      msg = c_msg;
      color = "greenLight";
   } else {
      char c_msg[255];
      snprintf(c_msg, 255, "Processing slice %d", curr_slice_idx);
      msg = c_msg;
      color = "greenLight";
   }
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

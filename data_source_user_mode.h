#ifndef DATA_SOURCE_USER_H
#define DATA_SOURCE_USER_H

#include "data_source_base.h"
#include "vx2740_wrapper.h"

/** \file
 * Data source that reads from real VX2740s that are running User firmware.
 */

/** Data source that reads from real VX2740s that are running User firmware.
 */
class DataSourceUserMode : public DataSourceRealBase {
public:
   DataSourceUserMode(HNDLE _hDB, INT _frontend_index, INT _board_id, bool _sw_eos);
   virtual ~DataSourceUserMode();
   virtual INT begin_of_run(double _slice_width_ms) override;
   virtual INT populate_next_event(std::vector<std::shared_ptr<VXData> >& data, BOOL debug) override;

private:
   HNDLE hDB; //!< ODB handle
   INT frontend_index; //!< FEP frontend index, so we can write more user-friendly error messages.
   INT board_id; //!< Board index within this FEP.
   DWORD max_raw_bytes; //!< Max number of bytes we can read from digitizer in a single event.
   bool software_eos; //!< Whether we should inject artificial end-of-slice events.

   uint16_t* waveform; //! Buffer for waveform data read from VX2740.
   uint64_t* user_info; //! Buffer for user info data read from VX2740.

   double slice_width_secs; //!< Width of each slice, for if `software_eos` is true.
   double last_eos_secs; //!< End time of most recent slice, for if `software_eos` is true.
};

#endif

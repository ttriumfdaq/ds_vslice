#include "data_source_scope_mode.h"
#include "data_structures.h"
#include "midas.h"
#include "odbxx.h"
#include "msystem.h"
#include "vslice_utils.h"
#include "caen_event.h"
#include "vx2740_wrapper.h"
#include <vector>
#include <string>

/** \file
 * Data source that reads from real VX2740s that are running Scope firmware.
 */

DataSourceScopeMode::DataSourceScopeMode(HNDLE _hDB, INT _frontend_index, INT _board_id, bool _sw_eos) {
   hDB = _hDB;
   frontend_index = _frontend_index;
   board_id = _board_id;
   software_eos = _sw_eos;
   max_raw_bytes = 0;
   raw_scope_event = NULL;
   slice_width_secs = 0;
   last_eos_secs = 0;
}

INT DataSourceScopeMode::begin_of_run(double _slice_width_ms) {
   if (!enable) {
      return SUCCESS;
   }

   slice_width_secs = _slice_width_ms / 1000.;
   last_eos_secs = 0;

   std::string fw_type;
   vx->params().get_firmware_type(fw_type);

   if (fw_type != "Scope") {
      cm_msg(MERROR, __FUNCTION__, "Board %03d/%02d is running '%s' firmware, not 'Scope'!", frontend_index, board_id, fw_type.c_str());
      return FE_ERR_DRIVER;
   }

   vx->params().get_max_raw_bytes_per_read(max_raw_bytes);

   vslice::ts_printf("Will read at most %u bytes per event from board %s running in scope mode\n", max_raw_bytes, name.c_str());

   if (raw_scope_event) {
      free(raw_scope_event);
      raw_scope_event = NULL;
   }

   raw_scope_event = (unsigned char*) malloc(max_raw_bytes);

   if (raw_scope_event == NULL) {
      return FE_ERR_DRIVER;
   }

   return SUCCESS;
}

INT DataSourceScopeMode::set_board_info(VX2740* _board, bool _enable, std::string _name) {
   DataSourceRealBase::set_board_info(_board, _enable, _name);

   if (vx && enable) {
      uint32_t nsamp;
      vx->params().get_waveform_length_samples(nsamp);

      // Warn user if a waveform length is set that's longer than VSlice can support
      if (nsamp > 0x7FFF) {
         cm_msg(MERROR, __FUNCTION__, "Warning: vertical slice only supports wavelengths up to %d samples, data will be truncated!", 0x7FFF);
      }
   }

   return SUCCESS;
}


INT DataSourceScopeMode::populate_next_event(std::vector<std::shared_ptr<VXData> >& data, BOOL debug) {
   INT status = SUCCESS;

   size_t read_size_bytes;
   status = vx->data().get_raw_data(100, raw_scope_event, read_size_bytes);

   if (status == VX_NO_EVENT) {
      return SUCCESS;
   } else if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to read data from VX2740 %02d (%s)!", board_id, name.c_str());
      return FE_ERR_DRIVER;
   }

   // May have read multiple scope events in one read.
   // Iterate through them all.
   unsigned int offset = 0;

   while (offset < read_size_bytes) {
      CaenEvent ev((uint64_t*) (raw_scope_event + offset));

      if (ev.header.size_64bit_words > read_size_bytes/sizeof(uint64_t)) {
         cm_msg(MERROR, __FUNCTION__, "Error: need to implement handling of segmented events! Event header at offset %u says event size is %d, but we read %d bytes", offset, ev.header.size_64bit_words * (int)sizeof(uint64_t), (int)read_size_bytes);
         return FE_ERR_DRIVER;
      }

      double ts_secs = ev.header.trigger_time / 125e6;

      while (software_eos && (ts_secs - last_eos_secs) > slice_width_secs) {
         // Add a dummy end-of-slice event.
         std::shared_ptr<VXData> eos = std::make_shared<VXData>();
         eos->timestamp_secs = last_eos_secs + slice_width_secs;
         eos->is_end_of_slice = true;
         eos->board_id = board_id;
         data.push_back(eos);
         
         last_eos_secs = eos->timestamp_secs;
      }

      if (ev.header.format == 0x10) {
         // Break up scope event to look like per-channel data
         for (int c = 0; c < 64; c++) {
            if (!(ev.header.ch_enable_mask & ((uint64_t)1) << c)) {
               continue;
            }

            std::shared_ptr<VXData> chan_data = std::make_shared<VXData>();
            chan_data->timestamp_secs = ts_secs;
            chan_data->is_end_of_slice = false;
            chan_data->channel_id = c;
            chan_data->board_id = board_id;
            chan_data->wf_samples = std::move(ev.get_channel_samples_vec(c));
            data.push_back(chan_data);
         }
      } else {
         // Unknown event format - just skip over the data
         vslice::ts_printf("Ignoring event with unknown format 0x%x\n", ev.header.format);
      }

      offset += ev.header.size_bytes();
   }

   return status;
}

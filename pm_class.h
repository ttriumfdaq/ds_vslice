#ifndef PM_CLASS_H
#define PM_CLASS_H

#include <assert.h>
#include <queue>
#include <set>
#include <signal.h>
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <zmqpp/zmqpp.hpp>

#include "midas.h"
#include "odbxx.h"
#include "tmfe_rev0.h"

#include "tsp_message.h"
#include "vslice_utils.h"

namespace vslice {
    void pmcallback(INT hDB, INT hkey, INT index, void *feptr);
}

class PoolManager : public TMFeRpcHandlerInterface,
                    public TMFePeriodicHandlerInterface {
public:
    TMFE *fMfe;
    TMFeEquipment *fEq;
    TMFeEquipment *fTspEq;
    MVOdb *fOdbEqStatus;

    PoolManager(TMFE *mfe, TMFeEquipment *eq, TMFeEquipment *tspeq);

    virtual ~PoolManager() {
        fMfe->Disconnect();
    }

    void Init();

    std::string HandleRpc(const char *cmd, const char *args) {
        fMfe->Msg(MINFO, "HandleRpc", "RPC cmd [%s], args [%s]", cmd, args);
        return "OK";
    }

    void HandleBeginRun();

    void HandleEndRun();

    void HandlePeriodic();
    void PollTSP();
    void fecallback(HNDLE hDB, HNDLE hkey, INT index);

private:
    struct TSPState {
        std::string endpoint;
        uint32_t pid;
        bool assigned;
        bool active;
        bool analyzing;
        bool active_from_previous_run;
        double heartbeat_time;

        double tsp_slice_kb, tsp_rx_time, tsp_data_rate, tsp_ana_time, tsp_ana_speed, tsp_ana_size, tsp_ana_cpu_pct;
        int tsp_num_proc, tsp_num_lost;

        void reset() {
            endpoint = "";
            pid = 0;
            assigned = false;
            active = false;
            analyzing = false;
            active_from_previous_run = false;
            heartbeat_time = 0;
            tsp_num_proc = 0;
            tsp_num_lost = 0;
        }

        void reset_bor() {
            if (active || analyzing) {
                active_from_previous_run = true;
            } else {
                active_from_previous_run = false;
            }

            assigned = false;
            active = false;
            analyzing = false;
            tsp_num_proc = 0;
            tsp_num_lost = 0;
        }
    };

    void UpdateODBState(int tsp_idx, int latest_state);

    std::map<int, TSPState> tsp_states;
    std::deque<int> tsp_queue;

    // Pop next TSP from queue and publish it to FEPs
    bool PubNextTSP();

    // Receive message from TSP socket, if available
    bool RecvTSPMessage(zmqpp::message &message) {
        return tsp_in_socket.receive(message, true);
    }

    void ResetODBStatus();
    int GetRunNumber();

    std::string WriteSliceMap(int run, int slice = -1, std::string filename = std::string());
    // Publish string to connected TSPs
    void PubTSPMessage(zmqpp::message &message);
    void PubTSPBeginOfRun();

    // Extract information from message
    bool ProcessTSPMessage(zmqpp::message &message, std::string& err_msg);
    void OpenSockets();

    // Tell connected TSP to terminate
    void TerminateTSP(uint32_t pid, std::string reason);

    // Tell connected TSPs to send next completed slice to tsp_tap
    void SwitchTSPTap(std::string tsp_tap_endpoint);

    bool enqueue(int tsp_idx);// add endpoint to queue
    int pop_from_queue();      // get and remove next TSP from queue

    int get_tsp_idx_from_endpoint(std::string endpoint);

    void RemoveTSP(int tsp_idx);

    bool debug;
    bool debug_heartbeats;
    int slice_idx_for_next_tsp;
    int last_finished_slice;

    zmqpp::context zmq_context;
    zmqpp::socket fep_socket, tsp_in_socket, tsp_out_socket;
    int tsp_in_port, tsp_out_port, fep_port;
    std::string tsp_in_endpoint, tsp_out_endpoint, fep_endpoint;

    midas::odb tsp_settings;
    midas::odb tsp_tap_readback;
    nlohmann::json tsp_bor_info;

    bool in_run;
    int curr_run_number;

    int tsp_tap = 0;            // number of slices to send from TSPs to tsp_tap to be wirtten to midas banks

    int tsp_heartbeat_timeout_secs;

    std::ofstream slice_map_file;

};

#endif

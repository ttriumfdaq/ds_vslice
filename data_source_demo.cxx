#include "data_source_demo.h"
#include "data_structures.h"
#include "midas.h"
#include "msystem.h"
#include "vslice_utils.h"

/** \file
 * Data source that generates fake data mimicking the VX2740 data format.
 */

#define TX_DEMO_DATA_ODB_SETTINGS_STR "\
Event rate (Hz) = FLOAT : 3\n\
WF size (64bit words) = INT : 100\n\
Channel mask (31-0) = DWORD : 0xFFFFFFFF\n\
Channel mask (63-32) = DWORD : 0xFFFFFFFF\n\
"

DataSourceDemo::DataSourceDemo(HNDLE _hDB, INT _frontend_index, INT _board_id) {
   hDB = _hDB;
   settings_key = 0;
   board_id = _board_id;
}

INT DataSourceDemo::open_records(char *base_odb_path) {
   char settings_path[256];
   snprintf(settings_path, 256, "%s/DemoData/Board %d", base_odb_path, board_id);

   INT status = db_check_record(hDB, 0, settings_path, TX_DEMO_DATA_ODB_SETTINGS_STR, TRUE);

   if (status != DB_SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to check record of %s", settings_path);
      return status;
   }

   status = db_find_key(hDB, 0, settings_path, &settings_key);

   if (status != DB_SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to find key for %s", settings_path);
      return status;
   }

   // Get current values from ODB.
   INT size = sizeof(user_settings);
   status = db_get_record(hDB, settings_key, &user_settings, &size, FALSE);

   if (status != SUCCESS) {
      return status;
   }

   // Open hotlink so we know about ODB changes.
   status = db_open_record(hDB, settings_key, &user_settings, sizeof(user_settings), MODE_READ, NULL, NULL);

   if (status != DB_SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Cannot make hotlink to %s.", settings_path);
      return status;
   }

   return status;
}

INT DataSourceDemo::begin_of_run(double _slice_width_ms) {
   uint64_t ch_mask_lo = user_settings.demo_data_ch_mask_31_0;
   uint64_t ch_mask_hi = user_settings.demo_data_ch_mask_63_32;
   uint64_t ch_mask = ch_mask_hi << 32 | ch_mask_lo;

   gen.set_board_id(board_id);
   gen.set_settings(ch_mask, user_settings.demo_data_rate_Hz, user_settings.demo_data_wf_64bit_word_size, _slice_width_ms / 1000.);
   gen.begin_of_run();

   return SUCCESS;
}

INT DataSourceDemo::populate_next_event(std::vector<std::shared_ptr<VXData> >& data, BOOL debug) {
   return gen.populate_next_event(data, debug);
}
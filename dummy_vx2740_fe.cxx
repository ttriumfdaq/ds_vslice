#include "tmfe_rev0.h"
#include "midas.h"
#include "msystem.h"
#include <iostream>
#include <map>
#include <thread>
#include <stdarg.h>
#include <stdio.h>
#include <chrono>
#include <atomic>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include "vslice_utils.h"
#include "socket_utils.h"
#include "fake_data_generator.h"
#include "data_source_dummy_vx_structs.h"

/** \file
 */

std::atomic<bool> g_stop_gen;

void usage() {
   printf("Usage: dummy_vx2740_fe.exe -i <index> [-b <board_id>]\n");
   printf("  -i should match the fep.exe index\n");
   printf("  If -b is not specified, will simulate all the boards in that FEP\n");
}

std::mutex board_socks_mutex;
std::map<int, int> board_socks;
std::map<int, int> bound_ports;
std::map<int, int> bound_socks;

INT thread_con(int board_id, int sock) {
   int accepted_sock = socket_utils::accept(sock);

   vslice::ts_printf("accept() for board %d returned %d\n", board_id, accepted_sock);

   board_socks_mutex.lock();
   board_socks[board_id] = accepted_sock;
   board_socks_mutex.unlock();
   return SUCCESS;
}

INT thread_gen(HNDLE hDB, int frontend_index, int board_id, std::vector<int> board_ids) {
   bool debug_data = false;
   HNDLE settings_key;
   DummyVxDataSettings user_settings = {};

   char settings_path[256];
   snprintf(settings_path, 256, "/Equipment/FEP_%03d/Settings/DummyVXData/Board %d", frontend_index, board_id);

   INT status = db_check_record(hDB, 0, settings_path, DUMMY_VX_DATA_ODB_SETTINGS_STR, TRUE);

   if (status != DB_SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to check record of %s", settings_path);
      return status;
   }

   status = db_find_key(hDB, 0, settings_path, &settings_key);

   if (status != DB_SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to find key for %s", settings_path);
      return status;
   }

   // Get current values from ODB.
   INT size = sizeof(user_settings);
   status = db_get_record(hDB, settings_key, &user_settings, &size, FALSE);

   if (status != SUCCESS) {
      return status;
   }

   double slice_width_ms = 0;
   int size_double = sizeof(double);
   status = db_get_value(hDB, 0, "/FEP shared/Slice width (ms)", &slice_width_ms, &size_double, TID_DOUBLE, FALSE);

   if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to read FEP slice width.");
      return FE_ERR_ODB;
   }

   uint64_t ch_mask_lo = user_settings.demo_data_ch_mask_31_0;
   uint64_t ch_mask_hi = user_settings.demo_data_ch_mask_63_32;
   uint64_t ch_mask = ch_mask_hi << 32 | ch_mask_lo;

   FakeDataGenerator gen;
   gen.set_board_id(board_id);
   gen.set_settings(ch_mask, user_settings.demo_data_rate_Hz, user_settings.demo_data_wf_64bit_word_size, slice_width_ms/1000.);
   
   bool debug = false;

   board_socks_mutex.lock();

   if (bound_ports.find(board_id) == bound_ports.end() || bound_ports[board_id] != user_settings.port) {
      vslice::ts_printf("Binding to port %d for board %d\n", user_settings.port, board_id);
      int new_bound_sock = socket_utils::bind(user_settings.port);

      if (new_bound_sock < 0) {
         board_socks_mutex.unlock();
         cm_msg(MERROR, __FUNCTION__, "Failed to bind to port %d: got code %d", user_settings.port, new_bound_sock);
         return FE_ERR_ODB;
      }

      bound_socks[board_id] = new_bound_sock;
      bound_ports[board_id] = user_settings.port;
   }

   int bound_sock = bound_socks[board_id];
   board_socks_mutex.unlock();

   std::thread con(thread_con, board_id, bound_sock);
   
   // Wait until we accept a connection or we're told to abort.
   // We do the accept in a separate thread as accept() blocks
   // until we accept a connection or the socket is closed, so
   // we need to be able to close() the socket from this thread
   // when needed.
   while (!con.joinable()) {
      std::this_thread::sleep_for(std::chrono::milliseconds(10));

      if (g_stop_gen) {
         // Closing socket will force accept() to return in connection thread.
         vslice::ts_printf("Forcing bound socket for board %d to close\n", board_id);
         close(bound_sock);
         return FE_ERR_DRIVER;
      }
   }

   con.join();

   while (!g_stop_gen) {
      bool all_accepted = true;
      board_socks_mutex.lock();

      for (auto board_id : board_ids) {
         if (board_socks.find(board_id) == board_socks.end()) {
            all_accepted = false;
         } else if (board_socks[board_id] < 0) {
            g_stop_gen = true;
         }
      }

      board_socks_mutex.unlock();

      if (all_accepted) {
         vslice::ts_printf("All boards have accepted connection - start generating data for board %d\n", board_id);
         break;
      }

      std::this_thread::sleep_for(std::chrono::milliseconds(10));
   }

   board_socks_mutex.lock();
   int accepted_sock = board_socks[board_id];
   board_socks_mutex.unlock();

   uint64_t* buf = (uint64_t*)calloc(100000, sizeof(uint64_t));
   gen.begin_of_run();

   while (!g_stop_gen) {
      std::vector<std::shared_ptr<VXData>> evs;
      gen.populate_next_event(evs, debug);

      for (auto& ev: evs) {
         uint32_t size_bytes = ev->get_encoded_size_bytes();
         ev->encode(buf);

         if (!socket_utils::write_fully_with_size_protocol(accepted_sock, (char*)buf, size_bytes)) {
            vslice::ts_printf("Failed to send event of size %u from board %d!\n", size_bytes, board_id);
            g_stop_gen = true;
            break;
         } else if (debug_data) {
            vslice::ts_printf("Wrote event of size %u with timestamp %lf from board %d\n", size_bytes, ev->timestamp_secs, board_id);
         }
      }
   }

   free(buf);

   return SUCCESS;
}

class VXRpcHandler : public TMFeRpcHandlerInterface {
public:
   VXRpcHandler(HNDLE _hDB, TMFeEquipment* _eq, int _frontend_index, int _board_index) {
      hDB = _hDB;
      eq = _eq;
      frontend_index = _frontend_index;

      if (_board_index >= 0) {
         // Sngle board
         board_ids.push_back(_board_index);
      } else {
         // All boards
         char settings_path[256];
         snprintf(settings_path, 256, "/Equipment/FEP_%03d/Settings/DummyVXData/Num boards (read at init)", frontend_index);
         INT num_boards = 0;
         INT size_int = sizeof(INT);
         
         if (db_get_value(hDB, 0, settings_path, &num_boards, &size_int, TID_INT, FALSE) != SUCCESS) {
            cm_msg(MERROR, __FUNCTION__, "Couldn't read %s. Assuming 1 board", settings_path);
            num_boards = 1;
         }

         for (int i = 0; i < num_boards; i++) {
            board_ids.push_back(i);
         }
      }
   }

   virtual ~VXRpcHandler() {};

   void HandleBeginRun() override {
      char error[255];
      g_stop_gen = false;

      board_socks.clear();

      vslice::ts_printf("Spawning generation threads\n");
      for (auto board_id : board_ids) {
         gen_threads[board_id] = std::thread(thread_gen, hDB, frontend_index, board_id, board_ids);
      }

      // Don't check for acceptance here. This program's BOR runs before
      // the FEP BOR. The FEP will say that there was a problem connecting
      // if needed, then our HandleStartAbortRun() will be called, which will
      // clean up any pending connections.

      in_run = true;

   }

   void HandleEndRun() override {
      char error[255];
      vslice::ts_printf("Stopping generation at end of run\n");
      g_stop_gen = true;

      vslice::ts_printf("Joining any generation threads\n");
      
      for (auto& gt : gen_threads) {
         gt.second.join();
      }

      vslice::ts_printf("Closing any connections we accepted\n");

      for (auto& bs : board_socks) {
         close(bs.second);
      }

      gen_threads.clear();

      in_run = false;
   }

   void HandleStartAbortRun() override {
      HandleEndRun();
      in_run = false;
   }

   void do_periodic_work() {
      if (!in_run) {
        return;
      }
   }

   bool is_event_ready_to_send() {
      return false;
   }

   INT send_data(char* pevent) {
      // We send data over sockets, not to midas buffers.
      return 0;
   }

   void update_variables() {

   }

   void get_status_strings(std::string& status, std::string& color) {
      if (in_run) {
        status = "Running";
      } else {
        status = "Idle";
      }
      color = "--var(mgreen)";
   }

private:
   HNDLE hDB;
   TMFeEquipment* eq;
   int frontend_index;
   std::vector<int> board_ids;
   std::map<int, std::thread> gen_threads;
   bool in_run = false;
};

int main(int argc, char *argv[]) {
   signal(SIGPIPE, SIG_IGN);

   int frontend_index = -1;
   int board_index = -1;

   for (int i = 1; i < argc; i++) {
      if (strlen(argv[i]) != 2 || argv[i][0] != '-' || i + 1 >= argc || argv[i + 1][0] == '-') {
         usage();
         return 1;
      }

      if (argv[i][1] == 'i') {
         frontend_index = atoi(argv[++i]);
      } else if (argv[i][1] == 'b') {
         board_index = atoi(argv[++i]);
      } else {
         usage();
         return 1;
      }
   }

   if (frontend_index < 0) {
      usage();
      return 1;
   }

   char name[255];

   if (board_index < 0) {
      snprintf(name, 255, "DummyVX_%03d_allboards", frontend_index);
   } else {
      snprintf(name, 255, "DummyVX_%03d_%02d", frontend_index, board_index);
   }

   TMFE* mfe = TMFE::Instance();

   TMFeError err = mfe->Connect(name, __FILE__);

   if (err.error) {
      printf("Cannot connect: %s\n", err.error_string.c_str());
      return 1;
   }

   TMFeCommon* common = new TMFeCommon();
   common->EventID = 199;
   common->LogHistory = 0;
   common->Buffer = "SYSTEM";

   TMFeEquipment* eq = new TMFeEquipment(mfe, name, common);
   eq->Init();
   eq->SetStatus("Starting...", "white");
   eq->ZeroStatistics();
   eq->WriteStatistics();

   INT max_event_size = 50 * 1024 * 1024;

   std::cout << "Registering equipment" << std::endl;
   VXRpcHandler rpc_handler(mfe->fDB, eq, frontend_index, board_index);

   mfe->RegisterEquipment(eq);
   mfe->RegisterRpcHandler(&rpc_handler);

   std::cout << "Registered" << std::endl;
   // Must be before FEP handles BOR
   mfe->SetTransitionSequenceStart(499);

   std::cout << "Started" << std::endl;
   eq->SetStatus("Running...", "greenLight");

   char* buffer = (char*) malloc(max_event_size + 100);

   timeval last_stats_update;
   gettimeofday(&last_stats_update, NULL);

   while (!mfe->fShutdownRequested) {
      rpc_handler.do_periodic_work();

      if (rpc_handler.is_event_ready_to_send()) {
         eq->ComposeEvent(buffer, max_event_size);
         char* pevent = buffer + sizeof(EVENT_HEADER);
         int midas_size_bytes = rpc_handler.send_data(pevent);

         if (midas_size_bytes > 0) {
            ((EVENT_HEADER*)buffer)->data_size = midas_size_bytes;
            eq->SendEvent(buffer);
         }
      }

      mfe->PollMidas(1);

      timeval now;
      gettimeofday(&now, NULL);

      // Update stats every second
      if (now.tv_sec > last_stats_update.tv_sec) {
         eq->WriteStatistics();

         rpc_handler.update_variables();

         std::string status, color;
         rpc_handler.get_status_strings(status, color);
         eq->SetStatus(status.c_str(), color.c_str());

         last_stats_update = now;
      }
   }

   eq->SetStatus("Frontend stopped", "redLight");

   mfe->Disconnect();

   return 0;
}

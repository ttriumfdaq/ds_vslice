#include <assert.h>// assert()
#include <signal.h>// SIGPIPE
#include <stdio.h>
#include <stdlib.h>// malloc()

#include "midas.h"
#include "tmfe_rev0.h"

#include "pm_class.h"

/** \file
 * Interface between midas frontend framework and PoolManager class.
 */

static void usage() {
   fprintf(stderr, "Usage: PoolManager\n");
   exit(1);
}

int main(int argc, char *argv[]) {
   // setbuf(stdout, NULL);
   // setbuf(stderr, NULL);

   signal(SIGPIPE, SIG_IGN);

   std::string name = "PoolManager";

   if (argc == 2) {
      name = argv[1];
   }

   TMFE *mfe = TMFE::Instance();

   TMFeError err = mfe->Connect(name.c_str(), __FILE__);
   if (err.error) {
      printf("Cannot connect, bye.\n");
      return 1;
   }

   //mfe->SetWatchdogSec(0);

   TMFeCommon *common = new TMFeCommon();
   common->EventID = 1;
   common->LogHistory = 1;
   //common->Buffer = "SYSTEM";

   TMFeEquipment *eq = new TMFeEquipment(mfe, name.c_str(), common);
   eq->Init();
   eq->SetStatus("Starting...", "white");
   eq->ZeroStatistics();
   eq->WriteStatistics();

   mfe->RegisterEquipment(eq);

   TMFeEquipment *tspeq = new TMFeEquipment(mfe, "TSP_Pool", common);
   tspeq->Init();
   tspeq->SetStatus("Starting...", "white");
   tspeq->ZeroStatistics();
   tspeq->WriteStatistics();

   mfe->RegisterEquipment(tspeq);

   PoolManager *pm = new PoolManager(mfe, eq, tspeq);

   mfe->RegisterRpcHandler(pm);

   // PM must handle BOR after all the FEPs have handled BOR
   // (as FEPs clear their list of where to send data to at BOR;
   // and PM sends the first slice destination in its BOR).
   mfe->SetTransitionSequenceStart(600);

   pm->Init();

   mfe->RegisterPeriodicHandler(eq, pm);
   mfe->RegisterPeriodicHandler(tspeq, pm);

   eq->SetStatus("Started...", "white");

   while (!mfe->fShutdownRequested) {
      mfe->PollMidas(10);
      pm->PollTSP();
   }
   eq->SetStatus("Frontend stopped", "redLight");
   tspeq->SetStatus("Frontend stopped", "redLight");

   mfe->Disconnect();

   return 0;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

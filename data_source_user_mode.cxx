#include "data_source_user_mode.h"
#include "data_structures.h"
#include "midas.h"
#include "odbxx.h"
#include "msystem.h"
#include "vslice_utils.h"
#include "vx2740_wrapper.h"
#include <vector>
#include <string>

/** \file
 * Data source that reads from real VX2740s that are running User firmware.
 */

DataSourceUserMode::DataSourceUserMode(HNDLE _hDB, INT _frontend_index, INT _board_id, bool _sw_eos) {
   hDB = _hDB;
   frontend_index = _frontend_index;
   board_id = _board_id;
   software_eos = _sw_eos;
   max_raw_bytes = 0;
   slice_width_secs = 0;
   last_eos_secs = 0;

   waveform = (uint16_t*) calloc(0x8000, sizeof(uint16_t));
   user_info = (uint64_t*) calloc(0x100, sizeof(uint64_t));
}

DataSourceUserMode::~DataSourceUserMode() {
   free(waveform);
}

INT DataSourceUserMode::begin_of_run(double _slice_width_ms) {
   if (!enable) {
      return SUCCESS;
   }

   slice_width_secs = _slice_width_ms / 1000.;
   last_eos_secs = 0;

   std::string fw_type;
   vx->params().get_firmware_type(fw_type);

   if (fw_type != "USER_DPP") {
      cm_msg(MERROR, __FUNCTION__, "Board %03d/%02d is running '%s' firmware, not 'USER_DPP'!", frontend_index, board_id, fw_type.c_str());
      return FE_ERR_DRIVER;
   }

   vx->params().get_max_raw_bytes_per_read(max_raw_bytes);

   vslice::ts_printf("Will read at most %u bytes per event from board %s running in scope mode\n", max_raw_bytes, name.c_str());

   return SUCCESS;
}

INT DataSourceUserMode::populate_next_event(std::vector<std::shared_ptr<VXData> >& data, BOOL debug) {
   INT status = SUCCESS;

   int timeout_ms = 100;
   uint8_t channel_id;
   uint64_t timestamp;
   size_t waveform_size;
   uint8_t flagsA;
   uint16_t flagsB;
   size_t user_info_size;

   status = vx->data().get_decoded_user_data(timeout_ms, channel_id, timestamp, waveform_size, flagsA, flagsB, user_info_size, waveform, user_info);

   if (status == VX_NO_EVENT) {
      return SUCCESS;
   } else if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to read data from VX2740 %02d (%s)!", board_id, name.c_str());
      return FE_ERR_DRIVER;
   }

   std::shared_ptr<VXData> ev = std::make_shared<VXData>();
   ev->channel_id = channel_id;
   ev->timestamp_secs = (double)timestamp/125e6;
   ev->wf_samples.resize(waveform_size);

   if (waveform_size > 0) {
      memcpy((void*)&(ev->wf_samples.front()), waveform, waveform_size*sizeof(uint16_t));
   }

   // Hardware end-of-slice event
   if (flagsA & 0x1) {
      ev->is_end_of_slice = true;
   }

   // Add a dummy end-of-slice event if needed.
   if (software_eos && (ev->timestamp_secs - last_eos_secs) > slice_width_secs) {
      std::shared_ptr<VXData> eos = std::make_shared<VXData>();
      eos->timestamp_secs = last_eos_secs + slice_width_secs;
      eos->is_end_of_slice = true;
      eos->board_id = board_id;
      data.push_back(eos);

      last_eos_secs = eos->timestamp_secs;
   }

   data.push_back(ev);

   return status;
}

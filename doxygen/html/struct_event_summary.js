var struct_event_summary =
[
    [ "energy_keV", "struct_event_summary.html#a623f809a93716b4bc9d3f1c5c0b8bc5a", null ],
    [ "pos_x_mm", "struct_event_summary.html#a805214ceb394af9fb1016b3468cc57c5", null ],
    [ "pos_y_mm", "struct_event_summary.html#afd40da1bc73c47fb0558f94ccd4a3590", null ],
    [ "pos_z_mm", "struct_event_summary.html#a55d3e83f9d73193e34aad716e9f06fa3", null ],
    [ "time_since_run_start_secs", "struct_event_summary.html#a2e7d908ff9267cdff08363cb98b7005f", null ]
];
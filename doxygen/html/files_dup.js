var files_dup =
[
    [ "data_source_base.h", "data__source__base_8h.html", "data__source__base_8h" ],
    [ "data_source_demo.cxx", "data__source__demo_8cxx.html", "data__source__demo_8cxx" ],
    [ "data_source_demo.h", "data__source__demo_8h.html", [
      [ "DataSourceDemo", "class_data_source_demo.html", "class_data_source_demo" ],
      [ "TxDemoDataSettings", "struct_data_source_demo_1_1_tx_demo_data_settings.html", "struct_data_source_demo_1_1_tx_demo_data_settings" ]
    ] ],
    [ "data_source_scope_mode.cxx", "data__source__scope__mode_8cxx.html", null ],
    [ "data_source_scope_mode.h", "data__source__scope__mode_8h.html", [
      [ "DataSourceScopeMode", "class_data_source_scope_mode.html", "class_data_source_scope_mode" ]
    ] ],
    [ "data_source_user_mode.cxx", "data__source__user__mode_8cxx.html", null ],
    [ "data_source_user_mode.h", "data__source__user__mode_8h.html", [
      [ "DataSourceUserMode", "class_data_source_user_mode.html", "class_data_source_user_mode" ]
    ] ],
    [ "data_structures.cxx", "data__structures_8cxx.html", "data__structures_8cxx" ],
    [ "data_structures.h", "data__structures_8h.html", "data__structures_8h" ],
    [ "fep.cxx", "fep_8cxx.html", "fep_8cxx" ],
    [ "fep_class.cxx", "fep__class_8cxx.html", "fep__class_8cxx" ],
    [ "fep_class.h", "fep__class_8h.html", [
      [ "FrontEndProcessor", "class_front_end_processor.html", "class_front_end_processor" ],
      [ "SliceInfo", "class_front_end_processor_1_1_slice_info.html", "class_front_end_processor_1_1_slice_info" ],
      [ "BoardReadout", "struct_front_end_processor_1_1_board_readout.html", "struct_front_end_processor_1_1_board_readout" ]
    ] ],
    [ "fep_filters.cxx", "fep__filters_8cxx.html", null ],
    [ "fep_filters.h", "fep__filters_8h.html", [
      [ "FEPFilterBase", "class_f_e_p_filter_base.html", "class_f_e_p_filter_base" ],
      [ "FEPFilterNone", "class_f_e_p_filter_none.html", "class_f_e_p_filter_none" ],
      [ "FEPFilterDownsample", "class_f_e_p_filter_downsample.html", "class_f_e_p_filter_downsample" ]
    ] ],
    [ "pm.cxx", "pm_8cxx.html", "pm_8cxx" ],
    [ "pm_class.cxx", "pm__class_8cxx.html", null ],
    [ "pm_class.h", "pm__class_8h.html", "pm__class_8h" ],
    [ "socket_utils.cxx", "socket__utils_8cxx.html", "socket__utils_8cxx" ],
    [ "socket_utils.h", "socket__utils_8h.html", "socket__utils_8h" ],
    [ "tsp_class.cxx", "tsp__class_8cxx.html", "tsp__class_8cxx" ],
    [ "tsp_class.h", "tsp__class_8h.html", "tsp__class_8h" ],
    [ "tsp_filters.cxx", "tsp__filters_8cxx.html", null ],
    [ "tsp_filters.h", "tsp__filters_8h.html", [
      [ "TSPFilterBase", "class_t_s_p_filter_base.html", "class_t_s_p_filter_base" ],
      [ "TSPFilterNone", "class_t_s_p_filter_none.html", "class_t_s_p_filter_none" ],
      [ "TSPBasicTimeFilter", "class_t_s_p_basic_time_filter.html", "class_t_s_p_basic_time_filter" ]
    ] ],
    [ "tsp_message.h", "tsp__message_8h.html", "tsp__message_8h" ],
    [ "tsp_midasfe.cxx", "tsp__midasfe_8cxx.html", "tsp__midasfe_8cxx" ],
    [ "tsp_standalone.cxx", "tsp__standalone_8cxx.html", "tsp__standalone_8cxx" ],
    [ "tsp_tap.cxx", "tsp__tap_8cxx.html", "tsp__tap_8cxx" ],
    [ "tsp_tap_class.cxx", "tsp__tap__class_8cxx.html", "tsp__tap__class_8cxx" ],
    [ "tsp_tap_class.h", "tsp__tap__class_8h.html", "tsp__tap__class_8h" ],
    [ "tsp_test.cxx", "tsp__test_8cxx.html", "tsp__test_8cxx" ],
    [ "vslice_utils.cxx", "vslice__utils_8cxx.html", "vslice__utils_8cxx" ],
    [ "vslice_utils.h", "vslice__utils_8h.html", "vslice__utils_8h" ]
];
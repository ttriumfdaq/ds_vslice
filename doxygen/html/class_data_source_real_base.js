var class_data_source_real_base =
[
    [ "DataSourceRealBase", "class_data_source_real_base.html#aaa2dc7d9afb53085fa1649cdfab892c6", null ],
    [ "~DataSourceRealBase", "class_data_source_real_base.html#aac7ebe37c402a7c82f4fe3877ae1fb55", null ],
    [ "get_acq_status", "class_data_source_real_base.html#a87d51ab78835b0485023205d2f2c3d26", null ],
    [ "get_temperatures", "class_data_source_real_base.html#a6998c1ef9e37918ae240335b9e506bc6", null ],
    [ "is_enabled", "class_data_source_real_base.html#a92499d42f8f88fbb5124b64f1e24c8d5", null ],
    [ "set_board_info", "class_data_source_real_base.html#ab1de7197c2ab8c0f84743b5042a9e008", null ],
    [ "enable", "class_data_source_real_base.html#a0a2b577d354a204577d5889ea416dbbb", null ],
    [ "name", "class_data_source_real_base.html#aa159ec71757b440f1e93390259d36120", null ],
    [ "vx", "class_data_source_real_base.html#ae109f62e578987f039d5e7e914582ef2", null ]
];
var vslice__utils_8h =
[
    [ "cpu_info", "structvslice_1_1cpu__info.html", "structvslice_1_1cpu__info" ],
    [ "empty_ring_buffer", "vslice__utils_8h.html#a6b1d2f175623519be89ef469e38558f2", null ],
    [ "format_bytes", "vslice__utils_8h.html#ab8268266dd04c6ffa83efa0e3a1dbb5e", null ],
    [ "format_list_of_ints", "vslice__utils_8h.html#af06f73a1c5a626e2bc605e8185d53b30", null ],
    [ "get_running_frontend_indexes", "vslice__utils_8h.html#ab557f8c3e67a3f2538fc5f959f87b790", null ],
    [ "init_cpu_info", "vslice__utils_8h.html#af5e0e379b72776a7b7e186c9f464acbe", null ],
    [ "odbxx_to_json", "vslice__utils_8h.html#a0c7e2a2eca9b4591e7af022780b4545a", null ],
    [ "ts_printf", "vslice__utils_8h.html#a01f6522384c60977f4a2dacb6be15902", null ],
    [ "update_cpu_info", "vslice__utils_8h.html#ae3a937cd1164557a5fc48f45b636bc39", null ]
];
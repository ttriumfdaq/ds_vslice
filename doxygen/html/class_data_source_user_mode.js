var class_data_source_user_mode =
[
    [ "DataSourceUserMode", "class_data_source_user_mode.html#aaf5059d2423f4d8e9dd552129e859698", null ],
    [ "~DataSourceUserMode", "class_data_source_user_mode.html#ae75295c5060ca0bc09d181ce980675a1", null ],
    [ "begin_of_run", "class_data_source_user_mode.html#a88ccf5f53567fe77b278a5aabcad6052", null ],
    [ "populate_next_event", "class_data_source_user_mode.html#a27e2c2860e22e967254f123380f1c882", null ],
    [ "board_id", "class_data_source_user_mode.html#ac055611a7994020afe0363a6601f2652", null ],
    [ "frontend_index", "class_data_source_user_mode.html#a19971e3011f1d9722b24707ea764accd", null ],
    [ "hDB", "class_data_source_user_mode.html#acadbf15f1c58ad61a1bbb1d1d068e007", null ],
    [ "last_eos_secs", "class_data_source_user_mode.html#af4c9723a0f95de1fe8ae12f441989d39", null ],
    [ "max_raw_bytes", "class_data_source_user_mode.html#ac55851a7e128707e7a9b3932e93dfc75", null ],
    [ "slice_width_secs", "class_data_source_user_mode.html#ad67a3c60be31ad9c71afbbf2b274cee5", null ],
    [ "software_eos", "class_data_source_user_mode.html#a9f1227f1e1ab6176ba893c62b530b934", null ],
    [ "waveform", "class_data_source_user_mode.html#a24bd7a6179ecd01cd1d1c050a3e954c0", null ]
];
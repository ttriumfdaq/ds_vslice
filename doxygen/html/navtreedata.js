/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Darkside-20k DAQ vertical slice", "index.html", [
    [ "Vertical slice overview", "index.html#autotoc_md21", [
      [ "Compiling", "index.html#autotoc_md22", [
        [ "Dependencies", "index.html#autotoc_md23", null ],
        [ "This code", "index.html#autotoc_md24", null ],
        [ "Executables", "index.html#autotoc_md25", null ]
      ] ],
      [ "Quick-start usage", "index.html#autotoc_md26", null ],
      [ "Program details", "index.html#autotoc_md27", [
        [ "PM usage", "index.html#autotoc_md28", null ],
        [ "FEP usage", "index.html#autotoc_md29", null ],
        [ "TSP usage", "index.html#autotoc_md30", null ],
        [ "TSP Tap usage", "index.html#autotoc_md31", null ]
      ] ]
    ] ],
    [ "Data format and introspection", "md__data__format.html", [
      [ "Data format", "md__data__format.html#autotoc_md1", null ],
      [ "Unfiltered FEP data in midas buffers", "md__data__format.html#autotoc_md2", null ],
      [ "Filtered FEP data in midas buffers", "md__data__format.html#autotoc_md3", null ],
      [ "Filtered TSP data in midas buffers", "md__data__format.html#autotoc_md4", null ],
      [ "Filtered TSP data on disk", "md__data__format.html#autotoc_md5", null ]
    ] ],
    [ "Developer overview", "md__developer__overview.html", [
      [ "Code design", "md__developer__overview.html#autotoc_md7", [
        [ "Midas integration", "md__developer__overview.html#autotoc_md8", null ]
      ] ],
      [ "Communication protocols", "md__developer__overview.html#autotoc_md9", [
        [ "PM > TSP", "md__developer__overview.html#autotoc_md10", null ],
        [ "TSP > PM", "md__developer__overview.html#autotoc_md11", null ],
        [ "TSP > TSP Tap", "md__developer__overview.html#autotoc_md12", null ],
        [ "PM > FEP", "md__developer__overview.html#autotoc_md13", null ],
        [ "FEP > TSP", "md__developer__overview.html#autotoc_md14", null ]
      ] ],
      [ "Detailed program descriptions", "md__developer__overview.html#autotoc_md15", null ],
      [ "Tests", "md__developer__overview.html#autotoc_md16", null ]
    ] ],
    [ "ODB settings", "md__o_d_b_settings.html", [
      [ "PM ODB settings", "md__o_d_b_settings.html#autotoc_md18", null ],
      [ "FEP ODB settings", "md__o_d_b_settings.html#autotoc_md19", null ],
      [ "TSP ODB settings", "md__o_d_b_settings.html#autotoc_md20", null ]
    ] ],
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"class_t_s_p_basic_time_filter.html#aabaca5ed50c9328884833e33462272b0",
"group___t_s_p___s_t_a_t_e___g_r_o_u_p.html#ga96011978007f7404c2327637ea94cc36"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
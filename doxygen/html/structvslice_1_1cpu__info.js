var structvslice_1_1cpu__info =
[
    [ "cpu_sys", "structvslice_1_1cpu__info.html#a44d4f105d03a3e17ceed3f5c6e873595", null ],
    [ "cpu_tot", "structvslice_1_1cpu__info.html#a0fb22497de21fb1c7d84cf86c4c06467", null ],
    [ "cpu_user", "structvslice_1_1cpu__info.html#aea318d73775b98094a0e0c3a750ab8ce", null ],
    [ "num_processors", "structvslice_1_1cpu__info.html#a9d1e9abf0268871f783c3518a7e695b5", null ],
    [ "this_proc_pct", "structvslice_1_1cpu__info.html#a8743aa1e9e74b06f92675a20682680d2", null ]
];
var class_v_x_data =
[
    [ "VXData", "class_v_x_data.html#a1e3387c37ef5a5bb594bd5bf7a84ca2e", null ],
    [ "decode", "class_v_x_data.html#a8e1b54e6253705b07e069f92c21722c1", null ],
    [ "decode_header", "class_v_x_data.html#a87d87878065c408505f41c0479249654", null ],
    [ "encode", "class_v_x_data.html#a9383fc86c88b3cea90f5fa230166960f", null ],
    [ "get_encoded_size_bytes", "class_v_x_data.html#a72e6f997c78db1c411b69324d78010eb", null ],
    [ "num_header_words", "class_v_x_data.html#a8dc6d0256541837f03c1584d7b622ee6", null ],
    [ "num_waveform_words", "class_v_x_data.html#a692d432342eb6e33c03ce5145f3f5b38", null ],
    [ "board_id", "class_v_x_data.html#a78eb42c86bf7cbedb43df776fc7367d4", null ],
    [ "channel_id", "class_v_x_data.html#a7a0e446eff5b201fb78a816f9bbf9322", null ],
    [ "is_end_of_slice", "class_v_x_data.html#ae4c3c9c9a262904eedeade625e839fd5", null ],
    [ "timestamp_secs", "class_v_x_data.html#a28153bc1608a1de56ab9c0cd0d07d46c", null ],
    [ "wf_samples", "class_v_x_data.html#a1ea98afed8878b20f5e95cc4878d1c68", null ]
];
var class_data_source_scope_mode =
[
    [ "DataSourceScopeMode", "class_data_source_scope_mode.html#ace34d97689b1d6e524497dd3ded409c9", null ],
    [ "~DataSourceScopeMode", "class_data_source_scope_mode.html#a10eb685b1f26b2462bf7abed7c81f3be", null ],
    [ "begin_of_run", "class_data_source_scope_mode.html#af9d1c0b12b7f7c359cbcb822e449abae", null ],
    [ "populate_next_event", "class_data_source_scope_mode.html#a48d95e8c3c19dcb7907525aa51820093", null ],
    [ "set_board_info", "class_data_source_scope_mode.html#a62c04601624b2229d62ae7da773ff1be", null ],
    [ "board_id", "class_data_source_scope_mode.html#a3fec7819597f9162f80941e204e8df45", null ],
    [ "frontend_index", "class_data_source_scope_mode.html#ac2c366c2d02ecc135ebc193eec112e14", null ],
    [ "hDB", "class_data_source_scope_mode.html#afaf1502ac51e3aeb55c7fdef7efef754", null ],
    [ "last_eos_secs", "class_data_source_scope_mode.html#adf8b5c228b2a05809c5af2f9a4948214", null ],
    [ "max_raw_bytes", "class_data_source_scope_mode.html#a9211de187f8f582d4062af638f985495", null ],
    [ "raw_scope_event", "class_data_source_scope_mode.html#a621c9eb0083513e5ba7155f706c44e80", null ],
    [ "slice_width_secs", "class_data_source_scope_mode.html#af106366478f388dc22566f72052da43a", null ],
    [ "software_eos", "class_data_source_scope_mode.html#a96c04c0b4f252c5360154c3ac2ff9267", null ]
];
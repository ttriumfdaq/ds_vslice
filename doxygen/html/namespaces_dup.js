var namespaces_dup =
[
    [ "socket_utils", "namespacesocket__utils.html", [
      [ "accept", "namespacesocket__utils.html#a50726001372189da183b44e602dca621", null ],
      [ "bind", "namespacesocket__utils.html#a71eaae8a1e59a8c8a35b252928877268", null ],
      [ "connect", "namespacesocket__utils.html#a09c340459a6f04545a68d90aee6760ca", null ],
      [ "read_fully", "namespacesocket__utils.html#a51d7f12949dc3ef383e40c9bfb41dd9b", null ],
      [ "read_fully_with_size_protocol", "namespacesocket__utils.html#a183469b55ff4325683a991978c7e6259", null ],
      [ "read_payload_size", "namespacesocket__utils.html#a5f0ac44353931824d9c411dcba6a43a3", null ],
      [ "wait_for_data", "namespacesocket__utils.html#a1ebbdfc514b61e21d97edf909b39a09e", null ],
      [ "write_fully", "namespacesocket__utils.html#a69a6ef96dcb61813a33024e7c60a6185", null ],
      [ "write_fully_with_size_protocol", "namespacesocket__utils.html#a77e638cde5dc9b1e16b655b096141378", null ],
      [ "write_payload_size", "namespacesocket__utils.html#a7c841ee6510f522967cc35ce02d22c93", null ]
    ] ],
    [ "vslice", "namespacevslice.html", "namespacevslice" ]
];
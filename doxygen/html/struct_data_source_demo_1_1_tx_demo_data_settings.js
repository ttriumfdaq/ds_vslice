var struct_data_source_demo_1_1_tx_demo_data_settings =
[
    [ "demo_data_ch_mask_31_0", "struct_data_source_demo_1_1_tx_demo_data_settings.html#a41030a714af0dbc61f3aa135863f0094", null ],
    [ "demo_data_ch_mask_63_32", "struct_data_source_demo_1_1_tx_demo_data_settings.html#ab848cb7f274b93781952251d16c89039", null ],
    [ "demo_data_rate_Hz", "struct_data_source_demo_1_1_tx_demo_data_settings.html#af7c02b3ee275f91149cedca4b01de8d1", null ],
    [ "demo_data_wf_64bit_word_size", "struct_data_source_demo_1_1_tx_demo_data_settings.html#ad08bb534cdfbe1d582dca4484a2b9b55", null ]
];
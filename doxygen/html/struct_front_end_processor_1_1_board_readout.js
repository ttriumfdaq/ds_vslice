var struct_front_end_processor_1_1_board_readout =
[
    [ "curr_slice_filter_bytes_total", "struct_front_end_processor_1_1_board_readout.html#a138e7d4c1a77601652ca1675af95246e", null ],
    [ "curr_slice_raw_bytes_total", "struct_front_end_processor_1_1_board_readout.html#a14630f0fa97b970314bbac8f1ad18758", null ],
    [ "data_source", "struct_front_end_processor_1_1_board_readout.html#a23c1399fb46eebd11116700a0712a1f7", null ],
    [ "filter_output", "struct_front_end_processor_1_1_board_readout.html#a091ffab8775b1f7b8bf2eae610e3e8d9", null ],
    [ "last_slice_filter_bytes_total", "struct_front_end_processor_1_1_board_readout.html#a83c80e074fdf23d21d1016fb39198101", null ],
    [ "last_slice_raw_bytes_total", "struct_front_end_processor_1_1_board_readout.html#ace5f2f5370247a777cb57911734a7215", null ],
    [ "num_end_slices_seen", "struct_front_end_processor_1_1_board_readout.html#a36302cef75ec6d578c0d282260dddc9e", null ],
    [ "raw_rb_handle", "struct_front_end_processor_1_1_board_readout.html#a3efe911641f115ad54d508874d2b62cb", null ]
];
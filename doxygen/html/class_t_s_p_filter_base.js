var class_t_s_p_filter_base =
[
    [ "TSPFilterBase", "class_t_s_p_filter_base.html#a57d517eed940e2cea356175abb661c06", null ],
    [ "~TSPFilterBase", "class_t_s_p_filter_base.html#ad9251890750e19d07c3328607aa2b6ba", null ],
    [ "apply", "class_t_s_p_filter_base.html#a7c5844ea1d82bf0e9941b8b9bce98e41", null ],
    [ "get_error_message", "class_t_s_p_filter_base.html#a6ad29e9226c70aadd8c34bba77a51e5c", null ],
    [ "reset", "class_t_s_p_filter_base.html#acca44dbe348a045fa6bbb7e4fa3e3b49", null ],
    [ "error_message", "class_t_s_p_filter_base.html#a94debdaa4545f33d5f60bd07f2179fbe", null ]
];
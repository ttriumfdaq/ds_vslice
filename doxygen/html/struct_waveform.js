var struct_waveform =
[
    [ "~Waveform", "struct_waveform.html#afe871149df16f9cd13db35327c2b18b4", null ],
    [ "decode", "struct_waveform.html#aa6c5e065ecc13300f30bf32d2e082e4a", null ],
    [ "encode", "struct_waveform.html#a33e87e104e5c2beb748f054acf0ce9b0", null ],
    [ "get_num_bytes_needed_to_encode", "struct_waveform.html#ac75025b39c4821bac516afcb6b679854", null ],
    [ "algorithms_applied", "struct_waveform.html#ae5ce0ec508c021a8614beca2b531802e", null ],
    [ "samples", "struct_waveform.html#a51f7bae44d9cae62145f3e01ea05806d", null ]
];
var searchData=
[
  ['board_5fid_714',['board_id',['../class_data_source_demo.html#ad11f612b3dbf930dd883c0a07a7868e5',1,'DataSourceDemo::board_id()'],['../class_data_source_scope_mode.html#a3fec7819597f9162f80941e204e8df45',1,'DataSourceScopeMode::board_id()'],['../class_data_source_user_mode.html#ac055611a7994020afe0363a6601f2652',1,'DataSourceUserMode::board_id()'],['../class_v_x_data.html#a78eb42c86bf7cbedb43df776fc7367d4',1,'VXData::board_id()'],['../struct_channel_id.html#a4bf2b3d382aee5bb0953f1a6ef4c08a4',1,'ChannelId::board_id()']]],
  ['board_5findex_715',['board_index',['../struct_f_e_p_board_thread_args.html#abdcd8bab0d395b0a5829e14e7516dffb',1,'FEPBoardThreadArgs']]],
  ['board_5freadout_716',['board_readout',['../class_front_end_processor.html#af864a813038730481ac1b822741e8f06',1,'FrontEndProcessor']]]
];

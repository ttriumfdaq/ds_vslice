var searchData=
[
  ['kboarddataflagnextchannel_194',['kBoardDataFlagNextChannel',['../class_slice.html#a40b79e27acd092d91f5e9521b1a46380',1,'Slice']]],
  ['keep_5fms_195',['keep_ms',['../class_t_s_p_basic_time_filter.html#a4962ba6f34fd3268458501480270ffa0',1,'TSPBasicTimeFilter']]],
  ['kglobalflaglastword_196',['kGlobalFlagLastWord',['../class_slice.html#a636d9b9ceff962fc9449833ca2b02b46',1,'Slice']]],
  ['khitflagqt_197',['kHitFlagQT',['../class_slice.html#a6d01f82800c8a056e486bd57d7d1b239',1,'Slice']]],
  ['khitflagwaveform_198',['kHitFlagWaveform',['../class_slice.html#a808db2df51cab0c5be543c90ba511d34',1,'Slice']]],
  ['kmaxdataformatversion_199',['kMaxDataFormatVersion',['../class_slice.html#a0f602766b6d2e0b31f656f760e21cd86',1,'Slice']]],
  ['ksectionflagchanneldata_200',['kSectionFlagChannelData',['../class_slice.html#a8ca64d9644d5d0ce118fafe242c462a8',1,'Slice']]],
  ['ksectionflageventsummaries_201',['kSectionFlagEventSummaries',['../class_slice.html#ad39675ae2e6d3e7fcd7ec5d5cb1da8ba',1,'Slice']]]
];

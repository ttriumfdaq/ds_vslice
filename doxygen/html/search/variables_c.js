var searchData=
[
  ['name_817',['name',['../class_data_source_real_base.html#aa159ec71757b440f1e93390259d36120',1,'DataSourceRealBase']]],
  ['next_5frun_5fnumber_818',['next_run_number',['../class_time_slice_processor.html#a4591fc1274f70aaf660b524ba6a21b93',1,'TimeSliceProcessor']]],
  ['next_5frun_5fsettings_819',['next_run_settings',['../class_time_slice_processor.html#a53fd999078b40bed21ea098fa84457d9',1,'TimeSliceProcessor']]],
  ['next_5frun_5fstart_5fpending_820',['next_run_start_pending',['../class_time_slice_processor.html#aa9d24fc48e210acde943d3bea89d3bbe',1,'TimeSliceProcessor']]],
  ['ntap_821',['ntap',['../class_tsp_tap.html#a00f823bd01a3d22cf300b06116fbc109',1,'TspTap']]],
  ['ntapped_822',['ntapped',['../class_tsp_tap.html#a374ed3854d90f98ee750bccb5c9673d7',1,'TspTap']]],
  ['num_5fboards_823',['num_boards',['../class_front_end_processor.html#a7286839475fde2678ad90a5c3eefe651',1,'FrontEndProcessor']]],
  ['num_5fend_5fslices_5fseen_824',['num_end_slices_seen',['../struct_front_end_processor_1_1_board_readout.html#a36302cef75ec6d578c0d282260dddc9e',1,'FrontEndProcessor::BoardReadout']]],
  ['num_5fprocessors_825',['num_processors',['../structvslice_1_1cpu__info.html#a9d1e9abf0268871f783c3518a7e695b5',1,'vslice::cpu_info']]],
  ['num_5fslices_5flost_826',['num_slices_lost',['../class_front_end_processor.html#af0aba9b23ae84fbde408f3c40f388ef7',1,'FrontEndProcessor']]]
];

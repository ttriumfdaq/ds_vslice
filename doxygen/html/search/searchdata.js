var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuvwz~",
  1: "bcdefhpqstvw",
  2: "sv",
  3: "dfoprstv",
  4: "abcdefghimnoprstuvw~",
  5: "abcdefghiklmnoprstuvwz",
  6: "f",
  7: "d",
  8: "cdhmptvz",
  9: "c",
  10: "do"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};


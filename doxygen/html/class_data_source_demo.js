var class_data_source_demo =
[
    [ "TxDemoDataSettings", "struct_data_source_demo_1_1_tx_demo_data_settings.html", "struct_data_source_demo_1_1_tx_demo_data_settings" ],
    [ "DataSourceDemo", "class_data_source_demo.html#a736f018858f8d2461805f87e7bdcaf5e", null ],
    [ "~DataSourceDemo", "class_data_source_demo.html#a3ac20abda7d61511368c3527a17b01bb", null ],
    [ "begin_of_run", "class_data_source_demo.html#a361179be8d2e908a6325171ee91d9cff", null ],
    [ "calc_next_gap", "class_data_source_demo.html#acbfd74204d891884aef1831532a9be57", null ],
    [ "open_records", "class_data_source_demo.html#ab41730a7895a702518bd48eb20ecf649", null ],
    [ "populate_next_event", "class_data_source_demo.html#a219d8817fb9eb92c2ddc9a7426a7a708", null ],
    [ "board_id", "class_data_source_demo.html#ad11f612b3dbf930dd883c0a07a7868e5", null ],
    [ "demo_data_slice_width_secs", "class_data_source_demo.html#a7931fe4867eaf108579ffdde0b4de961", null ],
    [ "gap_to_next_event_secs", "class_data_source_demo.html#af3689beee17270fcf1170b9f160a2308", null ],
    [ "hDB", "class_data_source_demo.html#a685fb7aced061e3b12f291d2b07ced0a", null ],
    [ "settings_key", "class_data_source_demo.html#ac5e0e69f2cca2eef4c0095c367709168", null ],
    [ "time_bor", "class_data_source_demo.html#aad93e62d5aa2909c001c061a341526b0", null ],
    [ "time_last_event_gen", "class_data_source_demo.html#a18663a70b73406b1b5e870b03a2cfbdd", null ],
    [ "time_last_slice_stop", "class_data_source_demo.html#a76f88e56fb5d4e5a7d6eb202e9ec5438", null ],
    [ "user_settings", "class_data_source_demo.html#a5f7b1c881b363fdd4ca0299fae9315b5", null ]
];
var hierarchy =
[
    [ "FrontEndProcessor::BoardReadout", "struct_front_end_processor_1_1_board_readout.html", null ],
    [ "ChannelId", "struct_channel_id.html", null ],
    [ "ConnectionThreadArgs", "struct_connection_thread_args.html", null ],
    [ "vslice::cpu_info", "structvslice_1_1cpu__info.html", null ],
    [ "DataSourceBase", "class_data_source_base.html", [
      [ "DataSourceDemo", "class_data_source_demo.html", null ],
      [ "DataSourceRealBase", "class_data_source_real_base.html", [
        [ "DataSourceScopeMode", "class_data_source_scope_mode.html", null ],
        [ "DataSourceUserMode", "class_data_source_user_mode.html", null ]
      ] ]
    ] ],
    [ "EventSummary", "struct_event_summary.html", null ],
    [ "FEPBoardThreadArgs", "struct_f_e_p_board_thread_args.html", null ],
    [ "FEPFilterBase", "class_f_e_p_filter_base.html", [
      [ "FEPFilterDownsample", "class_f_e_p_filter_downsample.html", null ],
      [ "FEPFilterNone", "class_f_e_p_filter_none.html", null ]
    ] ],
    [ "FEPSliceHeader", "class_f_e_p_slice_header.html", null ],
    [ "FilteredAndUnfiltered", "struct_filtered_and_unfiltered.html", null ],
    [ "FrontEndProcessor", "class_front_end_processor.html", null ],
    [ "HitBase", "struct_hit_base.html", [
      [ "EndOfSlice", "struct_end_of_slice.html", null ],
      [ "QT", "struct_q_t.html", null ],
      [ "Waveform", "struct_waveform.html", null ]
    ] ],
    [ "HitWithChannelId", "struct_hit_with_channel_id.html", null ],
    [ "Slice", "class_slice.html", null ],
    [ "FrontEndProcessor::SliceInfo", "class_front_end_processor_1_1_slice_info.html", null ],
    [ "sort_hits_by_time", "structsort__hits__by__time.html", null ],
    [ "TimeSliceProcessor", "class_time_slice_processor.html", null ],
    [ "TMFePeriodicHandlerInterface", null, [
      [ "PoolManager", "class_pool_manager.html", null ],
      [ "TspTap", "class_tsp_tap.html", null ]
    ] ],
    [ "TMFeRpcHandlerInterface", null, [
      [ "FEPRpcHandler", "class_f_e_p_rpc_handler.html", null ],
      [ "PoolManager", "class_pool_manager.html", null ],
      [ "TSPRpcHandler", "class_t_s_p_rpc_handler.html", null ],
      [ "TspTap", "class_tsp_tap.html", null ]
    ] ],
    [ "TSPFilterBase", "class_t_s_p_filter_base.html", [
      [ "TSPBasicTimeFilter", "class_t_s_p_basic_time_filter.html", null ],
      [ "TSPFilterNone", "class_t_s_p_filter_none.html", null ]
    ] ],
    [ "DataSourceDemo::TxDemoDataSettings", "struct_data_source_demo_1_1_tx_demo_data_settings.html", null ],
    [ "VXData", "class_v_x_data.html", null ]
];
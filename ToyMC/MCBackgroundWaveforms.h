#ifndef MC_BKG_WAVEFORMS_H
#define MC_BKG_WAVEFORMS_H
//Like the MC background class for the toy studies but instead generates the waveforms only

#include <string>
#include <vector>
#include <random>
#include <fstream>

#include "data_structures.h"

//Data structure
struct segStruct{
	//Time of wf segment (in seconds)
	double segTime;
	//Number of PE
	int nPE;
	//Index of the selected waveform
	int selWF;
	//Index of the fep, the board and the channel
	int fepID;
	int boardID;
	int channelID;

};

class MCBackgroundWaveforms{
    public:
        //Constructor single gamma emitter etc.
	//PEFile contains the grouped PE for this backgrounds
	//inWF is datastructure containing the individual generated waveforms for this file
        MCBackgroundWaveforms(std::string backgroundName, std::string PEFile, const std::map<int,std::vector<int>>& sipmToFEPMapping, const std::vector<std::vector<VXData*>>& inWF, double rate, double efficiency, bool prefiltered);
        
	//Constructor for dark noise.
        MCBackgroundWaveforms(const std::vector<std::vector<VXData*>>& inWF, double rate, int nFEPs, int nBoardsPerFEP);

        std::string getName() {return backName;};
        double getRate() {return bkgRate;};
	
	//Method to generate a bunch of digitiser data between the start and end times
	//defined
	std::vector<segStruct> generateTimeSpan(double startTime, double endTime);
    
    private:
        std::string backName;
        //Background rate (activity of the samples in Bq)
        double bkgRate;
        //DAQ Efficiency (i.e what fraction of the decays produce an event through the DAQ)
        double daqEfficiency;

	//bool are we generating dark noise or are we generating a standard background
	bool isDarkNoise;

	//int the number of FEPs to generate dark noise
	int numFEPs;
	int numBoardsPerFEP;
	int numChannels = 64;
	//Number of single PE waveforms for DCR selection
	int numSinglePEWFs;
        
        //Generator stuff
        std::mt19937 gen;
        //Used to determine the number of decays in the time period
        std::poisson_distribution<int> rateGen;
        //Used to determine the efficiency of each decay
        std::uniform_real_distribution<double> effGen;
        
	//Select random events from the list of the events
	std::uniform_int_distribution<int> eventSel;
	
	//Datastructure containing a reference of the pre-generated waveforms
	//2D vector as we have grouped it by PE (first element is 0 PE and so on)
	std::vector<std::vector<VXData*>>const *  wfDict;

	//Vector containing the segments which pass onto the next time window
	std::vector<segStruct> nextWindowSegs;
	
	std::vector<segStruct> generateDarkNoise(double startTime, double endTime);
	
	//Datastructure containing the PE times of all the pulses
	//vector of vectors inner vector is vector of WF segments for that
	//event
	std::vector<std::vector<segStruct>> eventList;

	//Method to load the PE times from a file: Code only loads channels flagged in selChannels as we have 4 FEPS we don't need to load the entire detector into memory.
	void loadWFSegments(std::string infile, std::map<int,std::vector<int>> selChannels, bool prefiltered);

	std::ofstream logStream;
        
};

#endif
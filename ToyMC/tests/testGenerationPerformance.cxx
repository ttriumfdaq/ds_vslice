#include <map>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <chrono>


#include "ToyMCWaveforms.h"

ToyMCWaveforms * mc;


int main(int argc, char * argv[]){
	//Catch Ctrl-C correctly by deleting ToyMC (Destructor disconnects from ODB correctly)
	//First create the waveform class
	mc = new ToyMCWaveforms("/zssd/home1/dsdaq/stringer/inputEvents/TestIndividualWaveforms.root","/zssd/home1/dsdaq/stringer/inputEvents/mapping1.json",24.0);
	//Now add the chain backgrounds
	mc->AddBackgroundList("Th232 Reflectors","/zssd/home1/dsdaq/stringer/inputEvents/InputJSONFiles/RatesAndEfficiencies/skimmed_mapping1/Th232Reflectors_Skimmed_mapping1.json",true);
	mc->AddBackgroundList("U238 Reflectors", "/zssd/home1/dsdaq/stringer/inputEvents/InputJSONFiles/RatesAndEfficiencies/skimmed_mapping1/U238Reflectors_Skimmed_mapping1.json",true);
	mc->AddBackgroundList("Th232 SiPMs", "/zssd/home1/dsdaq/stringer/inputEvents/InputJSONFiles/RatesAndEfficiencies/skimmed_mapping1/Th232SiPMs_Skimmed_mapping1.json",true);
	mc->AddBackgroundList("U238 SiPMs", "/zssd/home1/dsdaq/stringer/inputEvents/InputJSONFiles/RatesAndEfficiencies/skimmed_mapping1/U238SiPMs_Skimmed_mapping1.json",true);
	
	//Now create the argon 39 background
	mc->AddBackground("Ar39", "/zssd/home1/dsdaq/stringer/inputEvents/InputJSONFiles/Ar39TPC/e-_Skimmed_mapping1.json", 36.5, 1.0,true);
	mc->AddBackground("K40 Reflectors", "/zssd/home1/dsdaq/stringer/inputEvents/InputJSONFiles/K40Reflectors/K-40_Skimmed_mapping1.json", 58, 0.06167,true);
	mc->AddBackground("K40 SiPMs", "/zssd/home1/dsdaq/stringer/inputEvents/InputJSONFiles/K40SiPMs/K-40_Skimmed_mapping1.json", 594.22, 0.04961,true);
	mc->AddBackground("Co60 Steel Structure", "/zssd/home1/dsdaq/stringer/inputEvents/InputJSONFiles/Co60SteelStructure/Co-60_Skimmed_mapping1.json", 14, 0.22199771,true);
	
		

	while(true){
		auto startGen = std::chrono::high_resolution_clock::now();
		mc->threadGenerateData(true);
	}

    	return 0;

}

#include <circular_buffer.h>
#include <iostream>
#include <cassert>

using namespace std;

bool compareArrays(double * arr1, double * arr2, size_t siz){
	bool arrEq = true;
	cout << "Arr1: [";
	for(size_t i=0; i<siz; i++){
		cout << arr1[i]<<", ";
		arrEq = arrEq & (arr1[i]==arr2[i]);
	}
	cout << "]"<<endl;
	
	cout << "Arr2: [";
	for(size_t i=0; i<siz; i++){
		cout << arr2[i]<<", ";
	}
	cout << "]"<<endl;

	return arrEq;
}

int main(int argc, char * argv[]){

	//Create elements to go in the buffer
	double elem1 = 0;

	double elem2 = 1;

	double elem3 = 2;

	//Create a circular buffer with 3 elements
	circular_buffer<double> buff(3);
	
	//Try and get an element without filling 
	//Code should exit
	//double * val = buff.get();
	
	cout << "Is Buffer Empty (Should be true): "<<buff.empty()<<endl;
	
	//Add elements to the list
	buff.put(elem1);
	buff.put(elem2);
	cout << "Is Buffer Full (Should be false): "<<buff.full()<<endl;
	cout << "Is Buffer Empty (Should be false): "<<buff.empty()<<endl;
	buff.put(elem3);


	cout << "Is Buffer Empty (Should be false): "<<buff.empty()<<endl;
	cout << "Is Buffer Full (Should be true): "<<buff.full()<<endl;

	//Now get the elements and check they are equal to the way we inserted them

	double val1 = buff.get();
	cout << "Expected: "<<elem1<<" Got: "<<val1<<endl;
	
	double val2 = buff.get();
	cout << "Expected: "<<elem2<<" Got: "<<val2<<endl;
	
	double val3 = buff.get();
	cout << "Expected: "<<elem3<<" Got: "<<val3<<endl;
	
	cout << "Is Buffer Empty (Should be true): "<<buff.empty()<<endl;
	cout << "Is Buffer Full (Should be false): "<<buff.full()<<endl;

	//Now try and overfill the buffer to make sure we exit correctly
	buff.put(elem1);
	buff.put(elem2);
	buff.put(elem3);
	buff.put(elem1);


}

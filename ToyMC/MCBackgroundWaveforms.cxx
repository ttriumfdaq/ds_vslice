#include "MCBackgroundWaveforms.h"
#include <string>
#include <iostream>
#include <fstream>
#include "json.hpp"
#include "data_structures.h"
#include "vslice_utils.h"

#include <chrono>
#include <iomanip>
#include <climits>
#include <cstdlib>
#include <regex>
#include <experimental/filesystem>

using namespace std;
using json = nlohmann::json;
namespace fs = std::experimental::filesystem;


MCBackgroundWaveforms::MCBackgroundWaveforms(string backgroundName, string PEFile, const map<int,vector<int>>& sipmToFEPMapping,const vector<vector<VXData*>>& inWF,double rate,double efficiency, bool prefiltered){
	vslice::ts_printf("Configuring background %s at %lfHz...\n", backgroundName.c_str(), rate);

	//Set up variables
	wfDict = &inWF;
	backName = backgroundName;
	daqEfficiency = efficiency;
	bkgRate = rate;
	//Now set the list of WF segements to match the selected channels for each of the FEPS
	loadWFSegments(PEFile,sipmToFEPMapping, prefiltered);	

	//Also setup the generators
        effGen = uniform_real_distribution<double>(0.0,1.0);
	//Lets set the seed to the backgrou,0nd name
	seed_seq s (backgroundName.begin(),backgroundName.end());
	//Initialise the Random number generator
	gen = mt19937(s);
	//Set up the event selection generator
	eventSel = uniform_int_distribution<int>(0,eventList.size()-1);

	//Setup log file
	stringstream nameStream;
	nameStream << std::getenv("TOYMC_LOGS_DIR");
	nameStream <<"/ToyMCLogs/";
	//Now check if the directory exists	
	bool isDir = fs::is_directory(fs::path(nameStream.str()));
	if(! isDir){
		cout << "MCBackgroundWaveforms::Unable to find directory: "<<nameStream.str()<<" to create log file in "<<endl;
		exit(1);
	}
	nameStream << backgroundName << "_";
	time_t t = std::time(nullptr);
	auto tm = *localtime(&t);
	nameStream << put_time( &tm, "%Y-%m-%d_%H:%M" ) << ".log";
	string fileName = nameStream.str();
	fileName = regex_replace(fileName,regex(" "),"_");
	cm_msg(MINFO, __FUNCTION__, "Outputting logging information to: %s",fileName.c_str());
	logStream = ofstream(fileName, ios::out);
	//Do the MD5Sum on the datafile
	logStream << "Input event file is: "<<PEFile<<endl;
	stringstream cmdStream;
	string peConv = regex_replace(PEFile,regex(" "),"\\ ");
	cmdStream << "md5sum "<<peConv;
	string md5Sum = vslice::exec(cmdStream.str().c_str());
	logStream << "MD5Sum of file is: "<<md5Sum << endl;
	logStream.flush();
	//Constructing a regular background
	isDarkNoise = false;
}
        
MCBackgroundWaveforms::MCBackgroundWaveforms(const vector<vector<VXData*>>& inWF, double rate, int nFEPs, int nBoardsPerFEP){
	vslice::ts_printf("Configuring dark noise at %lfHz...\n", rate);

	//We are generating dark noise
	isDarkNoise = true;
	wfDict = &inWF;
	backName = "darkNoise";

	numFEPs = nFEPs;
	numBoardsPerFEP = nBoardsPerFEP;
	//Set the numSinglePE
	numSinglePEWFs = static_cast<int>(inWF[0].size());
	
	//Background rate for darknoise is the rate per channel * num Channels
	bkgRate = rate * nFEPs * nBoardsPerFEP * 64;

	//Also setup the generators
	//For dark noise eff gen sets the time distribution in the window
	effGen = uniform_real_distribution<double>(0.0,1.0);
	seed_seq s (backName.begin(),backName.end());
	//Initialise the Random number generator
	gen = mt19937(s);
	//We will use eventSel to get the 1PE dark noise waveforms and choose the
	//channelID boardID and fepID
	eventSel = uniform_int_distribution<int>(0,INT_MAX);

}

//This method generates all the events for the time range for this background
//Returns an UNSORTED list of waveform segments
vector<segStruct> MCBackgroundWaveforms::generateTimeSpan(double startTime, double endTime){
	//If we are generating darkNoise use the specific function
	if(isDarkNoise){
		return generateDarkNoise(startTime, endTime);
	}
	//Otherwize just generate a regular background
	//First we need to get the number of events generated
	double timeWindow = endTime-startTime;
	//logStream << "Time window: "<<timeWindow<<endl;
	rateGen = poisson_distribution<int>(bkgRate*timeWindow);
	int nDecays = rateGen(gen);
	//logStream << "ndecays: "<<nDecays<<endl;
	//Now we need to check if each of these decays produce a 
	//triggered event
	int trigCount = 0;
	//Check which of these decays generate a trigger
	for(int i=0; i<nDecays; i++){
		//Generate random number 
		double effTest = effGen(gen);
		if(effTest < daqEfficiency){
			trigCount++;
		}

	}
	//logStream << "ntriggers: "<<trigCount<<endl;
	//Now generate these decays throughout the time window 
	vector<double> evTimes (trigCount,0);
	for(int i=0; i<trigCount; i++){
		//Reuse effGen to generate the event times
		evTimes[i] = startTime+effGen(gen)*(endTime-startTime);
	}

	//Now for each event load a random event from the library
	vector<segStruct> outWFs;
	//First of all add the segments from the previous event into this event
	for(auto v : nextWindowSegs){
		outWFs.push_back(v);
	}
	//Now empty the vector 
	nextWindowSegs.clear();
	for(int i=0; i<trigCount; i++){
		//First select the event 
		int selEV = eventSel(gen);
		//logStream <<"Event: "<<i<<" Selected event: "<<selEV<<" Event time: "<<evTimes[i]<<endl;
		//Now iterate through all the wf segments in this selection
		for(int j=0; j<eventList[selEV].size(); j++){
			segStruct outS = eventList[selEV][j];
			//Select randomly from the library 
			//First lets just make a copy of the object
			//This can be optimized in the future as we dont need to
			//explicitly copy the waveform
			//First set the start time of th event
			//This will be eventTIme+timeWaveformSegment in the event
			outS.segTime += evTimes[i];
			//Save nPE to another variable
			int nPE = outS.nPE;
			//If we are above the maximum in our waveform library just use the maximum
			if(wfDict->size() < nPE){
				//logStream << "Segment: "<<j<< " Number of PE requested larger than in the library"<<endl;
				//logStream << "Number requested: "<<nPE<<" Max PE in library: "<<wfDict->size()<<endl;
				nPE = wfDict->size();
			}
			else{	
				//Choose a random waveform from the list
				//If the waveform size is 0 we need to either move up or down to find the closest PE
				if((*wfDict)[nPE-1].size() == 0){
					//Randomly choose to go up or down
					if(effGen(gen) < 0.5){
						//We need the -1 as first element of the vector
						//contains 1 PE pulses
						while((*wfDict)[nPE-1].size() == 0)
							nPE++;
					}
					//Otherwize iterrate down until we find somthing
					else{
						while((*wfDict)[nPE-1].size() == 0)
							nPE--;
					}
				}
			}
			//Reassign this to the segStruct after it is modified
			outS.nPE = nPE;
			uniform_int_distribution<int> wfSel = uniform_int_distribution<int>(0,(*wfDict)[nPE-1].size()-1);
			outS.selWF = wfSel(gen);
			//Now append to the output vector
			//Check the time of the event if it is after 
			if(outS.segTime >= endTime){
				nextWindowSegs.push_back(outS);
			}
			//Otherwize we add to the output
			else{
				outWFs.push_back(outS);
			}
		}
	}
	logStream.flush();
	return outWFs;
}
 
vector<segStruct> MCBackgroundWaveforms::generateDarkNoise(double startTime, double endTime){
	double timeWindow = endTime-startTime;
	logStream << endl;
	logStream << "Time window: "<<timeWindow<<endl;
	rateGen = poisson_distribution<int>(bkgRate*timeWindow);
	int nDecays = rateGen(gen);
	vector<segStruct> outWFs;
	
	for(int i=0; i<nDecays; i++){
		segStruct outS;
		//Always 1PE for dark noise
		outS.nPE = 1;
		//Selected waveform (From the 1 PE list)
		outS.selWF = eventSel(gen) % numSinglePEWFs;
		//fepID
		outS.fepID = eventSel(gen) % numFEPs;
		//boardID
		outS.boardID = eventSel(gen) % numBoardsPerFEP;
		//channelID
		outS.channelID = eventSel(gen) % numChannels;
		//Time is just randomly in the range
		outS.segTime = effGen(gen)*(timeWindow) + startTime;
		outWFs.push_back(outS);
	}
	return outWFs;
}

void MCBackgroundWaveforms::loadWFSegments(string infile, map<int,vector<int>> sipmToFEPMapping, bool prefiltered){
	ifstream inf(infile);
	json EVJson;
	//Pass the json file to the json object 
	try{
		inf >> EVJson;
	}
	catch(nlohmann::detail::parse_error){
		cout << "Error parsing json file: "<<infile<<" in method MCBackgroundWaveforms::loadWFSegments"<<endl;
		exit(1);
	}
	//Now we need to iterate through the events
	
	vector<segStruct> evVec;
	for (json::iterator iEv = EVJson.begin(); iEv != EVJson.end(); ++iEv) {
		//Get the vectors of wf segements for this event
		vector<double> wfTimes = iEv.value()["windowStarts"].get<vector<double>>();
		vector<int> nPE = iEv.value()["numPE"].get<vector<int>>();
		
		//Load the channels
		vector<int> channels = iEv.value()["channels"].get<vector<int>>();

		//Get the keys (The SIPMS to simulate)
		vector<int> sipmsToSim;
		for(auto it=sipmToFEPMapping.begin(); it != sipmToFEPMapping.end(); ++it)
			sipmsToSim.push_back(it->first);
		
		/*for(int i=0; i<sipmsToSim.size(); i++){
			cout <<"SiPM To Sim: "<<sipmsToSim[i]<<endl;
		}*/
		//Create the uninitialized vector of segStructs
		for(int i=0; i< wfTimes.size(); i++){
			bool channelExists = false;
			if(prefiltered){ 
				channelExists = true;
			}
			else{
				channelExists = find(sipmsToSim.begin(), sipmsToSim.end(), channels[i]) != sipmsToSim.end();
			}
			//If the event is in the selected list save it
			if(channelExists){
				
				//Need to convert to s from ns
				//Create a segstruct
				segStruct s;
				s.segTime = wfTimes[i]*1e-9;
				s.nPE = nPE[i];
				//Get the FEP/Board/Channel ID from the Map
				vector<int> FEPBoardCh = sipmToFEPMapping[channels[i]];
				//Assign the variables to the segStruct
				s.fepID = FEPBoardCh[0];
				s.boardID = FEPBoardCh[1];
				s.channelID = FEPBoardCh[2];
				evVec.push_back(s);
			}
		}
		
		if (!prefiltered) {
			cout << "Number of WF in event before selection: "<<wfTimes.size()<<endl;
			cout << "Number of WF in event after selection: "<<evVec.size()<<endl;
		}
		//Now just append this to the event list
		eventList.push_back(evVec);
		//Once we have appened reset the vector 
		evVec.clear();
	}
}


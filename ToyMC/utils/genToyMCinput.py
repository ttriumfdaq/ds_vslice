import numpy as np
from collections import defaultdict
import simplejson as json
import os

class GenToyMCinput():
    def __init__(self, path):
        self.outDict = defaultdict(lambda: defaultdict(list))
        self.flist = self.get_list_of_files(path)
        try:
            self.flist[-1]
        except:
            print('File list is empty')
            exit()
        self.evCounter=0

    class NumpyEncoder(json.JSONEncoder):
        """ Special json encoder for numpy types """
        def default(self, obj):
            if isinstance(obj, np.integer):
                return int(obj)
            elif isinstance(obj, np.floating):
                return float(obj)
            elif isinstance(obj, np.ndarray):
                return obj.tolist()
            return json.JSONEncoder.default(self, obj)

    def get_list_of_files(self,path):
        flist=[]
        if os.path.isdir(path):
            for f in os.listdir(path):
                fname=os.path.join(path,f)
                if os.path.isfile(fname) and f.endswith('.npz'):
                    flist.append(fname)
                    print(fname)
        elif os.path.isfile(path):
            flist.append(path)
        return flist

    def read_npz(self,fname):
        inDict = np.load(fname,allow_pickle=True)
        ch = inDict["channels"]
        wS = inDict["windowStarts"]
        nPE = inDict["numPE"]
        for i,c in enumerate(ch):
            if self.evCounter == 1000:
                print('Reached event limit',self.evCounter)
                break
            #If it is an empty vector just skip
            if c.size == 0: continue
            else:
                self.outDict["Event_%d" % self.evCounter]["channels"] = c
                self.outDict["Event_%d" % self.evCounter]["windowStarts"] = wS[i]
                self.outDict["Event_%d" % self.evCounter]["numPE"] = nPE[i]
                self.evCounter+=1

    def write(self,fname):
        with open(fname+".json","w") as of:
            json.dump(self.outDict,of,cls=self.NumpyEncoder)

    def convert(self):
        for f in self.flist:
            self.read_npz(f)


if __name__=="__main__":

    g=GenToyMCinput("/data/daqstore/andrea/toymc/electron_v1")
    g.convert()
    g.write("/data/daqstore/andrea/toymc/electron_v1/er")

#!/bin/bash

#Script creates the tmp log files checking if the environment variables are set.
if [ -z ${TOYMC_LOGS_DIR} ];
then
	echo "\$TOYMC_LOGS_DIR  not set. Set it before running script"
	exit 1
fi

#First create the local log for ToyMC info
mkdir $TOYMC_LOGS_DIR/ToyMCLogs/

#This is the code above but as a string to pass to ssh
ssh_cmd="$(cat <<-EOF
if [ -z \${TOYMC_LOGS_DIR} ];
then
	echo "\$TOYMC_LOGS_DIR  not set. Set it before running script"
	exit 1
else
	mkdir \$TOYMC_LOGS_DIR/FEPLogs/
fi
EOF
)"
echo $ssh_cmd
#Then create the FEP log directories via ssh
ssh  dsfe01 "$ssh_cmd"
ssh  dsfe02 "$ssh_cmd"
ssh  dsfe03 "$ssh_cmd"
ssh  dsfe04 "$ssh_cmd"

import simplejson as json
import numpy as np
import os
import sys
sys.path.append("..")
from collections import defaultdict
import periodictable as p
from particle import Particle  as par

def getParticleName(pdg):
    return str(par.from_pdgid(pdg).name)

def getIsotopePDGs(infiles):
    pdgList = []
    for f in infiles:
        pdg = int(f.split("_")[-1][:-4])
        pdgList.append(pdg)
    #Remove duplicates
    return list(set(pdgList))

def splitPDGCode(pdgNumber):
    numOff = pdgNumber - 1e9
    atomicNumber = numOff // 10000
    atomicMass = (numOff % 10000)/10
    excitation = numOff % 10
    #print("Atomic Number %d Atomic Mass %d Excitation %d" % (atomicNumber,atomicMass,excitation))
    return atomicNumber, atomicMass, excitation

def getIsotopeName(aN):
    for el in p.elements:
        if el.number == aN:
             return el.symbol


def convertPDGToIsotope(pdgCode):
    if pdgCode > 1e9:
        aN, aM, excit = splitPDGCode(pdgCode)
        #print(pdgCode)
        #print(aN)
        if excit > 0:
            return getIsotopeName(aN)+"-%d Excited" % aM
        else:
            return getIsotopeName(aN)+"-%d" % aM
    else:
         return getParticleName(pdgCode)


class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

def processDirectory(inDirectory):
    #First loop through and get the isotopes and convert the 
    isotopes = []
    isoToPDG = {}
    print(f"in Directory: {inDirectory}")
    for f in os.listdir(inDirectory):
        pdg = f.split("_")[-1][:-4]
        print(f,pdg)
        iso = convertPDGToIsotope(int(pdg))
        print(f'isotope {iso}')
        isotopes.append(iso)
        isoToPDG[iso] = pdg
    
    #Get the unique isotopes
    isotopes  = list(set(isotopes))
    #Now iterate through the isotopes looking for matching files
    for iso in isotopes:
        print("On isotope %s" % iso)
        pdg = isoToPDG[iso]
        evCounter = 0
        outDict = defaultdict(lambda: defaultdict(list))
        for f in os.listdir(inDirectory):
            #If we have 1000 events for this isotope break out of this loop as well
            if evCounter == 1000:
               break
            if pdg in f:
                inDict = np.load(os.path.join(inDirectory,f),allow_pickle=True)
                ch = inDict["channels"]
                wS = inDict["windowStarts"]
                nPE = inDict["nPE"]
                for i,c in enumerate(ch):
                    if evCounter == 1000:
                        break
                    #If it is an empty vector just skip
                    if len(c) == 0:
                        continue
                    else:
                        outDict["Event_%d" % evCounter]["channels"] = c
                        outDict["Event_%d" % evCounter]["windowStarts"] = wS[i]
                        outDict["Event_%d" % evCounter]["numPE"] = nPE[i]
                        evCounter+=1 
        with open(iso+".json","w") as of:
            json.dump(outDict,of,cls=NumpyEncoder)

if __name__=="__main__":
    topDir = os.getcwd()
    print(f'top dir: {topDir}')
    for d in os.listdir(sys.argv[1]):
        print(d)
        if os.path.isdir(os.path.join(sys.argv[1],d)):
            if os.path.exists(d):
                print('pass')
                pass
            else:
                os.mkdir(d)
                print(d)
            os.chdir(d)
            print(f'current dir: {os.getcwd()}')
            processDirectory(os.path.join(sys.argv[1],d))
            os.chdir(topDir)
        else:
            print('What do you want?')

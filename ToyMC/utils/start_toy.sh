#!/bin/bash

PM="NO"
FEP="NO"
TSP="NO"

for i in "$@"; do
  case $i in
    --FEP)
      FEP="YES"
      shift # past argument with no value
      ;;
    --PM)
      PM="YES"
      shift # past argument with no value
      ;;
    --TSP)
      TSP="YES"
      shift # past argument with no value
      ;;
    --all)
      PM="YES"
      FEP="YES"
      TSP="YES"
      shift # past argument with no value
      ;;
    -*|--*)
      echo "Unknown option $i"
      exit 1
      ;;
    *)
      ;;
  esac
done


SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
VSLICE_HOME=$SCRIPT_DIR/../..


# kill everything first
if [[ "$FEP" == "YES" ]]; then
   $VSLICE_HOME/ToyMC/utils/forceKillFEPS.sh &> /dev/null
   sleep 2
fi
if [[ "$TSP" == "YES" ]]; then
   $VSLICE_HOME/ToyMC/utils/forceKillTSPS.sh &> /dev/null
   sleep 2
fi
   
if [[ "$PM" == "YES" ]]; then 
    pgrep pm.exe | xargs kill -9
fi   

# is mserver running?
if [ -z "$(pgrep mserver)" ]; then
    echo "mserver is not running"
    mserver -D
    sleep 2
else
    echo "mserver is running"
fi

# start TSPs
if [[ "$TSP" == "YES" ]]; then
   $VSLICE_HOME/ToyMC/utils/initializeTSPs.sh
   sleep 2
fi
   
# start POOL MANAGER
if [[ "$PM" == "YES" ]]; then 
    $VSLICE_HOME/bin/pm.exe &> $VSLICE_HOME/logs/pm.log&
    sleep 2
fi

# start FEPs
if [[ "$FEP" == "YES" ]]; then
    $VSLICE_HOME/ToyMC/utils/initializeFEPs.sh
    # and TOY MC
    #$VSLICE_HOME/bin/runToyMC.exe &> $VSLICE_HOME/logs/run.log&
fi




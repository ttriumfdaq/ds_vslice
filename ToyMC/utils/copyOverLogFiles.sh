#!/bin/bash
feps=(01 02 03 04)
for fep in ${feps[*]}; do
	rsync -ra --info=progress2  dsfe$fep:/tmp/logs ./logFiles/fep${fep}Logs
done

#Also copy over the files from the local tmp file
rsync -ra --info=progress2  /tmp/logs ./logFiles/dispatchLogs/

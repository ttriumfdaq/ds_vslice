#Script first loads the channel mapping then passes all the background events
#Through this mapping to remove the events

import simplejson as json
import numpy as np
import os

def parseMapping(mappingFile):
    mappingDict = json.load(open(mappingFile,"r"))
    selList = [int(k) for k in mappingDict.keys()]
    return np.array(selList)


def parsePhysics(physicsFile):
    physicsDict = json.load(open(physicsFile,"r"))
    return physicsDict


def dumpSkimmedFile(physicsDict,filename):
    json.dump(physicsDict,open(filename,"w"))

def skimPhysics(physicsList, skimCh):
    outDict = {}
    for k in physicsList.keys():
        outDict[k] = {}
        ch = np.array(physicsList[k]["channels"])
        #Intersection of the two gives the channels hit which are selected
        matches  = np.intersect1d(ch, skimCh)
        #The isin will give the indices of the matches
        inds = np.isin(ch,matches)
        for k2 in physicsList[k].keys():
            outDict[k][k2] = np.array(physicsList[k][k2])[inds].tolist()
    return outDict

    

if __name__=="__main__":
    mappingFile ="../../../inputEvents/mapping1.json"
    selChannels = parseMapping(mappingFile)

    infiles = list(os.walk("../../../inputEvents/InputJSONFiles/"))
    for f in infiles[1:]:
        if "RatesAndEfficiencies" in f[0]:
            continue
        for j in f[2]:
            jsonPath = os.path.join(f[0],j)
            print(jsonPath)
            
            outFileName = jsonPath[:-5]+"_Skimmed_"+os.path.split(mappingFile)[-1]
    
            physList = parsePhysics(jsonPath)
            sD = skimPhysics(physList, selChannels)
            dumpSkimmedFile(sD,outFileName)





#Utility script to calculate max size of waveform segments
import uproot as up
import numpy as np
import sys

if __name__=="__main__":
    infile = sys.argv[1]
    numPoints = []
    inf = up.open(infile)
    for f in inf:
        inGr = inf[f]
        if type(inGr) == up.ReadOnlyDirectory:
            continue
        numPoints.append(inGr.members["fNpoints"])

    print(np.max(numPoints))
        

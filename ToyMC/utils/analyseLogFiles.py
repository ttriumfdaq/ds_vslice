import os
import re 
import datetime
import argparse
import matplotlib.pyplot as plt
import numpy as np

def logFileToDateTime(logFile):
    logDate = logFile.split("_")[-2]
    logTime = logFile.split("_")[-1][:-4]
    #Extract the year month and day
    (logYear, logMonth, logDay) = tuple(int(logDate.split("-")[i]) for i in range(3))
    #Extract the times
    (logHour, logMin) = tuple(int(logTime.split(":")[i]) for i in range(2))
    return datetime.datetime(logYear,logMonth,logDay,logHour,logMin)

#Provided with a dispatch log this functions looks for FEP logs nearby in time
def findCorrespondingLogFiles(dispatchLog,topLogDir):
    dispTime = logFileToDateTime(os.path.split(dispatchLog)[-1])
    allLogs = []
    print(topLogDir)
    for direc in os.listdir(topLogDir):
        print(direc)
        if "fep" not in direc:
            continue
        print(direc)
        logDir = os.path.join(topLogDir,direc)
        for tD,dirs,files in os.walk(logDir):
            print(files)
            #If there are no log files in the directory continue
            if len(files)==0:
                continue
            times = [logFileToDateTime(f) for f in files]
            timeDiffs = [abs(t-dispTime) for t in times]
            minTime = min(timeDiffs)
            closestLogs = [files[i] for i,t in enumerate(timeDiffs) if t==minTime]
            if len(closestLogs) != 8:
                print("Missing Log files (Should be 8)")
                print(closestLogs)
                exit(1)

            else:
                allLogs.extend([os.path.join(tD,c) for c in closestLogs])
    print(allLogs)
    return allLogs

#Datastructure for log files
class logFile:
    def __init__(self,inLog=""):
        if len(inLog) > 0:
            self.logFile = inLog
            self.logTime = logFileToDateTime(inLog)
        self.entries = []

    def parseDispLog(self):
        with open(self.logFile,"r") as inf:
            for i,line in enumerate(inf):
                self.parseDispLine(line)

    
    def parseDispLine(self,inline):
        dispTime = inline.split(" ")[0][1:-1]
        (hour,mini,sec) = (int(d) for d in dispTime.split(":"))
        disT = datetime.datetime(self.logTime.year,self.logTime.month,
            self.logTime.day,hour,mini,sec)
        dispInfo = inline.split("FEP: ")[-1].split()
        fepID = int(dispInfo[0])
        boardID = int(dispInfo[2])
        channelID = int(dispInfo[4])
        simTime = float(dispInfo[6])
        self.entries.append((disT,fepID,boardID,channelID,simTime))
    
    def parseFEPLog(self):
        logF = os.path.split(self.logFile)[-1]
        self.fepID = int(logF.split("_")[1])
        self.boardID = int(logF.split("_")[3])
        with open(self.logFile,"r") as inf:
            for i,line in enumerate(inf):
                self.parseFEPLine(line)

    def parseFEPLine(self,inline):
        recTime = inline.split(" ")[0][1:-1]
        (hour,mini,sec) = (int(d) for d in recTime.split(":"))
        recT = datetime.datetime(self.logTime.year,self.logTime.month,self.logTime.day,
                hour,mini,sec)
        splitLine = inline.split()
        channelID = int(splitLine[6])
        simTime = float(splitLine[8])
        self.entries.append((recT,self.fepID,self.boardID,channelID,simTime))
    

    def sortLog(self):
        #Sort by the sim time
        self.entries.sort(key=lambda x : x[4])

    #When adding just merge the entries lists 
    def __iadd__(self,x):
        self.entries.extend(x.entries)
        return self

   
    
    def cmpLogs(self,x):
        thisSet = set([e[1:] for e in self.entries])
        xSet = set([e[1:] for e in x.entries])
        
        print("Dispatched %d segments" % len(thisSet))
        print("Recieved %d segments" % len(xSet))

        print("Elements which were dispatched")

    #When checking for equality just look at the fepID boardID channelID and simTime
    def __eq__(self,x):
        cmpVals =  [1,2,3,4]
        for i,entry in enumerate(self.entries):
            for c in cmpVals:
                if entry[c] != x.entries[i][c]:
                    return False

        return True

    def plotDispatchVsSimTime(self,ax,label=None):
        dispTimes = [(e[0]-self.entries[0][0])/datetime.timedelta(0,1) for e in self.entries]
        simTimes = [e[-1] for e in self.entries]
        ax.plot(dispTimes,simTimes,"o",label=label)

    def plotNumDispatchedPerSecond(self,ax):
        simTimes = [e[-1] for e in self.entries]
        bins = np.arange(int(max(simTimes))+1)
        ax.hist(simTimes,bins=bins)
        ax.set_xlabel("Simulated Times (s)")



def main(dispatchFile,logTopDir):
    
    dispLog = logFile(dispatchFile)
    dispLog.parseDispLog()
    dispLog.sortLog()

    fepLogFiles = findCorrespondingLogFiles(dispatchFile,logTopDir)
    print("FEP Logs: %s" % fepLogFiles)
    fepLogs = logFile()
    for f in fepLogFiles:
        fL = logFile(f)
        fL.parseFEPLog()
        fepLogs += fL

    fepLogs.sortLog()

    print("Disp Eq: %s" % (dispLog == fepLogs))

    f,ax = plt.subplots(1)
    ax.set_xlabel("Dispatch Times since start (s)")
    ax.set_ylabel("Simulated waveform time")
    dispLog.plotDispatchVsSimTime(ax,label="Dispatch")
    fepLogs.plotDispatchVsSimTime(ax,label="FEPs")
    ax.legend(loc="best")
    f.savefig("DispatchComp.png")

    f2,ax2 = plt.subplots(1)
    dispLog.plotNumDispatchedPerSecond(ax2)
    f2.savefig("DispatchPerSecond.png")
    
    return


  

if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Check log files from \
            dispatch server and FEPs for consistency (i.e. are we losing data \
            and rate (i.e. is the dispatched data realtime)')
    parser.add_argument('dispatchLog',  type=str, 
                                help='dispatch log file')
    parser.add_argument('logTopDir',  type=str, 
                                help='Top directory of all the log files',nargs="?",default="./logFiles/")
    args = parser.parse_args()
    main(args.dispatchLog,args.logTopDir)

#Script to convert the npz files for the waveforms 
#argument 1 : Path containing npz file
#argument 2 : outfile name
import os
import numpy as np
import sys
import ROOT
from array import array

nWaveforms = 100
peRange = range(1,100)

def constructTGraphFromFile(waveform, pe, index):
    #Stupid ROOT stuff
    xVals = array("d")
    yVals = array("d")
    [xVals.append(x) for x in np.arange(len(waveform))]
    [yVals.append(y) for y in waveform]
    #Create the graph
    outGr = ROOT.TGraph(len(waveform),xVals,yVals)
    outGr.SetName("Waveform_PE_%d_Ind_%d" % (pe,index))
    #outGr.SetDirectory(0)
    outGr.SetTitle("Waveform_PE_%d_Ind_%d" % (pe,index))
    return outGr
     

def convertFileToEventDict(fil,evDict):
    inf = np.load(fil,allow_pickle=True)
    nPE = inf["numPE"]
    wfs = inf["waveforms"]
    for i in range(len(nPE)):
        if nPE[i] not in evDict.keys():
            evDict[nPE[i]] = []
        #If it is less than the number we want to store
        #create the graph and append to the vector
        if len(evDict[nPE[i]]) < nWaveforms:
            gr = constructTGraphFromFile(wfs[i], nPE[i], len(evDict[nPE[i]]))
            evDict[nPE[i]].append(gr)

def writeHistogramsToFile(evDict,ofN):
    of = ROOT.TFile(ofN,"recreate")
    for k in evDict.keys():
        direc = of.mkdir("PE_%d" % k)
        direc.cd()
        for gr in evDict[k]:
            gr.Write()
        of.cd()


if __name__=="__main__":
    #First make the waveform segments
    #Make the dict
    evDict = {}
    for e in peRange:
        evDict[e] = []
    for f in os.listdir(sys.argv[1]):
        convertFileToEventDict(os.path.join(sys.argv[1],f),evDict)
    #convertFileToEventDict(.join(sys.argv[1],f),evDict)

    writeHistogramsToFile(evDict,sys.argv[2])

import argparse
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.backends.backend_pdf

def main(dispatchFile):
    #Dispatch time per slice
    f,ax = plt.subplots(1)
    ax.set_title("Dispatch time per slice")
    ax.set_ylabel("Time per slice (s)")
    ax.set_xlabel("Slice Number")
    #Dispatch Speed
    f2,ax2 = plt.subplots(1)
    ax2.set_title("Dispatch speed")
    ax2.set_ylabel("Dispatch speed (MB/s)")
    ax2.set_xlabel("Slice Number")
    #Dispatch slice size
    f3,ax3 = plt.subplots(1)
    ax3.set_title("Slice Size")
    ax3.set_ylabel("Slice Size (MB)")
    ax3.set_xlabel("Slice Number")
    #Rolling average single dispatch time
    f4,ax4 = plt.subplots(1)
    ax4.set_title("Single WF Dispatch time")
    ax4.set_ylabel("Single WF Dispatch time (s)")
    ax4.set_xlabel("Slice Number")
    #Rolling average single dispatch time
    f5,ax5 = plt.subplots(1)
    ax5.set_title("Generation Time")
    ax5.set_ylabel("Generation Time (s)")
    ax5.set_xlabel("Slice Number")
   
    sliceWidth = None
    

    generationTimes = []
    dispatchTimes = []
    dispatchDataAmount = []
    dispatchDataSpeeds = []
    singleDispatchTimes = []


    with open(dispatchFile,"r") as inf:
            for line in inf:
                if "Generation took" in line:
                    #Append converting to s from us
                    generationTimes.append(float(line.split()[-2])/1e6)
                if "Dispatch of slice with width" in line:
                    #Set the slice width if it isnt set
                    sW = float(line.split(":")[1].split()[0])/1e3
                    if sliceWidth is None:
                        sliceWidth = sW
                    #Otherwise check it
                    elif sliceWidth != sW:
                        print("Error discrepancy in slice width.\
                        current width: %f new width %f" % (sliceWidth,sW))
                    #Now append the dispatch time
                    dispatchTimes.append(float(line.split()[-2])/1e3)
                #
                if "Dispatched:" in line:
                    dispatchDataAmount.append(float(line.split()[1]))
                    dispatchDataSpeeds.append(float(line.split()[-2]))

                if "Rolling Average Dispatch Time" in line:
                    singleDispatchTimes.append(float(line.split()[-2]))



    ax.plot(dispatchTimes)
    ax.axhline(sliceWidth,color="r")
    ax2.plot(dispatchDataSpeeds)
    ax3.plot(dispatchDataAmount)
    ax4.plot(singleDispatchTimes)
    ax5.plot(generationTimes)
    ax5.axhline(sliceWidth,color="r")

    pdfName =  "MonitoringPlots_"+("_".join(dispatchFile.split("_")[1:]))[:-4]+".pdf"
    pdf = matplotlib.backends.backend_pdf.PdfPages(pdfName)
    pdf.savefig(f)
    pdf.savefig(f2)
    pdf.savefig(f3)
    pdf.savefig(f4)
    pdf.savefig(f5)
    pdf.close()

if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Parse Monitoring Log Files and print \
            statistics to plots')
    parser.add_argument('--monitoringLog',  type=str, 
                                help='monitoring log file')
    args = parser.parse_args()
    main(args.monitoringLog)

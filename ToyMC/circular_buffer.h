//Circular buffer implementation based on explanation at
//https://embeddedartistry.com/blog/2017/05/17/creating-a-circular-buffer-in-c-and-c/
//Thread safe for one producer and consumer without locks
//Removed the capacity function as we don't need it for ToyMC
//This circular buffer 

#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

#include <iostream>
#include <memory>
#include <cstring>

template <class T>
class circular_buffer {
	public:
		explicit circular_buffer( size_t numElem) :
			//+1 is for thread safety
			buf_(std::unique_ptr<T[]>(new T[numElem+1])),
			max_size_(numElem+1) 
	{}
		//Add data into the buffer	
		void put(const T& item){
			//If we try to add to a full buffer close the program
			if(this->full()){
				std::cout << "ERROR: circular_buffer is full"<<std::endl;
				exit(1);
			}
			
			//Copy the item to the buffer
			buf_[head_] = item;

			size_t upHead = head_ + 1;
			if(upHead == max_size_)
				upHead = 0;
			head_ = upHead;

		};
		
		//Get the address of the data you would get from put
		//but dont increment head as we want to modify this data
		//without risking read from put
		//To be used with increment_head
		T * put_address_dont_increment()
		{
			//If we try to add to a full buffer close the program
			if(this->full()){
				std::cout << "ERROR: circular_buffer is full"<<std::endl;
				exit(1);
			}
			
			//Copy the item to the buffer
			return &buf_[head_];
		};
		
		void increment_head()
		{
			size_t upHead = head_ + 1;
			if(upHead == max_size_)
				upHead = 0;
			head_ = upHead;

		};

		T get()
		{
			if(this->empty()){
				std::cout << "ERROR: circular_buffer is empty"<<std::endl;
				exit(1);
			}
			
			T val = buf_[tail_];
			
			size_t upTail = tail_ + 1;
			if(upTail == max_size_)
				upTail = 0;
			tail_ = upTail;

			return val;
		};
		
		//Get the address of the data you would get from get
		//but dont increment tail as we want to use this data
		//without risking overwrite from put
		//To be used with increment_tail
		T * get_address_dont_increment()
		{
			if(this->empty()){
				std::cout << "ERROR: circular_buffer is empty"<<std::endl;
				if(this->full()){
				std::cout << "ERROR: circular_buffer is empty and full possible corruption"<<std::endl;
				}
				exit(1);
			}
			
			T * val = &buf_[tail_];
			
			return val;
		};
		
		void increment_tail()
		{
			size_t upTail = tail_ + 1;
			if(upTail == max_size_)
				upTail = 0;
			tail_ = upTail;

		};

		void reset()
		{
			head_ = tail_;
		};

		bool empty() const{
			return head_ == tail_;
		};


		bool full() const{
			size_t upHead =  head_ + 1;
			if(upHead == max_size_)
				upHead = 0;
			return upHead == tail_;
		};

		size_t capacity() const
		{
			return max_size_;
		};


	//private:
		std::unique_ptr<T[]> buf_;
		size_t head_ = 0;
		size_t tail_ = 0;
		const size_t max_size_;
};
#endif 

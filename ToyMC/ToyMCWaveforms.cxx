#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <random>
#include <chrono>
#include <map>
#include <thread>
#include <fstream>
#include <mutex>
#include <cstdlib>
#include <experimental/filesystem>
#include <regex>
#include <numeric>



#include "TGraph.h"
#include "TFile.h"

#include "json.hpp"

#include "ToyMCWaveforms.h"

#include "socket_utils.h"
#include "circular_buffer.h"


//MIDAS header for the ODB stuff
#include "midas.h"

#define DEBUG_TS_MARKERS 0


using json = nlohmann::json;

using namespace std;
namespace fs = std::experimental::filesystem;

//Utility method to sleep without relying on the OS scheduler 
inline void activeSleep(double timeInus){
	if(timeInus <= 0)
		return;
	auto time = chrono::steady_clock::now();
	std::chrono::duration<double,std::micro> sleepTime = chrono::steady_clock::now()-time;
	while(sleepTime.count() < timeInus){
		sleepTime = chrono::steady_clock::now()-time;
	}
}

//Constructor for the toy MC
ToyMCWaveforms::ToyMCWaveforms(string vxFile, string channelsFile, double darkRate){
	vslice::ts_printf("Initializing. Don't start a run until you see the 'Ready to start run' message!\n");

	loadVXDataFromROOTFile(vxFile);
	//Now load the channels file (which channels we want to simulate)
	loadChannelsFile(channelsFile);
	loadODBVariables();

	//Set the moving dispatch average to 0
	movingDispatchAverage = 0;
	movingAverageDisp = 0;

	numFEPs = fep_hosts.size();
	numBoards =  fep_ports.size();
	
	//First end of slice event is at slice end time (Create one for each board
	eosTimes = vector<double>(numFEPs*numBoards,sliceSize);

	vslice::ts_printf("Connecting to FEPs...\n");

	//The fep hosts contain the  hostnames of the individual FEPS
	for(int i=0; i<fep_hosts.size(); i++){
		for(int j=0; j< fep_ports.size(); j++){
			int sock = -1;
			while(sock == -1){
				outMutex.lock();
				cm_msg(MINFO, __FUNCTION__, "Waiting to connect to FEP %s on port  %d",fep_hosts[i].c_str(),fep_ports[j]);
				outMutex.unlock();
				sock = socket_utils::connect(fep_hosts[i], fep_ports[j], 1000, 1000, 1000);
			}
			//Reset the port to the start port when we move to the next FEP
			socketObjects.push_back(sock);
			circBuffs.push_back(move(circular_buffer<dispStruct>(10000)));
			
			outMutex.lock();
			cm_msg(MINFO, __FUNCTION__, "Connected to FEP %s on port  %d",fep_hosts[i].c_str(),fep_ports[j]);
			outMutex.unlock();

			//We also need to connect to the status ports if connecting to the first board
			if(j == 0){
				int sock = -1;
				while(sock == -1){
					outMutex.lock();
					cm_msg(MINFO, __FUNCTION__, "Waiting to connect to FEP %s on port  %d",fep_hosts[i].c_str(),status_port);
					outMutex.unlock();
					sock = socket_utils::connect(fep_hosts[i], status_port, 1000, 1000, 1000);
				}
				outMutex.lock();
				cm_msg(MINFO, __FUNCTION__, "Made status connection to FEP %s on port  %d",fep_hosts[i].c_str(),status_port);
				outMutex.unlock();
				statusSockets.push_back(sock);
			}
		}
		
	}
	
	//Create the fepReady vector (None of the FEPs are ready)
	fepReady = vector<atomic_bool>(4);
	for(auto& f: fepReady){
		f = false;
	}
	//Initialize the buffer (This is the max size defined by the datastructures.h file)

	//Initialize doubles to count the start of the generation window and the end of the generation window
	genWindowStart = 0;

	//EOS event 	
	eosEV = new VXData();
	//Just set to 0 for the time being (updated during dispatch)
	eosEV->board_id = 0;
	//Time stamp set to 0
	eosEV->timestamp_secs = 0;
	eosEV->is_end_of_slice = true;


	//If the dark rate is > 0 add the DCR to the background list
	if(darkRate > 0){
		//Create the DCR background
		MCBackgroundWaveforms * mcB = new MCBackgroundWaveforms(wfList, darkRate, fep_hosts.size(), fep_ports.size());
		//Add to the vector
		backgroundVec.push_back(mcB);
	}

	vslice::ts_printf("Setting up log files...\n");

	//Setup dispatch log file
	stringstream nameStream;
	nameStream << std::getenv("TOYMC_LOGS_DIR");
	nameStream <<"/ToyMCLogs/";
	//Now check if the directory exists	
	bool isDir = fs::is_directory(fs::path(nameStream.str()));
	if(! isDir){
		monitoringStream << "ToyMCWaveforms::Unable to find directory: "<<nameStream.str()<<" to create log file in "<<endl;
		exit(1);
	}
	nameStream << "Dispatch_";
	time_t t = std::time(nullptr);
	auto tm = *localtime(&t);
	nameStream << put_time( &tm, "%Y-%m-%d_%H:%M" ) << ".log";
	outMutex.lock();
	cm_msg(MINFO, __FUNCTION__, "Outputting logging information to: %s",nameStream.str().c_str());
	outMutex.unlock();
	logStream = ofstream(nameStream.str(), ios::out);

	//Replace dispatch with monitoring for monitoring log
	string monitorName = regex_replace(nameStream.str(),regex("Dispatch"),"Monitoring");
	monitoringStream = ofstream(monitorName, ios::out);
	outMutex.lock();
	cm_msg(MINFO, __FUNCTION__, "Outputting monitoring information to: %s",monitorName.c_str());
	outMutex.unlock();
}

ToyMCWaveforms::~ToyMCWaveforms(){
	cm_disconnect_experiment();
	for(auto b : backgroundVec)
		delete b;
	delete eosEV;
}

void ToyMCWaveforms::loadODBVariables(){
	vslice::ts_printf("Loading ODB settings...\n");

	// get default values from environment
	//First connect to the experiment
	cm_connect_experiment("dsvslice", "vslice","TOY MC", NULL );
	cm_start_watchdog_thread();
	//Load the global FEP Settings	
	midas::odb FEPSettings("/FEP shared");
	monitoringStream << "FEP Shared"<<endl;
	monitoringStream << FEPSettings << endl;


	//Get the slice width (We generate one slice per dispatch currently)
	if (!FEPSettings.is_subkey("Slice width (ms)")) {
		monitoringStream << "Using default slice width of 1 s" << endl;
		sliceSize = 1000;
	}
	else{
		sliceSize = FEPSettings["Slice width (ms)"];
	}

	//Convert back  to s 
	sliceSize *= 1e-3;

	//Just set the generation window size to 0.1 s TODO UPDATE ODB
	genWindowSize = 0.1;


	//Set the FEPs into the TOYMC  mode
	//We have four FEPs in the test setup (Numbered from one to four)
	for(int i=1; i<5; i++){
		string hostname;
		stringstream ss;
		ss <<  "/Equipment/FEP_"<< std::setfill('0') << std::setw(3) << i<<"/Settings/Global";
		string FEPODB = ss.str();
		midas::odb FEPGlobal(FEPODB);
		//Set the demo data to false and the toy MC to true
		FEPGlobal["Use demo data (read at init)"] = false;
		FEPGlobal["Use toy data (read at init)"] = true;
		ss.str("");
		ss.clear();	
		ss <<  "/Equipment/FEP_"<< std::setfill('0') << std::setw(3) << i<<"/Settings/ToyMC";
		midas::odb FEPToyMC;
		FEPToyMC.connect(ss.str());
		//Now get the ports from the DB
		if (!FEPSettings.is_subkey("digitiser_ports")){
			fep_ports = {1500, 1501, 1502, 1503, 1504, 1505, 1506, 1507};
			FEPToyMC["digitiser_ports"] = fep_ports;
		}	
		else{
			fep_ports = FEPToyMC["digitiser_ports"];
		}

		if (!FEPSettings.is_subkey("status_port")){
			status_port = 1600;
			FEPToyMC["status_port"] = status_port;
		}	
		else{
			status_port = FEPToyMC["status_port"];
		}

		//Now look for the FEP hostname
		if (!FEPSettings.is_subkey("hostname")){
			ss.str("");
			ss.clear();	
			ss << "dsfe" << std::setfill('0') << std::setw(2) << i;
			FEPToyMC["hostname"] = ss.str();
			hostname = ss.str();
		}	
		else{
			hostname = FEPToyMC["hostname"];
		}
		fep_hosts.push_back(hostname);
	}


}

void ToyMCWaveforms::loadChannelsFile(string channelsFile){
	vslice::ts_printf("Loading SiPM to VX2740 board/channel map...\n");

	ifstream inf(channelsFile);
	json EVJson;
	//Pass the json file to the json object 
	try{
		inf >> EVJson;
	}
	catch(nlohmann::detail::parse_error){
		monitoringStream << "Error parsing json file: "<<channelsFile<<" in method ToyMCWaveforms::loadChannelsFile"<<endl;
		exit(1);
	}
	//Convert the string JSON to the map	
	map<string,vector<int>> tmpMap = EVJson.get<map<string, vector<int>>>();
	for(auto it : tmpMap){
		sipmToFEPMapping[atoi(it.first.c_str())] = it.second;
	}

}

//Method to add additional backgrounds into the simulation    
void ToyMCWaveforms::AddBackground(string backgroundName, string JSONFile, double rate, double efficiency, bool prefiltered){
	//Construct the background
	MCBackgroundWaveforms * mcB = new MCBackgroundWaveforms(backgroundName, JSONFile, sipmToFEPMapping, wfList,rate,efficiency, prefiltered);
	//Add to the vector of backgrounds
	backgroundVec.push_back(mcB);
}

void ToyMCWaveforms::AddBackgroundList(string compName, string effRatesFile, bool prefiltered){
	stringstream ss;
	map<string,map<string,double>> effMap;
	map<string,string> filenameMap;

	parseRateEffJSON(effRatesFile, effMap, filenameMap);
	//Iterate through the map
	for(auto iso : effMap){
		monitoringStream << iso.first<<endl;
		//Now load the efficiency and rate
		double efficiency = iso.second["Efficiency"];
		double rate = iso.second["Rate"];
		string backgroundsFile = filenameMap[iso.first];

		//Create the name
		ss << compName << " "<<iso.first;
		string backgroundName = ss.str();	

		ss.str("");
		ss.clear();

		//Construct the background
		MCBackgroundWaveforms * mcB = new MCBackgroundWaveforms(backgroundName, backgroundsFile, sipmToFEPMapping, wfList, rate, efficiency, prefiltered);
		//Add to the vector of backgrounds
		backgroundVec.push_back(mcB);

	}
}


void ToyMCWaveforms::generateBackgroundList(vector<segStruct>& bList, vector<int>& diffs){
	bList.clear();
	diffs.clear();
	//Iterate through the background
	for(auto& b : backgroundVec){
		//Generate each background
		vector<segStruct> bkgSegs = b->generateTimeSpan(genWindowStart,genWindowStart+genWindowSize);
		//Iterate through all the segments adding to the corresponding socket
		for(auto seg : bkgSegs){
			bList.push_back(seg);
		}
		monitoringStream << b->getName() << endl;
		monitoringStream << "Generated: "<<bkgSegs.size()<<endl;
	}

	//Once we have added all the backgrounds we need to sort based on the time 
	sort(bList.begin(), bList.end(), ToyMCWaveforms::cmpSegStructTimes);

	//Now calculate the differences for each index
	//First initialise the vector
	diffs = vector<int>(bList.size()+1,0);
	//First difference is time from start of segment to first event
	diffs[0] = bList[0].segTime-genWindowStart;
	for(int j=1; j<bList.size(); j++){
		//Multiply by 1e6 to convert to us and cast to int
		diffs[j] = static_cast<int>((bList[j].segTime-bList[j-1].segTime)*1e6);
	}
	//Last element is delay until end of slice
	diffs[bList.size()] = static_cast<int>((genWindowStart+genWindowSize-bList[bList.size()-1].segTime)*1e6);
	//Update genWindowStart varaible
	genWindowStart += genWindowSize;
}


//Method to dispach the data over ethernet no delays
void ToyMCWaveforms::dispatchDataFast(int sockInd){
 
	//Create a dispStruct to write to
	dispStruct * d;
	auto startDisp = chrono::steady_clock::now();
	auto startDispTenThou = chrono::steady_clock::now();
	int totalDisp = 0;
	double succDisp = 0;
	//Total amount of fata dispatched (bytes)
	double totalData = 0;
	//Total sock time
	
	while (continueGen){
		if (circBuffs[sockInd].empty()) {
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
			continue;
		}

		//Get the element 
		d = circBuffs[sockInd].get_address_dont_increment();

		if (DEBUG_TS_MARKERS) {
			VXData vx;
			vx.decode(d->dataBuff);
			if (vx.is_end_of_slice) {
				vslice::ts_printf("Dispatching end-of-slice marker to socket %d for board %d at time %lf\n", d->socketIndex, vx.board_id, vx.timestamp_secs);
			}
		}

		//Get the index to dispatch
		bool sentData = socket_utils::write_fully_with_size_protocol(socketObjects[d->socketIndex], (char*) &(d->dataBuff), d->dS);
		if(!sentData){
			//monitoringStream << "Sent: "<<totalDisp<<" total for slice: "<<inVec.size()<<endl;
			/*outMutex.lock();
			cm_msg(MERROR, __FUNCTION__, "Failed to send data to FEP %s on port  %d tried to send %d bytes",fep_hosts[d->socketIndex/numBoards].c_str(),fep_ports[d->socketIndex % numBoards],(int)d->dS);
			outMutex.unlock();*/

		}
		else{
			succDisp++;
			totalData += (double) d->dS; 
		}
		
		//Now we are done update the tail
		circBuffs[sockInd].increment_tail();

		totalDisp++;
		if(totalDisp % 10000 == 0){

			auto endDisp = chrono::steady_clock::now();
			auto endDispTenThou = endDisp;
			//Post some stats
			//First accumulate the vectors over all the socket threads
			double perSucc = 100;
			if(totalDisp > 0)
				perSucc = succDisp/totalDisp*100;
			//Run duration in ms
			chrono::duration<double, std::nano> ns_double = (endDisp - startDisp);
			auto ns = chrono::duration_cast<chrono::nanoseconds>(ns_double);
			monitoringStream <<  "So far in run lasting "<< display_duration(ns)<<"attempted to send: "<<totalDisp<<" samples succesfully sent: " << succDisp << " ("<<perSucc<<" \%) of these"<<endl;
			monitoringStream << " Dispatched: "<<totalData/1e6<<" MB Dispatch Rate: "<<totalData/(1e6*ns_double.count()/1e9)<<" MB/s"<<endl;
			

			auto fullDispTime  = chrono::duration_cast<chrono::nanoseconds>(endDispTenThou-startDispTenThou);
		}



	}

}

//Cpr
void ToyMCWaveforms::listenForFEPReady(int ind){
	char readyBuf[10];
	uint64_t recv_bytes;
	int sock = statusSockets[ind];
	const char * fepOut = fep_hosts[ind].c_str();
	vslice::ts_printf("Waiting to recieve begin-of-run signal from FEP %s\n", fepOut);

	while (!socket_utils::wait_for_data(sock, 1000)) {
	}

	outMutex.lock();
	cm_msg(MINFO, __FUNCTION__, "Recieved Data from FEP");
	outMutex.unlock();
	bool read_success = socket_utils::read_fully_with_size_protocol(sock, readyBuf, 10, recv_bytes);
	//If we fail to read the event for some reason print some error messages
	if(!read_success){
		outMutex.lock();
		cm_msg(MERROR, __FUNCTION__, "Could not successfully read FEP ready over ethernet for FEP %s on socket %d",fepOut, sock);
		outMutex.unlock();
		fepReady[ind] = false;
		return;
	}

	//Now send a message back 
	outMutex.lock();
	cm_msg(MINFO, __FUNCTION__, "Sending connect response to  FEP %s", fepOut );
	outMutex.unlock();
	char resp[1] = {1}; 
	uint64_t respSize = 1;
	bool write_success = socket_utils::write_fully_with_size_protocol(sock, resp, respSize);
	if(!write_success){
		outMutex.lock();
		cm_msg(MERROR, __FUNCTION__, "Could not successfully respond to FEP over ethernet");
		outMutex.unlock();
		fepReady[ind] = false;
		return;
	}
	fepReady[ind] = true;
}

void ToyMCWaveforms::listenForEndOfRun(int ind){
	int sock = statusSockets[ind];
	//Just listen for EOR signal on the first port of the first FEP
	//Also only keep checking if continueGen is true if it is false we are done as another
	//thread has recieved the EOR signal
	while(true){
		while (!socket_utils::wait_for_data(sock, 10000) && continueGen) {
			//Mutex 
			outMutex.lock();
			cm_msg(MINFO, __FUNCTION__, "Waiting to recieve EOR");
			outMutex.unlock();
		}
		if (continueGen) {
			outMutex.lock();
			cm_msg(MINFO, __FUNCTION__, "Recieved Signal from FEP");
			outMutex.unlock();
			char eor[10] = {};
			uint64_t recv_bytes;
			uint64_t buffer_size = 10;
			bool read_success = socket_utils::read_fully_with_size_protocol(sock, eor, buffer_size, recv_bytes);
			if(!strcmp(eor,"EOR")){
				outMutex.lock();
				cm_msg(MINFO, __FUNCTION__, "EOR recieved from FEP. Stopping Generation");
				outMutex.unlock();
				continueGen = false;
				fepReady[ind] = false;
			}
			//If continueGen is true then we have received some garbage from the FEP
			else {
				outMutex.lock();
				cm_msg(MINFO, __FUNCTION__, "Unexpected data from FEP waiting for EOR");
				outMutex.unlock();
				//exit(1);
			}
		}
		//Otherwise continue gen is false and we got here because 
		//another thread received the EOR signal
		else{
			outMutex.lock();
			cm_msg(MINFO, __FUNCTION__, "EOR recieved from FEP On another thread. Stopping Generation");
			outMutex.unlock();
			fepReady[ind] = false;
			break;
		}
	}

}

//RunMC is just generating the segements and dispatching the events
void ToyMCWaveforms::runMC(){
	continueGen = false;
	//First wait for the ready signal for all the FEPS
	outMutex.lock();
	cm_msg(MINFO, __FUNCTION__, "Ready to start run.");
	outMutex.unlock();
	vector<thread> FEPReadyThreads;
	for(int i=0; i<statusSockets.size(); i++){
		FEPReadyThreads.push_back(move(thread(&ToyMCWaveforms::listenForFEPReady,this,i)));
	}

	vslice::ts_printf("Initialization complete. Ready to start run!\n");

	for(int i=0; i<FEPReadyThreads.size(); i++){
		FEPReadyThreads[i].join();
	}
	//Check if all the FEPs are ready 
	bool allReady = find(fepReady.begin(), fepReady.end(), false) == fepReady.end();
	//If they are not exit the program
	if(!allReady){
		outMutex.lock();
		cm_msg(MERROR, __FUNCTION__, "All FEPs don't appear to be ready to send data");
		outMutex.unlock();
		return;
	}
	//Initialize the continueGen variable to true
	else{
		continueGen = true;
	}



	vector<thread> listenThreads; 
	for(int i=0; i<statusSockets.size(); i++){
		listenThreads.push_back(move(thread(&ToyMCWaveforms::listenForEndOfRun,this,i)));
	}

	
	thread genThread(&ToyMCWaveforms::threadGenerateData,this,false);
	vector<thread> dispThreads;
	//Sleep for a little bit to fill the buffer
	this_thread::sleep_for(chrono::milliseconds(1000));
	for(int i=0; i<numFEPs; i++){
		for (int j = 0; j < numBoards; j++) {
			int sockInd = i*numBoards + j;
			dispThreads.push_back(move(thread(&ToyMCWaveforms::dispatchDataFast,this,sockInd)));
		}
	}

	genThread.join();

	for(int i=0; i<dispThreads.size(); i++){
		dispThreads[i].join();
	}

	for(int i=0; i<listenThreads.size(); i++){
		listenThreads[i].join();
	}

	outMutex.lock();
	cm_msg(MINFO, __FUNCTION__, "All FEPs ended run successfully");
	outMutex.unlock();
	
	// Mark had an exit() here. Should make logic better.
	cm_disconnect_experiment();
	exit(0);

	//Once we join all the threads i.e. the run has been stopped 
	//we can recursively call this function to check whether the run
	//has started and redispatch data once we are ready after we reset the fep
	//ready varaibles
	runMC();

}

inline bool ToyMCWaveforms::getCircBuffPointerSafe(int sockInd, dispStruct*& disp){
	while(circBuffs[sockInd].full() && continueGen){
		activeSleep(1);
	}
	if (!continueGen) {
		return false;
	}
	disp = circBuffs[sockInd].put_address_dont_increment();
	return true;
}


void ToyMCWaveforms::threadGenerateData(bool testing){
	//Immediately generate the data
	dispStruct * d;	
	auto threadStartTime = chrono::steady_clock::now();
	while(continueGen){
		auto now = chrono::steady_clock::now();
		auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(now - threadStartTime);
		double elapsed_s = elapsed_ms.count() / 1000.;

		if (genWindowStart > elapsed_s + 2) {
			// We're generating data too quickly. Back off a bit so we're
			// never more than a few seconds ahead of the "real" time in the run.
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
			continue;
		}

		auto genStartTime = chrono::steady_clock::now();

		generateBackgroundList(backList,differences);
		//Iterate through events
		vslice::ts_printf("Generated %u waveforms for the %.2lfs of data up to %.2lfs\n", (unsigned)backList.size(), genWindowSize, genWindowStart);
		for(auto& b : backList){
			//Now we can select the waveform from the waveform list
			VXData *  outData = wfList[b.nPE-1][b.selWF];
			//Now set the start times and the channels etc.
			//First get the socket index
			int sockInd = b.fepID * numBoards +  b.boardID;
			//If the segtime is greater than the current EOS we need to add an EOS event to the buffer first
			if(b.segTime > eosTimes[sockInd]){
				//Update the EOS event
				eosEV->board_id = b.boardID;
				eosEV->timestamp_secs = eosTimes[sockInd];
				//Add this to the ring buffer
				if (!getCircBuffPointerSafe(sockInd, d)) {
					// Time to finish generation
					break;
				}
				//Encode the event to disp struct
				d->socketIndex = sockInd;
				d->dS = eosEV->get_encoded_size_bytes();
				eosEV->encode(&(d->dataBuff[0]));
				//Now we are done increment the head pointer
				circBuffs[sockInd].increment_head();
				//Update the EOS time
				eosTimes[sockInd] += sliceSize;

				if (DEBUG_TS_MARKERS) {
					vslice::ts_printf("Added end-of-slice marker to socket %d for FEP %d board %d at time %lf\n", sockInd, b.fepID, b.boardID, eosEV->timestamp_secs);
				}
			}
			
			
			//This is the only place we do this so if we use the same data twice it will get overwritten after it is sent the first time
			outData->timestamp_secs = b.segTime;
			outData->board_id = b.boardID;
			outData->channel_id = b.channelID;    
			//Now copy to dispStruct
			if (!getCircBuffPointerSafe(sockInd, d)) {
				// Time to finish generation
				break;
			}

			d->socketIndex = sockInd;
			d->dS = outData->get_encoded_size_bytes();
			outData->encode(&(d->dataBuff[0]));
			//Now we are done increment the head pointer
			circBuffs[sockInd].increment_head();
		}
		auto genEndTime = chrono::steady_clock::now();
		auto diff = chrono::duration_cast<chrono::milliseconds>(genEndTime-genStartTime);
		monitoringStream << "To add "<<genWindowSize<<" s of data it took "<<static_cast<double>(diff.count())/1000<<" s"<<endl;
	}
}


//From stack overflow
vector<string> ToyMCWaveforms::split(const string& str, const string& delim)
{
	vector<string> tokens;
	size_t prev = 0, pos = 0;
	do
	{
		pos = str.find(delim, prev);
		if (pos == string::npos) pos = str.length();
		string token = str.substr(prev, pos-prev);
		tokens.push_back(token);
		prev = pos + delim.length();
	}
	while (pos < str.length() && prev < str.length());
	return tokens;
}

vector<string> ToyMCWaveforms::getIsotopeNames(string infile){
	vector<string> outNames;
	TFile * inf = new TFile(infile.c_str(),"read");
	TList * lis = inf->GetListOfKeys();
	for(const auto&& obj: *lis){
		outNames.push_back(obj->GetName());
	}
	inf->Close();
	delete inf;

	//Now split the strings up
	for(size_t i=0; i<outNames.size(); i++){
		outNames[i] = split(outNames[i],"_").back();
	}
	return outNames;
}

void ToyMCWaveforms::loadVXDataFromROOTFile(string infile){
	vslice::ts_printf("Loading fake VX2740 waveforms...\n");
	TFile * inf = new TFile(infile.c_str(),"read");
	//First iterate through
	int maxPE = -1;
	TList * dirLis = inf->GetListOfKeys();
	for(const auto&& dir: *dirLis){
		//Get the number of PE from the directory name
		int nPE = stoi(split(dir->GetName(),"_").back());
		if(nPE > maxPE)
			maxPE = nPE;
	}

	//Now initialize the vector
	wfList = vector<vector<VXData*>>(maxPE,vector<VXData*>{});
	//Now iterate through again 
	TList * fileList = inf->GetListOfKeys();
	for(const auto&& dir: *fileList){
		int nPE = stoi(split(dir->GetName(),"_").back());
		//Iterate through the histograms in the directory
		TDirectory * inDirec = (TDirectory*) inf->Get(dir->GetName());
		TList * grList = inDirec->GetListOfKeys();
		for(const auto&& gr: *grList){
			//Construct the waveform
			TGraph * graph = (TGraph*) inDirec->Get(gr->GetName());
			vector<uint16_t> wf = convertHistToVector(graph);
			//Now initialize the VXData
			VXData * outVX = new VXData();
			//Initialize the waveform
			outVX->wf_samples = move(wf);
			//We dont need to do anything else as the timestamp etc.
			//Get initialized at a different time
			wfList[nPE-1].push_back(outVX);
		}

	}

}

void ToyMCWaveforms::parseRateEffJSON(string jsonfile,map<string,map<string,double>>& effRateMap, map<string, string>& filenameMap){

	ifstream inf(jsonfile);
	json EVJson;
	//Pass the json file to the json object 
	try{
		inf >> EVJson;
	}
	catch(nlohmann::detail::parse_error){
		monitoringStream  << "Error parsing json file: "<<jsonfile<<" in method ToyMCWaveforms::parseRateEffJSON"<<endl;
		exit(1);
	}
	for(auto it : EVJson.items()){	
		//Map for the rate and efficiency
		map<string,double> eRMap;
		string k = it.key();
		//Get the Efficencies
		eRMap["Efficiency"] = it.value()["Efficiency"].get<double>();
		eRMap["Rate"] = it.value()["Rate"].get<double>();
		effRateMap[k] = eRMap;

		//Now get the filename
		filenameMap[k] = it.value()["filename"].get<string>();

	}
}



//Method to convert the histogram into a uint64t
vector<uint16_t> ToyMCWaveforms::convertHistToVector(TGraph * inGr){
	//Initialize the out vector
	//Number of bins is
	int nsamples = inGr->GetN();
	vector<uint16_t> outVec(nsamples,0);
	//Subtract the remainder to make sure we don't go beyond
	//the end of the TGraph
	for(int i=0; i<inGr->GetN(); i++){
		outVec[i] = static_cast<uint16_t>(inGr->GetY()[i]);
	}

	return outVec;
}

template<typename T>
string ToyMCWaveforms::print_time(std::chrono::time_point<T> time) {
	using namespace std;
	using namespace std::chrono;

	time_t curr_time = T::to_time_t(time);
	char sRep[100];
	strftime(sRep,sizeof(sRep),"%Y-%m-%d %H:%M:%S",localtime(&curr_time));

	typename T::duration since_epoch = time.time_since_epoch();
	seconds s = duration_cast<seconds>(since_epoch);
	since_epoch -= s;
	milliseconds milli = duration_cast<milliseconds>(since_epoch);

	return  string(sRep) + string(":") + to_string(milli.count());
}

string ToyMCWaveforms::display_duration(std::chrono::nanoseconds ns)
{
	stringstream os;
	typedef chrono::duration<int, ratio<86400>> days;
	char fill = os.fill();
	os.fill('0');
	auto d = chrono::duration_cast<days>(ns);
	ns -= d;
	auto h = chrono::duration_cast<chrono::hours>(ns);
	ns -= h;
	auto m = chrono::duration_cast<chrono::minutes>(ns);
	ns -= m;
	auto s = chrono::duration_cast<chrono::seconds>(ns);
	os << setw(2) << d.count() << "d:"
		<< setw(2) << h.count() << "h:"
		<< setw(2) << m.count() << "m:"
		<< setw(2) << s.count() << 's';
	return os.str();
};


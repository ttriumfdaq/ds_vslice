set(LIBSRC_TOYMC
  MCBackgroundWaveforms.cxx
  ToyMCWaveforms.cxx
)

execute_process(COMMAND root-config --incdir OUTPUT_VARIABLE ROOT_INCLUDE OUTPUT_STRIP_TRAILING_WHITESPACE) 
execute_process(COMMAND root-config --libs OUTPUT_VARIABLE ROOT_LIBS OUTPUT_STRIP_TRAILING_WHITESPACE) 

set(TOYMC_LIBS
  static_toymc
  ${EXE_LIBS}
)

set(TOYMC_INCLUDE_DIR ${CMAKE_CURRENT_LIST_DIR})


if (${CMAKE_SYSTEM_NAME} MATCHES Linux)
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pg")
endif()

add_library(objlib_toymc OBJECT ${LIBSRC_TOYMC})
set_property(TARGET objlib_toymc PROPERTY POSITION_INDEPENDENT_CODE 1)
add_library(static_toymc STATIC $<TARGET_OBJECTS:objlib_toymc>)

add_executable(runToyMC.exe runToyMC.cxx)
install(TARGETS runToyMC.exe DESTINATION ${CMAKE_SOURCE_DIR}/bin)


target_include_directories(objlib_toymc PUBLIC ${INC_DIRS_VSLICE} ${ROOT_INCLUDE} ${TOYMC_INCLUDE_DIR})
target_compile_options(objlib_toymc PUBLIC ${COMPILE_FLAGS})

target_link_libraries(runToyMC.exe ${TOYMC_LIBS} ${ROOT_LIBS})
target_include_directories(runToyMC.exe PUBLIC ${INC_DIRS_VSLICE} ${ROOT_INCLUDE} ${TOYMC_INCLUDE_DIR})


add_subdirectory(tests)

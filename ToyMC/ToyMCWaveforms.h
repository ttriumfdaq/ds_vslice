#ifndef TOY_MC_WAVEFORMS_H
#define TOY_MC_WAVEFORMS_H
#include <vector>
#include <string>
#include <mutex>
#include <array>
#include <condition_variable>
#include <deque>
#include <thread>

#include "MCBackgroundWaveforms.h"
#include "circular_buffer.h"

#include "TGraph.h"

//Maximum number of samples in a waveform if we use a different input file we will have to update this
#define MAX_SAMPLE_SIZE 22408

//Dispatch struct 
struct dispStruct{
	//Index for the socket to send to 
	int socketIndex;
	//Data size to dispatch
	int dS;
	//Data block to send out (allocated as maximum size)
	uint64_t dataBuff[MAX_SAMPLE_SIZE/4+1+3];
};

class ToyMCWaveforms{
	public:
		//Constructor for the toy MC
		ToyMCWaveforms(std::string vxFile, std::string channelsFile, double darkRate);

		//Copy constructor to stop automatic generation
		ToyMCWaveforms(const ToyMCWaveforms& t) = delete;

		//Destructor
		~ToyMCWaveforms();

		//Method to add additional backgrounds into the simulation    
		//void AddBackground(string backgroundName, string MCFile, double rate, bool dataRate=false, bool useNPZ=false);
		//void AddBackgroundChain(string backgroundName, string MCFile, double rate, string ignoreString, map<string,double> scaleMaps, bool dataRate=false);
		void AddBackground(std::string backgroundName, std::string JSonFile, double rate, double efficiency, bool prefiltered);
		void AddBackgroundList(std::string compName, std::string effRatesFile, bool prefiltered);

		void runMC();
		static std::vector<std::string> split(const std::string& str, const std::string& delim);

		//Method to generate the data from the backgrounds returns a sorted list for each of the sockets
		void generateBackgroundList(std::vector<segStruct>& bList, std::vector<int>& diffs);

		std::vector<std::string> GetFEPHosts(){return fep_hosts;}
		std::vector<int> GetFEPPorts(){return fep_ports;}
		
		std::vector<circular_buffer<dispStruct>> circBuffs;// Circular buffer per board
		
		//Generation wrapper method
		void threadGenerateData(bool testing=false);

	private:
		//Circular buffer for dispatch
		int nchannels;
		double dcr;
		//Defines the slice size (At the end of each dispatch we send a EOS VXData)
		double sliceSize;
		//Defines the generation window size
		double genWindowSize;
		//EOS slice times
		std::vector<double> eosTimes;
		//vector<int> addDarkNoise(int nchan=-1);
		//
		//vector of the channels to generate backgrounds for (FE01 B1 CH1 -> FE04 B7 CH64)
		//Should be 2048 channels
		std::map<int,std::vector<int>> sipmToFEPMapping;


		//Method to compare the times of the segstructs
		static bool cmpSegStructTimes(const segStruct& s1, const segStruct& s2){
			return s1.segTime < s2.segTime;
		}

		
		//Method to send out the data as fast as possible	
		void dispatchDataFast(int sockInd);
		//Method to listen to socket to recieve the ready signal
		void listenForFEPReady(int ind);

		void listenForEndOfRun(int ind);

		std::vector<MCBackgroundWaveforms*> backgroundVec;
		std::vector<std::string> getIsotopeNames(std::string infile);
		//WF List containing the list ordered py PE
		std::vector<std::vector<VXData*>> wfList;

		void parseRateEffJSON(std::string jsonfile, std::map<std::string,std::map<std::string,double>> & effRateMap, std::map<std::string, std::string>& filenameMap);
		void loadVXDataFromROOTFile(std::string infile);
		//Load channels from a json file
		void loadChannelsFile(std::string channelFile);
		std::vector<uint16_t> convertHistToVector(TGraph * inhist);

		std::vector<std::string> fep_hosts;
		std::vector<int> fep_ports;
		int status_port;

		inline bool getCircBuffPointerSafe(int sockInd, dispStruct*& disp);

		//Load all the necessary things from the ODB
		void loadODBVariables();

		std::mutex outMutex;
		//Vector of vectors of segstructs (One for each socket)
		std::vector<segStruct> backList; 

		//Vector of vector of ints (in us) to get the differences between each of the events 
		std::vector<int> differences;


		//List of the sockets to transfer data on
		std::vector<int> socketObjects;
		//List of the sockets
		std::vector<int> statusSockets;
		//List of whether the FEP is ready to recieve data
		std::vector<std::atomic_bool> fepReady;

		//Atomic bool for end of run dispatc
		volatile std::atomic_bool continueGen;

		//number of FEPs
		int numFEPs;
		//number of boards per FEP
		int numBoards;


		double genWindowStart;

		//Rolling average of the dispatch times
		double movingDispatchAverage;
		double movingAverageDisp;

		//Special end of slice Event 
		VXData * eosEV;

		//Whether to dispatch log file or not
		std::ofstream logStream;
		std::ofstream monitoringStream;

		template<typename T>
		std::string print_time(std::chrono::time_point<T> time);

		std::string display_duration(std::chrono::nanoseconds ns);

};

#endif
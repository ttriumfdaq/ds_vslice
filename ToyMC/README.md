# ToyMC Documentation


## Introduction

The ToyMC code is intended to provide a realistic physics datastream, including S1 and S2 signals as a result of all the expected physics backgrounds inside DS-20k as well as PMT noise. The code emulates the behaviour of the digitizers in the detector and provides a stream of data to the FEPs in the TRIUMF test setup. 

The primary purpose of the code is to test the throughput of the FEPs namely:
- Datarate : Can the network handle the amount of data coming into and out of the FEPs?
- Processing Speed : Can the FEPs process the incoming data fast enough?

The code in its current form cannot be used to test the TSP performance. This is because the FEPs only correspond to 1/4 of the FEPs intended in the final detector. Therefore the TSPs only see data for 1/4 of the detector and any S1/S2 reconstruction algorithms will not work as expected. The fake datastream can be used, to test the transport of data from the FEPs to the TSPs however.

## Inputs

Several inputs are required to process the data. These include:

- MC events for all required backgrounds processed to extract the digitizer waveforms.
- Waveform segments for a multitude of different PE generated using MC
- Decay rates for each background and probability each decay causes a TPC event 
- Channel Mapping - Which channels do we want to produce data for.

### Generating the MC events

The MC events that go into the code are in JSON format. The format of the event file is:

	"Event_1" : { "nPE" : ["number of PE in each waveform segment in event 1"]
	"channels" : ["channel hit for each waveform segment in event 1"]
	"windowStarts" : ["segment window start times relative to physics event"]
	}

Each background isotope and component has it's own json file. The JSON files can be made by processing the npz files used in the standard ToyMC evaluation using the `utils/convertNPZToJSON.py` code which produces a serio of JSON files one for each background.

As the JSON files are quite large and take quite a long time to load when initializing the code the loading time can be reduced by only including the channels we want to send to the FEPs. This can be acheived by preskimming the JSON files to only contain the channels needed. To skim the files you can use the `utils/produceSkimChannelsJSON.py`

### Generating the waveforms

To generate the waveforms for input the `pyreco` code is used. The code to generate a waveforms from a simulated g4ds ROOT file (Previously I used Ar39 simulations) can be found [here](https://gitlab.in2p3.fr/darkside/pyreco/-/blob/G4DSSegmentGeneration/event/g4ds_fast.py). This script converts the root file to a series of numpy files each containing the waveforms segements for a single physics event.

To produce the input file for the ToyMC code these waveforms are then processed. The processing script can be found at `utils/generateWaveformHistograms.py` For each number of PE in a segment up to 100 waveforms are stored in a ROOT file. These lists of waveforms are then sampled from during the simulation to generate the fake datastream.

One possible improvement to the code would be the inclusion of Naples test stand waveforms to produce a more realistic datastream. For large (10,000 PE) S2 pulses simulated pulse will still be required.

### Setting the decay rates
For single gamma/beta emitters (i.e. Ar39 and K40) the rates are hardcoded into the `runToyMC.cxx` file. In the case of the Uranium and Thorium chains the code reads from a json file containing the rates and efficiciences for each file. The format of the JSON file is:

	{"ISOTOPE_NAME" : {"Efficiency" : Prob_decay_causes_event, "Rate" : decay_rate_of_isotope, "filename" : path_to_mc_events_json}}

### Selecting the channels

The channels are selected using a json file. The format of the JSON file is:

	{"SIPM_ID" : [FEP_ID, BOARD_ID, CHANNEL_ID]}

The skim file is provided as an argument to the `ToyMCWaveforms` constructor. 

As mentioned previously the reccomended procedure is to use the `utils/produceSkimChannelsJSON.py` script with this json file to preskim the event channels to reduce initialization time. However the ToyMC code also has the ability to do this. Skimming of the code can be enabled by setting `prefiltered` to `false` in the `MCBackgroundWaveforms` constructor.

### Other connection variables

## Usage

The code is currently not incorporated onto the main branch of the dsvslice code. To get the code you must first checkout the. The compilation of the ToyMC code has been incorporated into the cmake script for ds\_vslice so no additional compilation steps are required.

As the ToyMC code is not yet integrated with the MIDAS system a few extra steps are required to setup the system:

1.  Ensure the run has stopped in the dsvslice [web interface](https://dsvslice.triumf.ca/). If the run refuses to end go to step 2 and then stop the run
2.  Kill the ToyMC code if it is running
3.  Kill the FEPs - this can be done by using the `utils/forceKillFEPS.sh` script. The killing of the FEPs is required to reinitialize the connections to the ToyMC server. You may need to run the script multiple times to ensure the FEPs are killed.
4.  Restart the FEPs - this can be done by using the `utils/initializeFEPs.sh` script. In the case where the terminal is spammed with messages about being unable to connect kill the FEPs again wait a few minutes and reinitialize them.
5.  Start the toyMC code `./runToyMC.exe` the code will first connect to all the FEPs and then load the required data from JSON/ROOT files. This latter step takes a few minutes. Once the data has all been loaded the message `Waiting to recieve ready signal from FEP...` will appear in the terminal at this point you can start a run on the web interface.
6.  During running the code will log information see the next section for information on logging.
7.  Once you are ready to stop the run you can use the web interface . As the ToyMC code is not integrated into MIDAS it waits for the stop running signal from the FEPs. Sometimes this message can drop and the ToyMC might not stop correctly. In this case you will need to Ctrl-C the code.

### Log Files
to the location of the environment variable `TOYMC_LOGS_DIR` in practice it was found that setting setting these to `/tmp/` is a good choice as the `/home/` directory is shared across the network and can result in reduced performance of the network. Within the `TOYMC_LOGS_DIR` the following subfolders must exist:

* `ToyMCLogs` \- This folder is where the logs produced by the ToyMC code are stored. In this folder there will be timestamped log files for each background isotope stating the input background file used and the number of decays and triggers, information. Another file is created to monitor the performance of the code stating the fraction of data successfully dispatched and the rate of transfer to the FEPs.
* `FEPLogs` \- These exist on the FEPs only. Several timestamped log files (one for each CAEN Board) containing a timestamp, the channel and the segment time since the run start for each segment.

Utility scripts exist for handling the log files:
- `utils/createLogs.sh` - Utility script to create the directories for the log files on both the FEPs and and the dispatch server
- `utils/copyOverLogFiles.sh` - Utility script to copy over the log files from the FEPs and `/tmp/`.
 
## Implementation details

Internally the code mostly runs on two threads. One of these threads writes to a ring buffer whilst the other thread reads from it and dispatches the data to the FEPs. The main code exists in the `ToyMCWaveforms.cxx` class. This code creates both the threads see the `threadGenerateData` for details on the generation thread and `dispatchDataFast` for details on the dispatch of the data.

### The generation thread

To add backgrounds to the simulation one uses the `ToyMCWaveforms::AddBackground` or `ToyMCWaveforms::AddBackgroundList` methods. Internally they create new `MCBackgroundWaveforms` classes which then handle the generation of individual backgrounds by sampling from a predefined list of background events and waveforms in the `generateTimeSpan` method.

The main generation thread works in the following way the method in which the actions occur are also shown:

1.  `generateBackgroundList` The code iterates through each background generating the `segStruct` objects for each background.
2.  `generateBackgroundList` Once all the backgrounds have been generated and we have a list of `segStruct` objects these are sorted based on the time of the waveform segment. The time differences between the segments are also calculated.
3.  `threadGenerateData` The code now iterates through the list of `segStruct` selecting the `VXData` object corresponding to the indices in the `segStruct` object and updating the header information (timestamp, board, channel) accordingly. the code then calculates the socket corresponding to the board and channel. If the segment occurs after a EOS the special end of slice event is generated and added to the circular buffer.
4. `threadGenerateData` Once the `VXData` for the waveforms has been updated to have the correct header information the VXData is encoded into the circular buffer directly ready to dispatch.

Peroically statistics information is added to the log file about the generation time.

Waveform information is stored in the `segStruct` object as seen below:

	struct segStruct{
		//Time of wf segment (in seconds)
		double segTime;
		//Number of PE
		int nPE;
		//Index of the selected waveform
		int selWF;
		//Index of the fep, the board and the channel
		int fepID;
		int boardID;
		int channelID;
	};

Storing the indicies of the waveform saves memory by not having to store large `VXData` objects in memory. 

#### Generating the data for each background

The following is a description of the algorithm to generate data for each background

0.  The code first loads the WF segments from the previous window which fall in this window into the output for this backgrounds. (i.e. If we are generating events for a time window [0,1] s If there is an Ar39 event at 0.99 and some hits occur after 1 s they are automatically added to the next window and removed from this one)
1.  The code first calcuates the size of the window to generate for. The code then calculates the mean number of events based on the rate and the window size and Poisson fluctuates this number to get the number of decays in the time window.
2.  The code then performs accept-reject sampling based on the probability each decay causes an event in the TPC (i.e. data into the DAQ).
3.  For the decays that cause a TPC event a random physics event is selected from the list of stored physics events (which were loaded from the JSON files mentioned above) the time of the physics event is chosen randomly within the window.
4.  For each waveform segment in the physics event a random waveform from the list of waveforms for the number of PE in the waveform segment is selected is selected:
	- If the number of PE is higher that the max number of PE in the waveforms list the highest PE in the waveform list is selected.
	- If the number of PE isn't found in the waveforms list the code randomly chooses the closest number of PE above or below this PE which exists in the waveform list.
6. Any WF segments which fall out of the generation window are saved for addition into the next window.

### The circular buffer

The circular buffer contains 450000 `dispStruct` objects. The size of the buffer was tuned such that the memory footprint was not too large but large enough it can store multiple generation windows (currently the generation window is set to 0.1 s). 450000 events corresponds to slightly less than 1 s of data. 


The structure of the data (`dispStruct`) in the  circular buffer is:

	#define MAX_SAMPLE_SIZE 22408

	//Dispatch struct 
	struct dispStruct{
		//Index for the socket to send to 
		int socketIndex;
		//Data size to dispatch
		int dS;
		//Data block to send out (allocated as maximum size)
		uint64_t dataBuff[MAX_SAMPLE_SIZE/4+1+3];
	};

Where `MAX_SAMPLE_SIZE` is the maximum size . The structure itself contains a socket index for the socket to send data to and the size of the data. The data itself is encoded in `dataBuff`.Note the size of the data array is defined at compile time. The variable `MAX_SAMPLE_SIZE` should be set to the largest waveform length in the input ROOT file. The code is implemented this way such that the `dispStruct` circular buffer is contigous in memory to attempt to increase performance. Allowing the `dataBuff` to have the maximum memory size allowed by the `VXData` object would result in too much memory being used.

In the code we access the addresses of the circular buffer directly using the `put_address_dont_increment` and `get_address_dont_increment` methods this enables encoding to and reading directly from the memory allocated to the circular buffer avoiding uneccessary copying of the data. Once the data is read/write the pointer is then incremented using the `increment_head()` or `increment_tail()` methods.
### The dispatch thread
The dispatch thread runs the `dispatchDataFast` method. This method simply reads from the circular buffer and writes out the data to via ethernet using the `socket_utils` class. Periodically the code also prints various statistics on the data transfer rate.

## Work to be done

The following is a list of tasks still needing completion:

-  Integration with MIDAS. As described above the code is currently not integrated with MIDAS so doesn't usually end correctly.
-  Removal of hard coded variables - some variables within the code are hardcoded these need to either be added to a JSON configuration file or uploaded to the ODB. Additionally the variable `MAX_SAMPLE_SIZE` should be updated possibly as part of the build process
-  Performance investigations - The performance of the code isn't as required currently further work needs to be done to check if this is caused by the code or the network.

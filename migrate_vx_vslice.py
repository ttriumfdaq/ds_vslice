"""
This tool lets you migrate VX2740 settings between a
standalone VX2740 frontend and a vertical slice FEP
frontend.

The ODB locations where VX2740 settings are stored are:
* Standalone frontend: /Equipment/VX2740_Config_Group_001/Settings etc.
* Vertical slice FEP:  /Equipment/FEP_001/Settings/VX2740 etc.

You should run this while the FEP/VX frontends are NOT running.
"""
import midas.client
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--move", choices=["to-fep", "from-fep"], required=True, help="Whether to move data from VX standalone to FEP or from FEP to VX standalone")
    parser.add_argument("--fep-id", type=int, required=True, help="Equipment 'index' for the FEP (e.g. to copy to/from FEP_001, specify 1)")
    parser.add_argument("--vx-id", type=int, required=True, help="Equipment 'index' for the VX stanalone frontend (e.g. to copy to/from VX_Config_Group_001, specify 1)")
    parser.add_argument("--keep-old", action="store_true", help="Keep the FEP/VX equipments that settings were copied from")
    parser.add_argument("--dry-run", action="store_true", help="Just print what would be done")
    args = parser.parse_args()

    client = midas.client.MidasClient("SettingsMigration")
    fep_path = "/Equipment/FEP_%03d" % args.fep_id
    vx_cfg_path = "/Equipment/VX2740_Config_Group_%03d" % args.vx_id
    vx_data_path = "/Equipment/VX2740_Data_Group_%03d" % args.vx_id

    if args.move == "to-fep":
        if not client.odb_exists(vx_cfg_path):
            print("VX group FE %d doesn't exist in ODB. Nothing to move." % args.vx_id)
            exit(1)

        settings_source = vx_cfg_path + "/Settings"
        readback_source = vx_cfg_path + "/Readback"
        settings_target = fep_path + "/Settings/VX2740"
        readback_target = fep_path + "/Readback/VX2740"
        to_delete = [vx_cfg_path, vx_data_path]
        prog_to_run = "/path/to/ds_vslice/bin/fep -i %d" % args.fep_id
    else:
        if not client.odb_exists(fep_path):
            print("FEP %d doesn't exist in ODB. Nothing to move." % args.fep_id)
            exit(1)

        settings_source = fep_path + "/Settings/VX2740"
        readback_source = fep_path + "/Readback/VX2740"
        settings_target = vx_cfg_path + "/Settings"
        readback_target = vx_cfg_path + "/Readback"
        to_delete = [fep_path]
        prog_to_run = "/path/to/dsproto_vx2740/bin/vx2740_group_fe -i %d" % args.vx_id

    settings = client.odb_get(settings_source)
    readback = client.odb_get(readback_source)

    if args.dry_run:
        print("> Would copy settings from %s to %s" % (settings_source, settings_target))
        print("> Would copy readback from %s to %s" % (readback_source, readback_target))
    else:
        client.odb_set(settings_target, settings)
        print("> Copied settings from %s to %s" % (settings_source, settings_target))
        client.odb_set(readback_target, readback)
        print("> Copied readback from %s to %s" % (readback_source, readback_target))

    if not args.keep_old:
        for path in to_delete:
            if not client.odb_exists(path):
                continue

            if args.dry_run:
                print("> Would delete %s" % path)
            else:
                client.odb_delete(path)
                print("> Deleted %s" % path)
            
    if not args.dry_run:
        print("*" * 70)
        print("Now run '%s'" % prog_to_run)
        print("to ensure the entire ODB equipment entry is set up correctly.")
        print("*" * 70)
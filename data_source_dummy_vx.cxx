#include "data_source_dummy_vx.h"
#include "data_source_dummy_vx_structs.h"
#include "data_structures.h"
#include "midas.h"
#include "msystem.h"
#include "vslice_utils.h"
#include "socket_utils.h"
#include "odbxx.h"

/** \file
 * Data source that generates fake data mimicking the VX2740 data format.
 */
DataSourceDummyVx::DataSourceDummyVx(HNDLE _hDB, INT _frontend_index, INT _board_id) {
   hDB = _hDB;
   settings_key = 0;
   fe_id = _frontend_index;
   board_id = _board_id;

	payload = new uint64_t[0x9000];
	max_size_bytes = (0x9000) * sizeof(uint64_t);
}

DataSourceDummyVx::~DataSourceDummyVx() {
   delete payload;
}

INT DataSourceDummyVx::open_records(char *base_odb_path) {
   char settings_path[256];
   snprintf(settings_path, 256, "%s/DummyVXData/Board %d", base_odb_path, board_id);

   INT status = db_check_record(hDB, 0, settings_path, DUMMY_VX_DATA_ODB_SETTINGS_STR, TRUE);

   if (status != DB_SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to check record of %s", settings_path);
      return status;
   }

   status = db_find_key(hDB, 0, settings_path, &settings_key);

   if (status != DB_SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to find key for %s", settings_path);
      return status;
   }

   // Get current values from ODB.
   INT size = sizeof(user_settings);
   status = db_get_record(hDB, settings_key, &user_settings, &size, FALSE);

   if (status != SUCCESS) {
      return status;
   }

   if (user_settings.port == 0) {
      // Set default port if not yet set by user.
      WORD default_port = 10000 + fe_id * 10 + board_id;
      HNDLE port_key;
      db_find_key(hDB, settings_key, "Port", &port_key);
      db_set_data(hDB, port_key, &default_port, sizeof(default_port), 1, TID_WORD);
   }

   // Open hotlink so we know about ODB changes.
   status = db_open_record(hDB, settings_key, &user_settings, sizeof(user_settings), MODE_READ, NULL, NULL);

   if (status != DB_SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Cannot make hotlink to %s.", settings_path);
      return status;
   }

   return status;
}

INT DataSourceDummyVx::begin_of_run(double _slice_width_ms) {
   sock = -1;

   // Connect to dummy VX frontend
   midas::odb clients("/System/Clients");
   char all_boards_name[128] = {};
   char one_board_name[128] = {};
   sprintf(all_boards_name, "DummyVX_%03d_allboards", fe_id);
   sprintf(one_board_name, "DummyVX_%03d_%02d", fe_id, board_id);
   std::string host;

   for (auto it = clients.begin(); it != clients.end(); it++) {
      midas::odb& client = *it;

      if (client["Name"] == std::string(all_boards_name) || client["Name"] == std::string(one_board_name)) {
         host = client["Host"];
         break;
      }
   }

   if (host == "") {
      cm_msg(MERROR, __FUNCTION__, "Failed to find hostname of DummyVX client for fe %d. Didn't see client name %s or %s", fe_id, all_boards_name, one_board_name);
      return FE_ERR_DRIVER;
   }

   sock = socket_utils::connect(host, user_settings.port);

   if (sock < 0) {
      cm_msg(MERROR, __FUNCTION__, "Failed to connect to DummyVX client for fe %d, board %d. Tried to connect to %s:%d", fe_id, board_id, host.c_str(), user_settings.port);
      return FE_ERR_DRIVER;
   }

   return SUCCESS;
}

INT DataSourceDummyVx::end_of_run() {
   if (sock != -1) {
      close(sock);
   }

   return SUCCESS;
}

INT DataSourceDummyVx::populate_next_event(std::vector<std::shared_ptr<VXData> >& data, BOOL debug) {
	if (!socket_utils::wait_for_data(sock, 10)) {
		// No data to read from Dummy VX program.
		return SUCCESS;
	}

   uint64_t recv_size_bytes = 0;
	bool read_success = socket_utils::read_fully_with_size_protocol(sock, (char*)payload, max_size_bytes, recv_size_bytes);
	
	if (!read_success){
		// If we fail to read the event for some reason print some error messages
		vslice::ts_printf("(data) Could not successfully read in data over ethernet\n");
		return FE_ERR_DRIVER;
	} 

	// Otherwise read in the data and process.
	std::shared_ptr<VXData> event = std::make_shared<VXData>();
	event->decode(payload);
	data.push_back(event);

   return SUCCESS;
}
#ifndef DATA_SOURCE_DUMMY_VX_H
#define DATA_SOURCE_DUMMY_VX_H

#include "data_source_base.h"
#include "sys/time.h"
#include "data_source_dummy_vx_structs.h"

/** \file
 * Data source that reads from another program that's pretending to
 * be a VX2740.
 */

/** Data source that reads from another program that's pretending to
 * be a VX2740. Halfway between the DataSourceDemo that generates
 * waveforms in the FEP itself and the DataSourceToyMC that reads
 * from a full Monte Carlo. This version does not have realistic 
 * waveforms, or full-detector-level simulation, but can sustain
 * high data rates across a network.
 *
 * The user can specify and average event rate, waveform length,
 * and which channels are enabled. The waveforms are just sawtooths.
 *
 * Two types of event are generated:
 * - Regular data events with waveforms etc. A separate event is
 *     created for each channel. The data rate, event size and
 *     channel selection are configurable via the ODB.
 * - Metadata events saying that the end of a slice has been reached.
 *     These have no waveform data, but do have a special flag set.
 */
class DataSourceDummyVx : public DataSourceBase {
public:
   DataSourceDummyVx(HNDLE _hDB, INT _frontend_index, INT _board_id);
   virtual ~DataSourceDummyVx();
   INT begin_of_run(double _slice_width_ms) override;
   INT end_of_run() override;
   INT open_records(char *base_odb_path) override;
   INT populate_next_event(std::vector<std::shared_ptr<VXData> >& data, BOOL debug) override;

private:

   HNDLE hDB; //!< ODB access
   HNDLE settings_key; //!< ODB settings handle
   DummyVxDataSettings user_settings; //!< ODB settings

   INT fe_id;
   INT board_id; //!< Board index within this FEP.

   INT sock;
   uint64_t* payload;
   uint64_t max_size_bytes;
};

#endif

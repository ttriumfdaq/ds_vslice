#ifndef TSP_CLASS_H
#define TSP_CLASS_H

#include "data_structures.h"
#include "tsp_filters.h"
#include "json.hpp"
#include "midas.h"
#include "vslice_utils.h"
#include "zmqpp/zmqpp.hpp"
#include <map>
#include <mutex>
#include <stdio.h>
#include <vector>


#define TSP_KILL_PM 100002

/** Assembles data from many FrontEndProcessor instances, applies filtering
 * algorithms, and writes data to disk.
 *
 * FEPs send data to us on raw TCP sockets (as ZMQ is too slow for large payloads).
 *
 * Normally the TSP does not have direct access to Midas, and requires the PoolManager
 * to tell it about run transitions and ODB settings.
 *
 * There are several threads:
 * - 1 main thread that talks to the PoolManager and controls everything.
 * - 1 thread that listens for incoming raw socket connections from FEPs.
 * - N threads (1 per FEP) for receiving and parsing data from FEPs
 *    (spawned each time the listener thread accepts a new connection).
 * - 1 thread for processing the data (spawned once we've received
 *    all the data we expect for a slice), which may spawn more helper threads.
 *
 * Base classes are used to define the interfaces for anything that
 * is physics-relevant, to allow for easier addition/removal/testing of
 * those components.
 * - TSPFilterBase is used to define filters that can be applied to the
 *    data. Only one filter may be enabled during each run.
 */
class TimeSliceProcessor {
public:
   TimeSliceProcessor();
   ~TimeSliceProcessor();

   /** Called when frontend program first starts. Tells the PoolManager who we are. */
   INT init();

   /** Called when we're ready to start processing data for the next run.
    * This may not be immeidately when the run starts, as if we were still
    * processing data from the previous run, we will finish that before handling
    * the start of this run.
    *
    * Spawns the readout thread for populating the ring buffer.
    *
    * \return Midas status code.
    */
   INT handle_delayed_begin_of_run();

   /** Called once we've finished processing data for a run.
    *
    * Closes the readout thread.
    *
    * \return Midas status code.
    */
   INT handle_delayed_end_of_run();

   /** This function is called frequently during a run.
    *
    * We see:
    * - Whether the PoolManager has sent us any messages.
    * - Whether it's time to handle begin-/end-of-run transitions.
    * - Whether there's an update about the processing (e.g. time to
    *    start processing, data receiving failed etc).
    *
    * \return Midas status code.
    */
   INT do_periodic_work();

   /** Whether we've finished processing a slice. If so, `send_data()` will be called. */
   bool is_event_ready_to_send();

   /** Write data for a fully-processed slice.
    *
    * - If we're writing data to disk, do so.
    * - If the "TSP tap" functionality is enabled, send the data to the tap via ZMQ.
    * - If the frontend was started with midas connectivity, and we're asked to write
    *    to midas banks, do so.
    *
    * \param[out] pevent Midas buffer pointer.
    * \param[in] write_to_banks Whether to write to midas buffer.
    * \param[in] max_bank_event_size_bytes Max event size the midas buffer will accept.
    * \return Number of bytes written to midas buffer.
    */
   INT send_data(char *pevent, bool write_to_banks = false, int max_bank_event_size_bytes = -1);

   /** Set the XMQ endpoints we'll use to talk to the PoolManager.
    *
    * \param[in] sub_endpoint Endpoint for PM -> TSP communication.
    * \param[in] push_endpoint Endpoint for TSP -> PM communication.
    */
   void set_pool_manager_endpoints(std::string sub_endpoint, std::string push_endpoint);

   /** Set the frontend index of this instance (`-i` flag when program was started). */
   void set_frontend_index(int fe_idx) {
      this_frontend_index = fe_idx;
   }

   /** Override the hostname that FEPs will use to send data to us
    * (e.g. if the result of `gethostname()` is inappropriate).
    */
   void set_hostname_override(std::string our_hostname) {
      hostname_override = our_hostname;
   }

   /** Set the FEP indexes we expect to get data from during this run. */
   void set_expected_frontend_indexes(std::vector<int> expected);

   /** Build the status string and color for midas status page (if running with midas functionality enabled).
    *
    * \param[out] msg Text to display on midas status page.
    * \param[out] color Color for our FE on midas status page `greenLight`, `yellowLight`
    *    and `redLight` match the default palette.
    */
   void get_status_strings(std::string& msg, std::string& color);

   /** Function that runs in a separate thread to listen for incoming socket connections
    * from FEPs that want to send us data. */
   void *thread_listen_for_connections();

   /** Function that runs in spawned threads for receiving and parsing data from FEPs. */
   void *thread_handle_connection(int connfd);

   /** Function that runs in a separate thread to process data after all FEPs have
    * sent us data. */
   void *thread_process_data();

private:
   /** Whether we've seen data from all the frontends for this slice. */
   bool seen_all_frontends_for_this_slice();

   /** When writing a Slice to disk, we need to write the midas event and bank
    * headers (which is normally done automatically by MFE/mlogger). Pass
    * in a buffer that is large enough for an `EVENT_HEADER`, a `BANK_HEADER` and a `BANK32`.
    */
   void populate_midas_headers(char* header_buffer);

   /** Whether to print debug statements or not. */
   bool debug();

   /** Whether to write data to disk or not. */
   bool should_write_data();

   /** Handle messages from the PoolManager.
    *
    * Messages may be run transitions, and instruction to shut down, or
    * to tell us to send the next slice we process to the TSP tap system.
    */
   void handle_pm_sub_payload();

   /** How often we should send `TSP_HEARTBEAT` messages to the PoolManager (in seconds). */
   double get_heartbeat_period();

   /** Send a `TSP_HEARTBEAT` message to the PoolManager if it's time to do so. */
   void heartbeat();

   /** Close any threads that were receiving data from FEPs.
    *
    * \param[in] force If true, we'll forcefully close the socket a thread was
    *    acting on, rather than waiting for all the data to be received first.
    */
   void tidy_up_conn_handler_threads(bool force = false);

   /** Clean up all resources used while processing a thread, and tell the PoolManager
    * about the result.
    *
    * \param[in] was_recv_timeout Send a `TSP_RECVTIMEOUT` message.
    * \param[in] was_ana_failure Send a `TSP_ANAFAILED` message.
    *
    * If both `was_recv_timeout` and `was_ana_failure` are false, we'll send a
    * `TSP_ANAFINISHED` message.
    */
   void tidy_up_slice_and_tell_pm(bool was_recv_timeout=false, bool was_ana_failure=false);

   /** Send our current state and diagnostics to the PoolManager.
    *
    * \param[in] state One of the TSP_STATE_GROUP message codes defined in tsp_message.h.
    * \param[in] duration If reporting that data receiving or data analysis is finished, how long
    *    that step took (in seconds).
    * \param[in] info Any extra integer info the PoolManager expects in this message (e.g. data sizes).
    * \param[in] block If true, wait until ZMQ has sent the message before continuing.
    * \return Midas status code.
    */
   INT send_pm_message(uint8_t state, double duration = 0., uint32_t info = 0, bool block = true);

   /** Open a new file when writing data to disk.
    * \return Midas status code.
    */
   INT open_next_subrun_file();

   /** Close the file that was opened by `open_next_subrun_file()`.
    * \return Midas status code.
    */
   INT close_current_file();


   ///@name Miscellaneous variables
   ///@{

   int this_frontend_index; //!< Frontend index (-i flag when program was started).
   INT max_event_size_bytes; //!< Max size of event we'll send to midas buffers.
   nlohmann::json settings; //!< ODB settings (sent to us by the PoolManager).
   std::string tsp_tap_endpoint; //!< Where to send processed data to (via ZMQ), if tap is enabled.
   vslice::cpu_info cpu_info; //!< Used to monitor how much CPU we're using.

   /** Code to transmit to PM on termination, 0 for normal exit,
    * 1 for termination from PM, others not implemented.
    */
   int error_state = 0;



   ///@}
   ///
   ///@name Run logic variables
   ///@{

   bool in_run; //!< Whether we're currently in a run.
   int curr_run_number; //!< Current run number.

   bool end_run_pending; //!< Whether the current run has ended (but we may still be processing data)
   int end_run_number; //!< Run number of the run that has ended.

   /** Whether a new run has started. If we were busy handling data from the previous run,
    * then we'll cache the new run info and only act on it when we've finished with the
    * previous data.
    */
   bool next_run_start_pending;

   int next_run_number; //!< Next run number.
   nlohmann::json next_run_settings; //!< ODB settings for the next run.

   /** Used to tell how long data receipt took. */
   std::chrono::time_point<std::chrono::steady_clock> timepoint_txstart;

   /** Used to tell how long data processing took. */
   std::chrono::time_point<std::chrono::steady_clock> timepoint_anastart;


   ///@}
   ///
   ///@name ZMQ communication parameters
   ///@{

   zmqpp::context zmq_context; //!< ZMQ access
   zmqpp::socket *zmq_pm_sub_socket; //!< ZMQ connection for PM -> TSP communication.
   zmqpp::socket *zmq_pm_push_socket; //!< ZMQ connection for TSP -> PM communication.
   zmqpp::socket *zmq_tap_push_socket = NULL; //!< ZMQ connection for TSP -> Tap communication.

   /** Protect access to `zmq_pm_push_socket`, as ZMQ is not thread-safe but we write from multiple threads. */
   std::mutex pm_push_mutex;


   ///@}
   ///
   ///@name Data receiving parameters
   ///@{

   /** Hostname FEPs should use when connecting to us to send data (if if the result of
    * `gethostname()` is inappropriate).
    */
   std::string hostname_override;
   std::string data_endpoint; //!< Hostname + post FEPs should use when connecting to us to send data.

   int data_listenfd; //!< Data accept socket

   timeval slice_first_fep_recv_time; //!< Time we received the first data for the current slice.
   int slice_recv_timeout_secs; //!< Time in which we must have received data from all FEPs.

   int next_slice_idx; //!< Index of the next slice to process. -1 if not started yet.
   int curr_slice_idx; //!< Index of the current slice we're processing. -1 if not started yet.
   int prev_slice_idx; //!< Index of the previous slice we processed. -1 if not processed anything yet.

   std::vector<int> fe_idxs_expected; //!< Which FEPs we expect to get data from during the run.
   std::vector<int> fe_idxs_seen; //!< Which FEPs we have received data from for the current slice.

   bool sent_txstarted; //!< Whether we've told PM that we've started to receive data
   bool sent_txcomplete; //!< Whether we've told PM that we've finished receiving data

   std::mutex parsed_data_mutex; //!< Mutex for protecting access to `data_in` when receiving data from FEPs in multiple threads

   Slice next_data_in;  //!< Data received from FEPs for the next slice.
   uint32_t next_data_in_size_bytes; //!< Size of `data_in`.
   Slice data_in; //!< Data received from FEPs for this slice.
   uint32_t data_in_size_bytes; //!< Size of `data_in`.

   pthread_t *listener_thread; //!< Thread that listens for incoming connections.
   std::map<int, pthread_t *> connection_threads; //!< Threads that handle data connections, keyed by socket number.

   /** Whether we've finished each connection and can close the corresponding
    * thread in `connection_threads`. Keyed by socket number.
    */
   std::map<int, bool> connection_finished;

   std::mutex connection_threads_mutex; //!< Mutex for protecting access to `connection_threads` and `connection_finished`.


   ///@}
   ///
   ///@name Data filtering parameters
   ///@{

   TSPFilterBase* filter; //!< Object that actually does the filtering

   Slice data_out; //!< Processed data for writing to disk.
   uint32_t data_out_size_bytes; //!< Size of `data_out`.

   pthread_t *processing_thread; //!< Thread that actually does the main processing once all data has been received for a slice.
   bool processing_complete; //!< Whether we've finished processing the slice.
   std::string processing_failure_message; //!< If processing failed, the reason why.


   ///@}
   ///
   ///@name Writing data to disk parameters
   ///@{

   std::string this_run_data_dir; //!< Directory to write data to.
   FILE *output_file; //!< File we're writing data to.
   std::string curr_file_name; //!< Current file name.
   uint64_t curr_file_size_bytes; //!< Maximum file size user has asked for.
   int subrun; //!< Output file subrun number.
   int event_serial; //!< Midas event serial number.

   ///@}

};

#endif

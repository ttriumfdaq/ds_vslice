/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file is part of zmqpp.
 * Copyright (c) 2011-2015 Contributors as noted in the AUTHORS file.
 */

#include <cstdlib>
#include <iostream>
#include <limits.h>
#include <sstream>
#include <string>
#include <time.h>
#include <unistd.h>
#include <zmqpp/zmqpp.hpp>

#include "tsp_message.h"
#include "vslice_utils.h"

using namespace std;

#ifndef HOST_NAME_MAX
// Sometimes defined by system, sometimes not.
#define HOST_NAME_MAX 64
#endif

int main(int argc, char *argv[]) {
   srand(time(NULL));
   char hostname[HOST_NAME_MAX];
   gethostname(hostname, HOST_NAME_MAX);
   cout << "hostname is: " << hostname << endl;

   string endpoint;
   {// limiting scope of oss
      std::ostringstream oss;
      oss << "tcp://" << hostname << ':' << 4711;
      endpoint = oss.str();
   }

   string rcvendpoint;
   int rcvport = 9876;
   if (argc > 1)
      rcvport = atoi(argv[1]);

   {// limiting scope of oss
      std::ostringstream oss;
      oss << "tcp://" << hostname << ':' << rcvport;
      rcvendpoint = oss.str();
   }

   int id = getpid();

   srand(id);
   // initialize the 0MQ context
   zmqpp::context context;

   // generate a subscribe socket
   zmqpp::socket_type type = zmqpp::socket_type::push;
   zmqpp::socket socket(context, type);
   // open the connection
   cout << "Opening connection to " << endpoint << "..." << endl;
   socket.connect(endpoint);

   int ts = 0;
   int lastts = 0;

   sleep(5);
   zmqpp::message message;
   message << rcvendpoint << (uint8_t) TSP_IDLE << int(1) << (double) 0.0 << uint32_t(1);
   cout << "Send " << (int) socket.send(message) << endl;
   sleep(5);

   bool analyzing = false;
   int duration = rand() % 10;
   cout << "next duration " << duration << endl;
   while (true) {
      if (ts - lastts >= duration) {
         zmqpp::message message2;
         message2 << rcvendpoint << (analyzing ? (uint8_t) TSP_ANAFINISHED : (uint8_t) TSP_TXCOMPLETE) << int(1) << (double) (ts - lastts) << uint32_t(1);
         cout << "Sending: " << message2.parts() << endl;
         socket.send(message2);
         if (analyzing) {
            vslice::ts_printf("Ana complete after duration %d\n", duration);
         } else {
            vslice::ts_printf("TX complete after duration %d\n", duration);
         }
         duration = rand() % 10;
         vslice::ts_printf("next duration %d\n", duration);
         lastts = ts;
         analyzing = !analyzing;
      }
      sleep(1);
      ts++;
   }
   cout << "Finished." << endl;
}

# Data format and introspection

## Data format

`data_structures.h` contains the Slice object, which stores all the hits in a given time slice. The data can be encoded/decoded from a buffer of `uint64_t`. The on-disk / in-buffer representation minimizes the amount of duplicated data, but is still somewhat-readable by a human (no advanced compression algorithms).

The Slice object is used for the output of both the FrontEndProcessor and the TimeSliceProcessor.

## Unfiltered FEP data in midas buffers

The FrontEndProcessor can output unfiltered data for a slice to the midas bank `U001` (for frontend index 1). Each bank only contains data from the boards controlled by that FEP. You can use the slice index numbers to associate data from multiple FEPs. 

Set the ODB key `/Equipment/FEP_001/Settings/Global/Write unfiltered to midas banks` to enable this peeking.

## Filtered FEP data in midas buffers

The FrontEndProcessor can output filtered data for a slice to the midas banks `F001` (for frontend index 1). Each bank only contains data from the boards controlled by that FEP. You can use the slice index numbers to associate data from multiple FEPs. 

Set the ODB key `/Equipment/FEP_001/Settings/Global/Write data to midas banks` to enable this peeking.

## Filtered TSP data in midas buffers

The TimeSliceProcessor does not have a direct connection to midas. However, the TSP Tap system can be used to peek at the filtered data.

Ensure the TSP Tap executable is running, then set the ODB key `/Equipment/PoolManager/Settings/TSP_tapN` to the number of slices that should be sent to the tap. The tap will then write them as the `T001` bank for TSP frontend index 1 etc.

## Filtered TSP data on disk

The TimeSliceProcessor can write data to disk as midas files. This does not use the main midas logger (as the TSPs don't have a direct connection to midas). We have therefore only implemented writing "plain" midas files, without any compression (no gzip/lz4 option). A maximum file size can be specified.

Each file is named with the format `run123456_tsp001_subrun0001.mid`. So each TSP writes to its own file, and will increment the subrun number when the max file size is reached. 

The PoolManager writes an extra text file that notes which file each slice was written to. This is known as the "slice map" and is named `slice_maps/slice_map_run123456.txt`.

ODB settings controlling writing data to disk are:
* `/Equipment/PoolManager/Settings/TSP/Write data`
* `/Equipment/PoolManager/Settings/TSP/Max file size (MiB)`
* `/Logger/Data dir`.

# Developer overview

## Code design

Most of the code is designed to be flexible. Wherever there is a physical-relevant algorithm, the code uses base classes to define an interface, so we can easily add/change/test algorithms in future.

For example, in the FrontEndProcessor, the origin of the data is abstracted to a DataSourceBase, and the filters to apply all inherit from FEPFilterBase. In the TimeSliceProcessor, all the filters inherit from TSPFilterBase.

### Midas integration

We expect to run ~20 FEPs, 80+ TSPs, 1 PM and 1 TSP Tap for the DS-20k system. Having all of these register as clients with midas would be unwieldy, so the TSPs have been designed to run without a direct connection to midas.

The FEPs, PM and TSP Tap all have a connection to midas, while the TSPs do not.

The PM is responsible for telling the TSPs about run transitions and any ODB settings.

There has been some effort to decouple the core code from midas. In the FEP and TSP, most of the midas interactions are contained in fep.cxx and tsp_midasfe.cxx, while the "business logic" is in fep_class.cxx and tsp_class.cxx. A bit more work is needed to completely decouple the FEP logic from midas. Once this is done, it will be much easier to write automated tests without having to talk to a full midas installation.

## Communication protocols

We generally use ZMQ for inter-process communication. This has a simple interface, and allows for easily sending messages mixing different data types.

The exception is when we're sending the main Slice data from the FEP to the TSP. ZMQ has a significant overhead, and we found that it was ~50% slower than a raw TCP socket. For small metadata messages, this is not an issue. For the firehose of raw data, it is too significant. We use the socket_utils utilities to simplify how we connect to the TSPs and send/receive the data.

### PM > TSP

* At start of run, PM informs TSPs that new run is starting, and what settings should be used for the new run.
* During run, PM may tell TSP to send next slice to TSP Tap
* At end of run, PM informs TSPs that run is finished

### TSP > PM

* When TSP is ready to accept data, tells PM that it is IDLE
* When TSP has received all the data it expects for a slice, tells PM that TXCOMPLETE
* When TSP has finished analyzing a slice, tells the PM that ANAFINISHED

### TSP > TSP Tap

TSP may send an entire slice to the TSP Tap

### PM > FEP

* For each slice, PM tells FEPs where to send the data

### FEP > TSP

* For each slice, FEP connects to TSP and sends the data over a raw socket. So that FEPs don't have hundreds of open sockets, the connection to each TSP is closed once the data has been sent.


## Detailed program descriptions

See the FrontEndProcessor, TimeSliceProcessor, and PoolManager documentations for details about each program's structure.

## Tests

Currently there are automated tests of the Slice object (encoding/decoding to buffers) in the tests directory. The plan is to add many more tests of the core FrontEndProcessor, TimeSliceProcessor, and PoolManager logic.
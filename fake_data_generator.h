#ifndef FAKE_DATA_GENERATOR_H
#define FAKE_DATA_GENERATOR_H

#include "data_structures.h"
#include "midas.h"
#include <vector>
#include "sys/time.h"

/** 
 * Class used in DataSourceDemo and the Dummy VX2740 FE for generating
 * random waveforms at a sepcified rate.
 */
class FakeDataGenerator {
  public:
    FakeDataGenerator() {};
    ~FakeDataGenerator() {};

    void begin_of_run();
    void end_of_run();

    void set_board_id(int _board_id);
    void set_settings(uint64_t _ch_mask, float _rate_Hz, uint16_t _event_size_words, double _slice_width_secs);
    INT populate_next_event(std::vector<std::shared_ptr<VXData> >& data, BOOL debug);

  private:

   /**
    * Calculate the gap to the next event, based on the average event rate
    * requested by the user, plus some randomization.
    *
    * Populates `gap_to_next_event_secs`.
    */
   void calc_next_gap();

   int board_id = -1;

   uint64_t ch_mask = 0;
   float rate_Hz = 1;
   uint16_t event_size_words = 100;
   double slice_width_secs = 1; //!< Length of each slice.

   timeval time_bor = {0,0}; //!< Time when run started
   timeval time_last_event_gen = {0,0}; //!< Time when we last generated a fake data event
   double gap_to_next_event_secs = 0.1; //!< Gap from last event to next event
   timeval time_last_slice_stop = {0,0}; //!< Time when we last sent an "end of slice" special event
};

#endif
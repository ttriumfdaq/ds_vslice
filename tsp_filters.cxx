#include "tsp_filters.h"
#include "data_structures.h"
#include "json.hpp"
#include "midas.h"
#include "vslice_utils.h"

/** \file
 * Filters used in the TSP to reduce the amount of data saved per slice.
 */

TSPFilterBase::TSPFilterBase(nlohmann::json settings) {};

TSPFilterBase::~TSPFilterBase() {
   reset();
};

void TSPFilterBase::reset() {}

std::string TSPFilterBase::get_error_message() {
   return error_message;
}

TSPFilterNone::TSPFilterNone(nlohmann::json settings) : TSPFilterBase(settings) {};
TSPFilterNone::~TSPFilterNone(){};

INT TSPFilterNone::apply(Slice& in, Slice& out) {
   // Simply copy the data over to the output event
   for (auto it : in.get_hits_sorted_by_time()) {
      out.add_hit(it);
   }

   return SUCCESS;
}

TSPBasicTimeFilter::TSPBasicTimeFilter(nlohmann::json settings) : TSPFilterBase(settings)  {
   keep_ms = settings["Basic time filter"]["Keep first ms of slice"];
   fake_delay_ms = settings["Basic time filter"]["Fake processing delay (ms)"];
};

TSPBasicTimeFilter::~TSPBasicTimeFilter(){};

INT TSPBasicTimeFilter::apply(Slice& in, Slice& out) {
   double slice_start = in.slice_start_time_secs;
   double max_t = slice_start + (keep_ms / 1000);

   // Input data to keep. Use pointers in this case to avoid having
   // to copy too much over.
   int num_kept = 0;
   int num_total = 0;

   for (auto it : in.get_hits_sorted_by_time()) {
      num_total++;

      if (it->hit->time_since_run_start_secs < max_t) {
         out.add_hit(it);
         num_kept++;
      }
   }

   vslice::ts_printf("(proc) Basic time filter: Keeping %lu/%lu entries that were before %lfs for slice %d\n", num_kept, num_total, max_t, in.slice_idx);

   if (fake_delay_ms > 0) {
      ss_sleep(fake_delay_ms);
   }

   return SUCCESS;
}

#ifndef TSP_MESSAGE_H
#define TSP_MESSAGE_H

#include <string>
#include <stdio.h>

/** \file
 *
 * Constants for TSP/PM communication.
 */

/** @defgroup TSP_STATE_GROUP Codes sent by TSP to PM to report the current status.
 *
 * @{
 */

/** TSP is offline. */
#define TSP_OFFLINE 0

/** TSP is starting. */
#define TSP_INIT 10

/** TSP is ready to accept a new slice. */
#define TSP_IDLE 11

/** TSP has started to receive data. */
#define TSP_TXSTARTED 20

/** TSP has finished receiving data. */
#define TSP_TXCOMPLETE 21

/** TSP experienced a timeout waiting for data from a FEP. */
#define TSP_RECVTIMEOUT 22

/** TSP has started analyzing data. */
#define TSP_ANASTARTED 30

/** TSP has finished analyzing data and is ready to accept a new slice. */
#define TSP_ANAFINISHED 31

/** TSP experienced an error processing data. */
#define TSP_ANAFAILED 32

/** TSP is still alive (send periodically). */
#define TSP_HEARTBEAT 99

/** @} */

/** Convert one of the TSP_xxx states (defined in TSP_STATE_GROUP) to text. */
inline std::string tsp_state_to_text(int state) {
    switch (state) {
        case TSP_OFFLINE: return "TSP_OFFLINE";
        case TSP_INIT: return "TSP_INIT";
        case TSP_IDLE: return "TSP_IDLE";
        case TSP_TXSTARTED: return "TSP_TXSTARTED";
        case TSP_TXCOMPLETE: return "TSP_TXCOMPLETE";
        case TSP_ANASTARTED: return "TSP_ANASTARTED";
        case TSP_ANAFINISHED: return "TSP_ANAFINISHED";
        case TSP_ANAFAILED: return "TSP_ANAFAILED";
        case TSP_HEARTBEAT: return "TSP_HEARTBEAT";
        default:
            char msg[128];
            snprintf(msg, 128, "Unknown state %d", state);
            return msg;
    }
}

#endif

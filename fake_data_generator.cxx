#include "fake_data_generator.h"
#include "vslice_utils.h"

void FakeDataGenerator::begin_of_run() {
   gettimeofday(&time_bor, NULL);
   gettimeofday(&time_last_event_gen, NULL);
   gettimeofday(&time_last_slice_stop, NULL);
   calc_next_gap();
}

void FakeDataGenerator::end_of_run() {

}

void FakeDataGenerator::set_board_id(int _board_id) {
  board_id = _board_id;
}

void FakeDataGenerator::set_settings(uint64_t _ch_mask, float _rate_Hz, uint16_t _event_size_words, double _slice_width_secs) {
  ch_mask = _ch_mask;
  rate_Hz = _rate_Hz;
  event_size_words = _event_size_words;
  slice_width_secs = _slice_width_secs;
}

INT FakeDataGenerator::populate_next_event(std::vector<std::shared_ptr<VXData> >& data, BOOL debug) {
   INT status = SUCCESS;
   timeval now;
   gettimeofday(&now, NULL);

   double delta_last_ev_secs = (now.tv_sec - time_last_event_gen.tv_sec) + (1e-6 * (now.tv_usec - time_last_event_gen.tv_usec));
   double delta_last_stop_secs = (now.tv_sec - time_last_slice_stop.tv_sec) + (1e-6 * (now.tv_usec - time_last_slice_stop.tv_usec));
   double delta_bor_secs = (now.tv_sec - time_bor.tv_sec) + (1e-6 * (now.tv_usec - time_bor.tv_usec));

   bool includes_end_of_slice = false;

   if (delta_last_stop_secs > slice_width_secs && delta_bor_secs > slice_width_secs) {
      // Create special end-of-slice event
      std::shared_ptr<VXData> event = std::make_shared<VXData>();
      event->board_id = board_id;
      event->timestamp_secs = delta_bor_secs;
      event->is_end_of_slice = true;

      if (debug) {
         vslice::ts_printf("(data) Board %d generated end-of-slice event at time %lf of size %u bytes\n", board_id, event->timestamp_secs, event->get_encoded_size_bytes());
      }

      data.push_back(event);
      includes_end_of_slice = true;
   } else if (delta_last_ev_secs > gap_to_next_event_secs) {
      // Create regular event with data

      for (int chan_id = 0; chan_id < 64; chan_id++) {
         // See if channel enabled
         if (!(ch_mask & ((uint64_t)1 << chan_id))) {
            continue;
         }

         std::shared_ptr<VXData> event = std::make_shared<VXData>();
         event->board_id = board_id;
         event->timestamp_secs = delta_bor_secs;
         event->channel_id = chan_id;

         // Add waveform data for normal events
         event->wf_samples.resize(event_size_words * 4);

         for (int w = 0; w < event_size_words * 4; w += 4) {
            uint16_t s1 = (w + (uint64_t) chan_id * 0x1000) & 0xFFFF;
            event->wf_samples[w] = s1;
            event->wf_samples[w + 1] = (s1 + 1) & 0xFFFF;
            event->wf_samples[w + 2] = (s1 + 2) & 0xFFFF;
            event->wf_samples[w + 3] = (s1 + 3) & 0xFFFF;
         }

         if (debug) {
            vslice::ts_printf("(data) Board %d generated regular event at time %lf of size %u bytes for channel %d\n", board_id, event->timestamp_secs, event->get_encoded_size_bytes(), chan_id);
         }
         data.push_back(event);
      }

      time_last_event_gen = now;
      calc_next_gap();
   }

   if (includes_end_of_slice) {
      time_last_slice_stop = now;
   }

   return status;
}

void FakeDataGenerator::calc_next_gap() {
   double random_mean = 1. / rate_Hz;
   double random_lower = 1e-3;

   if (random_mean < random_lower) {
      random_mean = random_lower;
   }

   double random_range = 2 * (random_mean - random_lower);
   gap_to_next_event_secs = random_range * (random() / (RAND_MAX + 1.0)) + random_lower;
}
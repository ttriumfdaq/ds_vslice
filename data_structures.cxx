#include "data_structures.h"
#include <cstdint>
#include <cstring>
#include <vector>
#include <algorithm>

/** \file
 * Structures for VX2740/Slice data, and their conversion to/from binary buffers.
 */

#define CLOCK_RATE_HZ 125000000

// Although the real boards will never give more than 0x800 64-bit words
// from user FW, the data format can fit up to 0x7FFF 64-bit words.
// Use that value, as we may want to run with Scope FW and convert to
// user format.
#define MAX_WF_WORDS 0x7FFF

VXData::VXData() {
   timestamp_secs = 0;
   board_id = 0;
   channel_id = 0;
   is_end_of_slice = false;
}

uint32_t VXData::get_encoded_size_bytes() {
   return (num_waveform_words() + num_header_words()) * sizeof(uint64_t);
}

uint16_t VXData::num_header_words() {
   // timestamp + energy + wf_size
   return 3;
}

uint16_t VXData::num_waveform_words() {
   uint32_t num_words = wf_samples.size() / 4;

   if (wf_samples.size() % 4 > 0) {
      num_words++;
   }

   if (num_words > MAX_WF_WORDS) {
      num_words = MAX_WF_WORDS;
   }

   return num_words;
}

// Encode to a buffer of uint64_t.
void VXData::encode(uint64_t *buf) {
   uint64_t timestamp_word = 0;
   uint64_t energy_word = 0;
   uint64_t wf_size_word = 0;

   timestamp_word |= ((uint64_t)(timestamp_secs * CLOCK_RATE_HZ) & 0xFFFFFFFFFFFF);
   timestamp_word |= ((uint64_t)(channel_id & 0x3F) << 56);

   if (is_end_of_slice) {
      timestamp_word |= ((uint64_t) 1 << 48);
   }

   energy_word |= (board_id & 0xFFFF);

   if (wf_samples.size() > MAX_WF_WORDS * 4) {
      // Set truncated bit
      wf_size_word = 0x8000 | MAX_WF_WORDS;
   }

   // Ensure vector is a multiple of 4 words, and truncated to max length if needed.
   wf_samples.resize(num_waveform_words() * 4);

   wf_size_word |= (num_waveform_words() & 0x7FFF);

   *buf++ = timestamp_word;
   *buf++ = energy_word;
   *buf++ = wf_size_word;

   // Convert vector of 16-bit samples into 64-bit words.
   // This is a critical path, so do a direct memcpy rather than
   // a more readable but slower iterative algorithm.
   memcpy((void*)buf, (void*)&wf_samples.front(), sizeof(uint16_t)*wf_samples.size());
   buf += (wf_samples.size() / 4);
}

// Decode from a buffer of uint64_t.
void VXData::decode(uint64_t *buf) {
   uint16_t wf_words_size;
   uint32_t size_bytes;
   decode_header(buf, wf_words_size, size_bytes);
   buf += num_header_words();

   // Convert 64-bit words into vector of 16-bit samples.
   // This is a critical path, so do a direct memcpy rather than
   // a more readable but slower iterative algorithm.
   wf_samples.resize(wf_words_size * 4);
   memcpy((void*)&wf_samples.front(), (void*)buf, sizeof(uint16_t)*wf_samples.size());
}

void VXData::decode_header(uint64_t* buf, uint16_t& wf_words_size, uint32_t& size_bytes) {
   uint64_t timestamp_word = *buf++;
   uint64_t energy_word = *buf++;
   uint64_t wf_size_word = *buf++;

   timestamp_secs = (double) (timestamp_word & 0xFFFFFFFFFFFF) / CLOCK_RATE_HZ;
   channel_id = (timestamp_word >> 56) & 0x3F;
   is_end_of_slice = (timestamp_word >> 48) & 0x1;

   board_id = (energy_word & 0xFFFF);

   wf_words_size = (wf_size_word & 0x7FFF);
   size_bytes = (num_header_words() + wf_words_size) * sizeof(uint64_t);
}

int FEPSliceHeader::get_num_bytes_needed_to_encode() {
   return sizeof(uint64_t);
}

void FEPSliceHeader::encode(uint64_t *buf) {
   *buf++ = (((uint64_t) slice_idx) << 32) | fe_idx;
}

void FEPSliceHeader::decode(uint64_t *buf) {
   uint64_t word1 = *buf++;
   slice_idx = word1 >> 32;
   fe_idx = word1 & 0xFFFFFFFF;
}


bool QT::encode(uint64_t* buf, uint32_t& num_bytes_written, double slice_start_time_secs) {
   uint32_t ticks = time_diff_to_ticks(slice_start_time_secs, time_since_run_start_secs);
   *buf++ = encode_3bit_flag(Slice::kHitFlagQT) | encode_bits(charge, 29, 24) | encode_bits(ticks, 0, 29);
   num_bytes_written = sizeof(uint64_t);
   return true;
}

bool QT::decode(uint64_t* buf, uint32_t& num_bytes_read, double slice_start_time_secs) {
   uint64_t word = *buf++;

   if (!check_3bit_flag(word, Slice::kHitFlagQT)) {
      return false;
   }

   charge = extract_bits(word, 29, 24);

   uint64_t ticks = extract_bits(word, 0, 29);
   time_since_run_start_secs = slice_start_time_secs + ((double)ticks / VX2740_CLOCK_FREQ);

   num_bytes_read = sizeof(uint64_t);

   return true;
}

uint32_t QT::get_num_bytes_needed_to_encode() {
   return sizeof(uint64_t);
}

bool MultiHit::encode(uint64_t* buf, uint32_t& num_bytes_written, double slice_start_time_secs) {
   uint64_t* buf_start = buf;
   uint32_t ticks = time_diff_to_ticks(slice_start_time_secs, time_since_run_start_secs);

   if (prominances.size() > 256) {
      printf("Too many prominances to encode in a single MultiHit");
      return false;
   }

   uint16_t prom_0 = 0;

   if (prominances.size() > 0) {
      if (prominances.find(0) == prominances.end()) {
         printf("No prominance with a delta-t of 0!");
         return false;
      }

      prom_0 = prominances[0];
   }

   *buf++ = encode_3bit_flag(Slice::kHitFlagMultiHit) | encode_bits(prominances.size(), 53, 8) | encode_bits(charge, 29, 24) | encode_bits(ticks, 0, 29);
   *buf++ = encode_bits(rise_time, 48, 16) | encode_bits(length, 32, 16) | encode_bits(prom_0, 0, 16);
   *buf = 0;
   
   bool upper_bits = true;

   for (auto prom : prominances) {
      if (prom.first == 0) {
         // Already handled prominance at delta-t of 0
         continue;
      }

      uint16_t prom_pos = upper_bits ? 32 : 0;
      uint16_t time_pos = upper_bits ? 48 : 16;

      *buf |= encode_bits(prom.first, time_pos, 16) | encode_bits(prom.second, prom_pos, 16);

      if (!upper_bits) {
         *buf++ = 0;
      }

      upper_bits = !upper_bits;
   }

   if (!upper_bits) {
      buf++;
   }

   num_bytes_written = (buf - buf_start) * sizeof(uint64_t);
   return true;
}

bool MultiHit::decode(uint64_t* buf, uint32_t& num_bytes_read, double slice_start_time_secs) {
   uint64_t* buf_start = buf;
   uint64_t word_0 = *buf++;

   if (!check_3bit_flag(word_0, Slice::kHitFlagMultiHit)) {
      return false;
   }

   uint16_t n_prominances = extract_bits(word_0, 53, 8);
   charge = extract_bits(word_0, 29, 24);

   uint64_t ticks = extract_bits(word_0, 0, 29);
   time_since_run_start_secs = slice_start_time_secs + ((double)ticks / VX2740_CLOCK_FREQ);

   uint64_t word_1 = *buf++;
   rise_time = extract_bits(word_1, 48, 16);
   length = extract_bits(word_1, 32, 16);

   if (n_prominances > 0) {
      prominances[0] = extract_bits(word_1, 0, 16);
   }

   for (int i = 1; i < n_prominances; i += 2) {
      uint64_t word = *buf++;
      uint16_t dt_a = extract_bits(word, 48, 16);
      uint16_t prom_a = extract_bits(word, 32, 16);
      prominances[dt_a] = prom_a;

      if (i + 1 < n_prominances) {
         uint16_t dt_b = extract_bits(word, 16, 16);
         uint16_t prom_b = extract_bits(word, 0, 16);
         prominances[dt_b] = prom_b;
      }
   }

   num_bytes_read = (buf - buf_start) * sizeof(uint64_t);

   return true;
}

uint32_t MultiHit::get_num_bytes_needed_to_encode() {
   // 2 64-bit words minimum (including first prominance).
   // Then 2 prominances per 64-bit word.
   return sizeof(uint64_t) * (2 + (prominances.size() / 2));
}

bool Waveform::encode(uint64_t* buf, uint32_t& num_bytes_written, double slice_start_time_secs) {
   uint64_t* buf_start = buf;

   uint32_t ticks = time_diff_to_ticks(slice_start_time_secs, time_since_run_start_secs);
   uint32_t wf_size_words = (samples.size() / 4); // Truncate to multiple of 4 samples if needed
   uint32_t wf_size_bytes = wf_size_words * sizeof(uint64_t);
   *buf++ = encode_3bit_flag(Slice::kHitFlagWaveform) | encode_bits(algorithms_applied, 45, 16) | encode_bits(wf_size_bytes, 29, 16) | encode_bits(ticks, 0, 29);

   // Convert vector of 16-bit samples into 64-bit words.
   // This is a critical path, so do a direct memcpy rather than
   // a more readable but slower iterative algorithm.
   memcpy((void*)buf, (void*)&samples.front(), wf_size_bytes);
   buf += wf_size_words;

   num_bytes_written = (buf - buf_start) * sizeof(uint64_t);
   return true;
}

bool Waveform::decode(uint64_t* buf, uint32_t& num_bytes_read, double slice_start_time_secs) {
   uint64_t* buf_start = buf;
   num_bytes_read = 0;

   if (!check_3bit_flag(*buf, Slice::kHitFlagWaveform)) {
      return false;
   }

   uint64_t ticks = extract_bits(*buf, 0, 29);
   time_since_run_start_secs = slice_start_time_secs + ((double)ticks / VX2740_CLOCK_FREQ);

   uint32_t wf_size_bytes = extract_bits(*buf, 29, 16);
   uint32_t num_samples = wf_size_bytes / sizeof(uint16_t);
   uint32_t wf_size_words = wf_size_bytes / sizeof(uint64_t);
   samples.resize(num_samples);

   algorithms_applied = extract_bits(*buf, 45, 16);

   // Convert 64-bit words into vector of 16-bit samples.
   // This is a critical path, so do a direct memcpy rather than
   // a more readable but slower iterative algorithm.
   buf++;
   memcpy((void*)&samples.front(), (void*)buf, wf_size_bytes);
   buf += wf_size_words;

   num_bytes_read = (buf - buf_start) * sizeof(uint64_t);

   return true;
}

uint32_t Waveform::get_num_bytes_needed_to_encode() {
   // 1 header, then 4 samples per 64-bit word.
   uint32_t num_words = 1;
   num_words += samples.size() / 4;

   return num_words * sizeof(uint64_t);
}

Slice::Slice() {
   data_format_version = kMaxDataFormatVersion;
   slice_idx = 0;
   slice_start_time_secs = 0;
}

void Slice::reset() {
   sprintf(error_message, "%s", "");
   data_format_version = kMaxDataFormatVersion;
   slice_idx = 0;
   slice_start_time_secs = 0;
   channel_qts.clear();
   channel_multihits.clear();
   channel_waveforms.clear();
   event_summaries.clear();
}

void Slice::dump(bool with_hits) {
   printf("Slice #%d starting at %lfs:\n", slice_idx, slice_start_time_secs);

   printf(" QTs:\n");

   for (auto it : channel_qts) {
      printf("    FE %d, board %d, chan %d:", it.first.frontend_id, it.first.board_id, it.first.channel_id);

      if (with_hits) {
         printf("\n");

         for (auto qt : it.second) {
            printf("      T = %lfs, Q = %u\n", qt->time_since_run_start_secs, qt->charge);
         }
      } else {
         printf(" %lu QTs\n", it.second.size());
      }
   }

   printf(" MultiHits:\n");

   for (auto it : channel_multihits) {
      printf("    FE %d, board %d, chan %d:", it.first.frontend_id, it.first.board_id, it.first.channel_id);

      if (with_hits) {
         printf("\n");

         for (auto mh : it.second) {
            printf("      T = %lfs, Q = %u, Nhits = %lu\n", mh->time_since_run_start_secs, mh->charge, mh->prominances.size());
         }
      } else {
         printf(" %lu MultiHits\n", it.second.size());
      }
   }

   printf(" Waveforms:\n");

   for (auto it : channel_waveforms) {
      printf("    FE %d, board %d, chan %d:", it.first.frontend_id, it.first.board_id, it.first.channel_id);

      if (with_hits) {
         printf("\n");

         for (auto wf : it.second) {
            printf("      T = %lfs, len = %lu samples\n", wf->time_since_run_start_secs, wf->samples.size());
         }
      } else {
         printf(" %lu waveforms\n", it.second.size());
      }
   }
}


std::set<ChannelId> Slice::get_channels_with_hit_data() {
   std::set<ChannelId> all_channels;

   for (auto it : channel_qts) {
      all_channels.insert(it.first);
   }

   for (auto it : channel_multihits) {
      all_channels.insert(it.first);
   }
   for (auto it : channel_waveforms) {
      all_channels.insert(it.first);
   }

   return all_channels;
}

bool Slice::encode(uint64_t* buf, uint32_t& num_bytes_written) {
   uint64_t* buf_start = buf;

   // Global header (size will be updated later)
   *buf++ = encode_bits(data_format_version, 54, 10) | encode_bits(slice_idx, 32, 22) | encode_bits(0, 0, 32);
   *buf++ = encode_bits(slice_start_time_secs * VX2740_CLOCK_FREQ, 0, 48);

   // Section for channel data (size will be updated later)
   uint64_t* section_header = buf;
   *buf++ = encode_4bit_flag(kSectionFlagChannelData) | encode_bits(0, 0, 32);

   for (auto chan : get_channels_with_hit_data()) {
      // Next channel header (size will be updated later)
      uint64_t* buf_chan_header = buf;
      *buf++ = encode_4bit_flag(kBoardDataFlagNextChannel) | encode_bits(chan.frontend_id, 44, 6) | encode_bits(chan.board_id, 40, 4) | encode_bits(chan.channel_id, 32, 8) | encode_bits(0, 0, 32);

      for (auto qt : channel_qts[chan]) {
         uint32_t obj_size = 0;
         qt->encode(buf, obj_size, slice_start_time_secs);

         buf += (obj_size / sizeof(uint64_t));
      }

      for (auto mh : channel_multihits[chan]) {
         uint32_t obj_size = 0;
         mh->encode(buf, obj_size, slice_start_time_secs);

         buf += (obj_size / sizeof(uint64_t));
      }

      for (auto wf : channel_waveforms[chan]) {
         uint32_t obj_size = 0;
         wf->encode(buf, obj_size, slice_start_time_secs);

         buf += (obj_size / sizeof(uint64_t));
      }

      // Update channel header size
      *buf_chan_header |= encode_bits((buf - buf_chan_header) * sizeof(uint64_t), 0, 32);
   }

   // Update section header size
   *section_header |= encode_bits((buf - section_header) * sizeof(uint64_t), 0, 32);


   // Event-level info would go here





   // Final word to note end of data payload
   *buf++ = encode_4bit_flag(kGlobalFlagLastWord) | encode_bits(slice_idx, 0, 22);

   num_bytes_written = (buf - buf_start) * sizeof(uint64_t);

   // Update overall event size
   *buf_start |= encode_bits(num_bytes_written, 0, 32);

   return true;
}

bool Slice::decode(uint64_t* buf, uint32_t& num_bytes_read) {
   reset();

   data_format_version = extract_bits(*buf, 54, 10);

   if (data_format_version > kMaxDataFormatVersion) {
      snprintf(error_message, 1023, "Invalid data format version %u > %u", data_format_version, kMaxDataFormatVersion);
      return false;
   }

   uint64_t* buf_start = buf;

   // Global header (2 words)
   slice_idx = extract_bits(*buf, 32, 22);
   uint32_t total_size_words = extract_bits(*buf, 0, 32) / sizeof(uint64_t);
   buf++;
   slice_start_time_secs = (double)extract_bits(*buf, 0, 48) / VX2740_CLOCK_FREQ;

   buf++;
   bool found_trailer = false;

   // Sections
   while (buf - buf_start < total_size_words) {
      uint16_t section_flag = extract_4bit_flag(*buf);

      if (section_flag == kGlobalFlagLastWord) {
         found_trailer = true;
         break;
      } else if (section_flag == kSectionFlagChannelData) {
         uint64_t* section_start = buf;
         uint32_t section_size_words = extract_bits(*buf, 0, 32) / sizeof(uint64_t);

         buf++;

         // Find all the channels with data
         while (buf - section_start < section_size_words) {
            uint8_t next_chan_flag = extract_4bit_flag(*buf);

            if (next_chan_flag != kBoardDataFlagNextChannel) {
               snprintf(error_message, 1023, "Unexpected 'next channel' flag 0x%x at word %ld", next_chan_flag, (buf - buf_start));
               return false;
            }

            ChannelId chan;
            chan.frontend_id = extract_bits(*buf, 44, 6);
            chan.board_id = extract_bits(*buf, 40, 4);
            chan.channel_id = extract_bits(*buf, 32, 8);
            uint32_t chan_size_words = extract_bits(*buf, 0, 32) / sizeof(uint64_t);
            uint64_t* chan_start = buf;

            buf++;

            // Find all the objects for this channel
            while (buf - chan_start < chan_size_words) {
               uint32_t obj_size_bytes = 0;

               if (extract_3bit_flag(*buf) == kHitFlagWaveform) {
                  std::shared_ptr<Waveform> wf = std::make_shared<Waveform>();

                  if (!wf->decode(buf, obj_size_bytes, slice_start_time_secs)) {
                     snprintf(error_message, 1023, "Failed to decode waveform at word %ld", (buf - buf_start));
                     return false;
                  }

                  channel_waveforms[chan].push_back(wf);
               } else if (extract_3bit_flag(*buf) == kHitFlagQT) {
                  std::shared_ptr<QT> qt = std::make_shared<QT>();

                  if (!qt->decode(buf, obj_size_bytes, slice_start_time_secs)) {
                     snprintf(error_message, 1023, "Failed to decode QT at word %ld", (buf - buf_start));
                     return false;
                  }

                  channel_qts[chan].push_back(qt);
               } else if (extract_3bit_flag(*buf) == kHitFlagMultiHit) {
                  std::shared_ptr<MultiHit> mh = std::make_shared<MultiHit>();

                  if (!mh->decode(buf, obj_size_bytes, slice_start_time_secs)) {
                     snprintf(error_message, 1023, "Failed to decode MultiHit at word %ld", (buf - buf_start));
                     return false;
                  }

                  channel_multihits[chan].push_back(mh);
               } else {
                  snprintf(error_message, 1023, "Unexpected hit type at word %ld", (buf - buf_start));
                  return false;
               }

               buf += (obj_size_bytes / sizeof(uint64_t));
            }
         }
      } else if (section_flag == kSectionFlagEventSummaries) {
         snprintf(error_message, 1023, "Event summary decoding not implemented yet");
         return false;
      } else {
         snprintf(error_message, 1023, "Unexpected section flag 0x%x at word %ld", section_flag, (buf - buf_start));
         return false;
      }
   }

   if (!found_trailer) {
      snprintf(error_message, 1023, "Didn't see end-of-data flag");
      return false;
   }

   // Include the trailing word.
   num_bytes_read = (1 + buf - buf_start) * sizeof(uint64_t);

   return true;
}

int Slice::get_num_bytes_needed_to_encode() {
   // Global header and footer.
   int n_bytes = 3 * sizeof(uint64_t);

   // Channel data section
   n_bytes += 1 * sizeof(uint64_t);

   for (auto chan : get_channels_with_hit_data()) {
      // Next channel header
      n_bytes += 1 * sizeof(uint64_t);

      for (auto qt : channel_qts[chan]) {
         n_bytes += qt->get_num_bytes_needed_to_encode();
      }

      for (auto mh : channel_multihits[chan]) {
         n_bytes += mh->get_num_bytes_needed_to_encode();
      }

      for (auto wf : channel_waveforms[chan]) {
         n_bytes += wf->get_num_bytes_needed_to_encode();
      }
   }

   return n_bytes;
}

/** Operator for sorting HitWithChannelId objects by timestamp. */
struct sort_hits_by_time {
   bool operator()(const std::shared_ptr<HitWithChannelId> a, const std::shared_ptr<HitWithChannelId> b) {
      return a->hit->time_since_run_start_secs < b->hit->time_since_run_start_secs;
   }
};

std::vector<std::shared_ptr<HitWithChannelId> > Slice::get_hits_sorted_by_time() {
   std::vector<std::shared_ptr<HitWithChannelId> > hits;

   for (auto chan_qt : channel_qts) {
      for (auto qt : chan_qt.second) {
         std::shared_ptr<HitWithChannelId> hit = std::make_shared<HitWithChannelId>();
         hit->chan.board_id = chan_qt.first.board_id;
         hit->chan.frontend_id = chan_qt.first.frontend_id;
         hit->chan.channel_id = chan_qt.first.channel_id;
         hit->hit = qt;
         hits.push_back(hit);
      }
   }

   for (auto chan_mh : channel_multihits) {
      for (auto mh : chan_mh.second) {
         std::shared_ptr<HitWithChannelId> hit = std::make_shared<HitWithChannelId>();
         hit->chan.board_id = chan_mh.first.board_id;
         hit->chan.frontend_id = chan_mh.first.frontend_id;
         hit->chan.channel_id = chan_mh.first.channel_id;
         hit->hit = mh;
         hits.push_back(hit);
      }
   }

   for (auto chan_wf : channel_waveforms) {
      for (auto wf : chan_wf.second) {
         std::shared_ptr<HitWithChannelId> hit = std::make_shared<HitWithChannelId>();
         hit->chan.board_id = chan_wf.first.board_id;
         hit->chan.frontend_id = chan_wf.first.frontend_id;
         hit->chan.channel_id = chan_wf.first.channel_id;
         hit->hit = wf;
         hits.push_back(hit);
      }
   }

   std::sort(hits.begin(), hits.end(), sort_hits_by_time());
   return hits;
}

void Slice::add_hit(std::shared_ptr<HitWithChannelId> hit) {
   if (is_qt(hit->hit)) {
      channel_qts[hit->chan].push_back(std::dynamic_pointer_cast<QT>(hit->hit));
   } else if (is_multihit(hit->hit)) {
      channel_multihits[hit->chan].push_back(std::dynamic_pointer_cast<MultiHit>(hit->hit));
   } else if (is_waveform(hit->hit)) {
      channel_waveforms[hit->chan].push_back(std::dynamic_pointer_cast<Waveform>(hit->hit));
   } else {
      printf("Unhandled object type (not QT or waveform)!\n");
   }
}

bool Slice::merge(Slice& other) {
   if (slice_idx != other.slice_idx) {
      return false;
   }

   for (auto it : other.get_hits_sorted_by_time()) {
      add_hit(it);
   }

   for (auto it : other.event_summaries) {
      event_summaries.push_back(it);
   }

   slice_start_time_secs = std::min(slice_start_time_secs, other.slice_start_time_secs);
   data_format_version = std::max(data_format_version, other.data_format_version);

   return true;
}

bool Slice::contains(ChannelId& chan, std::shared_ptr<HitBase> hit) {
   if (is_qt(hit)) {
      return std::find(channel_qts[chan].begin(), channel_qts[chan].end(), hit) != channel_qts[chan].end();
   } else if (is_multihit(hit)) {
      return std::find(channel_multihits[chan].begin(), channel_multihits[chan].end(), hit) != channel_multihits[chan].end();
   } else if (is_waveform(hit)) {
      return std::find(channel_waveforms[chan].begin(), channel_waveforms[chan].end(), hit) != channel_waveforms[chan].end();
   } else {
      return false;
   }
}

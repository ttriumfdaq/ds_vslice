/** \file
 *
 * Version of the TSP that runs as a standalone client.
 * Receives run transitions and settings from the pool manager.
 */

#include "midas.h"
#include "tsp_class.h"
#include "zmqpp/zmqpp.hpp"
#include <string.h>
#include <string>
#include <signal.h>

static BOOL ctrlc_pressed = FALSE;

void ctrlc_handler(int sig) {
    if (ctrlc_pressed) {
        printf("Received 2nd break. Hard abort.\n");
        exit(0);
    }
    printf("Received break. Aborting...\n");
    ctrlc_pressed = TRUE;
}

void usage() {
   printf("Usage: tsp_standalone.exe\n");
   printf("    -i <index>\n");
   printf("    -s <pool_manager_sub_endpoint>\n");
   printf("    -p <pool_manager_push_endpoint>\n");
   printf("   [-o <our_hostname>]\n");
   printf("\n");
   printf("If -o not specified, gethostname() will be used to look up the hostname that FEPs should use\n");
   printf("to send data to us.\n");
}

int main(int argc, char *argv[]) {
   // Parse options
   int frontend_index = -1;
   std::string pool_manager_sub_endpoint;
   std::string pool_manager_push_endpoint;
   std::string our_hostname;

   for (int i = 1; i < argc; i++) {
      if (strlen(argv[i]) != 2 || argv[i][0] != '-' || i + 1 >= argc || argv[i + 1][0] == '-') {
         usage();
         return 1;
      }

      if (argv[i][1] == 'i') {
         frontend_index = atoi(argv[++i]);
      } else if (argv[i][1] == 'p') {
         pool_manager_push_endpoint = argv[++i];
      } else if (argv[i][1] == 's') {
         pool_manager_sub_endpoint = argv[++i];
      } else if (argv[i][1] == 'o') {
         our_hostname = argv[++i];
      } else {
         usage();
         return 1;
      }
   }

   if (frontend_index < 0 || pool_manager_push_endpoint == "" || pool_manager_sub_endpoint == "") {
      usage();
      return 1;
   }

   signal(SIGINT, ctrlc_handler);
   TimeSliceProcessor tsp_class;

   tsp_class.set_frontend_index(frontend_index);
   tsp_class.set_pool_manager_endpoints(pool_manager_sub_endpoint, pool_manager_push_endpoint);

   if (our_hostname != "") {
      tsp_class.set_hostname_override(our_hostname);
   }

   INT status = tsp_class.init();

   if (status != SUCCESS) {
      return status;
   }

   try{
       do {
           tsp_class.do_periodic_work();

           if (tsp_class.is_event_ready_to_send()) {
               tsp_class.send_data(NULL);
           }

           if (ctrlc_pressed) {
               status = RPC_SHUTDOWN;
           }

           ss_sleep(1);
       } while (status != RPC_SHUTDOWN);
   }
   catch(int e){
       if(e == TSP_KILL_PM){
           return e;
       }
   }
   return 0;
}

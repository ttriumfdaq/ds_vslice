#ifndef FEP_CLASS_H
#define FEP_CLASS_H

#include "data_source_base.h"
#include "data_structures.h"
#include "vslice_utils.h"
#include "midas.h"
#include "odbxx.h"
#include "sys/time.h"
#include "zmqpp/zmqpp.hpp"
#include <map>
#include <mutex>
#include <thread>
#include <queue>
#include <vector>
#include <atomic>

#ifdef HAVE_VX
#include "vx2740_fe_class.h"
#endif

/** \file
 * Implementation of front end processor.
 */

/** Implements most of the logic for reading data from a real/simulated
 * VX2740, and sending the data to TSPs over a raw socket (and optionally
 * to midas buffers).
 *
 * There are many threads:
 * - N threads (1 per VX2740) for reading data from the boards.
 * - N threads (1 per VX2740) for filtering the data.
 * - 1 thread for accumulating the filtered data from each board
 *     into time slices.
 * - 1 main thread that talks to midas and sends data to midas
 *     buffers and/or TSPs.
 *
 * Raw data is written into ring buffers.
 * Filtered data is written to a queue of HitBase objects (Waveform or QT).
 * Time sliced data is written into a Slice.
 *
 * Integration with the MFE framework is in fep.cxx.
 *
 * Base classes are used to define the interfaces for anything that
 * is physics-relevant, to allow for easier addition/removal/testing of
 * those components.
 * - DataSourceBase is used to abstract away the reading of data from
 *    digitizers (or similutating it).
 * - FEPFilterBase is used to define filters that can be applied to the
 *    data. Multiple filters may be run if desired.
 */
class FrontEndProcessor {
public:

   /** Keeps track of the state of a given Slice. */
   class SliceInfo {
   public:
      bool complete; //!< We have all the data we need from all the boards.
      bool sending; //!< We're currently sending the data to a TSP.
      bool tsp_send_finished; //!< We've finished sending the data to a TSP.
      bool done_midas_event; //!< We've sent event to midas buffers.
      bool send_thread_finished; //!< We've done everything we need in `send_thread`, and it can be joined.
      INT tsp_send_retries; //!< Number of times we've tried to send the data to a TSP.
      Slice data; //!< The actual data in this slice.
      Slice unfiltered; //!< The unfiltered waveforms that were the basis for the final data in this slice.
      std::thread* send_thread; //!< Thread for sending this slice's data to a TSP.

      uint64_t* payload; //!< Encoded version of `data` that will be sent to TSP.

      /** Initialize a new Slice with the specified index. */
      SliceInfo(int slice_idx, double slice_start_time_secs);
   };

   FrontEndProcessor();
   ~FrontEndProcessor();

   /** Open ZMQ socket to pool manager, closing any existing socket if necessary
    *
    * \param[in] endpoint The PoolManager ZMQ endpoint (e.g. `tcp:://hostname:6543`).
    */
   void open_pm_socket(std::string endpoint);

   /** Check whether the PoolManager has sent us any messages.
    * The only messages we expect tell us which TSP to send data to
    * for a given slice.
    */
   void poll_pm();

   /** Note where to send data to for a given slice.
    * Updates the `zmq_data_endpoints` map.
    *
    * \param[in] endpoint TSP ZMQ endpoint.
    * \param[in] slice Slice index.
    */
   void enqueue_tsp_endpoint(std::string endpoint, int slice);

   /** Called when frontend program first starts.
    *
    * \param[in] _hDB Midas ODB handle.
    * \param[in] _max_event_size Max size of events we can sent to midas buffers.
    * \param[in] _fe_idx Index of this frontend (`-i` flag when program was started).
    * \return Midas status code.
    */
   INT init(HNDLE _hDB, INT _max_event_size, INT _fe_idx);

   /** Called at the start of each run.
    * Tidies up from the previous run, connects to digitizers if needed,
    * and spawns threads for reading data, filtering data, and accumulating data.
    *
    * \param[in] run_number Midas run number.
    * \param[out] error Error message, if we we don't want the run to start.
    * \return Midas status code.
    */
   INT begin_of_run(INT run_number, char *error);

   /** Called at the end of each run.
    * Closes all the threads that were spawned in `begin_of_run()`.
    *
    * \param[in] run_number Midas run number.
    * \param[out] error Error message, if we we don't want the run to start.
    * \return Midas status code.
    */
   INT end_of_run(INT run_number, char *error);

   /** This function is called frequently during a run. We just call `poll_pm()`. */
   INT do_periodic_work();

   /** Returns whether there is any data we need to send to TSPs (and the PM
    * has told us which TSP to send the data to).
    */
   bool is_event_ready_to_send();

   /** Main function for sending data to TSPs and/or midas buffers.
    *
    * If the event size is less than `max_event_size_bytes` and the user has
    * requested that events be sent to midas buffers (via an ODB key), we will
    * do that.
    *
    * If we haven't sent data to the TSP successfully yet, we will retry up to
    * `max_tsp_retries` times (configurable in the ODB).
    *
    * \param[in] pevent Midas buffer location to write data to.
    * \return Number of bytes written to midas buffer.
    */
   INT send_data(char *pevent);

   /** The ZMQ-specific bits of sending data to the relevant TSP.
    *
    * \param[in,out] slice Contains the Slice to send, and fields for recording
    *    the status of sending to the TSP.
    * \return Midas status code.
    */
   INT send_data_to_tsp(SliceInfo &slice);

   /** Update status variables in the ODB (e.g. CPU usage, raw/filtered slice
    * sizes etc.).
    *
    * \param[in] force If true, update the ODB even if we're not in a run.
    * \return Midas status code.
    */
   INT update_variables(bool force=false);

   /** Build the status string and color for midas status page.
    *
    * \param[out] msg Text to display on midas status page.
    * \param[out] color Color for our FE on midas status page `greenLight`, `yellowLight`
    *    and `redLight` match the default palette.
    */
   void get_status_strings(std::string& msg, std::string& color);

   /** Function that runs in a separate thread for reading data from the digitizer (or
    * simulating that) and populating a ring buffer. The ring buffer handle is stored
    * in `BoardReadout::raw_rb_handle`.
    *
    * \param[in] board_id Board number (generally 0-7) that this thread handles.
    */
   void *thread_data_generation(int board_id);

   /** Function that runs in a separate thread for filtering data that
    * `thread_data_generation()` put in a ring buffer. The results of the
    * filtered data are put in a queue of hits (in `BoardReadout::filtered_hits`).
    *
    * \param[in] board_id Board number (generally 0-7) that this thread handles.
    */
   void *thread_data_filter(int board_id);

   /** Function that runs in a separate thread for merging filtered
    * data from all boards. Reads from all the `BoardReadout::filtered_hits`
    * queues, and writes to the Slice object(s) in `slice_info`.
    */
   void *thread_accumulator();

   /** Function that runs in a separate thread for sending data to TSPs.
    * Reads that data from `slice_info` and updates the state of the
    * slice accordingly.
    *
    * \param[in] slice_index Index of Slice that should be sent by this thread.
    */
   void *thread_send_data(int slice_index);

private:
   /**
    * Helper struct for keeping all the data and stats for reading/filtering
    * a board in a single place.
    */
   typedef struct {
      DataSourceBase *data_source; //!< Data source used in `thread_data_generation()` that populates the ring buffer.

      void add_raw(std::vector<std::shared_ptr<VXData>> new_raw_vec) {
         std::lock_guard<std::mutex> lock(raw_mutex);

         for (auto& new_raw : new_raw_vec) {
            raw_data.push(new_raw);
            raw_data_size_bytes += new_raw->get_encoded_size_bytes();
         }
      }

      std::shared_ptr<VXData> get_raw() {
         std::lock_guard<std::mutex> lock(raw_mutex);
         
         std::shared_ptr<VXData> retval = raw_data.front();
         raw_data.pop();

         if (raw_data_size_bytes < retval->get_encoded_size_bytes()) {
            raw_data_size_bytes = 0;
         } else {
            raw_data_size_bytes -= retval->get_encoded_size_bytes();
         }

         return retval;
      }

      void add_filt(std::vector<std::shared_ptr<FilteredAndUnfiltered>> new_filt_vec) {
         std::lock_guard<std::mutex> lock(filter_mutex);

         for (auto new_filt : new_filt_vec) {
            filter_output.push(new_filt);
            filter_output_size_bytes += new_filt->filtered->get_num_bytes_needed_to_encode();
            filter_output_size_bytes += new_filt->unfiltered->get_num_bytes_needed_to_encode();
         }
      }

      std::shared_ptr<FilteredAndUnfiltered> get_filt() {
         std::lock_guard<std::mutex> lock(filter_mutex);

         std::shared_ptr<FilteredAndUnfiltered> retval = filter_output.front();
         filter_output.pop();
         
         if (filter_output_size_bytes < retval->get_total_size_bytes()) {
            filter_output_size_bytes = 0;
         } else {
            filter_output_size_bytes -= retval->get_total_size_bytes();
         }

         return retval;
      }

      void reset_queues() {
         std::lock_guard<std::mutex> raw_lock(raw_mutex);
         std::lock_guard<std::mutex> filt_lock(filter_mutex);

         while (!raw_data.empty()) {
            raw_data.pop();
         }

         while (!filter_output.empty()) {
            filter_output.pop();
         }

         raw_data_size_bytes = 0;
         filter_output_size_bytes = 0;

         raw_mutex.unlock();
         filter_mutex.unlock();
      }

      inline bool has_raw() {
         return !raw_data.empty();
      }

      inline bool has_filt() {
         return !filter_output.empty();
      }

      double peek_raw_timestamp() {
         return has_raw() ? raw_data.front()->timestamp_secs : -1;
      }

      double peek_filt_timestamp() {
         return has_filt() ? filter_output.front()->filtered->time_since_run_start_secs : -1;
      }

      std::atomic<uint32_t> raw_data_size_bytes;
      std::atomic<uint32_t> filter_output_size_bytes;

      DWORD curr_slice_raw_bytes_total; //!< Size of raw data for this slice (so far). Filled by filter thread.
      DWORD curr_slice_filter_bytes_total;  //!< Size of filtered data for this slice (so far). Filled by filter thread.
      DWORD last_slice_raw_bytes_total; //!< Size of raw data for previous slice. Filled by filter thread.
      DWORD last_slice_filter_bytes_total; //!< Size of filtered data for previous slice. Filled by filter thread.
      int num_end_slices_seen_filt; //!< Number of `EndOfSlice` markers seen from this board. Filled by filter thread.
      int num_end_slices_seen_accum; //!< Number of `EndOfSlice` markers seen from this board. Filled by accumulator thread.

      std::mutex meta_mutex;

   private:
      std::mutex raw_mutex;
      std::mutex filter_mutex;
      std::queue<std::shared_ptr<VXData> > raw_data; //!< Data from the board.
      std::queue<std::shared_ptr<FilteredAndUnfiltered> > filter_output; //!< Data after filtering.
   } BoardReadout;

   /** Get which TSP to send data to for the given slice.
    *
    * \param[in] slice_idx The slice number we want to send.
    * \return Empty string if we don't know where to send the data yet, or the
    *    TSP ZMQ endpoint if we do.
    */
   std::string get_data_endpoint_for_slice(int slice_idx);

   /** Get the next slice that is ready to be sent to TSPs. If no slice available,
    * calling `.get()` on shared_ptr will return 0.
    */
   std::shared_ptr<SliceInfo> get_next_slice_ready_to_be_sent();

   /** Get the number of threads that are currently sending data to TSPs. 
    * This function uses the `slice_info_mutex`. Do not call it if you have 
    * already acquired that mutex! 
    */
   int get_num_send_threads();

   ///@name Core logic variables
   ///@{
   bool in_run; //!< Whether we're currently in a run or not.
   bool in_end_of_run; //!< Signal to the spawned threads that it's time to stop.

   /** Board readout, keyed by board ID. */
   std::map<int, BoardReadout> board_readout;

   /** Threads for generating fake data and adding to raw ring buffers. Keyed by board ID. */
   std::map<int, std::thread*> readout_threads;

   /** Threads for filtering raw data and adding to queue of filtered data. Keyed by board ID. */
   std::map<int, std::thread*> filter_threads;

   /** Thread for assembling filtered data from all boards into a single memory region. */
   std::thread *accumulator_thread;

   /** Filtered data from all boards, assembled into slices. */
   std::deque<std::shared_ptr<SliceInfo> > slice_info;

   /** Mutex for accessing content of `slice_info` (mostly so queue doesn't)
    * change size while another thread is iterating over it.
    */
   std::mutex slice_info_mutex;

#ifdef HAVE_VX
   /** Handles board settings for real digitizers (DAC offsets, trigger config etc).
    * `VX2740GroupFrontend` class is defined in the dsproto_vx2740 repository.
    */
   VX2740GroupFrontend* configurator;
#endif

   uint64_t first_slice_send_time_ms; //!< Time we sent the first slice out.
   uint64_t last_slice_send_time_ms; //!< Time we sent the last slice out.
   int last_slice_sent; //!< Last slice we sent out.
   int num_slices_lost; //!< Number of slices we failed to send to TSP.

   int this_frontend_index; //!< Frontend index (-i flag when the program was started).
   INT max_event_size_bytes; //!< Max size of event we'll send to midas buffers.
   INT max_event_read_per_board_bytes; //!< Max data we'll read from a board per event (max ring buffer increment).

   ///@}
   ///
   ///@name ZMQ communication parameters
   ///@{

   zmqpp::context zmq_context; //!< ZMQ access
   std::string zmq_pm_endpoint; //!< How to connect to the PoolManager
   zmqpp::socket pm_socket; //!< ZMQ connection to PoolManager

   std::map<int, std::string> data_endpoints; //!< TSP endpoints, keyed by slice index.

   ///@}
   ///
   ///@name Members related to ODB settings
   ///@{

   HNDLE hDB; //!< Midas ODB access.
   midas::odb fep_settings; //!< ODB settings specific to this FEP.
   midas::odb fep_global_settings; //!< /Global subdirectory of `fep_settings`.
   midas::odb shared_settings; //!< ODB settings common to all FEPs.

   bool debug; //!< Quick access to ODB value we use a lot - whether to print debug statements to stdout.
   bool use_demo_data; //!< Quick access to ODB value we use a lot - whether to read real or fake data.
   bool use_dummy_vx_data; //!< Quick access to ODB value we use a lot - whether to read real or fake data.
   bool use_toy_mc; //!< Quick access to ODB value we use a lot - whether to expect fake data from the ToyMC machine.
   bool write_data_to_banks; //!< Quick access to ODB value we use a lot - whether to write filtered data to midas buffers/banks.
   bool write_unfiltered_to_banks; //!< Quick access to ODB value we use a lot - whether to write unfiltered data to midas buffers/banks.
   int max_tsp_retries; //!< Quick access to ODB value we use a lot - max number of tries to send data to TSP before giving up.
   int max_raw_buf_bytes; //!< Quick access to ODB value we use a lot - max size of BoardReadout::raw_data per board
   int max_filt_buf_bytes; //!< Quick access to ODB value we use a lot - max size of BoardReadout::filter_output per board
   std::string pm_endpoint; //!< Quick access to ODB value we use a lot - endpoint for us to talk to the PoolManager.

   int num_boards; //!< Number of boards this FEP talks to. Read from the ODB at frontend init only.

   std::vector<int> port_ids; //!< Port IDs for the FEP to listen on to get data from ToyMC server (One port per fake digitiser) 
   int status_port; //!< Port IDs for the FEP to listen on to get data from ToyMC server (One port per fake digitiser) 
   bool is_scope_mode; //!< Whether real VX boards are running Scope or User firmware.

   ///@}
   ///
   ///@name Members related to ODB variables
   ///@{

   midas::odb variables; //!< ODB Equipment variables (detailed status)

   /** Slice index when we last updated the ODB variables. Vector index is board ID. */
   std::vector<int> last_var_write_slice_idx;

   /** Size of raw data for most recent 100 slices. Vector index is board ID. */
   std::vector<std::deque<DWORD> > raw_slice_sizes_last_100;

   /** Size of filtered data for most recent 100 slices. Vector index is board ID. */
   std::vector<std::deque<DWORD> > filt_slice_sizes_last_100;

   /** Total size of raw data for all slices this run. Vector index is board ID. */
   std::vector<uint64_t> raw_slice_sizes_run_total;

   /** Total size of filtered data for all slices this run. Vector index is board ID. */
   std::vector<uint64_t> filt_slice_sizes_run_total;

   /** Number of slices added to `raw_slice_sizes_run_total` and `filt_slice_sizes_run_total`.
    * May not simply be the number of slices seen, if we ever miss adding a slice. 
    * Vector index is board ID. */
   std::vector<uint64_t> num_entries_for_run_totals;

   std::vector<DWORD> acq_status; //!< Acquisition status as read from the board. Vector index is board ID.
   std::vector<float> temp_in; //!< Input temperature read from each board. Vector index is board ID.
   std::vector<float> temp_out; //!< Output temperature read from each board. Vector index is board ID.
   std::vector<float> temp_hottest_adc; //!< Hottest ADC temperature read from each board. Vector index is board ID.

   vslice::cpu_info cpu_info; //!< Used to monitor how much CPU we're using.

   ///@}

};

#endif

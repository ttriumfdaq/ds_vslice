#include "vslice_utils.h"
#include <string>

/** \file
 * Helper functions for Linux TCP sockets.
 */

/** \namespace socket_utils

Helper functions for creating a TCP socket or server socket.

Ensures sensible default parameters, and includes wrappers for ensuring
the correct amount of data is read by the other end.

### Example server-side usage

\code{.cxx}
int port = 1234;
int listenfd = socket_utils::bind(port);

if (listenfd < 0) {
   printf("Failed to bind to port %d\n", port);
   return;
}

// Will block until somebody connects to us.
// You can specify a timeout if desired.
int connfd = socket_utils::accept(listenfd);

if (connfd < 0) {
   printf("Failed to accept on port %d\n", port);
   return;
} else {
   printf("Accepted connection on port %d\n", port);
}

// Wait for data to arrive
while (!socket_utils::wait_for_data(connfd, 5000)) {
   printf("Waiting for data...\n");
}

// Read all the data. If you know the max size you expect to read, you can
// pre-allocate a buffer like this. If not, you can replace the call to
// `read_fully_with_size_protocol()` with separate calls to `read_payload_size()`
// and `read_fully()`, allocating the buffer once you know the payload size.
unsigned int read_len;
unsigned int size_bytes = 10000;
char* recv_buffer = (char*) malloc(recv_buffer);

if (!socket_utils::read_fully_with_size_protocol(connfd, recv_buffer, size_bytes, read_len)) {
   printf("Reading data failed\n");
   break;
}

// Send a reply
char payload[10];
payload[0] = 1;
payload[1] = 36;

if (!socket_utils::write_fully_with_size_protocol(connfd, payload, 10)) {
   printf("Sending reply failed\n");
   break;
}

free(recv_buffer);
close(connfd);
close(listenfd);
\endcode


### Example clint-side usage


\code{.cxx}
std::string dest_host = "localhost";
int dest_port = 1234;

int sockfd = socket_utils::connect(dest_host.c_str(), dest_port);

if (sockfd < 0) {
   printf("Connect failed\n");
   return;
}

// Send the data
unsigned int send_size_bytes = 10000;
char* payload = (char*) malloc(send_size_bytes);
payload[0] = 99;
payload[1] = 123;

if (!socket_utils::write_fully_with_size_protocol(sockfd, payload, send_size_bytes)) {
   printf("Sending failed\n");
   break;
}

// Wait for a reply
while (!socket_utils::wait_for_data(sockfd, 1000)) {
   printf("Waiting for response...\n");
}

// Read reply
unsigned int reply_len;
unsigned int buffer_size_bytes = 10000;
char* recv_buffer = malloc(recv_buffer);

if (!socket_utils::read_fully_with_size_protocol(sockfd, recv_buffer, buffer_size_bytes, reply_len)) {
   printf("Reading response failed\n");
   break;
}

free(payload);
free(recv_buffer);
close(sockfd);
\endcode
*/
namespace socket_utils {

/** Connect to a remote port.
 *
 * \param[in] dest_host IP address or hostname to connect to.
 * \param[in] dest_port Port number to connect to.
 * \param[in] connect_timeout_ms Timeout waiting for connection to be made.
 * \param[in] send_timeout_ms Timeout that will be used in later calls to `write_fully()` and `write_fully_with_size_protocol()`.
 * \param[in] recv_timeout_ms Timeout that will be used in later calls to `read_fully()` and `read_fully_with_size_protocol()`.
 * \param[in] enable_tcp_no_delay Whether to enable the TCP_NODELAY option in this socket (generally you want this enabled).
 * \return The socket file descriptor for use in later calls, or -1 if unable to connect.
 */
int connect(std::string dest_host, int dest_port, int connect_timeout_ms=1000, int send_timeout_ms=1000, int recv_timeout_ms=1000, bool enable_tcp_no_delay=true);

/** Bind to a local port, so other people can connect to you.
 *
 * You don't read/write data on this socket, but wait for other connections that you can `accept()`.
 * You can read/write data on the sockets returned by `accept()`.
 *
 * \param[in] port Port number to bind to.
 * \param[in] recv_timeout_ms Timeout for receiving data on this socket.
 * \param[in] max_pending_conns Maximum number of clients that can be waiting to connect, that you haven't accepted yet.
 * \param[in] enable_tcp_no_delay Whether to enable the TCP_NODELAY option in this socket (generally you want this enabled).
 * \return The socket file descriptor for use in later calls, or -1 if unable to bind.
 */
int bind(int port, int recv_timeout_ms=1000, int max_pending_conns=50, bool enable_tcp_no_delay=true);

/** Accept a connection on a port that was previously bound to.
 *
 * This function will BLOCK until either a connection is accepted, or the socket that was
 * bound to is closed.
 *
 * \param[in] sockfd The socket that was created by calling `bind()`.
 * \param[in] send_timeout_ms Timeout that will be used in later calls to `write_fully()` and `write_fully_with_size_protocol()`.
 * \param[in] recv_timeout_ms Timeout that will be used in later calls to `read_fully()` and `read_fully_with_size_protocol()`.
 * \param[in] enable_tcp_no_delay Whether to enable the TCP_NODELAY option in this socket (generally you want this enabled).
 * \return The socket file descriptor of accepted connection for use when reading/writing data.
 */
int accept(int sockfd, int send_timeout_ms=1000, int recv_timeout_ms=1000, bool enable_tcp_no_delay=true);

/** Wait for data to appear on the socket, up to a given timeout.
 *
 * Useful as a regular "read" blocks and can make it tricky to abort / clean up threads.
 *
 * \param[in] sockfd The socket that was created by calling `connect()` or `accept()`.
 * \param[in] timeout_in_ms Timeout after which this function will return if it hasn't received any data.
 * \return Whether data was available or not.
 */
bool wait_for_data(int sockfd, int timeout_in_ms);

/** Write a payload to a socket, with a header telling the other end how large the payload is.
 *
 * Can be read on the other end using `read_fully_with_size_protocol()`.
 *
 * \param[in] sockfd The socket that was created by calling `connect()` or `accept()`.
 * \param[in] payload The data to send.
 * \param[in] size_bytes The size of the payload in bytes.
 * \return Whether data could be written.
 */
bool write_fully_with_size_protocol(int sockfd, char *payload, uint64_t size_bytes);

/** Read a payload of unknown size from a socket, if it was sent using `write_fully_with_size_protocol()`.
 *
 * You must have pre-allocated a buffer that's large enough to fit the data that was sent. If you don't
 * know the maximum size you might receive, you could call `read_payload_size()`, then allocate a buffer
 * that's large enough, then call `read_fully()`.
 *
 * \param[in] sockfd The socket that was created by calling `connect()` or `accept()`.
 * \param[in] payload The data buffer to write the received data in to.
 * \param[in] max_size_bytes The size of the `payload` data buffer.
 * \param[out] recv_size_bytes The number of bytes written to `payload`.
 * \return Whether data could be read (and fits in the supplied buffer).
 */
bool read_fully_with_size_protocol(int sockfd, char *payload, uint64_t max_size_bytes, uint64_t& recv_size_bytes);

/** Write raw data to a socket.
 *
 * \param[in] sockfd The socket that was created by calling `connect()` or `accept()`.
 * \param[in] payload The data to send.
 * \param[in] size_bytes The size of the payload in bytes.
 * \return Whether data could be written.
 */
bool write_fully(int sockfd, char *payload, uint64_t size_bytes);

/** Read raw data from a socket until `size_bytes` has been read.
 *
 * \param[in] sockfd The socket that was created by calling `connect()` or `accept()`.
 * \param[in] payload The data buffer to write the received data in to.
 * \param[in] size_bytes The number of bytes to read.
 * \return Whether data could be read.
 */
bool read_fully(int sockfd, char *payload, uint64_t size_bytes);

/** Write a short payload to a socket containing a few magic numbers and
 * then a single unsigned int (representing the number of bytes to follow).
 *
 * You must follow this with a call to `write_fully()` that writes the specified
 * number of bytes to the same socket.
 *
 * \param[in] sockfd The socket that was created by calling `connect()` or `accept()`.
 * \param[in] size_bytes The number of bytes that you will send when you call `write_fully()`.
 * \return Whether data could be written.
 */
bool write_payload_size(int sockfd, uint64_t size_bytes);

/** Read a short payload from a socket that called `write_payload_size()`.
 *
 * You must follow this with a call to `read_fully()` that reads the specified number
 * of bytes from the same socket.
 *
 * \param[in] sockfd The socket that was created by calling `connect()` or `accept()`.
 * \param[out] size_bytes The number of bytes that you need to read when you call `read_fully()`.
 * \return Whether data could be read and whether the magic numbers match what we expect them to be.
 */
bool read_payload_size(int sockfd, uint64_t& size_bytes);
}// end of namespace socket_utils

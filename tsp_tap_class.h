#ifndef TSP_TAP_CLASS_H
#define TSP_TAP_CLASS_H
#include <string>
#include "zmqpp/zmqpp.hpp"
#include "midas.h"
#include "tmfe_rev0.h"
#include "odbxx.h"
#include "vslice_utils.h"

namespace vslice {
    void tapcallback(INT hDB, INT hkey, INT index, void *feptr);
}

class TspTap :
   public TMFeRpcHandlerInterface,
   public  TMFePeriodicHandlerInterface
{
public:
   TMFE* fMfe;
   TMFeEquipment* fEq;

   int fEventSize;
   char* fEventBuf;

   TspTap(TMFE* mfe, TMFeEquipment* eq); // ctor
   virtual ~TspTap(); // dtor

   void Init();
   void SendEvent(const char *bank, const char *buf, int eventsize);
   std::string HandleRpc(const char* cmd, const char* args);
   void HandleBeginRun();
   void HandleEndRun();
   void HandlePeriodic();
   void PollTSP();
   void fecallback(HNDLE hDB, HNDLE hkey, INT index);
private:
   zmqpp::context zmq_context;
   zmqpp::socket socket;
   std::string endpoint;
   int port = 31415;
   midas::odb readback;
   std::string hostname;
   int ntap = 0;
   int ntapped = 0;
};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

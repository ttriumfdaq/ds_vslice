#include "socket_utils.h"
#include "vslice_utils.h"
#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sstream>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

/** \file
 * Helper functions for Linux TCP sockets.
 */

bool enable_nodelay(int sockfd) {
   // Set TCP_NODELAY option, which can avoid 40ms latencies for small data packets.
   int flag = 1;
   int ret = setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof(flag));
   return ret != -1;
}

bool set_send_timeout(int sockfd, int millisecs) {
   // Set a timeout on sending data for a socket.
   struct timeval tv;
   tv.tv_sec = millisecs / 1000;
   tv.tv_usec = (millisecs % 1000) * 1000;
   int ret = setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (const char *) &tv, sizeof(tv));
   return ret != -1;
}

bool set_recv_timeout(int sockfd, int millisecs) {
   // Set a timeout on receiving data for a socket.
   struct timeval tv;
   tv.tv_sec = millisecs / 1000;
   tv.tv_usec = (millisecs % 1000) * 1000;
   int ret = setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (const char *) &tv, sizeof(tv));
   return ret != -1;
}

int socket_utils::connect(std::string dest_host, int dest_port, int connect_timeout_ms, int send_timeout_ms, int recv_timeout_ms, bool enable_tcp_no_delay) {
   int sockfd = socket(AF_INET, SOCK_STREAM, 0);

   if (sockfd == -1) {
      vslice::ts_printf("Failed to create socket\n");
      return -1;
   }

   // Build socket address
   struct sockaddr_in serv_addr;
   memset(&serv_addr, '0', sizeof(serv_addr));

   serv_addr.sin_family = AF_INET;
   serv_addr.sin_port = htons(dest_port);

   // Convert hostname to IP address
   struct hostent *host = gethostbyname(dest_host.c_str());

   if (host && host->h_length) {
      serv_addr.sin_addr.s_addr = *(long *) host->h_addr_list[0];
   }  else {
      vslice::ts_printf("No IP address found for host %s\n", dest_host.c_str());
      close(sockfd);
      return -1;
   }

   // Set socket options requested. Non-fatal errors if we can't set them.
   if (send_timeout_ms > 0) {
      if (!set_send_timeout(sockfd, send_timeout_ms)) {
         vslice::ts_printf("Failed to set send timeout to %d ms when connecting to %s:%d\n", send_timeout_ms, dest_host.c_str(), dest_port);
      }
   }

   if (recv_timeout_ms > 0) {
      if (!set_recv_timeout(sockfd, recv_timeout_ms)) {
         vslice::ts_printf("Failed to set recv timeout to %d ms when connecting to %s:%d\n", recv_timeout_ms, dest_host.c_str(), dest_port);
      }
   }

   if (enable_tcp_no_delay) {
      if (!enable_nodelay(sockfd)) {
         vslice::ts_printf("Failed to enable TCP_NODELAY when connecting to %s:%d\n", dest_host.c_str(), dest_port);
      }
   }

   // Try to connect. Retrying up to the timeout specified.
   bool connected = false;
   timeval start;
   gettimeofday(&start, NULL);

   while (!connected) {
      if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) == 0) {
         connected = true;
      } else {
         timeval now;
         gettimeofday(&now, NULL);

         if (1000. * (now.tv_sec - start.tv_sec) + 1e-3 * (now.tv_usec - start.tv_usec) < connect_timeout_ms) {
            // Failed to connect. Sleep a bit then retry.
            ss_sleep(50);
         } else {
            // Failed to connect, even after waiting a while.
            vslice::ts_printf("Connecting to %s:%d failed\n", dest_host.c_str(), dest_port);
            close(sockfd);
            return -1;
         }
      }
   }

   return sockfd;
}

int socket_utils::bind(int port, int recv_timeout_ms, int max_pending_conns, bool enable_tcp_no_delay) {
   int listenfd = socket(AF_INET, SOCK_STREAM, 0);

   // Build socket address
   struct sockaddr_in serv_addr;
   memset(&serv_addr, '0', sizeof(serv_addr));

   serv_addr.sin_family = AF_INET;
   serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
   serv_addr.sin_port = htons(port);

   // Set socket options requested
   if (recv_timeout_ms > 0) {
      if (!set_recv_timeout(listenfd, recv_timeout_ms)) {
         vslice::ts_printf("Failed to set recv timeout to %d ms when binding to port %d\n", recv_timeout_ms, port);
      }
   }

   if (enable_tcp_no_delay) {
      if (!enable_nodelay(listenfd)) {
         vslice::ts_printf("Failed to enable TCP_NODELAY when binding to port %d\n", port);
      }
   }

   // Try to bind
   vslice::ts_printf("Binding to port %d for receiving data\n", port);
   int status = bind(listenfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));

   if (status) {
      vslice::ts_printf("Failed to bind\n");
      return -1;
   }

   listen(listenfd, max_pending_conns);

   return listenfd;
}

int socket_utils::accept(int sockfd, int send_timeout_ms, int recv_timeout_ms, bool enable_tcp_no_delay) {
   int connfd = -1;

   while (connfd == -1) {
      connfd = accept(sockfd, (struct sockaddr *) NULL, NULL);

      if (connfd == -1) {
         if (errno == EWOULDBLOCK) {
            // Timeout, keep trying
            continue;
         } else {
            // Other socket error (maybe shutdown). Abort.
            break;
         }
      } else {
         if (send_timeout_ms > 0) {
            if (!set_send_timeout(sockfd, send_timeout_ms)) {
               vslice::ts_printf("Failed to set send timeout to %d ms on accepted connection\n", send_timeout_ms);
            }
         }

         if (recv_timeout_ms > 0) {
            if (!set_recv_timeout(sockfd, recv_timeout_ms)) {
               vslice::ts_printf("Failed to set recv timeout to %d ms on accepted connection\n", recv_timeout_ms);
            }
         }

         if (enable_tcp_no_delay) {
            if (!enable_nodelay(connfd)) {
               vslice::ts_printf("Failed to enable TCP_NODELAY on accepted connection\n");
            }
         }
      }
   }

   return connfd;
}

bool socket_utils::write_fully_with_size_protocol(int sockfd, char *payload, uint64_t size_bytes) {
   // Tell recipient how much data we're sending
   if (!write_payload_size(sockfd, size_bytes)) {
      vslice::ts_printf("Failed to write payload size\n");
      return false;
   }

   // Then send the actual data
   return write_fully(sockfd, payload, size_bytes);
}

bool socket_utils::read_fully_with_size_protocol(int sockfd, char *payload, uint64_t max_size_bytes, uint64_t &recv_size_bytes) {
   // Read how much data we expect to receive
   if (!read_payload_size(sockfd, recv_size_bytes)) {
      vslice::ts_printf("Failed to read payload size\n");
      return false;
   }

   // Sanity-check to make sure we can actually read that...
   if (recv_size_bytes > max_size_bytes) {
      vslice::ts_printf("Would want to read %d bytes into buffer of size %d bytes!", recv_size_bytes, max_size_bytes);
      return false;
   }

   // Read the data
   return read_fully(sockfd, payload, recv_size_bytes);
}

bool socket_utils::write_fully(int sockfd, char *payload, uint64_t size_bytes) {
   uint64_t tot_written = 0;

   while (tot_written < size_bytes) {
      uint64_t left = size_bytes - tot_written;
      
      if (left >= (1<<30)) {
         // Never try to write more bytes than can be represented in a signed int.
         left = (1<<30) - 1;
      };

      int this_written = write(sockfd, payload + tot_written, left);

      if (this_written == -1) {
         break;
      }

      tot_written += this_written;
   }

   return tot_written == size_bytes;
}

bool socket_utils::read_fully(int sockfd, char *payload, uint64_t size_bytes) {
   uint64_t tot_read = 0;

   while (tot_read < size_bytes) {
      uint64_t left = size_bytes - tot_read;
      
      if (left >= (1<<30)) {
         // Never try to read more bytes than can be represented in a signed int.
         left = (1<<30) - 1;
      }

      int this_read = read(sockfd, payload + tot_read, left);

      if (this_read == -1) {
         // Error
         break;
      }

      if (this_read == 0) {
         // EOF - other end hung up
         break;
      }

      tot_read += this_read;
   }

   return tot_read == size_bytes;
}

bool socket_utils::wait_for_data(int sockfd, int timeout_in_ms) {
   timeval tv;
   tv.tv_sec = timeout_in_ms / 1000;
   tv.tv_usec = (timeout_in_ms % 1000) * 1000;
   fd_set fdset;
   FD_ZERO(&fdset);
   FD_SET(sockfd, &fdset);
   int n = ::select(sockfd + 1, &fdset, 0, 0, &tv);

   if (n < 0) {
      if (errno != EAGAIN && errno != EINTR) {
         vslice::ts_printf("select() error: %d\n", errno);
      }

      return false;
   }

   if ((n == 0) || (!FD_ISSET(sockfd, &fdset))) {
      // Read timeout
      return false;
   }

   return true;
}

// Magic numbers to ensure we don't read garbage when trying to parse header info
// that tells us the amount of data to follow.
#define PAYLOAD_HEADER_NUM_WORDS 4
#define PAYLOAD_HEADER_MAGIC_WORD_0 0xACE87234
#define PAYLOAD_HEADER_MAGIC_WORD_1 0x36363636

bool socket_utils::write_payload_size(int sockfd, uint64_t size_bytes) {
   unsigned int payload[PAYLOAD_HEADER_NUM_WORDS];
   payload[0] = PAYLOAD_HEADER_MAGIC_WORD_0;
   payload[1] = PAYLOAD_HEADER_MAGIC_WORD_1;
   payload[2] = (size_bytes>>32);
   payload[3] = size_bytes;

   return write_fully(sockfd, (char *) payload, PAYLOAD_HEADER_NUM_WORDS * sizeof(unsigned int));
}

bool socket_utils::read_payload_size(int sockfd, uint64_t &size_bytes) {
   unsigned int payload[PAYLOAD_HEADER_NUM_WORDS];
   bool res = read_fully(sockfd, (char *) payload, PAYLOAD_HEADER_NUM_WORDS * sizeof(unsigned int));

   size_bytes = (((uint64_t)payload[2]) << 32) | payload[3];

   return (res && payload[0] == PAYLOAD_HEADER_MAGIC_WORD_0 && payload[1] == PAYLOAD_HEADER_MAGIC_WORD_1);
}

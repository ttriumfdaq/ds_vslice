#include "fep_class.h"
#include "data_source_demo.h"
#include "data_source_toy_mc.h"
#include "data_source_dummy_vx.h"
#include "data_structures.h"
#include "fep_filters.h"
#include "mfe.h"
#include "midas.h"
#include "msystem.h"
#include "socket_utils.h"
#include "vslice_utils.h"
#include "zmqpp/zmqpp.hpp"
#include <algorithm>
#include <iostream>
#include <thread>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <vector>
#include <chrono>

#ifdef HAVE_VX
#include "data_source_scope_mode.h"
#include "data_source_user_mode.h"
#include "vx2740_fe_class.h"
#endif

/** \file
 * Implementation of front end processor.
 */

/** Trivial struct for passing arguments to threads that need to
 * know about the main FrontEndProcessor object that spawned them,
 * and the board ID they should work on.
 */
typedef struct {
   FrontEndProcessor *obj; //!< Object that spawned the thread.
   int board_index; //!< Board ID the thread should work on.
} FEPBoardThreadArgs;

/** Trivial struct for passing arguments to threads that need to
 * know about the main FrontEndProcessor object that spawned them,
 * and the board ID they should work on.
 */
typedef struct {
   FrontEndProcessor *obj; //!< Object that spawned the thread.
   int slice_index; //!< Slice the thread should work on.
} FEPSliceThreadArgs;

/** We need an object that lives a long time to back the arguments passed to
 * thread spawn functions. Using a global map is the simplest way to do this.
 */
std::map<int, FEPBoardThreadArgs> fep_thread_args;

/** We need an object that lives a long time to back the arguments passed to
 * thread spawn functions. Using a global map is the simplest way to do this.
 */
std::map<int, FEPSliceThreadArgs> fep_slice_thread_args;

/** Wrapper that calls `FrontEndProcessor::thread_data_generation()` when a new thread starts. */
void *thread_data_generation_helper(void *arg) {
   FEPBoardThreadArgs *arg_cast = (FEPBoardThreadArgs *) arg;
   FrontEndProcessor *obj = arg_cast->obj;
   return obj->thread_data_generation(arg_cast->board_index);
}

/** Wrapper that calls `FrontEndProcessor::thread_data_filter()` when a new thread starts. */
void *thread_data_filter_helper(void *arg) {
   FEPBoardThreadArgs *arg_cast = (FEPBoardThreadArgs *) arg;
   FrontEndProcessor *obj = arg_cast->obj;
   return obj->thread_data_filter(arg_cast->board_index);
}

/** Wrapper that calls `FrontEndProcessor::thread_accumulator()` when a new thread starts. */
void *thread_accumulator_helper(void *arg) {
   FrontEndProcessor *obj = (FrontEndProcessor *) arg;
   return obj->thread_accumulator();
}

/** Wrapper that calls `FrontEndProcessor::thread_send_data()` when a new thread starts. */
void *thread_send_data_helper(void *arg) {
   FEPSliceThreadArgs *arg_cast = (FEPSliceThreadArgs *) arg;
   FrontEndProcessor *obj = arg_cast->obj;
   return obj->thread_send_data(arg_cast->slice_index);
}

FrontEndProcessor::SliceInfo::SliceInfo(int slice_idx, double slice_start_time_secs) {
   data.slice_idx = slice_idx;
   unfiltered.slice_idx = slice_idx;

   data.slice_start_time_secs = slice_start_time_secs;
   unfiltered.slice_start_time_secs = slice_start_time_secs;

   complete = false;
   sending = false;
   done_midas_event = false;
   tsp_send_finished = false;
   tsp_send_retries = 0;
   send_thread = 0;
   send_thread_finished = false;
   payload = NULL;
}

FrontEndProcessor::FrontEndProcessor() : pm_socket(zmq_context, zmqpp::socket_type::subscribe){
   this_frontend_index = -1;
   in_run = false;
   in_end_of_run = false;
   hDB = 0;
   debug = false;
   use_demo_data = false;
   use_dummy_vx_data = false;
   use_toy_mc = false;
   write_data_to_banks = true;
   write_unfiltered_to_banks = true;
   max_tsp_retries = 5;
   max_event_size_bytes = 0;
   max_event_read_per_board_bytes = 0;
   accumulator_thread = NULL;
   num_boards = -1;
   is_scope_mode = false;
   last_slice_sent = -1;
   first_slice_send_time_ms = 0;
   last_slice_send_time_ms = 0;
   num_slices_lost = 0;
   max_raw_buf_bytes = 500 * 1024 * 1024;   // per board
   max_filt_buf_bytes = 500 * 1024 * 1024;   // per board
   pm_socket.subscribe("");

   cpu_info = vslice::init_cpu_info();
   configurator = NULL;
}

FrontEndProcessor::~FrontEndProcessor() {
#ifdef HAVE_VX
   if (configurator) {
      delete configurator;
   }
#endif
}

void FrontEndProcessor::open_pm_socket(std::string endpoint) {
   if (endpoint != zmq_pm_endpoint) {
      if (zmq_pm_endpoint.size()) {
         pm_socket.disconnect(zmq_pm_endpoint);
      }

      zmq_pm_endpoint = endpoint;
      pm_socket.connect(zmq_pm_endpoint);
      vslice::ts_printf("Connected to %s for PM comms\n", zmq_pm_endpoint.c_str());
   }
}

void FrontEndProcessor::poll_pm() {
   zmqpp::message message;

   if (pm_socket.receive(message, true)) {
      std::string endpoint;
      int slice;
      message >> endpoint >> slice;
      bool success = endpoint.size();
      if (!success) {
         cm_msg(MERROR, __FUNCTION__, "Couldn't decode message from PM");
      }
      enqueue_tsp_endpoint(endpoint, slice);
   }
}

void FrontEndProcessor::enqueue_tsp_endpoint(std::string endpoint, int slice) {
   data_endpoints[slice] = endpoint;
   vslice::ts_printf("(main) PM has told us to send slice %d to %s\n", slice, endpoint.c_str());
}

void *FrontEndProcessor::thread_data_generation(int board_id) {
   vslice::ts_printf("(data) Spawning data generation thread for board %d\n", board_id);

   DataSourceBase *data_source = board_readout[board_id].data_source;

   // We readout metadata from the boards in this thread (rather than from the main
   // thread that actually writes the updated values to the ODB). This avoids issues
   // where the main thread would block waiting for access to the board if this thread
   // is doing a crazy amount of data readout.
   timeval last_meta_readout;
   gettimeofday(&last_meta_readout, NULL);

   while (!in_end_of_run) {
      if (!in_run) {
	      vslice::ts_printf("Sleeping waiting for run to start\n");
         std::this_thread::sleep_for(std::chrono::milliseconds(1));
         continue;
      }

      if (board_readout[board_id].raw_data_size_bytes > max_raw_buf_bytes) {
         // Buffer for raw data is full
         std::this_thread::sleep_for(std::chrono::milliseconds(1));
         continue;
      }

      std::vector<std::shared_ptr<VXData> > new_data;
      INT status = data_source->populate_next_event(new_data, debug);

      if (new_data.empty()) {
         std::this_thread::sleep_for(std::chrono::milliseconds(1));
      } else if (status != SUCCESS) {
         cm_msg(MERROR, __FUNCTION__, "Failed to generate event for board %d!", board_id);
         return NULL;
      }
      
      board_readout[board_id].add_raw(new_data);

      timeval now;
      gettimeofday(&now, NULL);

      if (now.tv_sec - last_meta_readout.tv_sec + 1e-6 * (now.tv_usec - last_meta_readout.tv_usec) > 2) {
         // At least 2 seconds since we last read metadata.
         gettimeofday(&last_meta_readout, NULL);

         data_source->get_acq_status(acq_status[board_id]);
         data_source->get_temperatures(temp_in[board_id], temp_out[board_id], temp_hottest_adc[board_id]);

         // Now yield to ensure other threads can do some work.
         std::this_thread::sleep_for(std::chrono::milliseconds(1));
         continue;
      }
   }

   vslice::ts_printf("(data) Stopping data generation thread for board %d\n", board_id);
   return NULL;
}

void *FrontEndProcessor::thread_data_filter(int board_id) {
   vslice::ts_printf("(filt) Spawning data filter thread for board %d\n", board_id);

   board_readout[board_id].meta_mutex.lock();
   board_readout[board_id].num_end_slices_seen_filt = 0;
   board_readout[board_id].last_slice_raw_bytes_total = 0;
   board_readout[board_id].last_slice_filter_bytes_total = 0;
   board_readout[board_id].curr_slice_raw_bytes_total = 0;
   board_readout[board_id].curr_slice_filter_bytes_total = 0;
   board_readout[board_id].meta_mutex.unlock();

   // Need to make sure we sleep occasionally,
   // to ensure main thread can grab mutex occasionally.
   timeval last_sleep;
   gettimeofday(&last_sleep, NULL);

   std::vector<FEPFilterBase*> filters;

   if (FEPFilterDownsample::is_enabled(fep_settings)) {
      filters.push_back(new FEPFilterDownsample(fep_settings));
   }

   if (FEPFilterExp2GausMatched::is_enabled(fep_settings)) {
      filters.push_back(new FEPFilterExp2GausMatched(fep_settings));
   }

   if (FEPDS20kProcessing::is_enabled(fep_settings)) {
      filters.push_back(new FEPDS20kProcessing(fep_settings));
   }

   // If no "real" filters are enabled, just add a No-op filter that copies the data into
   // the right place.
   if (filters.size() == 0) {
      filters.push_back(new FEPFilterNone(fep_settings));
   }

   while (!in_end_of_run) {
      timeval now;
      gettimeofday(&now, NULL);

      unsigned char *rp = NULL;

      if (!board_readout[board_id].has_raw()) {
         // No raw events to read
         gettimeofday(&last_sleep, NULL);
         std::this_thread::sleep_for(std::chrono::milliseconds(1));
         continue;
      }

      if (board_readout[board_id].filter_output_size_bytes > max_filt_buf_bytes) {
         // Buffer for filter output is full
         gettimeofday(&last_sleep, NULL);
         std::this_thread::sleep_for(std::chrono::milliseconds(1));
         continue;
      }

      std::shared_ptr<VXData> raw_data = board_readout[board_id].get_raw();
      int raw_size_bytes = raw_data->get_encoded_size_bytes();
      int filter_size_bytes = 0;

      std::vector<std::shared_ptr<HitBase> > final_filtered_hits;
      std::shared_ptr<Waveform> input = std::make_shared<Waveform>();

      if (raw_data->is_end_of_slice) {
         if (debug) {
            vslice::ts_printf("(filt) Don't need to filter end-of-slice event from board %d, time %lfs, size %d bytes\n", board_id, raw_data->timestamp_secs, raw_size_bytes);
         }

         std::shared_ptr<EndOfSlice> eos = std::make_shared<EndOfSlice>();
         eos->time_since_run_start_secs = raw_data->timestamp_secs;
         final_filtered_hits.push_back(eos);
      } else {
         if (debug) {
            std::string human_size = vslice::format_bytes(raw_size_bytes);
            vslice::ts_printf("(filt) Filtering raw event from board %d, chan %d, time %lfs, size %d bytes (%s)\n", board_id, raw_data->channel_id, raw_data->timestamp_secs, raw_size_bytes, human_size.c_str());
         }

         // Start with the single unfiltered waveform
         input->time_since_run_start_secs = raw_data->timestamp_secs;
         input->samples = std::move(raw_data->wf_samples);

         std::vector<std::shared_ptr<HitBase> > next_filter_input;
         next_filter_input.push_back(input);

         // Now apply each filter in turn. A filter may split a waveform into multiple
         // Waveforms/QTs if it wants, so we eventually end up with a vector of "hits".
         for (size_t filter_idx = 0; filter_idx < filters.size(); filter_idx++) {
            FEPFilterBase* filter = filters[filter_idx];
            std::vector<std::shared_ptr<HitBase> > these_filtered_hits;

            for (auto hit : next_filter_input) {
               if (!is_waveform(hit)) {
                  // Filters only deal with Waveforms. If something is already a QT,
                  // just keep it in the list.
                  these_filtered_hits.push_back(hit);
                  continue;
               }

               ChannelId chan = {};
               chan.board_id = board_id;
               chan.frontend_id = this_frontend_index;
               chan.channel_id = raw_data->channel_id;
               std::shared_ptr<Waveform> cast_hit = std::dynamic_pointer_cast<Waveform>(hit);
               INT status = filter->apply(chan, cast_hit, these_filtered_hits, debug);

               if (status != SUCCESS) {
                  cm_msg(MERROR, __FUNCTION__, "Failed to apply filter: %s", filter->get_error_message().c_str());
                  break;
               }
            }

            if (filter_idx + 1 == filters.size()) {
               // We've run all the filters. The final output is the output of this last filter.
               final_filtered_hits = these_filtered_hits;
            } else {
               // More filters to run. The input to the next filter is the output of this filter.
               next_filter_input = these_filtered_hits;
            }
         }

         for (auto hit : final_filtered_hits) {
            filter_size_bytes += hit->get_num_bytes_needed_to_encode();
         }

         if (debug) {
            std::string human_size = vslice::format_bytes(filter_size_bytes);
            vslice::ts_printf("(filt) --> Filtered size %d bytes (%s) for board %d, channel %d\n", filter_size_bytes, human_size.c_str(), board_id, raw_data->channel_id);
         }
      }

      std::vector<std::shared_ptr<FilteredAndUnfiltered>> to_add;

      // Add metadata of channel ID etc, then add to global queue of hits from this board.
      for (auto& hit : final_filtered_hits) {
         std::shared_ptr<FilteredAndUnfiltered> chan_hit = std::make_shared<FilteredAndUnfiltered>();
         chan_hit->chan.board_id = board_id;
         chan_hit->chan.frontend_id = this_frontend_index;
         chan_hit->chan.channel_id = raw_data->channel_id;
         chan_hit->filtered = hit;

         if (raw_data->is_end_of_slice) {
            chan_hit->unfiltered = hit;
         } else {
            chan_hit->unfiltered = input;
         }

         to_add.push_back(chan_hit);
      }
      
      board_readout[board_id].add_filt(to_add);

      board_readout[board_id].meta_mutex.lock();

      if (raw_data->is_end_of_slice) {
         board_readout[board_id].last_slice_raw_bytes_total = board_readout[board_id].curr_slice_raw_bytes_total;
         board_readout[board_id].last_slice_filter_bytes_total = board_readout[board_id].curr_slice_filter_bytes_total;
         board_readout[board_id].curr_slice_raw_bytes_total = 0;
         board_readout[board_id].curr_slice_filter_bytes_total = 0;
         board_readout[board_id].num_end_slices_seen_filt++;
      } else {
         board_readout[board_id].curr_slice_raw_bytes_total += raw_size_bytes;
         board_readout[board_id].curr_slice_filter_bytes_total += filter_size_bytes;
      }

      board_readout[board_id].meta_mutex.unlock();

      if (now.tv_sec - last_sleep.tv_sec + 1e-6 * (now.tv_usec - last_sleep.tv_usec) > 2) {
         // At least 2 seconds since we last slept. Yield to ensure other thread can
         // do some work.
         gettimeofday(&last_sleep, NULL);
         std::this_thread::sleep_for(std::chrono::milliseconds(1));
         continue;
      }
   }

   vslice::ts_printf("(filt) Stopping data filter thread for board %d\n", board_id);

   for (auto& filter : filters) {
      delete filter;
   }

   return NULL;
}

void *FrontEndProcessor::thread_accumulator() {
   vslice::ts_printf("(accu) Spawning accumulator thread\n");

   // Need to make sure we sleep occasionally,
   // to ensure main thread can grab mutex occasionally.
   timeval last_sleep;
   gettimeofday(&last_sleep, NULL);

   float slice_width_ms = shared_settings["Slice width (ms)"];
   float slice_overlap_ms = shared_settings["Slice overlap (ms)"];

   while (!in_end_of_run) {
      timeval now;
      gettimeofday(&now, NULL);

      if (!in_run) {
         continue;
      }

      // Keep track of what slice number we've seen so far.
      uint32_t max_slice_idx_created = 0;

      for (auto& it : slice_info) {
         max_slice_idx_created = std::max(max_slice_idx_created, it->data.slice_idx);
      }

      // Free memory from slices that have been sent out to TSPs (and midas if enabled).
      // We do this in this thread (rather than the main thread), so that this is the only
      // thread that manipulates the slice_data queue, and it can iterate over it safely without
      // requiring a mutex. We still need a mutex when manipulating the queue though, to protect
      // other threads.
      if (slice_info.size() && slice_info.front()->send_thread_finished) {
         int slice_idx = slice_info.front()->data.slice_idx;
         vslice::ts_printf("(accu) \e[1;35mFreeing space used for slice %d that has been completely sent\e[0;39m\n", slice_idx);
         slice_info.front()->send_thread->join();
         delete slice_info.front()->send_thread;

         slice_info_mutex.lock();
         slice_info.pop_front();
         slice_info_mutex.unlock();
      }

      // We've tried to send this slice too many times and failed. To stop us
      // getting stuck with this data forever, we accept some data loss and just give up.
      if (slice_info.size() && slice_info.front()->tsp_send_retries > max_tsp_retries) {
         int slice_idx = slice_info.front()->data.slice_idx;
         vslice::ts_printf("(accu) \e[1;35m*** Have tried to send slice %d %d times. DISCARDING DATA so we don't get stuck with it forever. ***\e[0;39m\n", slice_idx, slice_info.front()->tsp_send_retries);
         slice_info.front()->send_thread->join();
         delete slice_info.front()->send_thread;

         slice_info_mutex.lock();
         slice_info.pop_front();
         slice_info_mutex.unlock();
      }

      // Choose the board with the earliest event time.
      int earliest_board_id = -1;
      double earliest_time_secs = -1;

      for (int board_id = 0; board_id < num_boards; board_id++) {
         if (!board_readout[board_id].data_source->is_enabled()) {
            continue;
         }

         double this_time_secs = board_readout[board_id].peek_filt_timestamp();

         if (this_time_secs >= 0 && (earliest_board_id == -1 || this_time_secs < earliest_time_secs)) {
            earliest_time_secs = this_time_secs;
            earliest_board_id = board_id;
         }
      }

      // Grab data from filter output and add to slice(s).
      if (earliest_board_id != -1) {
         std::shared_ptr<FilteredAndUnfiltered> filter_output = board_readout[earliest_board_id].get_filt();

         int num_end_slices_seen = board_readout[earliest_board_id].num_end_slices_seen_accum;
         unsigned int data_size_bytes = filter_output->filtered->get_num_bytes_needed_to_encode();
         double timestamp_secs = filter_output->filtered->time_since_run_start_secs;
         bool is_end_of_slice_event = is_eos(filter_output->filtered);
         int chan_id = filter_output->chan.channel_id;

         if (is_end_of_slice_event) {
            vslice::ts_printf("(accu) \e[1;34mBoard %d has seen end of slice %d at %lfs\e[0;39m\n", earliest_board_id, num_end_slices_seen, timestamp_secs);

            num_end_slices_seen++;

            board_readout[earliest_board_id].meta_mutex.lock();
            board_readout[earliest_board_id].num_end_slices_seen_accum = num_end_slices_seen;
            board_readout[earliest_board_id].meta_mutex.unlock();

            if (max_slice_idx_created == num_end_slices_seen) {
               int new_slice_idx = num_end_slices_seen + 1;
               double slice_start_time = timestamp_secs + (slice_width_ms - slice_overlap_ms) / 1000;

               std::shared_ptr<SliceInfo> new_slice = std::make_shared<SliceInfo>(new_slice_idx, slice_start_time);

               slice_info_mutex.lock();
               slice_info.push_back(new_slice);
               slice_info_mutex.unlock();

               if (slice_info.size() > 1) {
                  // Sanity-check the slice start time
                  double this_start = slice_info.back()->data.slice_start_time_secs;
                  double prev_start = slice_info.at(slice_info.size() - 2)->data.slice_start_time_secs;
                  double start_delta_ms = (this_start - prev_start) * 1000;

                  if (start_delta_ms > slice_width_ms * 1.5 || start_delta_ms < slice_width_ms * 0.5) {
                     DWORD prev_slice_idx = slice_info.at(slice_info.size() - 2)->data.slice_idx;
                     cm_msg(MERROR, __FUNCTION__, "Possible data corruption! Computed start of slice %d as %lf, and slice %d as %lf!", new_slice_idx, this_start, prev_slice_idx, prev_start);
                     abort();
                  }
               }

               vslice::ts_printf("(accu) \e[1;35mInitialised data slice %d starting at %lf\e[0;39m\n", new_slice_idx, new_slice->data.slice_start_time_secs);

               max_slice_idx_created = new_slice_idx;
            } else if (max_slice_idx_created < num_end_slices_seen) {
               cm_msg(MERROR, __FUNCTION__, "Logic error - Board %d has seen %d end-of-slices, but we have created space up to slice %d", earliest_board_id, num_end_slices_seen, max_slice_idx_created);
               abort();
            }

         } else {
            // Regular event
            if (debug) {
               vslice::ts_printf("(accu) Accumulating filtered event from board %d, chan %d, time %lfs, size %d bytes\n", earliest_board_id, chan_id, timestamp_secs, data_size_bytes);
            }

            // Copy data to main memory region(s).
            // We're the only thread that extends/pops from the queue, so we don't need a mutex here.
            for (auto it : slice_info) {
               if (it->complete) {
                  continue;
               }

               uint32_t slice_idx = it->data.slice_idx;

               if (slice_idx >= num_end_slices_seen && timestamp_secs > it->data.slice_start_time_secs) {
                  if (debug) {
                     vslice::ts_printf("(accu) --> Adding to slice %d\n", slice_idx);
                  }

                  std::shared_ptr<HitWithChannelId> filt = std::make_shared<HitWithChannelId>();
                  filt->chan = filter_output->chan;
                  filt->hit = filter_output->filtered;
                  it->data.add_hit(filt);

                  if (!it->unfiltered.contains(filter_output->chan, filter_output->unfiltered)) {
                     // Unfiltered waveforms may have produced multiple filtered hits.
                     // Only add the unfiltered waveform to the slice once.
                     std::shared_ptr<HitWithChannelId> unfilt = std::make_shared<HitWithChannelId>();
                     unfilt->chan = filter_output->chan;
                     unfilt->hit = filter_output->unfiltered;
                     it->unfiltered.add_hit(unfilt);
                  }
               }
            }
         }
      }

      int min_num_end_slices_seen = INT_MAX;

      for (int board_id = 0; board_id < num_boards; board_id++) {
         board_readout[board_id].meta_mutex.lock();
         if (board_readout[board_id].data_source->is_enabled()) {
            min_num_end_slices_seen = std::min(min_num_end_slices_seen, board_readout[board_id].num_end_slices_seen_accum);
         }
         board_readout[board_id].meta_mutex.unlock();
      }


      for (auto& it : slice_info) {
         if (min_num_end_slices_seen > it->data.slice_idx && !it->complete) {
            // All boards have given data up to and including the "end-of-slice" marker
            // for this slice. Mark it as complete so we can send out the data.
            vslice::ts_printf("(accu) \e[1;34mMarking slice %d as complete - have all data from all boards\e[0;39m\n", it->data.slice_idx);
            it->complete = true;
         }
      }

      if (now.tv_sec - last_sleep.tv_sec + 1e-6 * (now.tv_usec - last_sleep.tv_usec) > 2) {
         // At least 2 seconds since we last slept. Yield to ensure other thread can
         // do some work.
         gettimeofday(&last_sleep, NULL);
         std::this_thread::sleep_for(std::chrono::milliseconds(1));
         continue;
      }
   }

   vslice::ts_printf("(accu) Stopping accumulator thread\n");
   return NULL;
}

void *FrontEndProcessor::thread_send_data(int find_slice_index) {
   slice_info_mutex.lock();

   std::shared_ptr<SliceInfo> slice;

   for (auto& it : slice_info) {
      if (it->data.slice_idx == find_slice_index) {
         slice = it;
      }
   }

   slice_info_mutex.unlock();

   if (slice.get() == 0) {
      cm_msg(MERROR, __FUNCTION__, "Logic error - unable to find slice %d when trying to send it!", find_slice_index);
   }

   uint32_t slice_idx = slice->data.slice_idx;

   // Send data out to TSP
   slice->sending = true;
   
   FEPSliceHeader header;
   header.fe_idx = this_frontend_index;
   header.slice_idx = slice_idx;

   uint32_t header_size = header.get_num_bytes_needed_to_encode();
   uint32_t body_size = slice->data.get_num_bytes_needed_to_encode();
   uint32_t payload_size = header_size + body_size;

   slice->payload = (uint64_t*)malloc(payload_size);
   header.encode(slice->payload);
   slice->data.encode(slice->payload + (header_size/sizeof(uint64_t)), body_size);

   // Free all the memory used by the in-memory version of the slice, now
   // that we have the encoded version.
   slice->data.reset();
   slice->data.slice_idx = slice_idx;

   while (slice->tsp_send_retries <= max_tsp_retries) {
      std::string failure_msg;
      int sockfd = -1;

      std::string endpoint = get_data_endpoint_for_slice(slice_idx);

      if (debug) {
         vslice::ts_printf("(send) Opening data connection to %s for slice %d\n", endpoint.c_str(), slice_idx);
      }

      char host[128];
      int port;

      if (sscanf(endpoint.c_str(), "%[^:]:%d", host, &port) != 2) {
         failure_msg = "Failed to parse endpoint";
      } else {
         // Try to open the connection
         sockfd = socket_utils::connect(host, port);
      }

      char ack = 0;
      uint64_t recv_size = 0;

      if (sockfd == -1) {
         failure_msg = "Failed to connect";
      } else if (!socket_utils::write_fully_with_size_protocol(sockfd, (char*)slice->payload, payload_size)) {
         failure_msg = "Failed to write data";
      } else if (!socket_utils::wait_for_data(sockfd, 1000)) {
         failure_msg = "Timeout waiting for ack";
      } else if (!socket_utils::read_fully_with_size_protocol(sockfd, &ack, 1, recv_size)) {
         failure_msg = "Failed to read ack";
      } else {
         slice->tsp_send_finished = true;
      }

      if (sockfd != -1) {
         close(sockfd);
      }

      if (slice->tsp_send_finished) {
         vslice::ts_printf("(send) \e[1;31mFinished sending slice %d to %s\e[0;39m\n", slice_idx, endpoint.c_str());
         data_endpoints.erase(slice_idx);
         last_slice_sent = slice_idx;

         if (first_slice_send_time_ms == 0) {
            first_slice_send_time_ms = vslice::current_time_ms();
         }

         last_slice_send_time_ms = vslice::current_time_ms();
         break;
      } else {
         // Failed to send! We'll try again later.
         vslice::ts_printf("(send) \e[1;31mFailed to send slice %d to %s (%s). Will retry.\e[0;39m\n", slice_idx, endpoint.c_str(), failure_msg.c_str());
         slice->tsp_send_retries++;
         std::this_thread::sleep_for(std::chrono::milliseconds(50));
      }
   }

   if (!slice->tsp_send_finished) {
      cm_msg(MERROR, __FUNCTION__, "Data loss: failed to send slice %d %d times to %s", slice_idx, slice->tsp_send_retries, get_data_endpoint_for_slice(slice_idx).c_str());
   }

   slice->send_thread_finished = true;
   free(slice->payload);

   return NULL;
}

bool FrontEndProcessor::is_event_ready_to_send() {
   return get_next_slice_ready_to_be_sent().get() != 0;
}

std::string FrontEndProcessor::get_data_endpoint_for_slice(int slice_idx) {
   if (data_endpoints.find(slice_idx) != data_endpoints.end()) {
      return data_endpoints[slice_idx];
   } else {
      return std::string();
   }
}

int FrontEndProcessor::get_num_send_threads() {
   slice_info_mutex.lock();
   int num_slices_sending = 0;

   for (auto& slice : slice_info) {
      if (slice->sending) {
         num_slices_sending++;
      }
   }

   slice_info_mutex.unlock();

   return num_slices_sending;
}

std::shared_ptr<FrontEndProcessor::SliceInfo> FrontEndProcessor::get_next_slice_ready_to_be_sent() {
   std::shared_ptr<SliceInfo> next_slice;

   // Don't get into a state where there are too many FEP threads sending to
   // TSPs at the same time, which would introduce lots of network competition.
   // We'll try again later.
   if (get_num_send_threads() < shared_settings["Max TSP data send threads"]) {
      slice_info_mutex.lock();

      for (auto& slice : slice_info) {
         if (slice->complete && !slice->done_midas_event && get_data_endpoint_for_slice(slice->data.slice_idx) != "") {
            next_slice = slice;
         }
      }

      slice_info_mutex.unlock();
   }

   return next_slice;
}

INT FrontEndProcessor::send_data(char *pevent) {
   int midas_size_bytes = 0;

   std::shared_ptr<SliceInfo> next_slice = get_next_slice_ready_to_be_sent();

   if (next_slice.get() == 0) {
      return 0;
   }

   // Encode the header info at the start of the data buffer
   // (we already created enough space for this when initializing the
   // SliceData object).

   int slice_idx = next_slice->data.slice_idx;
   uint32_t size_bytes = next_slice->data.get_num_bytes_needed_to_encode();
   std::string human_size = vslice::format_bytes(size_bytes);
   vslice::ts_printf("(main) \e[1;31mSending out %d bytes (%s) for slice %d\e[0;39m\n", size_bytes, human_size.c_str(), slice_idx);

   // Copy data from buffer into bank if this is the first time
   // we've tried to send this.
   uint32_t ev_size_bytes = 0;

   if (write_data_to_banks) {
      ev_size_bytes += next_slice->data.get_num_bytes_needed_to_encode();
   }

   if (write_unfiltered_to_banks) {
      ev_size_bytes += next_slice->unfiltered.get_num_bytes_needed_to_encode();
   }

   if (ev_size_bytes > max_event_size_bytes) {
      vslice::ts_printf("(main) Event size %d bytes is too large to be written to midas bank (max size %d bytes)\n", ev_size_bytes, max_event_size_bytes);
   } else if (ev_size_bytes > 0) {
      bk_init32(pevent);

      if (write_data_to_banks) {
         // Write filtered data to bank F001 etc.
         uint64_t *pdata;
         char bank_name[5];
         sprintf(bank_name, "F%03d", this_frontend_index);
         uint32_t bytes_written;

         bk_create(pevent, bank_name, TID_QWORD, (void **) &pdata);
         next_slice->data.encode(pdata, bytes_written);
         pdata += bytes_written / sizeof(uint64_t);
         bk_close(pevent, pdata);

         if (debug) {
            vslice::ts_printf("(main) Wrote %d bytes of filtered data to bank %s\n", bytes_written, bank_name);
         }
      }

      if (write_unfiltered_to_banks) {
         // Write unfiltered data to bank U001 etc.
         uint64_t *pdata;
         char bank_name[5];
         sprintf(bank_name, "U%03d", this_frontend_index);
         uint32_t bytes_written;

         bk_create(pevent, bank_name, TID_QWORD, (void **) &pdata);
         next_slice->unfiltered.encode(pdata, bytes_written);
         pdata += bytes_written / sizeof(uint64_t);
         bk_close(pevent, pdata);

         if (debug) {
            vslice::ts_printf("(main) Wrote %d bytes of unfiltered data to bank %s\n", bytes_written, bank_name);
         }
      }

      midas_size_bytes = bk_size(pevent);
   }

   // Free the memory used by the unfiltered Slice
   next_slice->unfiltered.reset();
   next_slice->unfiltered.slice_idx = slice_idx;

   next_slice->done_midas_event = true;

   // Spawn thread to send data out to TSP
   fep_slice_thread_args[slice_idx].obj = this;
   fep_slice_thread_args[slice_idx].slice_index = slice_idx;
   next_slice->send_thread = new std::thread(thread_send_data_helper, &fep_slice_thread_args[slice_idx]);

   if (!next_slice->send_thread) {
      cm_msg(MERROR, __FUNCTION__, "Failed to create data sending thread for slice %d", slice_idx);
   }

   return midas_size_bytes;
}

INT FrontEndProcessor::do_periodic_work() {
   poll_pm();
   return SUCCESS;
}

INT FrontEndProcessor::init(HNDLE _hDB, INT _max_event_size, INT _fe_idx) {
   hDB = _hDB;
   max_event_size_bytes = _max_event_size;
   max_event_read_per_board_bytes = 1024 * 1024;
   this_frontend_index = _fe_idx;

   if (this_frontend_index < 0) {
      cm_msg(MERROR, __FUNCTION__, "You must run this frontend with the -i argument to specify the frontend index.");
      return FE_ERR_DRIVER;
   }

   INT status = SUCCESS;

   // Do the shared settings that are common to all FEP instances
   shared_settings.connect("/FEP shared");

   if (!shared_settings.is_subkey("Slice width (ms)")) {
      shared_settings["Slice width (ms)"] = 1000.;
   }

   if (!shared_settings.is_subkey("Slice overlap (ms)")) {
      shared_settings["Slice overlap (ms)"] = 100.;
   }

   if (!shared_settings.is_subkey("PoolManager endpoint")) {
      shared_settings["PoolManager endpoint"] = "tcp://localhost:4713";
   }

   if (!shared_settings.is_subkey("Max TSP data send retries")) {
      shared_settings["Max TSP data send retries"] = 5;
   }

   if (!shared_settings.is_subkey("Max TSP data send threads")) {
      shared_settings["Max TSP data send threads"] = 24;
   }

   if (!shared_settings.is_subkey("Max raw buffer per board (MB)")) {
      shared_settings["Max raw buffer per board (MB)"] = 500;
   }

   if (!shared_settings.is_subkey("Max filt buffer per board (MB)")) {
      shared_settings["Max filt buffer per board (MB)"] = 500;
   }

   shared_settings.set_auto_refresh_read(false);

   // Now move on to the per-instance FEP settings.
   char settings_path_base[256];
   sprintf(settings_path_base, "/Equipment/FEP_%03d/Settings", this_frontend_index);

   fep_settings.connect(settings_path_base);
   fep_global_settings.connect(std::string(settings_path_base) + "/Global");

   if (!fep_global_settings.is_subkey("Use demo data (read at init)")) {
      fep_global_settings["Use demo data (read at init)"] = true;
   }

   if (!fep_global_settings.is_subkey("Use dummyVX data (read at init)")) {
      fep_global_settings["Use dummyVX data (read at init)"] = true;
   }

   if (!fep_global_settings.is_subkey("Use toy data (read at init)")) {
      fep_global_settings["Use toy data (read at init)"] = true;
   }

   if (!fep_global_settings.is_subkey("Write data to midas banks")) {
      fep_global_settings["Write data to midas banks"] = true;
   }

   if (!fep_global_settings.is_subkey("Write unfiltered to midas banks")) {
      fep_global_settings["Write unfiltered to midas banks"] = true;
   }

   if (!fep_global_settings.is_subkey("Debug")) {
      fep_global_settings["Debug"] = false;
   }

   // Create filter-specific settings.
   FEPFilterDownsample::create_settings(fep_settings);
   FEPFilterExp2GausMatched::create_settings(fep_settings);
   FEPDS20kProcessing::create_settings(fep_settings);

   fep_global_settings.set_auto_refresh_read(false);
   fep_settings.set_auto_refresh_read(false);

   debug = fep_global_settings["Debug"];
   use_demo_data = fep_global_settings["Use demo data (read at init)"];
   use_dummy_vx_data = fep_global_settings["Use dummyVX data (read at init)"];
   use_toy_mc = fep_global_settings["Use toy data (read at init)"];

   int num_fake_data = 0;
   if (use_demo_data) {
      num_fake_data++;
   }
   if (use_dummy_vx_data) {
      num_fake_data++;
   }
   if (use_toy_mc) {
      num_fake_data++;
   }
   if (num_fake_data > 1){
      cm_msg(MERROR, __FUNCTION__, "Only one Demo/DummyVX/ToyMC data sources can be enabled at a given time");
      return FE_ERR_ODB;
   }

   if (use_demo_data) {
      cm_msg(MINFO, __FUNCTION__, "Starting FEP %d in DEMO data mode. Will not connect to real VX2740s!", this_frontend_index);
      std::string demo_path = std::string(settings_path_base) + "/DemoData";
      midas::odb demo;
      demo.connect(demo_path);

      if (!demo.is_subkey("Num boards (read at init)")) {
         demo["Num boards (read at init)"] = 2;
      }

      num_boards = demo["Num boards (read at init)"];
   } else if (use_dummy_vx_data) {
      cm_msg(MINFO, __FUNCTION__, "Starting FEP %d in DUMMY VX data mode. Will not connect to real VX2740s!", this_frontend_index);
      std::string dummy_vx_path = std::string(settings_path_base) + "/DummyVXData";
      midas::odb dummy;
      dummy.connect(dummy_vx_path);

      if (!dummy.is_subkey("Num boards (read at init)")) {
         dummy["Num boards (read at init)"] = 2;
      }

      num_boards = dummy["Num boards (read at init)"];
   } else if (use_toy_mc) {
      cm_msg(MINFO, __FUNCTION__, "Starting FEP %d in TOYMC data mode. Will not connect to real VX2740s!", this_frontend_index);
      std::string toymc_path = std::string(settings_path_base) + "/ToyMC";
      midas::odb toyMC;
      toyMC.connect(toymc_path);
      //Get the vector for the portIDs for the boards
      port_ids = toyMC["digitiser_ports"];
      num_boards = port_ids.size();
      status_port = toyMC["status_port"];
   } else {
#ifdef HAVE_VX
      // Tell VX helper where to store settings in ODB (by default it's in
      // /Equipment/VX2740_Group_01 etc, but we want to store them in FEP
      // directory instead).
      char config_set_dir[255], config_rdb_dir[255];
      snprintf(config_set_dir, 255, "/Equipment/FEP_%03d/Settings/VX2740", this_frontend_index);
      snprintf(config_rdb_dir, 255, "/Equipment/FEP_%03d/Readback/VX2740", this_frontend_index);

      std::shared_ptr<VX2740FeSettingsODB> settings_strategy = std::make_shared<VX2740FeSettingsODB>(config_set_dir, config_rdb_dir);

      // Tell VX helper to only configure the boards, not
      // read out any data (we'll handle readout ourselves).
      bool enable_readout = false;
      bool single_fe_mode = false;
      
      configurator = new VX2740GroupFrontend(settings_strategy, single_fe_mode, enable_readout);

      status = configurator->init(this_frontend_index);

      if (status != SUCCESS) {
         return status;
      }

      num_boards = configurator->get_num_boards();
#else
      cm_msg(MERROR, __FUNCTION__, "Demo data and Toy MC is disabled, but FE was compiled without real VX support. Abort.");
      return FE_ERR_ODB;
#endif
   }

   char vars_path[255];
   snprintf(vars_path, 255, "/Equipment/FEP_%03d/Variables", this_frontend_index);
   variables.connect(vars_path);
   raw_slice_sizes_last_100.resize(num_boards);
   filt_slice_sizes_last_100.resize(num_boards);
   raw_slice_sizes_run_total.resize(num_boards);
   filt_slice_sizes_run_total.resize(num_boards);
   num_entries_for_run_totals.resize(num_boards);
   last_var_write_slice_idx.resize(num_boards);

   acq_status.resize(num_boards);
   temp_in.resize(num_boards);
   temp_out.resize(num_boards);
   temp_hottest_adc.resize(num_boards);

   for (int board_id = 0; board_id < num_boards; board_id++) {
      vslice::ts_printf("(main) Setting up data source for board %d\n", board_id);

      BoardReadout &this_board_readout = board_readout[board_id];

      if (use_demo_data) {
         this_board_readout.data_source = new DataSourceDemo(hDB, this_frontend_index, board_id);
      } else if (use_dummy_vx_data) {
         this_board_readout.data_source = new DataSourceDummyVx(hDB, this_frontend_index, board_id);
      } else if (use_toy_mc) {
         //Get the port number from the DB 
         int portNumber = port_ids[board_id];
         cm_msg(MINFO, __FUNCTION__, "Creating a ToyMC data source using port %d for board %d", portNumber,board_id);
         
         //If we are connecting on the first board also initialize the status port
         if (board_id == 0) {
            this_board_readout.data_source = new DataSourceToyMC(portNumber,this_frontend_index, board_id, status_port);
         } else {
            this_board_readout.data_source = new DataSourceToyMC(portNumber, this_frontend_index, board_id);
         }
      } else {
#ifdef HAVE_VX
         midas::odb real;
         std::string real_path = std::string(settings_path_base) + "/RealData";
         real.connect(real_path);

         if (!real.is_subkey("SW end-of-slices (read at init)")) {
            real["SW end-of-slices (read at init)"] = false;
         }

         if (configurator->is_scope_mode(board_id)) {
            this_board_readout.data_source = new DataSourceScopeMode(hDB, this_frontend_index, board_id, real["SW end-of-slices (read at init)"]);
         } else {
            this_board_readout.data_source = new DataSourceUserMode(hDB, this_frontend_index, board_id, real["SW end-of-slices (read at init)"]);
         }
#else
         cm_msg(MERROR, __FUNCTION__, "Frontend was compiled without support for real VX2740 boards!");
         return FE_ERR_ODB;
#endif
      }

      std::cout <<  "Opening records" << std::endl;
      status = this_board_readout.data_source->open_records(settings_path_base);
      std::cout <<  "Opened records" << std::endl;
   }

   if (status != SUCCESS) {
      return status;
   }

   pm_endpoint = shared_settings["PoolManager endpoint"];
   open_pm_socket(pm_endpoint);
	
   std::cout <<  "Updating variables" << std::endl;
   update_variables(true);
   
   std::cout <<  "Completed init" << std::endl;

   return SUCCESS;
}

INT FrontEndProcessor::begin_of_run(INT run_number, char *error) {
   INT status = SUCCESS;
   in_end_of_run = false;
   num_slices_lost = 0;
   last_slice_sent = -1;
   first_slice_send_time_ms = 0;
   last_slice_send_time_ms = 0;

   fep_settings.read();
   fep_global_settings.read();
   shared_settings.read();

   debug = fep_global_settings["Debug"];
   write_data_to_banks = fep_global_settings["Write data to midas banks"];
   write_unfiltered_to_banks = fep_global_settings["Write unfiltered to midas banks"];
   max_tsp_retries = shared_settings["Max TSP data send retries"];
   max_raw_buf_bytes = shared_settings["Max raw buffer per board (MB)"] * 1024 * 1024;
   max_filt_buf_bytes = shared_settings["Max filt buffer per board (MB)"] * 1024 * 1024;
   pm_endpoint = shared_settings["PoolManager endpoint"];

   open_pm_socket(pm_endpoint);// only (re-)opens socket if endpoint changed

   vslice::ts_printf("(main) Clearing stale list of TSP data endpoints at start of run\n");
   data_endpoints.clear();

   while (slice_info.size()) {
      if (slice_info.front()->send_thread) {
         slice_info.front()->send_thread->join();
         delete slice_info.front()->send_thread;
      }

      slice_info.pop_front();
   }

   std::shared_ptr<SliceInfo> first_slice = std::make_shared<SliceInfo>(0, 0);
   slice_info.push_back(first_slice);

   double slice_1_start_time_secs = (shared_settings["Slice width (ms)"] - shared_settings["Slice overlap (ms)"]) / 1000.;
   std::shared_ptr<SliceInfo> second_slice = std::make_shared<SliceInfo>(1, slice_1_start_time_secs);
   slice_info.push_back(second_slice);

   std::vector<std::string> history_names;

#ifdef HAVE_VX
   if (!use_demo_data && !use_dummy_vx_data && !use_toy_mc) {
      status = configurator->begin_of_run(run_number, error);

      if (status != SUCCESS) {
         vslice::ts_printf("Failed to configure boards\n");
         return status;
      }

      std::map<int, VX2740*> boards = configurator->get_boards();
      std::map<int, std::string> board_names = configurator->get_board_names();

      std::vector<int> boards_to_read_from;

      for (auto& it : boards) {
         if (configurator->should_read_from_board(it.first)) {
            boards_to_read_from.push_back(it.first);
         }
      }

      for (int board_id = 0; board_id < num_boards; board_id++) {
         bool do_read = std::find(boards_to_read_from.begin(), boards_to_read_from.end(), board_id) != boards_to_read_from.end();
         bool have_board = boards[board_id] != NULL;
         status = ((DataSourceRealBase*)(board_readout[board_id].data_source))->set_board_info(boards[board_id], have_board && do_read, board_names[board_id]);

         if (status != SUCCESS) {
            return status;
         }

         char real_board_name[255];
         snprintf(real_board_name, 255, "Board %02d: %s", board_id, board_names[board_id].c_str());
         history_names.push_back(real_board_name);
      }
   }
#endif

   if (use_demo_data || use_dummy_vx_data || use_toy_mc ) {
      for (int board_id = 0; board_id < num_boards; board_id++) {
         char fake_board_name[255];
         snprintf(fake_board_name, 255, "Dummy board %02d", board_id);
         history_names.push_back(fake_board_name);
      }
   }


   fep_settings["Comment on names"] = "These are SET by the frontend, not read by it. Change hostnames on VX2740 webpage";
   fep_settings["Names"] = history_names;

   poll_pm();

   for (int board_id = 0; board_id < num_boards; board_id++) {
      raw_slice_sizes_last_100[board_id].clear();
      filt_slice_sizes_last_100[board_id].clear();
      raw_slice_sizes_run_total[board_id] = 0;
      filt_slice_sizes_run_total[board_id] = 0;
      num_entries_for_run_totals[board_id] = 0;
      last_var_write_slice_idx[board_id] = -1;

      if (!board_readout[board_id].data_source->is_enabled()) {
         vslice::ts_printf("(main) Ignoring board %d\n", board_id);

         acq_status[board_id] = 0;
         temp_in[board_id] = 0;
         temp_out[board_id] = 0;
         temp_hottest_adc[board_id] = 0;
         continue;
      }

      vslice::ts_printf("(main) Setting up readout and filtering of board %d\n", board_id);

      board_readout[board_id].num_end_slices_seen_accum = 0;

      status = board_readout[board_id].data_source->begin_of_run(shared_settings["Slice width (ms)"]);

      if (status != SUCCESS) {
         return status;
      }

      // Skip over any unhandled data from previous run.
      board_readout[board_id].reset_queues();

      // Create threads for generating data and filtering it (one of each per board)
      fep_thread_args[board_id].obj = this;
      fep_thread_args[board_id].board_index = board_id;
      readout_threads[board_id] = new std::thread(thread_data_generation_helper, &fep_thread_args[board_id]);

      if (!readout_threads[board_id]) {
         cm_msg(MERROR, __FUNCTION__, "Failed to create data generation thread for board %d", board_id);
         return FE_ERR_DRIVER;
      }

      filter_threads[board_id] = new std::thread(thread_data_filter_helper, &fep_thread_args[board_id]);

      if (!filter_threads[board_id]) {
         cm_msg(MERROR, __FUNCTION__, "Failed to create filtering thread for board %d", board_id);
         return FE_ERR_DRIVER;
      }
   }

   accumulator_thread = new std::thread(thread_accumulator_helper, this);

   if (!accumulator_thread) {
      cm_msg(MERROR, __FUNCTION__, "Failed to create accumulator thread");
      return FE_ERR_DRIVER;
   }

   in_run = true;

   return SUCCESS;
}

INT FrontEndProcessor::end_of_run(INT run_number, char *error) {
   vslice::ts_printf("(main) End of run %d\n", run_number);
   in_end_of_run = true;

   for (int board_id = 0; board_id < num_boards; board_id++) {

      
      if (readout_threads[board_id]) {
         readout_threads[board_id]->join();
         delete readout_threads[board_id];
         readout_threads[board_id] = NULL;
      }

      if (filter_threads[board_id]) {
         (filter_threads[board_id])->join();
         delete filter_threads[board_id];
         filter_threads[board_id] = NULL;
      }

      INT status = board_readout[board_id].data_source->end_of_run();
      if(status != SUCCESS){
    	vslice::ts_printf("Error ending run for board %d\n", board_id);

      }
   }

   if (accumulator_thread) {
      accumulator_thread->join();
      delete accumulator_thread;
      accumulator_thread = NULL;
   }

   // "Sending data" threads are allowed to continue. Will be
   // cleaned up at start of next run.

   in_run = false;

   return SUCCESS;
}

void FrontEndProcessor::get_status_strings(std::string& msg, std::string& color) {
   if (!in_run) {
      msg = "Run finished";
      color = "greenLight";
   } else if (num_slices_lost > 0) {
      char c_msg[255];
      snprintf(c_msg, 255, "Lost %d slices! Sent slice %d", num_slices_lost, last_slice_sent);
      msg = c_msg;
      color = "yellowLight";
   } else if (slice_info.size() > 5) {
      char c_msg[255];
      snprintf(c_msg, 255, "Unsent slice backlog growing large! Sent slice %d", last_slice_sent);
      msg = c_msg;
      color = "yellowLight";
   } else {
      char c_msg[255];
      snprintf(c_msg, 255, "Sent slice %d", last_slice_sent);
      msg = c_msg;
      color = "greenLight";
   }
}

INT FrontEndProcessor::update_variables(bool force) {
   if (!in_run && !force) {
      return SUCCESS;
   }

   // Backlog, lost, cpu_pct, and num_send_threads don't 
   // really need to be arrays, but the midas Variables 
   // webpage works better when all variables have the same 
   // length.

   // Stats from each board for this FE
   std::vector<INT> backlog(board_readout.size());
   std::vector<INT> lost(board_readout.size());
   std::vector<float> slice_rate(board_readout.size());
   std::vector<float> raw_pct(board_readout.size());
   std::vector<float> filt_pct(board_readout.size());
   std::vector<INT> raw_slice_size_last(board_readout.size());
   std::vector<INT> filt_slice_size_last(board_readout.size());
   std::vector<float> avg_raw_slice_size_last_100(board_readout.size());
   std::vector<float> avg_filt_slice_size_last_100(board_readout.size());
   std::vector<float> avg_raw_slice_size_run(board_readout.size());
   std::vector<float> avg_filt_slice_size_run(board_readout.size());
   std::vector<float> cpu_pct(board_readout.size());
   std::vector<INT> num_send_threads(board_readout.size());

   vslice::update_cpu_info(cpu_info);

   int num_slices_sending = get_num_send_threads();

   double elapsed_s = (last_slice_send_time_ms - first_slice_send_time_ms) / 1000.;
   float slices_per_s = first_slice_send_time_ms > 0 ? last_slice_sent / elapsed_s : 0;

   for (int board_id = 0; board_id < num_boards; board_id++) {
      board_readout[board_id].meta_mutex.lock();

      int raw_level, filt_level;
      BoardReadout& info = board_readout[board_id];
      int slice_idx = info.num_end_slices_seen_filt;

      backlog[board_id] = std::max((int)slice_info.size() - 2, 0);
      lost[board_id] = num_slices_lost;
      slice_rate[board_id] = slices_per_s;
      cpu_pct[board_id] = cpu_info.this_proc_pct;
      num_send_threads[board_id] = num_slices_sending;

      raw_pct[board_id] = (double)info.raw_data_size_bytes / max_raw_buf_bytes;
      filt_pct[board_id] = (double)info.filter_output_size_bytes / max_filt_buf_bytes;

      if (slice_idx > 0) {
         DWORD raw_size = info.last_slice_raw_bytes_total;
         DWORD filt_size = info.last_slice_filter_bytes_total;
         raw_slice_size_last[board_id] = raw_size;
         filt_slice_size_last[board_id] = filt_size;

         if (last_var_write_slice_idx[board_id] != slice_idx) {
            // New data to add to our running totals
            raw_slice_sizes_last_100[board_id].push_back(raw_size);
            filt_slice_sizes_last_100[board_id].push_back(filt_size);

            while (raw_slice_sizes_last_100[board_id].size() > 100) {
               raw_slice_sizes_last_100[board_id].pop_front();
            }

            while (filt_slice_sizes_last_100[board_id].size() > 100) {
               filt_slice_sizes_last_100[board_id].pop_front();
            }

            raw_slice_sizes_run_total[board_id] += raw_size;
            filt_slice_sizes_run_total[board_id] += filt_size;
            num_entries_for_run_totals[board_id]++;
            last_var_write_slice_idx[board_id] = slice_idx;
         }

         uint64_t raw_total = 0;
         for (auto it : raw_slice_sizes_last_100[board_id]) {
            raw_total += it;
         }
         if (raw_slice_sizes_last_100[board_id].size() > 0) {
            avg_raw_slice_size_last_100[board_id] = raw_total / raw_slice_sizes_last_100[board_id].size();
         }

         uint64_t filt_total = 0;
         for (auto it : filt_slice_sizes_last_100[board_id]) {
            filt_total += it;
         }
         if (filt_slice_sizes_last_100[board_id].size() > 0) {
            avg_filt_slice_size_last_100[board_id] = filt_total / filt_slice_sizes_last_100[board_id].size();
         }

         if (num_entries_for_run_totals[board_id] > 0) {
            avg_raw_slice_size_run[board_id] = raw_slice_sizes_run_total[board_id] / num_entries_for_run_totals[board_id];
            avg_filt_slice_size_run[board_id] = filt_slice_sizes_run_total[board_id] / num_entries_for_run_totals[board_id];
         }
      }

      board_readout[board_id].meta_mutex.unlock();
   }

   // Write the stats to the ODB
   // Always expect 2 active slices. Any more => a backlog.
   variables["Slice backlog"] = backlog;
   variables["Num slices lost"] = lost;
   variables["Dispatch rate (slices per sec)"] = slice_rate;
   variables["Raw buffer fill pct"] = raw_pct;
   variables["Filtered buffer fill pct"] = filt_pct;
   variables["Last slice raw bytes"] = raw_slice_size_last;
   variables["Last slice filtered bytes"] = filt_slice_size_last;
   variables["100 slice avg raw bytes"] = avg_raw_slice_size_last_100;
   variables["100 slice avg filtered bytes"] = avg_filt_slice_size_last_100;
   variables["Whole run avg raw bytes"] = avg_raw_slice_size_run;
   variables["Whole run avg filtered bytes"] = avg_filt_slice_size_run;
   variables["FE CPU usage pct"] = cpu_pct;
   variables["Num threads sending to TSPs"] = num_send_threads;

   // Stats from the board itself (populated by data readout thread)
   variables["Board acquisition status"] = acq_status;
   variables["Board temp (in) (C)"] = temp_in;
   variables["Board temp (out) (C)"] = temp_out;
   variables["Board temp (hottest ADC) (C)"] = temp_hottest_adc;

   return SUCCESS;
}


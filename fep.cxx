#include "fep_class.h"
#include "tmfe_rev0.h"
#include "midas.h"
#include "msystem.h"
#include <iostream>
#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

/** \file
 * Interface between midas frontend framework and FrontEndProcessor class.
 */

void usage() {
   printf("Usage: fep.exe -i <index>\n");
}

INT get_run_number(HNDLE hDB) {
   INT run_number;
   INT size = sizeof(run_number);
   db_get_value(hDB, 0, "/Runinfo/Run number", &run_number, &size, TID_INT, FALSE);
   return run_number;
}

class FEPRpcHandler : public TMFeRpcHandlerInterface {
public:
   FEPRpcHandler(HNDLE _hDB, FrontEndProcessor* _fep_class, TMFeEquipment* _eq) {
      hDB = _hDB;
      fep_class = _fep_class;
      eq = _eq;
   }

   virtual ~FEPRpcHandler() {};

   virtual void HandleBeginRun() override {
      char error[255];
      fep_class->begin_of_run(get_run_number(hDB), error);
   }

   virtual void HandleEndRun() override {
      char error[255];
      fep_class->end_of_run(get_run_number(hDB), error);
   }

   virtual void HandleStartAbortRun() override {
      HandleEndRun();
   }

private:
   HNDLE hDB;
   FrontEndProcessor* fep_class;
   TMFeEquipment* eq;
};

int main(int argc, char *argv[]) {
   signal(SIGPIPE, SIG_IGN);

   int frontend_index = -1;

   for (int i = 1; i < argc; i++) {
      if (strlen(argv[i]) != 2 || argv[i][0] != '-' || i + 1 >= argc || argv[i + 1][0] == '-') {
         usage();
         return 1;
      }

      if (argv[i][1] == 'i') {
         frontend_index = atoi(argv[++i]);
      } else {
         usage();
         return 1;
      }
   }

   if (frontend_index < 0) {
      usage();
      return 1;
   }

   char name[255];
   snprintf(name, 255, "FEP_%03d", frontend_index);

   TMFE* mfe = TMFE::Instance();

   TMFeError err = mfe->Connect(name, __FILE__);

   if (err.error) {
      printf("Cannot connect: %s\n", err.error_string.c_str());
      return 1;
   }

   TMFeCommon* common = new TMFeCommon();
   common->EventID = 102;
   common->LogHistory = 0;
   common->Buffer = "SYSTEM";

   TMFeEquipment* eq = new TMFeEquipment(mfe, name, common);
   eq->Init();
   eq->SetStatus("Starting...", "white");
   eq->ZeroStatistics();
   eq->WriteStatistics();

   INT max_event_size = 50 * 1024 * 1024;

   FrontEndProcessor fep_class;

   if (fep_class.init(mfe->fDB, max_event_size, frontend_index) != SUCCESS) {
      mfe->Disconnect();
      return 1;
   }
   std::cout << "Registering equipment" << std::endl;
   FEPRpcHandler rpc_handler(mfe->fDB, &fep_class, eq);

   mfe->RegisterEquipment(eq);
   mfe->RegisterRpcHandler(&rpc_handler);

   std::cout << "Registered" << std::endl;
   // Must be before PM handles BOR
   mfe->SetTransitionSequenceStart(500);

   std::cout << "Started" << std::endl;
   eq->SetStatus("Running...", "greenLight");

   char* buffer = (char*) malloc(max_event_size + 100);

   timeval last_stats_update;
   gettimeofday(&last_stats_update, NULL);

   while (!mfe->fShutdownRequested) {
      fep_class.do_periodic_work();

      if (fep_class.is_event_ready_to_send()) {
         eq->ComposeEvent(buffer, max_event_size);
         char* pevent = buffer + sizeof(EVENT_HEADER);
         int midas_size_bytes = fep_class.send_data(pevent);

         if (midas_size_bytes > 0) {
            ((EVENT_HEADER*)buffer)->data_size = midas_size_bytes;
            eq->SendEvent(buffer);
         }
      }

      mfe->PollMidas(1);

      timeval now;
      gettimeofday(&now, NULL);

      // Update stats every second
      if (now.tv_sec > last_stats_update.tv_sec) {
         eq->WriteStatistics();

         fep_class.update_variables();

         std::string status, color;
         fep_class.get_status_strings(status, color);
         eq->SetStatus(status.c_str(), color.c_str());

         last_stats_update = now;
      }
   }

   eq->SetStatus("Frontend stopped", "redLight");

   mfe->Disconnect();

   return 0;
}

#ifndef DATA_SOURCE_DEMO_H
#define DATA_SOURCE_DEMO_H

#include "data_source_base.h"
#include "fake_data_generator.h"
#include "sys/time.h"

/** \file
 * Data source that generates fake data mimicking the VX2740 data format.
 */

/** Data source that generates fake data mimicking the VX2740 data format.
 *
 * The user can specify and average event rate, waveform length,
 * and which channels are enabled. The waveforms are just sawtooths.
 *
 * Two types of event are generated:
 * - Regular data events with waveforms etc. A separate event is
 *     created for each channel. The data rate, event size and
 *     channel selection are configurable via the ODB.
 * - Metadata events saying that the end of a slice has been reached.
 *     These have no waveform data, but do have a special flag set.
 */
class DataSourceDemo : public DataSourceBase {
public:
   DataSourceDemo(HNDLE _hDB, INT _frontend_index, INT _board_id);
   virtual ~DataSourceDemo() {}
   INT begin_of_run(double _slice_width_ms) override;
   INT open_records(char *base_odb_path) override;
   INT populate_next_event(std::vector<std::shared_ptr<VXData> >& data, BOOL debug) override;

private:
   /**
    * \struct TxDemoDataSettings
    *
    * User settings from the ODB.
    */
   typedef struct {
      float demo_data_rate_Hz; //!< Average event rate.
      INT demo_data_wf_64bit_word_size; //!< Number of 64-bit words per channel per event.
      DWORD demo_data_ch_mask_31_0; //!< Bitmask of which channels to generate waveforms for.
      DWORD demo_data_ch_mask_63_32; //!< Bitmask of which channels to generate waveforms for.
   } TxDemoDataSettings;

   HNDLE hDB; //!< ODB access
   HNDLE settings_key; //!< ODB settings handle
   TxDemoDataSettings user_settings; //!< ODB settings
   FakeDataGenerator gen; //!< Logic for generating fake events

   INT board_id; //!< Board index within this FEP.
};

#endif
